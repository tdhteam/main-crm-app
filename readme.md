# Development tools

VS 2017 community or Rider 2017
nodejs >= 6.x, recommended version 8.x
.net core sdk 2.x
SQL Express 2014 or above

# How to front front end app  

cd CrmApp/DEA.CRM.WebClientApp

npm install

npm run build-dev

Start project from visual studio or rider

access http://localhost:5000


# Set up app database

Refer to this page https://bitbucket.org/tdhteam/main-crm-app/wiki/Setup%20app%20database  


# How to init default user

Start the web api server project DEA.CRM.Api  

Api root url is at http://localhost:5001/api/

To init admin user make POST to this url http://localhost:5001/api/initialisation  
with the payload  


{
  "UserName": "admin",
  "Password": "password",
  "UserRole": {
    "RoleId": "H2"
  },
  "CalendarColor": "#FF00EE",
  "FirstName": "Admin",
  "LastName": "CRM",
  "IsAdmin": true,
  "CurrencyId": 1,
  "DateFormat": "dd/MM/yyyy",
  "HourFotmat": "hh:mm",
  "Deleted": false,
  "Language": "en",
  "TimeZone": "UTC",
  "CurrencyGroupingPattern": "123,456.50",
  "CurrencyDecimalSeparator": ".",
  "CurrencyGroupingSeparator": ",",
  "CurrencySymbolPlacement": "VND",
  "NumberOfCurrencyDecimal": 2,
  "AccessAllCompanies": true,
  "AccessAllDealers": true,
  "AccessAllShowrooms": true
}


# To get login token

Make Post to http://localhost:5001/api/auth/login  
with payload  

{
  "UserName": "admin",
  "Password": "password"
}

The response is something like  
{
  "id": "950807f6-3655-4aa8-9675-8671fea8363e",
  "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbiIsImp0aSI6IjczNTk4ZjFlLTAzZDktNDU3MS04N2Y5LTFlZDkyMjk4ZjU4YiIsImlhdCI6MTUyMjc1OTAzNSwicm9sIjoiYXBpX2FjY2VzcyIsImlkIjoiOTUwODA3ZjYtMzY1NS00YWE4LTk2NzUtODY3MWZlYTgzNjNlIiwibmJmIjoxNTIyNzU5MDM1LCJleHAiOjE1MjI3NjYyMzUsImlzcyI6IkRlYUNybVdlYkFwaSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NTAwMS8ifQ.UbiIQcQ4hhIsRWSVuWtsRHdtUTDVCl8LUiuIQ8CsXD4",
  "expires_in": 7200
}  

If login is incorrect the response is  
{"login_failure":["Invalid username or password."]}  

# Client make request to api must include headers  

Content-Type : application/json  
Authorization: Bearer <<the auth token response from login api>>  

