﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DEA.CRM.EfModel
{
    public class MsSqlCrmAppDbContextFactory : IDesignTimeDbContextFactory<CRMAppDbContext>
    {
        public CRMAppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CRMAppDbContext>();
            optionsBuilder.UseSqlServer("Server=localhost,1401;Database=CRMApp;User Id=sa;Password=P@ssw0rd;MultipleActiveResultSets=true");

            return new CRMAppDbContext(optionsBuilder.Options);
        }
    }
}
