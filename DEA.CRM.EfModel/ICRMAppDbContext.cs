﻿using DEA.CRM.Model;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.EfModel
{
    public interface ICRMAppDbContext
    {
        DbSet<Activity> Activities { get; set; }
        DbSet<ActivityAssignment> ActivityAssignments { get; set; }
        DbSet<ActivityStatus> ActivityStatuses { get; set; }
        DbSet<ActivityPriority> ActivityPriorities { get; set; }
        DbSet<Attachment> Attachments { get; set; }
        DbSet<CalendarEvent> CalendarEvents { get; set; }
        DbSet<CalendarEventType> CalendarEventTypes { get; set; }
        DbSet<Company> Companies { get; set; }
        DbSet<Contact> Contacts { get; set; }
        DbSet<ContactAttachment> ContactAttachments { get; set; }
        DbSet<ContactComment> ContactComments { get; set; }
        DbSet<ContactCommentAttachment> ContactCommentAttachments { get; set; }
        DbSet<ContactMeetingProgress> ContactMeetingProgresses { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<CustomerService> CustomerServices { get; set; }
        DbSet<CustomerServiceComment> CustomerServiceComments { get; set; }
        DbSet<CustomerServiceCommentAttachment> CustomerServiceCommentAttachments { get; set; }
        DbSet<DatabaseVersion> DatabaseVersions { get; set; }
        DbSet<Dealer> Dealers { get; set; }
        DbSet<EventInvitee> EventInvitees { get; set; }
        DbSet<EventRecurrence> EventRecurrences { get; set; }
        DbSet<EventReminder> EventReminders { get; set; }
        DbSet<EventStatus> EventStatuses { get; set; }
        DbSet<FreeTag> FreeTags { get; set; }
        DbSet<GeneralAuditTrail> GeneralAuditTrails { get; set; }
        DbSet<HandOverContract> HandOverContracts { get; set; }
        DbSet<HandOverContractComment> HandOverContractComments { get; set; }
        DbSet<HandOverContractCommentAttachment> HandOverContractCommentAttachments { get; set; }
        DbSet<HandOverContractLine> HandOverContractLines { get; set; }
        DbSet<ParentTab> ParentTabs { get; set; }
        DbSet<PickList> PickLists { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<ProductAttachment> ProductAttachments { get; set; }
        DbSet<ProductCategory> ProductCategories { get; set; }
        DbSet<ProductCategoryGroup> ProductCategoryGroups { get; set; }
        DbSet<Profile> Profiles { get; set; }
        DbSet<ProfileToStandardPermission> ProfileToStandardPermissions { get; set; }
        DbSet<Permission> Permissions { get; set; }
        DbSet<ProfileToCustomPermission> ProfileToCustomPermissions { get; set; }
        DbSet<ProfileToTab> ProfileToTabs { get; set; }
        DbSet<RecurringType> RecurringTypes { get; set; }
        DbSet<Report> Reports { get; set; }
        DbSet<ReportFolder> ReportFolders { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<RoleSeq> RoleSeqs { get; set; }
        DbSet<RoleToProfile> RoleToProfiles { get; set; }
        DbSet<Setting> Settings { get; set; }
        DbSet<Showroom> Showrooms { get; set; }
        DbSet<Tab> Tabs { get; set; }
        DbSet<UserDashboard> UserDashboards { get; set; }
        DbSet<UserInfo> UserInfoes { get; set; }
        DbSet<UserInfoToCompany> UserInfoToCompanies { get; set; }
        DbSet<UserInfoToDealer> UserInfoToDealers { get; set; }
        DbSet<UserInfoToRole> UserInfoToRoles { get; set; }
        DbSet<UserInfoToShowroom> UserInfoToShowrooms { get; set; }
        DbSet<DocumentFolder> DocumentFolders { get; set; }
        DbSet<UserDocument> UserDocuments { get; set; }
        DbSet<Currency> Currencies { get; set; }

        DbSet<CalendarEventListViewSearchResult> CalendarEventListViewSearchResults { get; set; }
        DbSet<UserInfoSearch> UserInfoSearchs { get; set; }
    }
}
