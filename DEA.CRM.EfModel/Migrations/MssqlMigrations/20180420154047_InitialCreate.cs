﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DEA.CRM.EfModel.Migrations.MssqlMigrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    DueDateTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    DurationHours = table.Column<int>(nullable: true),
                    DurationMinutues = table.Column<int>(nullable: true),
                    EventStatus = table.Column<string>(maxLength: 50, nullable: true),
                    IsRecurring = table.Column<bool>(nullable: false),
                    Location = table.Column<string>(maxLength: 255, nullable: true),
                    OwnerUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    Priority = table.Column<string>(maxLength: 50, nullable: true),
                    RecurringType = table.Column<string>(maxLength: 255, nullable: false),
                    RelatedToContactId = table.Column<int>(nullable: true),
                    RelatedToCustomerServiceId = table.Column<int>(nullable: true),
                    RelatedToHandoverContractId = table.Column<int>(nullable: true),
                    SendNotifiction = table.Column<bool>(nullable: true),
                    SendReminder = table.Column<bool>(nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    StartDateTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Subject = table.Column<string>(maxLength: 255, nullable: false),
                    Tags = table.Column<string>(maxLength: 255, nullable: true),
                    Visibility = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Presence = table.Column<bool>(nullable: false),
                    SortOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Attachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContentType = table.Column<string>(maxLength: 100, nullable: true),
                    DateAttached = table.Column<DateTime>(type: "datetime", nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    FileUrl = table.Column<string>(maxLength: 255, nullable: true),
                    ThumbnailUrl = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CalendarEventType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Color = table.Column<string>(maxLength: 10, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Presence = table.Column<bool>(nullable: false),
                    SortOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarEventType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 255, nullable: false),
                    City = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyCode = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Country = table.Column<string>(maxLength: 50, nullable: true),
                    Fax = table.Column<string>(maxLength: 20, nullable: true),
                    LogoRelativeUrl = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    State = table.Column<string>(maxLength: 255, nullable: true),
                    WebSite = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddressCity = table.Column<string>(maxLength: 255, nullable: true),
                    AddressCountry = table.Column<string>(maxLength: 50, nullable: true),
                    AddressDistrict = table.Column<string>(maxLength: 255, nullable: true),
                    AddressPostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    AddressState = table.Column<string>(maxLength: 255, nullable: true),
                    AddressStreet = table.Column<string>(maxLength: 255, nullable: true),
                    ApproachingType = table.Column<string>(maxLength: 255, nullable: true),
                    CarIsUsing = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyEmail = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    CompanyName = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyOwner = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyTaxCode = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyWebSite = table.Column<string>(maxLength: 255, nullable: true),
                    ContactNo = table.Column<string>(maxLength: 50, nullable: false),
                    ContactNote = table.Column<string>(maxLength: 2000, nullable: true),
                    ContactSource = table.Column<string>(maxLength: 255, nullable: true),
                    ContactType = table.Column<string>(maxLength: 50, nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime", nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    FirstMeetingDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    Gender = table.Column<string>(maxLength: 50, nullable: true),
                    HandlerUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    HomePhone = table.Column<string>(maxLength: 20, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    MobilePhone = table.Column<string>(maxLength: 20, nullable: true),
                    Occupation = table.Column<string>(maxLength: 255, nullable: true),
                    OfficePhone = table.Column<string>(maxLength: 20, nullable: true),
                    PaymentType = table.Column<string>(maxLength: 255, nullable: true),
                    PlanToBuyDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Salutation = table.Column<string>(maxLength: 50, nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    Tags = table.Column<string>(maxLength: 255, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContactAttachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttachmentId = table.Column<int>(nullable: false),
                    ContactId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactAttachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContactCommentAttachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttachmentId = table.Column<int>(nullable: false),
                    ContactCommentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactCommentAttachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: true),
                    CompanyName = table.Column<string>(maxLength: 255, nullable: true),
                    ContactType = table.Column<string>(maxLength: 50, nullable: true),
                    ConvertedFromContactId = table.Column<int>(nullable: true),
                    CustomerNo = table.Column<string>(maxLength: 50, nullable: false),
                    CustomerSource = table.Column<string>(maxLength: 255, nullable: true),
                    CustomerType = table.Column<string>(maxLength: 100, nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime", nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    Gender = table.Column<string>(maxLength: 50, nullable: true),
                    HomePhone = table.Column<string>(maxLength: 20, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    MobilePhone = table.Column<string>(maxLength: 20, nullable: true),
                    Occupation = table.Column<string>(maxLength: 255, nullable: true),
                    OfficePhone = table.Column<string>(maxLength: 20, nullable: true),
                    PaymentType = table.Column<string>(maxLength: 100, nullable: true),
                    Salutation = table.Column<string>(maxLength: 50, nullable: true),
                    ShowroomId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerService",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    AppointedSalesPersonnelFullName = table.Column<string>(maxLength: 255, nullable: true),
                    AppointedSalesUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    Category = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    ContactReferenceId = table.Column<int>(nullable: true),
                    ContractDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CustomerId = table.Column<int>(nullable: false),
                    DealerId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    FirstName = table.Column<string>(maxLength: 255, nullable: false),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 50, nullable: true),
                    PlanServiceCompleteDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PlateNumber = table.Column<string>(maxLength: 30, nullable: true),
                    Priority = table.Column<string>(maxLength: 50, nullable: true),
                    ProductModel = table.Column<string>(maxLength: 255, nullable: true),
                    ProductionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReasonToService = table.Column<string>(maxLength: 500, nullable: true),
                    ReminderPersonnelFullName = table.Column<string>(maxLength: 255, nullable: true),
                    ReminderPersonnelUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    SalesPersonnelFullName = table.Column<string>(maxLength: 255, nullable: true),
                    SalesUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    ServiceDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ServicePersonnel = table.Column<string>(maxLength: 255, nullable: true),
                    ServiceResult = table.Column<string>(maxLength: 2000, nullable: true),
                    ServiceUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    UseTransportation = table.Column<bool>(nullable: true),
                    VehicleRunKm = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerService", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerServiceCommentAttachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttachmentId = table.Column<int>(nullable: false),
                    CustomerServiceCommentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerServiceCommentAttachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DatabaseVersion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Build = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Major = table.Column<int>(nullable: false),
                    Minor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatabaseVersion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Presence = table.Column<bool>(nullable: false),
                    SortOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FreeTag",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OwnerUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    RawTag = table.Column<string>(maxLength: 50, nullable: true),
                    Tag = table.Column<string>(maxLength: 50, nullable: false),
                    Visibility = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FreeTag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralAuditTrail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Action = table.Column<string>(maxLength: 255, nullable: false),
                    ActionDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Entity = table.Column<string>(maxLength: 255, nullable: false),
                    Module = table.Column<string>(maxLength: 255, nullable: false),
                    RecordId = table.Column<string>(maxLength: 100, nullable: false),
                    UserFullName = table.Column<string>(maxLength: 255, nullable: false),
                    UserKey = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralAuditTrail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HandOverContractCommentAttachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttachmentId = table.Column<int>(nullable: false),
                    HandOverContractCommentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandOverContractCommentAttachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParentTab",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Label = table.Column<string>(maxLength: 50, nullable: false),
                    Sequence = table.Column<int>(nullable: false),
                    Visible = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParentTab", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    DisplayOrder = table.Column<int>(nullable: false, defaultValue: -1),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PickList",
                columns: table => new
                {
                    PickListId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Query = table.Column<string>(maxLength: 2048, nullable: true),
                    System = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickList", x => x.PickListId);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttachment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttachmentId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttachment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategoryGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategoryGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Profile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    DirectlyRelatedToRole = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RecurringType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Color = table.Column<string>(maxLength: 10, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Presence = table.Column<bool>(nullable: false),
                    SortOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecurringType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportFolder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportFolder", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    RoleId = table.Column<string>(maxLength: 36, nullable: false),
                    Depth = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentRoleId = table.Column<string>(maxLength: 36, nullable: true),
                    ParentRoleName = table.Column<string>(maxLength: 255, nullable: true),
                    TreePath = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "RoleSeq",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleSeq", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Setting",
                columns: table => new
                {
                    SettingId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PickListId = table.Column<int>(nullable: true),
                    SettingGroup = table.Column<string>(maxLength: 50, nullable: false),
                    SettingValue = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Setting", x => x.SettingId);
                });

            migrationBuilder.CreateTable(
                name: "Tab",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Label = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ParentTabId = table.Column<int>(nullable: true),
                    ParentTabLabel = table.Column<string>(maxLength: 50, nullable: true),
                    Sequence = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tab", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityAssignment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: false),
                    AssigneeUserFullName = table.Column<string>(maxLength: 256, nullable: false),
                    AssigneeUserKey = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityAssignment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityAssignment_Activity",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActivityReminder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: false),
                    RecurringId = table.Column<int>(nullable: true),
                    ReminderSent = table.Column<int>(nullable: true),
                    ReminderTime = table.Column<int>(nullable: false),
                    ReminderType = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityReminder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityReminder_Activity",
                        column: x => x.ActivityId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CalendarEvent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CalendarEventTypeId = table.Column<int>(nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    DueDateTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    DurationHours = table.Column<int>(nullable: true),
                    DurationMinutues = table.Column<int>(nullable: true),
                    EventStatus = table.Column<string>(maxLength: 50, nullable: true),
                    IsRecurring = table.Column<bool>(nullable: false),
                    Location = table.Column<string>(maxLength: 255, nullable: true),
                    OwnerUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    Priority = table.Column<string>(maxLength: 50, nullable: true),
                    RecurringType = table.Column<string>(maxLength: 255, nullable: true),
                    RelatedToContactId = table.Column<int>(nullable: true),
                    RelatedToCustomerServiceId = table.Column<int>(nullable: true),
                    RelatedToHandoverContractId = table.Column<int>(nullable: true),
                    SendNotifiction = table.Column<bool>(nullable: true),
                    SendReminder = table.Column<bool>(nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    StartDateTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Subject = table.Column<string>(maxLength: 255, nullable: false),
                    Tags = table.Column<string>(maxLength: 255, nullable: true),
                    Visibility = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarEvent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CalendarEvent_CalendarEventType",
                        column: x => x.CalendarEventTypeId,
                        principalTable: "CalendarEventType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Dealer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 255, nullable: false),
                    City = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    Country = table.Column<string>(maxLength: 50, nullable: true),
                    Fax = table.Column<string>(maxLength: 20, nullable: true),
                    LogoRelativeUrl = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    State = table.Column<string>(maxLength: 255, nullable: true),
                    WebSite = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dealer_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactComment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentContent = table.Column<string>(maxLength: 4000, nullable: false),
                    ContactId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactComment_Contact",
                        column: x => x.ContactId,
                        principalTable: "Contact",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactMeetingProgress",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CarColorOfInterest = table.Column<string>(maxLength: 50, nullable: true),
                    CarOfInterest = table.Column<string>(maxLength: 255, nullable: true),
                    ContactId = table.Column<int>(nullable: true),
                    DateOfMeeting = table.Column<DateTime>(type: "datetime", nullable: true),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    MeetingStatus = table.Column<string>(maxLength: 50, nullable: true),
                    OtherNote = table.Column<string>(maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactMeetingProgress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactMeetingProgress_Contact",
                        column: x => x.ContactId,
                        principalTable: "Contact",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HandOverContract",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccidentInsurance = table.Column<decimal>(nullable: true),
                    AddressCity = table.Column<string>(maxLength: 255, nullable: true),
                    AddressCountry = table.Column<string>(maxLength: 50, nullable: true),
                    AddressDistrict = table.Column<string>(maxLength: 255, nullable: true),
                    AddressPostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    AddressState = table.Column<string>(maxLength: 255, nullable: true),
                    AddressStreet = table.Column<string>(maxLength: 255, nullable: true),
                    AdjustmentAmount = table.Column<decimal>(nullable: true),
                    AnnualRoadTax = table.Column<decimal>(nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    CompanyName = table.Column<string>(maxLength: 255, nullable: true),
                    ContactType = table.Column<string>(maxLength: 50, nullable: false),
                    ContractApprovalDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ContractDescription = table.Column<string>(maxLength: 2000, nullable: true),
                    ContractPersonnelFullName = table.Column<string>(maxLength: 255, nullable: true),
                    ContractSubmittedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CustomerId = table.Column<int>(nullable: true),
                    CustomerRequestDeliveryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CustomerType = table.Column<string>(maxLength: 255, nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    DeliveryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    DeliveryPriority = table.Column<string>(maxLength: 50, nullable: true),
                    DepositAmount = table.Column<decimal>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    NewVehicleRegistrationFee = table.Column<decimal>(nullable: true),
                    PackageAndTransportationFee = table.Column<decimal>(nullable: true),
                    PaymentBankInstallmentAmount = table.Column<decimal>(nullable: true),
                    PaymentCashRemainAmount = table.Column<decimal>(nullable: true),
                    PaymentInitialAmount = table.Column<decimal>(nullable: true),
                    PaymentInstallmentAmount = table.Column<decimal>(nullable: true),
                    PDICompletedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDIInstallationCompletedtDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDIInstallationRequestDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDIRequestDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDSActualDeliveryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDSActualRegistrationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDSPlanRegistrationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PDSSaleDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ProductFactoryDeliveryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ProductName = table.Column<string>(maxLength: 255, nullable: true),
                    ProductRequestToFactoryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ProductType = table.Column<string>(maxLength: 255, nullable: true),
                    RegistrationFee = table.Column<decimal>(nullable: true),
                    RegistrationServiceFee = table.Column<decimal>(nullable: true),
                    RegistrationTax = table.Column<decimal>(nullable: true),
                    SalesUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    Tags = table.Column<string>(maxLength: 255, nullable: true),
                    TermAndCondition = table.Column<string>(maxLength: 4000, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: false),
                    VehicleInsurance = table.Column<decimal>(nullable: true),
                    VIN = table.Column<string>(maxLength: 50, nullable: true),
                    WarrantyDescription = table.Column<string>(maxLength: 2000, nullable: true),
                    WarrantyDurationInMonths = table.Column<string>(maxLength: 50, nullable: true),
                    WarrantyInKm = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandOverContract", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HandOverContract_Customer",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomerServiceComment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentContent = table.Column<string>(maxLength: 4000, nullable: false),
                    CustomerServiceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerServiceComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerServiceComment_CustomerService",
                        column: x => x.CustomerServiceId,
                        principalTable: "CustomerService",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryGroupId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCategory_ProductCategoryGroup",
                        column: x => x.CategoryGroupId,
                        principalTable: "ProductCategoryGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProfileToCustomPermission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PermissionId = table.Column<int>(nullable: false),
                    ProfileId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileToCustomPermission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfileToCustomPermission_Permission",
                        column: x => x.PermissionId,
                        principalTable: "Permission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProfileToCustomPermission_Profile",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Report",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessRoute = table.Column<string>(maxLength: 255, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    FolderId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ReportType = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Report", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Report_ReportFolder",
                        column: x => x.FolderId,
                        principalTable: "ReportFolder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleToProfile",
                columns: table => new
                {
                    RoleId = table.Column<string>(maxLength: 36, nullable: false),
                    ProfileId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleToProfile", x => new { x.RoleId, x.ProfileId });
                    table.ForeignKey(
                        name: "FK_RoleToProfile_Profile",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoleToProfile_Role",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfoToRole",
                columns: table => new
                {
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    RoleId = table.Column<string>(maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfoToRole", x => x.UserKey);
                    table.ForeignKey(
                        name: "FK_UserInfoToRole_Role",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProfileToStandardPermission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Operation = table.Column<int>(nullable: false),
                    Permission = table.Column<bool>(nullable: false),
                    ProfileId = table.Column<int>(nullable: false),
                    TabId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileToStandardPermission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfileToStandardPermission_Profile",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProfileToStandardPermission_Tab",
                        column: x => x.TabId,
                        principalTable: "Tab",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProfileToTab",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Permission = table.Column<bool>(nullable: false),
                    ProfileId = table.Column<int>(nullable: false),
                    TabId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileToTab", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfileToTab_Profile",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProfileToTab_Tab",
                        column: x => x.TabId,
                        principalTable: "Tab",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventInvitee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssigneeUserFullName = table.Column<string>(maxLength: 256, nullable: true),
                    AssigneeUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    EventId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventInvitee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventInvitee_CalendarEvent",
                        column: x => x.EventId,
                        principalTable: "CalendarEvent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventRecurrence",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventId = table.Column<int>(nullable: false),
                    RecurringDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    RecurringEndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    RecurringFrequency = table.Column<int>(nullable: false),
                    RecurringInfo = table.Column<string>(maxLength: 100, nullable: true),
                    RecurringType = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventRecurrence", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventRecurrence_CalendarEvent",
                        column: x => x.EventId,
                        principalTable: "CalendarEvent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventReminder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EventId = table.Column<int>(nullable: false),
                    RecurringId = table.Column<int>(nullable: true),
                    ReminderSent = table.Column<int>(nullable: true),
                    ReminderTime = table.Column<int>(nullable: false),
                    ReminderType = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventReminder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventReminder_CalendarEvent",
                        column: x => x.EventId,
                        principalTable: "CalendarEvent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Showroom",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 255, nullable: false),
                    City = table.Column<string>(maxLength: 255, nullable: true),
                    Country = table.Column<string>(maxLength: 50, nullable: true),
                    DealerId = table.Column<int>(nullable: false),
                    Fax = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    State = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Showroom", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Showroom_Dealer",
                        column: x => x.DealerId,
                        principalTable: "Dealer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HandOverContractComment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentContent = table.Column<string>(maxLength: 4000, nullable: false),
                    HandOverContractId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandOverContractComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HandOverContractComment_HandOverContract",
                        column: x => x.HandOverContractId,
                        principalTable: "HandOverContract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HandOverContractLine",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DiscountAmount = table.Column<decimal>(nullable: true),
                    DiscountPercent = table.Column<decimal>(nullable: true),
                    FinalCost = table.Column<decimal>(nullable: true),
                    HandOverContractId = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(maxLength: 255, nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    UnitCost = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandOverContractLine", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HandOverContractLine_HandOverContract",
                        column: x => x.HandOverContractId,
                        principalTable: "HandOverContract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BankOfGuarantee = table.Column<string>(maxLength: 255, nullable: true),
                    Color = table.Column<string>(maxLength: 50, nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    ContractId = table.Column<int>(nullable: true),
                    DateEnteredStock = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateRelease = table.Column<DateTime>(type: "datetime", nullable: true),
                    DealerId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Engine = table.Column<string>(maxLength: 255, nullable: true),
                    EngineNumber = table.Column<string>(maxLength: 50, nullable: true),
                    HandlerUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    InActiveForSale = table.Column<bool>(nullable: false),
                    InStockAge = table.Column<double>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    NumberOfSeats = table.Column<int>(nullable: true),
                    PaymentStatus = table.Column<string>(maxLength: 2000, nullable: true),
                    PercentSales = table.Column<decimal>(nullable: true),
                    PercentService = table.Column<decimal>(nullable: true),
                    PercentVat = table.Column<decimal>(nullable: true),
                    PlateNumber = table.Column<string>(maxLength: 20, nullable: true),
                    ProductActive = table.Column<bool>(nullable: false),
                    ProductCategoryId = table.Column<int>(nullable: false),
                    ProductNo = table.Column<string>(maxLength: 50, nullable: false),
                    ProductType = table.Column<string>(maxLength: 255, nullable: true),
                    ProductionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PurchaseOrderNo = table.Column<string>(maxLength: 50, nullable: true),
                    QuantityInStock = table.Column<decimal>(nullable: true),
                    ShowroomId = table.Column<int>(nullable: true),
                    Status = table.Column<string>(maxLength: 255, nullable: true),
                    TransmissionType = table.Column<string>(maxLength: 255, nullable: true),
                    UnitPrice = table.Column<decimal>(type: "decimal(18, 8)", nullable: true),
                    UOM = table.Column<string>(maxLength: 100, nullable: true),
                    VIN = table.Column<string>(maxLength: 50, nullable: true),
                    WarehouseName = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_ProductCategory",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfo",
                columns: table => new
                {
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    AccessAllCompanies = table.Column<bool>(nullable: false),
                    AccessAllDealers = table.Column<bool>(nullable: false),
                    AccessAllShowrooms = table.Column<bool>(nullable: false),
                    AddressCity = table.Column<string>(maxLength: 255, nullable: true),
                    AddressCountry = table.Column<string>(maxLength: 50, nullable: true),
                    AddressDistrict = table.Column<string>(maxLength: 255, nullable: true),
                    AddressPostalCode = table.Column<string>(maxLength: 25, nullable: true),
                    AddressState = table.Column<string>(maxLength: 255, nullable: true),
                    AddressStreet = table.Column<string>(maxLength: 255, nullable: true),
                    AvatarRelativeUrl = table.Column<string>(maxLength: 255, nullable: true),
                    CalendarColor = table.Column<string>(unicode: false, maxLength: 25, nullable: false, defaultValueSql: "('#E6FAD8')"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CurrencyDecimalSeparator = table.Column<string>(unicode: false, maxLength: 2, nullable: false),
                    CurrencyGroupingPattern = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    CurrencyGroupingSeparator = table.Column<string>(unicode: false, maxLength: 2, nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    CurrencySymbolPlacement = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    DateFormat = table.Column<string>(maxLength: 20, nullable: false),
                    DefaultCompanyId = table.Column<int>(nullable: true),
                    DefaultDealerId = table.Column<int>(nullable: true),
                    DefaultShowroomId = table.Column<int>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Department = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Fax = table.Column<string>(maxLength: 50, nullable: true),
                    FirstName = table.Column<string>(maxLength: 128, nullable: false),
                    HomePhone = table.Column<string>(maxLength: 50, nullable: true),
                    HourFotmat = table.Column<string>(maxLength: 20, nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    Language = table.Column<string>(maxLength: 10, nullable: false),
                    LastName = table.Column<string>(maxLength: 128, nullable: true),
                    MobilePhone = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedByUserFullName = table.Column<string>(maxLength: 255, nullable: false),
                    ModifiedByUserKey = table.Column<string>(maxLength: 128, nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    NumberOfCurrencyDecimal = table.Column<int>(nullable: false),
                    OtherPhone = table.Column<string>(maxLength: 50, nullable: true),
                    ReminderIntervalInSeconds = table.Column<int>(nullable: true),
                    ReportToUserKey = table.Column<string>(maxLength: 128, nullable: true),
                    Salutation = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 25, nullable: true),
                    TimeZone = table.Column<string>(maxLength: 20, nullable: false),
                    Title = table.Column<string>(type: "nchar(10)", nullable: true),
                    UserName = table.Column<string>(maxLength: 255, nullable: false),
                    WorkPhone = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfo", x => x.UserKey);
                    table.ForeignKey(
                        name: "FK_UserInfo_UserInfoToRole",
                        column: x => x.UserKey,
                        principalTable: "UserInfoToRole",
                        principalColumn: "UserKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserDashboard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    WidgetSettings = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDashboard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDashboard_UserInfo",
                        column: x => x.UserKey,
                        principalTable: "UserInfo",
                        principalColumn: "UserKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfoToCompany",
                columns: table => new
                {
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfoToCompany", x => new { x.UserKey, x.CompanyId });
                    table.ForeignKey(
                        name: "FK_UserInfoToCompany_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserInfoToCompany_UserInfo",
                        column: x => x.UserKey,
                        principalTable: "UserInfo",
                        principalColumn: "UserKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfoToDealer",
                columns: table => new
                {
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    DealerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfoToDealer", x => new { x.UserKey, x.DealerId });
                    table.ForeignKey(
                        name: "FK_UserInfoToDealer_Dealer",
                        column: x => x.DealerId,
                        principalTable: "Dealer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserInfoToDealer_UserInfo",
                        column: x => x.UserKey,
                        principalTable: "UserInfo",
                        principalColumn: "UserKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserInfoToShowroom",
                columns: table => new
                {
                    UserKey = table.Column<string>(maxLength: 128, nullable: false),
                    ShowroomId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserInfoToShowroom", x => new { x.UserKey, x.ShowroomId });
                    table.ForeignKey(
                        name: "FK_UserInfoToShowroom_Showroom",
                        column: x => x.ShowroomId,
                        principalTable: "Showroom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserInfoToShowroom_UserInfo",
                        column: x => x.UserKey,
                        principalTable: "UserInfo",
                        principalColumn: "UserKey",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAssignment_ActivityId",
                table: "ActivityAssignment",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityReminder_ActivityId",
                table: "ActivityReminder",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_CalendarEvent_CalendarEventTypeId",
                table: "CalendarEvent",
                column: "CalendarEventTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactComment_ContactId",
                table: "ContactComment",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactMeetingProgress_ContactId",
                table: "ContactMeetingProgress",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerServiceComment_CustomerServiceId",
                table: "CustomerServiceComment",
                column: "CustomerServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_CompanyId",
                table: "Dealer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EventInvitee_EventId",
                table: "EventInvitee",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_EventRecurrence_EventId",
                table: "EventRecurrence",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_EventReminder_EventId",
                table: "EventReminder",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_HandOverContract_CustomerId",
                table: "HandOverContract",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_HandOverContractComment_HandOverContractId",
                table: "HandOverContractComment",
                column: "HandOverContractId");

            migrationBuilder.CreateIndex(
                name: "IX_HandOverContractLine_HandOverContractId",
                table: "HandOverContractLine",
                column: "HandOverContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProductCategoryId",
                table: "Product",
                column: "ProductCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategory_CategoryGroupId",
                table: "ProductCategory",
                column: "CategoryGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToCustomPermission_PermissionId",
                table: "ProfileToCustomPermission",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToCustomPermission_ProfileId",
                table: "ProfileToCustomPermission",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToStandardPermission_ProfileId",
                table: "ProfileToStandardPermission",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToStandardPermission_TabId",
                table: "ProfileToStandardPermission",
                column: "TabId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToTab_ProfileId",
                table: "ProfileToTab",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileToTab_TabId",
                table: "ProfileToTab",
                column: "TabId");

            migrationBuilder.CreateIndex(
                name: "IX_Report_FolderId",
                table: "Report",
                column: "FolderId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleToProfile_ProfileId",
                table: "RoleToProfile",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Showroom_DealerId",
                table: "Showroom",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDashboard_UserKey",
                table: "UserDashboard",
                column: "UserKey");

            migrationBuilder.CreateIndex(
                name: "IX_UserInfoToCompany_CompanyId",
                table: "UserInfoToCompany",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserInfoToDealer_DealerId",
                table: "UserInfoToDealer",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserInfoToRole_RoleId",
                table: "UserInfoToRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserInfoToShowroom_ShowroomId",
                table: "UserInfoToShowroom",
                column: "ShowroomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityAssignment");

            migrationBuilder.DropTable(
                name: "ActivityReminder");

            migrationBuilder.DropTable(
                name: "ActivityStatus");

            migrationBuilder.DropTable(
                name: "Attachment");

            migrationBuilder.DropTable(
                name: "ContactAttachment");

            migrationBuilder.DropTable(
                name: "ContactComment");

            migrationBuilder.DropTable(
                name: "ContactCommentAttachment");

            migrationBuilder.DropTable(
                name: "ContactMeetingProgress");

            migrationBuilder.DropTable(
                name: "CustomerServiceComment");

            migrationBuilder.DropTable(
                name: "CustomerServiceCommentAttachment");

            migrationBuilder.DropTable(
                name: "DatabaseVersion");

            migrationBuilder.DropTable(
                name: "EventInvitee");

            migrationBuilder.DropTable(
                name: "EventRecurrence");

            migrationBuilder.DropTable(
                name: "EventReminder");

            migrationBuilder.DropTable(
                name: "EventStatus");

            migrationBuilder.DropTable(
                name: "FreeTag");

            migrationBuilder.DropTable(
                name: "GeneralAuditTrail");

            migrationBuilder.DropTable(
                name: "HandOverContractComment");

            migrationBuilder.DropTable(
                name: "HandOverContractCommentAttachment");

            migrationBuilder.DropTable(
                name: "HandOverContractLine");

            migrationBuilder.DropTable(
                name: "ParentTab");

            migrationBuilder.DropTable(
                name: "PickList");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductAttachment");

            migrationBuilder.DropTable(
                name: "ProfileToCustomPermission");

            migrationBuilder.DropTable(
                name: "ProfileToStandardPermission");

            migrationBuilder.DropTable(
                name: "ProfileToTab");

            migrationBuilder.DropTable(
                name: "RecurringType");

            migrationBuilder.DropTable(
                name: "Report");

            migrationBuilder.DropTable(
                name: "RoleSeq");

            migrationBuilder.DropTable(
                name: "RoleToProfile");

            migrationBuilder.DropTable(
                name: "Setting");

            migrationBuilder.DropTable(
                name: "UserDashboard");

            migrationBuilder.DropTable(
                name: "UserInfoToCompany");

            migrationBuilder.DropTable(
                name: "UserInfoToDealer");

            migrationBuilder.DropTable(
                name: "UserInfoToShowroom");

            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropTable(
                name: "CustomerService");

            migrationBuilder.DropTable(
                name: "CalendarEvent");

            migrationBuilder.DropTable(
                name: "HandOverContract");

            migrationBuilder.DropTable(
                name: "ProductCategory");

            migrationBuilder.DropTable(
                name: "Permission");

            migrationBuilder.DropTable(
                name: "Tab");

            migrationBuilder.DropTable(
                name: "ReportFolder");

            migrationBuilder.DropTable(
                name: "Profile");

            migrationBuilder.DropTable(
                name: "Showroom");

            migrationBuilder.DropTable(
                name: "UserInfo");

            migrationBuilder.DropTable(
                name: "CalendarEventType");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "ProductCategoryGroup");

            migrationBuilder.DropTable(
                name: "Dealer");

            migrationBuilder.DropTable(
                name: "UserInfoToRole");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}
