﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.EfModel
{
    public static class DataContextExtensions
    {
        public static void DeleteById<TEntity>(this DbSet<TEntity> target, params object[] keyValues) where TEntity : class
        {
            var entity = target.Find(keyValues);
            if (entity != null)
                target.Remove(entity);
        }
    }
}
