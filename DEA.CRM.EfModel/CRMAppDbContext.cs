﻿using DEA.CRM.Model;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.EfModel
{
    public partial class CRMAppDbContext : DbContext, ICRMAppDbContext
    {
        public CRMAppDbContext(DbContextOptions<CRMAppDbContext> options)
            : base(options) { }

        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<ActivityAssignment> ActivityAssignments { get; set; }
        public virtual DbSet<ActivityStatus> ActivityStatuses { get; set; }
        public virtual DbSet<ActivityPriority> ActivityPriorities { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<CalendarEvent> CalendarEvents { get; set; }
        public virtual DbSet<CalendarEventType> CalendarEventTypes { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<LeadSource> LeadSources { get; set; }
        public virtual DbSet<ContactAttachment> ContactAttachments { get; set; }
        public virtual DbSet<ContactComment> ContactComments { get; set; }
        public virtual DbSet<ContactCommentAttachment> ContactCommentAttachments { get; set; }
        public virtual DbSet<ContactMeetingProgress> ContactMeetingProgresses { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerService> CustomerServices { get; set; }
        public virtual DbSet<CustomerServiceComment> CustomerServiceComments { get; set; }
        public virtual DbSet<CustomerServiceCommentAttachment> CustomerServiceCommentAttachments { get; set; }
        public virtual DbSet<DatabaseVersion> DatabaseVersions { get; set; }
        public virtual DbSet<Dealer> Dealers { get; set; }
        public virtual DbSet<EventInvitee> EventInvitees { get; set; }
        public virtual DbSet<EventRecurrence> EventRecurrences { get; set; }
        public virtual DbSet<EventReminder> EventReminders { get; set; }
        public virtual DbSet<EventStatus> EventStatuses { get; set; }
        public virtual DbSet<FreeTag> FreeTags { get; set; }
        public virtual DbSet<GeneralAuditTrail> GeneralAuditTrails { get; set; }
        public virtual DbSet<HandOverContract> HandOverContracts { get; set; }
        public virtual DbSet<HandOverContractComment> HandOverContractComments { get; set; }
        public virtual DbSet<HandOverContractCommentAttachment> HandOverContractCommentAttachments { get; set; }
        public virtual DbSet<HandOverContractLine> HandOverContractLines { get; set; }
        public virtual DbSet<ParentTab> ParentTabs { get; set; }
        public virtual DbSet<PickList> PickLists { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductAttachment> ProductAttachments { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductCategoryGroup> ProductCategoryGroups { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<ProfileToStandardPermission> ProfileToStandardPermissions { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<ProfileToCustomPermission> ProfileToCustomPermissions { get; set; }
        public virtual DbSet<ProfileToTab> ProfileToTabs { get; set; }
        public virtual DbSet<RecurringType> RecurringTypes { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<ReportFolder> ReportFolders { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleSeq> RoleSeqs { get; set; }
        public virtual DbSet<RoleToProfile> RoleToProfiles { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<Showroom> Showrooms { get; set; }
        public virtual DbSet<Tab> Tabs { get; set; }
        public virtual DbSet<UserDashboard> UserDashboards { get; set; }
        public virtual DbSet<UserInfo> UserInfoes { get; set; }
        public virtual DbSet<UserInfoToCompany> UserInfoToCompanies { get; set; }
        public virtual DbSet<UserInfoToDealer> UserInfoToDealers { get; set; }
        public virtual DbSet<UserInfoToRole> UserInfoToRoles { get; set; }
        public virtual DbSet<UserInfoToShowroom> UserInfoToShowrooms { get; set; }
        public virtual DbSet<DocumentFolder> DocumentFolders { get; set; }
        public virtual DbSet<UserDocument> UserDocuments { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }

        // View mappings
        public virtual DbSet<CalendarEventListViewSearchResult> CalendarEventListViewSearchResults { get; set; }
        public virtual DbSet<UserInfoSearch> UserInfoSearchs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ActivityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ActivityAssignmentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ActivityStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ActivityPriorityEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new CurrencyEntityTypeConfiguration());

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.Property(e => e.FileName).IsRequired().HasMaxLength(255);

                entity.Property(e => e.ContentType).HasMaxLength(100);

                entity.Property(e => e.CreatedByUserKey).IsRequired().HasMaxLength(128);

                entity.Property(e => e.DateAttached).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.FileSize).IsRequired();

                entity.Property(e => e.FileUrl).HasMaxLength(255);

                entity.Property(e => e.ThumbnailUrl).HasMaxLength(255);
            });

            modelBuilder.Entity<CalendarEvent>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.DueDateTime).HasColumnType("datetime");

                entity.Property(e => e.EventStatus).HasMaxLength(50);

                entity.Property(e => e.Location).HasMaxLength(255);

                entity.Property(e => e.OwnerUserKey).HasMaxLength(128);

                entity.Property(e => e.Priority).HasMaxLength(50);

                entity.Property(e => e.RecurringType).HasMaxLength(255);

                entity.Property(e => e.StartDateTime).HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Tags).HasMaxLength(255);

                entity.Property(e => e.Visibility).HasMaxLength(50);

                entity.HasOne(d => d.CalendarEventType)
                    .WithMany(p => p.CalendarEvent)
                    .HasForeignKey(d => d.CalendarEventTypeId)
                    .HasConstraintName("FK_CalendarEvent_CalendarEventType");
            });

            modelBuilder.Entity<CalendarEventType>(entity =>
            {
                entity.Property(e => e.Color).HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.LogoRelativeUrl).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.WebSite).HasMaxLength(255);
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.Property(e => e.AddressCity).HasMaxLength(255);

                entity.Property(e => e.AddressCountry).HasMaxLength(50);

                entity.Property(e => e.AddressDistrict).HasMaxLength(255);

                entity.Property(e => e.AddressPostalCode).HasMaxLength(20);

                entity.Property(e => e.AddressState).HasMaxLength(255);

                entity.Property(e => e.AddressStreet).HasMaxLength(255);

                entity.Property(e => e.ApproachingType).HasMaxLength(255);

                entity.Property(e => e.CarIsUsing).HasMaxLength(255);

                entity.Property(e => e.CompanyEmail).HasMaxLength(255);

                entity.Property(e => e.CompanyName).HasMaxLength(255);

                entity.Property(e => e.CompanyOwner).HasMaxLength(255);

                entity.Property(e => e.CompanyTaxCode).HasMaxLength(50);

                entity.Property(e => e.CompanyWebSite).HasMaxLength(255);

                entity.Property(e => e.ContactNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ContactNote).HasMaxLength(2000);

                entity.Property(e => e.ContactSource).HasMaxLength(255);

                entity.Property(e => e.ContactType).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstMeetingDate).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.HandlerUserKey).HasMaxLength(128);

                entity.Property(e => e.HomePhone).HasMaxLength(20);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.MobilePhone).HasMaxLength(20);

                entity.Property(e => e.Occupation).HasMaxLength(255);

                entity.Property(e => e.OfficePhone).HasMaxLength(20);

                entity.Property(e => e.PaymentType).HasMaxLength(255);

                entity.Property(e => e.PlanToBuyDate).HasColumnType("datetime");

                entity.Property(e => e.Salutation).HasMaxLength(50);

                entity.Property(e => e.Tags).HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);
            });

            modelBuilder.Entity<ContactComment>(entity =>
            {
                entity.Property(e => e.CommentContent)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.ContactComments)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContactComment_Contact");
            });

            modelBuilder.Entity<ContactMeetingProgress>(entity =>
            {
                entity.Property(e => e.CarColorOfInterest).HasMaxLength(50);

                entity.Property(e => e.CarOfInterest).HasMaxLength(255);

                entity.Property(e => e.DateOfMeeting).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.MeetingStatus).HasMaxLength(50);

                entity.Property(e => e.OtherNote).HasMaxLength(2000);

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.ContactMeetingProgresses)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_ContactMeetingProgress_Contact");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.CompanyName).HasMaxLength(255);

                entity.Property(e => e.ContactType).HasMaxLength(50);

                entity.Property(e => e.CustomerNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CustomerSource).HasMaxLength(255);

                entity.Property(e => e.CustomerType).HasMaxLength(100);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.HomePhone).HasMaxLength(20);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.MobilePhone).HasMaxLength(20);

                entity.Property(e => e.Occupation).HasMaxLength(255);

                entity.Property(e => e.OfficePhone).HasMaxLength(20);

                entity.Property(e => e.PaymentType).HasMaxLength(100);

                entity.Property(e => e.Salutation).HasMaxLength(50);
            });

            modelBuilder.Entity<CustomerService>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AppointedSalesPersonnelFullName).HasMaxLength(255);

                entity.Property(e => e.AppointedSalesUserKey).HasMaxLength(128);

                entity.Property(e => e.Category).HasMaxLength(255);

                entity.Property(e => e.ContractDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PlanServiceCompleteDate).HasColumnType("datetime");

                entity.Property(e => e.PlateNumber).HasMaxLength(30);

                entity.Property(e => e.Priority).HasMaxLength(50);

                entity.Property(e => e.ProductModel).HasMaxLength(255);

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.ReasonToService).HasMaxLength(500);

                entity.Property(e => e.ReminderPersonnelFullName).HasMaxLength(255);

                entity.Property(e => e.ReminderPersonnelUserKey).HasMaxLength(128);

                entity.Property(e => e.SalesPersonnelFullName).HasMaxLength(255);

                entity.Property(e => e.SalesUserKey).HasMaxLength(128);

                entity.Property(e => e.ServiceDate).HasColumnType("datetime");

                entity.Property(e => e.ServicePersonnel).HasMaxLength(255);

                entity.Property(e => e.ServiceResult).HasMaxLength(2000);

                entity.Property(e => e.ServiceUserKey).HasMaxLength(128);

                entity.Property(e => e.VehicleRunKm).HasMaxLength(30);
            });

            modelBuilder.Entity<CustomerServiceComment>(entity =>
            {
                entity.Property(e => e.CommentContent)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.CustomerService)
                    .WithMany(p => p.CustomerServiceComment)
                    .HasForeignKey(d => d.CustomerServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerServiceComment_CustomerService");
            });

            modelBuilder.Entity<DatabaseVersion>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Dealer>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.LogoRelativeUrl).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.WebSite).HasMaxLength(255);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Dealer)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Dealer_Company");
            });

            modelBuilder.Entity<EventInvitee>(entity =>
            {
                entity.Property(e => e.AssigneeUserFullName).HasMaxLength(256);

                entity.Property(e => e.AssigneeUserKey).HasMaxLength(128);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventInvitee)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EventInvitee_CalendarEvent");
            });

            modelBuilder.Entity<EventRecurrence>(entity =>
            {
                entity.Property(e => e.RecurringDate).HasColumnType("datetime");

                entity.Property(e => e.RecurringEndDate).HasColumnType("datetime");

                entity.Property(e => e.RecurringInfo).HasMaxLength(100);

                entity.Property(e => e.RecurringType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventRecurrence)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EventRecurrence_CalendarEvent");
            });

            modelBuilder.Entity<EventReminder>(entity =>
            {
                entity.Property(e => e.ReminderType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventReminder)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EventReminder_CalendarEvent");
            });

            modelBuilder.Entity<EventStatus>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<FreeTag>(entity =>
            {
                entity.Property(e => e.OwnerUserKey).HasMaxLength(128);

                entity.Property(e => e.RawTag).HasMaxLength(50);

                entity.Property(e => e.Tag)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Visibility)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<GeneralAuditTrail>(entity =>
            {
                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ActionDate).HasColumnType("datetime");

                entity.Property(e => e.Entity)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Module)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.RecordId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UserFullName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.UserKey)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<HandOverContract>(entity =>
            {
                entity.Property(e => e.AddressCity).HasMaxLength(255);

                entity.Property(e => e.AddressCountry).HasMaxLength(50);

                entity.Property(e => e.AddressDistrict).HasMaxLength(255);

                entity.Property(e => e.AddressPostalCode).HasMaxLength(20);

                entity.Property(e => e.AddressState).HasMaxLength(255);

                entity.Property(e => e.AddressStreet).HasMaxLength(255);

                entity.Property(e => e.CompanyName).HasMaxLength(255);

                entity.Property(e => e.ContactType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ContractApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.ContractDate).HasColumnType("datetime");

                entity.Property(e => e.ContractDescription).HasMaxLength(2000);

                entity.Property(e => e.ContractPersonnelFullName).HasMaxLength(255);

                entity.Property(e => e.ContractSubmittedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerRequestDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerType).HasMaxLength(255);

                entity.Property(e => e.DeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.DeliveryPriority).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.PdicompletedDate)
                    .HasColumnName("PDICompletedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdiinstallationCompletedtDate)
                    .HasColumnName("PDIInstallationCompletedtDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdiinstallationRequestDate)
                    .HasColumnName("PDIInstallationRequestDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdirequestDate)
                    .HasColumnName("PDIRequestDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdsactualDeliveryDate)
                    .HasColumnName("PDSActualDeliveryDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdsactualRegistrationDate)
                    .HasColumnName("PDSActualRegistrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdsplanRegistrationDate)
                    .HasColumnName("PDSPlanRegistrationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdssaleDate)
                    .HasColumnName("PDSSaleDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProductFactoryDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.ProductName).HasMaxLength(255);

                entity.Property(e => e.ProductRequestToFactoryDate).HasColumnType("datetime");

                entity.Property(e => e.ProductType).HasMaxLength(255);

                entity.Property(e => e.SalesUserKey).HasMaxLength(128);

                entity.Property(e => e.Tags).HasMaxLength(255);

                entity.Property(e => e.TermAndCondition).HasMaxLength(4000);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Vin)
                    .HasColumnName("VIN")
                    .HasMaxLength(50);

                entity.Property(e => e.WarrantyDescription).HasMaxLength(2000);

                entity.Property(e => e.WarrantyDurationInMonths).HasMaxLength(50);

                entity.Property(e => e.WarrantyInKm).HasMaxLength(30);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.HandOverContract)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_HandOverContract_Customer");
            });

            modelBuilder.Entity<HandOverContractComment>(entity =>
            {
                entity.Property(e => e.CommentContent)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.HandOverContract)
                    .WithMany(p => p.HandOverContractComment)
                    .HasForeignKey(d => d.HandOverContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HandOverContractComment_HandOverContract");
            });

            modelBuilder.Entity<HandOverContractLine>(entity =>
            {
                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.HandOverContract)
                    .WithMany(p => p.HandOverContractLine)
                    .HasForeignKey(d => d.HandOverContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HandOverContractLine_HandOverContract");
            });

            modelBuilder.Entity<ParentTab>(entity =>
            {
                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PickList>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Query).HasMaxLength(2048);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.BankOfGuarantee).HasMaxLength(255);

                entity.Property(e => e.Color).HasMaxLength(50);

                entity.Property(e => e.DateEnteredStock).HasColumnType("datetime");

                entity.Property(e => e.DateRelease).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.Engine).HasMaxLength(255);

                entity.Property(e => e.EngineNumber).HasMaxLength(50);

                entity.Property(e => e.HandlerUserKey).HasMaxLength(128);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.PaymentStatus).HasMaxLength(2000);

                entity.Property(e => e.PlateNumber).HasMaxLength(20);

                entity.Property(e => e.ProductNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ProductType).HasMaxLength(255);

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.PurchaseOrderNo).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(255);

                entity.Property(e => e.TransmissionType).HasMaxLength(255);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 8)");

                entity.Property(e => e.Uom)
                    .HasColumnName("UOM")
                    .HasMaxLength(100);

                entity.Property(e => e.Vin)
                    .HasColumnName("VIN")
                    .HasMaxLength(50);

                entity.Property(e => e.WarehouseName).HasMaxLength(255);

                entity.HasOne(d => d.ProductCategory)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.ProductCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductCategory");
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.CategoryGroup)
                    .WithMany(p => p.ProductCategory)
                    .HasForeignKey(d => d.CategoryGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductCategory_ProductCategoryGroup");
            });

            modelBuilder.Entity<ProductCategoryGroup>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.DisplayOrder)
                    .IsRequired()
                    .HasDefaultValue(-1);
            });

            modelBuilder.Entity<ProfileToStandardPermission>(entity =>
            {
                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfileToStandardPermissions)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToStandardPermission_Profile");

                entity.HasOne(d => d.Tab)
                    .WithMany(p => p.ProfileToStandardPermission)
                    .HasForeignKey(d => d.TabId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToStandardPermission_Tab");
            });

            modelBuilder.Entity<ProfileToCustomPermission>(entity =>
            {
                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfileToCustomPermissions)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToCustomPermission_Profile");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.ProfileToCustomPermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToCustomPermission_Permission");
            });

            modelBuilder.Entity<ProfileToTab>(entity =>
            {
                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.ProfileToTabs)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToTab_Profile");

                entity.HasOne(d => d.Tab)
                    .WithMany(p => p.ProfileToTab)
                    .HasForeignKey(d => d.TabId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileToTab_Tab");
            });

            modelBuilder.Entity<RecurringType>(entity =>
            {
                entity.Property(e => e.Color).HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.Property(e => e.AccessRoute)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ReportType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Folder)
                    .WithMany(p => p.Report)
                    .HasForeignKey(d => d.FolderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Report_ReportFolder");
            });

            modelBuilder.Entity<ReportFolder>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId)
                    .HasMaxLength(36)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ParentRoleId).HasMaxLength(36);

                entity.Property(e => e.ParentRoleName).HasMaxLength(255);

                entity.Property(e => e.TreePath).HasMaxLength(500);
            });

            modelBuilder.Entity<RoleSeq>(entity =>
            {
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<RoleToProfile>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.ProfileId });

                entity.Property(e => e.RoleId).HasMaxLength(36);

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.RoleToProfiles)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleToProfile_Profile");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleToProfile)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleToProfile_Role");
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.Property(e => e.SettingId).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SettingGroup)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SettingValue).HasMaxLength(1024);
            });

            modelBuilder.Entity<Showroom>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.Showrooms)
                    .HasForeignKey(d => d.DealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Showroom_Dealer");
            });

            modelBuilder.Entity<Tab>(entity =>
            {
                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ParentTabLabel).HasMaxLength(50);
            });

            modelBuilder.Entity<UserDashboard>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.UserKey)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.WidgetSettings).HasColumnType("text");

                entity.HasOne(d => d.UserKeyNavigation)
                    .WithMany(p => p.UserDashboard)
                    .HasForeignKey(d => d.UserKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserDashboard_UserInfo");
            });

            modelBuilder.Entity<UserInfo>(entity =>
            {
                entity.HasKey(e => e.UserKey);

                entity.Property(e => e.UserKey)
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.AddressCity).HasMaxLength(255);

                entity.Property(e => e.AddressCountry).HasMaxLength(50);

                entity.Property(e => e.AddressDistrict).HasMaxLength(255);

                entity.Property(e => e.AddressPostalCode).HasMaxLength(25);

                entity.Property(e => e.AddressState).HasMaxLength(255);

                entity.Property(e => e.AddressStreet).HasMaxLength(255);

                entity.Property(e => e.AvatarRelativeUrl).HasMaxLength(255);

                entity.Property(e => e.CalendarColor)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('#E6FAD8')");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrencyDecimalSeparator)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyGroupingPattern)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyGroupingSeparator)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencySymbolPlacement)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateFormat)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Department).HasMaxLength(255);

                entity.Property(e => e.CurrencyCode).HasMaxLength(10);

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.HomePhone).HasMaxLength(50);

                entity.Property(e => e.HourFotmat)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.LastName).HasMaxLength(128);

                entity.Property(e => e.MobilePhone).HasMaxLength(50);

                entity.Property(e => e.ModifiedByUserFullName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedByUserKey)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OtherPhone).HasMaxLength(50);

                entity.Property(e => e.ReportToUserKey).HasMaxLength(128);

                entity.Property(e => e.Salutation).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(25);

                entity.Property(e => e.TimeZone)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title).HasColumnType("nchar(10)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.WorkPhone).HasMaxLength(50);

                entity.HasOne(d => d.UserRole)
                    .WithOne(p => p.UserInfo)
                    .HasForeignKey<UserInfo>(d => d.UserKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfo_UserInfoToRole");
            });

            modelBuilder.Entity<UserInfoToCompany>(entity =>
            {
                entity.HasKey(e => new { e.UserKey, e.CompanyId });

                entity.Property(e => e.UserKey).HasMaxLength(128);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.UserInfoToCompany)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToCompany_Company");

                entity.HasOne(d => d.UserKeyNavigation)
                    .WithMany(p => p.UserInfoToCompany)
                    .HasForeignKey(d => d.UserKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToCompany_UserInfo");
            });

            modelBuilder.Entity<UserInfoToDealer>(entity =>
            {
                entity.HasKey(e => new { e.UserKey, e.DealerId });

                entity.Property(e => e.UserKey).HasMaxLength(128);

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.UserInfoToDealer)
                    .HasForeignKey(d => d.DealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToDealer_Dealer");

                entity.HasOne(d => d.UserKeyNavigation)
                    .WithMany(p => p.UserInfoToDealer)
                    .HasForeignKey(d => d.UserKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToDealer_UserInfo");
            });

            modelBuilder.Entity<UserInfoToRole>(entity =>
            {
                entity.HasKey(e => e.UserKey);

                entity.Property(e => e.UserKey)
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleId)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserInfoToRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToRole_Role");
            });

            modelBuilder.Entity<UserInfoToShowroom>(entity =>
            {
                entity.HasKey(e => new { e.UserKey, e.ShowroomId });

                entity.Property(e => e.UserKey).HasMaxLength(128);

                entity.HasOne(d => d.Showroom)
                    .WithMany(p => p.UserInfoToShowroom)
                    .HasForeignKey(d => d.ShowroomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToShowroom_Showroom");

                entity.HasOne(d => d.UserKeyNavigation)
                    .WithMany(p => p.UserInfoToShowroom)
                    .HasForeignKey(d => d.UserKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserInfoToShowroom_UserInfo");
            });

            modelBuilder.ApplyConfiguration(new DocumentFolderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserDocumentEntityTypeConfiguration());
        }
    }
}
