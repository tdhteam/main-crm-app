﻿using DEA.CRM.Authentication.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.Authentication
{    
    public class AuthApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public AuthApplicationDbContext(DbContextOptions<AuthApplicationDbContext> options) 
            : base(options) { }
    }
}
