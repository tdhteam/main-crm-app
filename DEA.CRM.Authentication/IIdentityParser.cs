﻿using System.Security.Principal;

namespace DEA.CRM.Authentication
{
    public interface IIdentityParser<T>
    {
        T Parse(IPrincipal principal);
    }
}
