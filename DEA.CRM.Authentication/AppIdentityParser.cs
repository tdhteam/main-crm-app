﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using DEA.CRM.Authentication.Helpers;
using DEA.CRM.Authentication.Models;

namespace DEA.CRM.Authentication
{
    public class AppIdentityParser : IIdentityParser<ApplicationUser>
    {
        public ApplicationUser Parse(IPrincipal principal)
        {
            // Pattern matching 'is' expression
            // assigns "claims" if "principal" is a "ClaimsPrincipal"
            if (principal is ClaimsPrincipal claims)
            {
                return new ApplicationUser
                {                                       
                    Id = claims.Claims.FirstOrDefault(x => x.Type == Constants.Strings.JwtClaimIdentifiers.Id)?.Value ?? string.Empty,                    
                    UserName = claims.Claims.FirstOrDefault(x => x.Type == Constants.Strings.JwtClaimIdentifiers.UserName)?.Value ?? string.Empty                    
                };
            }
            throw new ArgumentException(message: "The principal must be a ClaimsPrincipal", paramName: nameof(principal));
        }
    }
}
