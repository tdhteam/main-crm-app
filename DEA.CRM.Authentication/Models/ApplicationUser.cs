﻿using Microsoft.AspNetCore.Identity;

namespace DEA.CRM.Authentication.Models
{
    public class ApplicationUser : IdentityUser
    {        
    }
}
