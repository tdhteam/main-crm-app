﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DEA.CRM.Authentication.JwtAuth;
using DEA.CRM.Authentication.Models;
using Newtonsoft.Json;

namespace DEA.CRM.Authentication.Helpers
{
    public class AppJwtToken
    {
        public string id;
        public string auth_token;
        public int expires_in;
    }
    
    public class Tokens
    {
        public static async Task<string> GenerateJwt(
            ClaimsIdentity identity, 
            IJwtFactory jwtFactory, 
            string userName, 
            JwtIssuerOptions jwtOptions, 
            JsonSerializerSettings serializerSettings)
        {
            var response = new AppJwtToken
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await jwtFactory.GenerateEncodedToken(userName, identity),
                expires_in = (int)jwtOptions.ValidFor.TotalSeconds
            };

            return JsonConvert.SerializeObject(response, serializerSettings);
        }
        
        public static async Task<AppJwtToken> GenerateJwtAsObject(
            ClaimsIdentity identity, 
            IJwtFactory jwtFactory, 
            string userName, 
            JwtIssuerOptions jwtOptions)
        {
            return new AppJwtToken
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await jwtFactory.GenerateEncodedToken(userName, identity),
                expires_in = (int)jwtOptions.ValidFor.TotalSeconds
            };
        }
    }
}
