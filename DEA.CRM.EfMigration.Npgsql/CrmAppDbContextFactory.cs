﻿using DEA.CRM.EfModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DEA.CRM.EfMigration.Npgsql
{
    public class CrmAppDbContextFactory : IDesignTimeDbContextFactory<NpgsqlCRMAppDbContext>
    {
        public NpgsqlCRMAppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CRMAppDbContext>();
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=CRMApp;Username=thinh;Password=;");

            return new NpgsqlCRMAppDbContext(optionsBuilder.Options);
        }
    }
}
