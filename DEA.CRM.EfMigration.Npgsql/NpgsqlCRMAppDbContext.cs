﻿using DEA.CRM.EfModel;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.EfMigration.Npgsql
{
    public class NpgsqlCRMAppDbContext : CRMAppDbContext
    {
        public NpgsqlCRMAppDbContext(DbContextOptions<CRMAppDbContext> options) : base(options)
        {
        }
    }
}
