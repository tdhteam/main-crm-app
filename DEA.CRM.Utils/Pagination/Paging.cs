﻿using System.ComponentModel;

namespace DEA.CRM.Utils.Pagination
{
    public class Paging
    {
        /// <summary>
        /// Select by page number
        /// </summary>
        [DefaultValue(1)]
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Number of record per page
        /// </summary>
        [DefaultValue(10)]
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Order by field
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// Order  ASC=true | DESC=false , default is true
        /// </summary>
        [DefaultValue(true)]
        public bool OrderAsc { get; set; } = true;

        /// <summary>
        /// To control the cache validity of data
        /// </summary>
        public long Timestamp { get; set; }

        public bool FindAll { get; set; }

        public bool CountAll { get; set; } = true;

        protected bool Equals(Paging other)
        {
            return PageNumber == other.PageNumber && PageSize == other.PageSize & Timestamp == other.Timestamp;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Paging) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (PageNumber*397) ^ PageSize;
            }
        }
    }
}
