﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DEA.CRM.Utils.Pagination
{
    /// <summary>
    /// Provides strongly typed OrderBy expression for Paging objects
    /// </summary>
    public static class PagingExt
    {
        /// <summary>
        /// Set the OrderBy using a strong typed expression, thus is refactoring friendly
        /// and error free.
        /// </summary>
        /// <remarks>
        /// Expression creation is costly. Comparing to the direct string assignment
        /// approach this is 100x slower when the expression is built on the fly.
        /// Still it's capable of creating ~200K expressions per second, which should
        /// be enough for the C10k requirements.
        /// If raw performance is a must, use a static expression for each of the command
        /// type or revert to using direct string assignment.
        /// </remarks>
        public static void SetOrderBy<T, T1>(this T model, Expression<Func<T, T1>> member) where T : Paging
        {
            model.OrderBy = ((MemberExpression)member.Body).Member.Name;
        }

        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> target, Paging command)
        {
            return command.FindAll
                ? target
                : target
                    .Skip((command.PageNumber - 1) * command.PageSize)
                    .Take(command.PageSize);
        }

        public static IQueryable<T> Paginate<T>(this IQueryable<T> target, Paging command)
        {
            return command.FindAll
                ? target.Skip(0)
                : target
                    .Skip((command.PageNumber - 1) * command.PageSize)
                    .Take(command.CountAll ? command.PageSize : command.PageSize + 1);
        }

        //public static IQueryable<T> SetOrderByField<T, TProp>(this IQueryable<T> target, bool isAsc, Func<T, TProp> keySelector)
        //{
        //    return isAsc ? target.OrderBy(keySelector).AsQueryable() : target.OrderByDescending(keySelector).AsQueryable();
        //}

        public static IQueryable<T> SetOrderByField<T>(this IQueryable<T> target, bool isAsc, string fieldName)
        {
            return isAsc ? target.OrderBy(x=>x.GetReflectedPropertyValue(fieldName)).AsQueryable() : target.OrderByDescending(x=>x.GetReflectedPropertyValue(fieldName)).AsQueryable();
        }

        public static string GetReflectedPropertyValue(this object subject, string field)
        {
            field = char.ToUpper(field[0]) + field.Substring(1);
            object reflectedValue = subject.GetType().GetProperty(field).GetValue(subject, null);
            return reflectedValue != null ? reflectedValue.ToString() : "";
        }
    }
}