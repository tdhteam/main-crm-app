﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DEA.CRM.Utils.Pagination
{
    /// <summary>
    /// This extend paging base because , previous paging info should be returned to keep track for next action
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class PagingResult<TResult> : Paging
    {
        /// <summary>
        /// Number of total pages.
        /// If counting is supported in this function
        /// </summary>
        [DefaultValue(0)]
        public int PageTotal { get; set; }

        public bool HasMore { get; set; }

        /// <summary>
        /// If counting is supported in this function
        /// </summary>
        public long ResultCount { get; set; }

        /// <summary>
        /// Number of milisecond which SQLs is executed. Used to benkmark SQL performance.
        /// </summary>
        public long ExecutionDuration { get; set; }

        /// <summary>
        /// Result set of found items
        /// </summary>
        public IEnumerable<TResult> ResultSet { get; }

        public PagingResult(IEnumerable<TResult> resultSet)
        {
            ResultSet = resultSet;
        }

        public PagingResult<U> Map<U>(Func<TResult, U> map)
        {
            return new PagingResult<U>(ResultSet.Select(map).ToArray())
                { ExecutionDuration = ExecutionDuration
                , HasMore = HasMore
                , PageTotal = PageTotal
                , ResultCount = ResultCount
                , PageNumber = PageNumber
                , PageSize = PageSize
                };
        }

        public static PagingResult<TResult> Empty()
        {
            return new PagingResult<TResult>(Enumerable.Empty<TResult>());
        }

        public static PagingResult<TResult> FromResult
            ( IEnumerable<TResult> resultSet
            , long executionTicks
            , long resultCount
            , int pageSize
            , int pageNumber
            )
        {
            var result = new PagingResult<TResult>(resultSet)
                { ExecutionDuration = executionTicks
                , ResultCount = resultCount
                , PageSize = pageSize
                , PageNumber = pageNumber
                , PageTotal = (int) (resultCount / pageSize) + (resultCount % pageSize > 0 ? 1 : 0)
                };

            return result;
        }

        public static PagingResult<TResult> FromResult
            ( ICollection<TResult> resultSet
            , bool hasMore
            , int pageSize
            , int pageNumber
            )
        {
            var result = new PagingResult<TResult>(resultSet)
                { ResultCount = resultSet.Count
                , HasMore = hasMore
                , PageSize = pageSize
                , PageNumber = pageNumber
                };

            return result;
        }

        public static PagingResult<TResult> AllResult(IList<TResult> resultSet)
        {
            var resultCount = resultSet.Count;
            return new PagingResult<TResult>(resultSet)
                { ResultCount = resultCount
                , PageSize = resultCount
                , PageNumber = 1
                , PageTotal = 1
                };
        }
    }

    public static class PagingHelper
    {
        public static int GetPageTotal(long resultCount, int pageSize)
        {
            return (int)resultCount/pageSize + (resultCount % pageSize > 0 ? 1 : 0);
        }
    }
}
