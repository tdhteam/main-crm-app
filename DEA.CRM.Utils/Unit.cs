﻿using System;
using System.Diagnostics.Contracts;

namespace DEA.CRM.Utils
{
    public struct Unit : IEquatable<Unit>
    {
        public static readonly Unit Default = new Unit();

        [Pure]
        public override int GetHashCode() => 0;

        [Pure]
        public override bool Equals(object obj) => obj is Unit;

        [Pure]
        public override string ToString() => "()";

        [Pure]
        public bool Equals(Unit other) =>
            true;

        [Pure]
        public static bool operator ==(Unit lhs, Unit rhs) => true;

        [Pure]
        public static bool operator !=(Unit lhs, Unit rhs) => false;
    }
}
