﻿using System;
using DEA.CRM.Utils.Memoization;

namespace DEA.CRM.Utils
{
    public class Actions
    {
        public static void DoNothing()
        {
        }

        public static void DoNothing<A>(A a)
        {
        }

        public static void DoNothing<A, B>(A a, B b)
        {
        }

        public static void DoNothing<A, B, C>(A a, B b, C c)
        {
        }

        public static void DoNothing<A, B, C, D>(A a, B b, C c, D d)
        {
        }

        public static void DoNothing<A, B, C, D, E>(A a, B b, C c, D d, E e)
        {
        }
    }

    public class Functions
    {
        public static T Identity<T>(T x) => x;

        public static Func<bool, bool, bool> Or => (l, r) => l || r;

        public static Func<bool, bool, bool> And => (l, r) => l && r;

        public static Func<bool, bool> Not => b => !b;

        public static Func<TResult> Compose<T1, TResult>(Func<T1, TResult> f, Func<T1> g)
        {
            return () => f(g());
        }

        public static Func<T1, TResult> Compose<T1, T2, TResult>(Func<T2, TResult> f, Func<T1, T2> g)
        {
            return a => f(g(a));
        }

        public static Func<T1, T3, TResult> Compose<T1, T2, T3, TResult>(Func<T2, T3, TResult> f, Func<T1, T2> g)
        {
            return (a, b) => f(g(a), b);
        }

        public static Func<T1, T2, TResult> Compose<T1, T2, T3, TResult>(Func<T3, TResult> f, Func<T1, T2, T3> g)
        {
            return (a, b) => f(g(a, b));
        }

        public static Func<T1, T2, T3, TResult> Compose<T1, T2, T3, T4, TResult>(Func<T4, TResult> f, Func<T1, T2, T3, T4> g)
        {
            return (a, b, c) => f(g(a, b, c));
        }

        public static Func<T1, T2, T3, T4, TResult> Compose<T1, T2, T3, T4, T5, TResult>(Func<T5, TResult> f, Func<T1, T2, T3, T4, T5> g)
        {
            return (a, b, c, d) => f(g(a, b, c, d));
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T1, T2, T3, T4, T5, T6> g)
        {
            return (a, b, c, d, e) => f(g(a, b, c, d, e));
        }

        public static Func<TResult> Compose<T1, T2, TResult>(Func<T2, TResult> f, Func<T1, T2> g, Func<T1> h)
        {
            return () => f(g(h()));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, TResult>(Func<T3, TResult> f, Func<T2, T3> g, Func<T1, T2> h)
        {
            return a => f(g(h(a)));
        }

        public static Func<T1, T2, TResult> Compose<T1, T2, T3, T4, TResult>(Func<T4, TResult> f, Func<T3, T4> g, Func<T1, T2, T3> h)
        {
            return (a, b) => f(g(h(a, b)));
        }

        public static Func<T1, T2, T3, TResult> Compose<T1, T2, T3, T4, T5, TResult>(Func<T5, TResult> f, Func<T4, T5> g, Func<T1, T2, T3, T4> h)
        {
            return (a, b, c) => f(g(h(a, b, c)));
        }

        public static Func<T1, T2, T3, T4, TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T5, T6> g, Func<T1, T2, T3, T4, T5> h)
        {
            return (a, b, c, d) => f(g(h(a, b, c, d)));
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, TResult>(Func<T7, TResult> f, Func<T6, T7> g, Func<T1, T2, T3, T4, T5, T6> h)
        {
            return (a, b, c, d, e) => f(g(h(a, b, c, d, e)));
        }

        public static Func<TResult> Compose<T1, T2, T3, TResult>(Func<T3, TResult> f, Func<T2, T3> g, Func<T1, T2> h, Func<T1> i)
        {
            return () => f(g(h(i())));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, T4, TResult>(Func<T4, TResult> f, Func<T3, T4> g, Func<T2, T3> h, Func<T1, T2> i)
        {
            return a => f(g(h(i(a))));
        }

        public static Func<T1, T2, TResult> Compose<T1, T2, T3, T4, T5, TResult>(Func<T5, TResult> f, Func<T4, T5> g, Func<T3, T4> h, Func<T1, T2, T3> i)
        {
            return (a, b) => f(g(h(i(a, b))));
        }

        public static Func<T1, T2, T3, TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T5, T6> g, Func<T4, T5> h, Func<T1, T2, T3, T4> i)
        {
            return (a, b, c) => f(g(h(i(a, b, c))));
        }

        public static Func<T1, T2, T3, T4, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, TResult>(Func<T7, TResult> f, Func<T6, T7> g, Func<T5, T6> h, Func<T1, T2, T3, T4, T5> i)
        {
            return (a, b, c, d) => f(g(h(i(a, b, c, d))));
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(Func<T8, TResult> f, Func<T7, T8> g, Func<T6, T7> h, Func<T1, T2, T3, T4, T5, T6> i)
        {
            return (a, b, c, d, e) => f(g(h(i(a, b, c, d, e))));
        }

        public static Func<TResult> Compose<T1, T2, T3, T4, TResult>(Func<T4, TResult> f, Func<T3, T4> g, Func<T2, T3> h, Func<T1, T2> i, Func<T1> j)
        {
            return () => f(g(h(i(j()))));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, T4, T5, TResult>(Func<T5, TResult> f, Func<T4, T5> g, Func<T3, T4> h, Func<T2, T3> i, Func<T1, T2> j)
        {
            return a => f(g(h(i(j(a)))));
        }

        public static Func<T1, T2, TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T5, T6> g, Func<T4, T5> h, Func<T3, T4> i, Func<T1, T2, T3> j)
        {
            return (a, b) => f(g(h(i(j(a, b)))));
        }

        public static Func<T1, T2, T3, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, TResult>(Func<T7, TResult> f, Func<T6, T7> g, Func<T5, T6> h, Func<T4, T5> i, Func<T1, T2, T3, T4> j)
        {
            return (a, b, c) => f(g(h(i(j(a, b, c)))));
        }

        public static Func<T1, T2, T3, T4, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(Func<T8, TResult> f, Func<T7, T8> g, Func<T6, T7> h, Func<T5, T6> i, Func<T1, T2, T3, T4, T5> j)
        {
            return (a, b, c, d) => f(g(h(i(j(a, b, c, d)))));
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult>(Func<T9, TResult> f, Func<T8, T9> g, Func<T7, T8> h, Func<T6, T7> i, Func<T1, T2, T3, T4, T5, T6> j)
        {
            return (a, b, c, d, e) => f(g(h(i(j(a, b, c, d, e)))));
        }


        #region 6-params
        public static Func<TResult> Compose<T1, T2, T3, T4, T5, TResult>(Func<T5, TResult> f, Func<T4, T5> g, Func<T3, T4> h, Func<T2, T3> i, Func<T1, T2> j, Func<T1> k)
        {
            return () => f(g(h(i(j(k())))));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T5, T6> g, Func<T4, T5> h, Func<T3, T4> i, Func<T2, T3> j, Func<T1, T2> k)
        {
            return a => f(g(h(i(j(k(a))))));
        }

        #endregion

        #region 7-params
        public static Func<TResult> Compose<T1, T2, T3, T4, T5, T6, TResult>(Func<T6, TResult> f, Func<T5, T6> g, Func<T4, T5> h, Func<T3, T4> i, Func<T2, T3> j, Func<T1, T2> k, Func<T1> l)
        {
            return () => f(g(h(i(j(k(l()))))));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, TResult>(Func<T7, TResult> f, Func<T6, T7> g, Func<T5, T6> h, Func<T4, T5> i, Func<T3, T4> j, Func<T2, T3> k, Func<T1, T2> l)
        {
            return a => f(g(h(i(j(k(l(a)))))));
        }

        #endregion

        #region 8-params
        public static Func<TResult> Compose<T1, T2, T3, T4, T5, T6, T7, TResult>(Func<T7, TResult> f, Func<T6, T7> g, Func<T5, T6> h, Func<T4, T5> i, Func<T3, T4> j, Func<T2, T3> k, Func<T1, T2> l, Func<T1> m)
        {
            return () => f(g(h(i(j(k(l(m())))))));
        }

        public static Func<T1, TResult> Compose<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(Func<T8, TResult> f, Func<T7, T8> g, Func<T6, T7> h, Func<T5, T6> i, Func<T4, T5> j, Func<T3, T4> k, Func<T2, T3> l, Func<T1, T2> m)
        {
            return a => f(g(h(i(j(k(l(m(a))))))));
        }

        #endregion


        public static Func<TResult> Lambda<TResult>(Func<TResult> func)
        {
            return func;
        }

        public static Func<T, TResult> Lambda<T, TResult>(Func<T, TResult> func)
        {
            return func;
        }

        public static Func<T1, T2, TResult> Lambda<T1, T2, TResult>(Func<T1, T2, TResult> func)
        {
            return func;
        }

        public static Func<T1, T2, T3, TResult> Lambda<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> func)
        {
            return func;
        }

        public static Func<T1, T2, T3, T4, TResult> Lambda<T1, T2, T3, T4, TResult>(Func<T1, T2, T3, T4, TResult> func)
        {
            return func;
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Lambda<T1, T2, T3, T4, T5, TResult>(Func<T1, T2, T3, T4, T5, TResult> func)
        {
            return func;
        }

        public static Action<T> Lambda<T>(Action<T> func)
        {
            return func;
        }

        public static Action<T1, T2> Lambda<T1, T2>(Action<T1, T2> func)
        {
            return func;
        }

        public static Action<T1, T2, T3> Lambda<T1, T2, T3>(Action<T1, T2, T3> func)
        {
            return func;
        }

        public static Action<T1, T2, T3, T4> Lambda<T1, T2, T3, T4>(Action<T1, T2, T3, T4> func)
        {
            return func;
        }

        public static Action<T1, T2, T3, T4, T5> Lambda<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> func)
        {
            return func;
        }

        public static Func<TResult> Memoize<TResult>(Func<TResult> func)
        {
            return func.Memoize();
        }

        public static Func<T, TResult> Memoize<T, TResult>(Func<T, TResult> func)
        {
            return func.Memoize();
        }

        public static Func<T1, T2, TResult> Memoize<T1, T2, TResult>(Func<T1, T2, TResult> func)
        {
            return func.Memoize();
        }

        public static Func<T1, T2, T3, TResult> Memoize<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> func)
        {
            return func.Memoize();
        }

        public static Func<T1, T2, T3, T4, TResult> Memoize<T1, T2, T3, T4, TResult>(Func<T1, T2, T3, T4, TResult> func)
        {
            return func.Memoize();
        }

        public static Func<T1, T2, T3, T4, T5, TResult> Memoize<T1, T2, T3, T4, T5, TResult>(Func<T1, T2, T3, T4, T5, TResult> func)
        {
            return func.Memoize();
        }

        public static Func<CacheCommand, TResult> Memoize<TResult>(Func<TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand, Func<T, TResult>> Memoize<T, TResult>(Func<T, TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand, Func<T1, T2, TResult>> Memoize<T1, T2, TResult>(Func<T1, T2, TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand, Func<T1, T2, T3, TResult>> Memoize<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand, Func<T1, T2, T3, T4, TResult>> Memoize<T1, T2, T3, T4, TResult>(Func<T1, T2, T3, T4, TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand, Func<T1, T2, T3, T4, T5, TResult>> Memoize<T1, T2, T3, T4, T5, TResult>(Func<T1, T2, T3, T4, T5, TResult> func, CacheConfig cacheConfig)
        {
            return func.Memoize(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, TResult> MemoizeWithContext<TResult, TContext>(Func<TContext, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, Func<T, TResult>> MemoizeWithContext<T, TResult, TContext>(Func<TContext, T, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, TResult>> MemoizeWithContext<T1, T2, TResult, TContext>(Func<TContext, T1, T2, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, TResult>> MemoizeWithContext<T1, T2, T3, TResult, TContext>(Func<TContext, T1, T2, T3, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, TResult>> MemoizeWithContext<T1, T2, T3, T4, TResult, TContext>(Func<TContext, T1, T2, T3, T4, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<CacheCommand<TContext>, Func<T1, T2, T3, T4, T5, TResult>> MemoizeWithContext<T1, T2, T3, T4, T5, TResult, TContext>(Func<TContext, T1, T2, T3, T4, T5, TResult> func, CacheConfig cacheConfig = null)
        {
            return func.MemoizeCtx(cacheConfig);
        }

        public static Func<T2, T1, TResult> Flip<T1, T2, TResult>(Func<T1, T2, TResult> f)
        {
            return (b, a) => f(a, b);
        }

        public static Func<T2, T1, T3, TResult> Flip<T1, T2, T3, TResult>(Func<T1, T2, T3, TResult> f)
        {
            return (b, a, c) => f(a, b, c);
        }

        public static Func<T2, T1, T3, T4, TResult> Flip<T1, T2, T3, T4, TResult>(Func<T1, T2, T3, T4, TResult> f)
        {
            return (b, a, c, d) => f(a, b, c, d);
        }

        public static Func<T2, T1, T3, T4, T5, TResult> Flip<T1, T2, T3, T4, T5, TResult>(Func<T1, T2, T3, T4, T5, TResult> f)
        {
            return (b, a, c, d, e) => f(a, b, c, d, e);
        }
    }
}
