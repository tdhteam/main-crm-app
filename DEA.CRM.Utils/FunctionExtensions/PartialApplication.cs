﻿using System;

namespace DEA.CRM.Utils.FunctionExtensions
{
    public static class PartialApplication
    {
        #region One-param Partial Application

        public static Func<TResult> Partial<T1, TResult>(this Func<T1, TResult> f, T1 a)
        {
            return () => f(a);
        }

        public static Func<T2, TResult> Partial<T1, T2, TResult>(this Func<T1, T2, TResult> f, T1 a)
        {
            return b => f(a, b);
        }

        public static Func<T2, T3, TResult> Partial<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T1 a)
        {
            return (b, c) => f(a, b, c);
        }

        public static Func<T2, T3, T4, TResult> Partial<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T1 a)
        {
            return (b, c, d) => f(a, b, c, d);
        }

        public static Func<T2, T3, T4, T5, TResult> Partial<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a)
        {
            return (b, c, d, e) => f(a, b, c, d, e);
        }

        public static Action Partial<T1>(this Action<T1> f, T1 a)
        {
            return () => f(a);
        }

        public static Action<T2> Partial<T1, T2>(this Action<T1, T2> f, T1 a)
        {
            return b => f(a, b);
        }

        public static Action<T2, T3> Partial<T1, T2, T3>(this Action<T1, T2, T3> f, T1 a)
        {
            return (b, c) => f(a, b, c);
        }

        public static Action<T2, T3, T4> Partial<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T1 a)
        {
            return (b, c, d) => f(a, b, c, d);
        }

        public static Action<T2, T3, T4, T5> Partial<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a)
        {
            return (b, c, d, e) => f(a, b, c, d, e);
        }
        #endregion


        #region Two-param Partial Application

        public static Func<TResult> Partial<T1, T2, TResult>(this Func<T1, T2, TResult> f, T1 a, T2 b)
        {
            return () => f(a, b);
        }

        public static Func<T3, TResult> Partial<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T1 a, T2 b)
        {
            return c => f(a, b, c);
        }

        public static Func<T3, T4, TResult> Partial<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T1 a, T2 b)
        {
            return (c, d) => f(a, b, c, d);
        }

        public static Func<T3, T4, T5, TResult> Partial<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a, T2 b)
        {
            return (c, d, e) => f(a, b, c, d, e);
        }

        public static Action Partial<T1, T2>(this Action<T1, T2> f, T1 a, T2 b)
        {
            return () => f(a, b);
        }

        public static Action<T3> Partial<T1, T2, T3>(this Action<T1, T2, T3> f, T1 a, T2 b)
        {
            return c => f(a, b, c);
        }

        public static Action<T3, T4> Partial<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T1 a, T2 b)
        {
            return (c, d) => f(a, b, c, d);
        }

        public static Action<T3, T4, T5> Partial<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a, T2 b)
        {
            return (c, d, e) => f(a, b, c, d, e);
        }

        #endregion


        #region Three-param Partial Application

        public static Func<TResult> Partial<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T1 a, T2 b, T3 c)
        {
            return () => f(a, b, c);
        }

        public static Func<T4, TResult> Partial<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T1 a, T2 b, T3 c)
        {
            return d => f(a, b, c, d);
        }

        public static Func<T4, T5, TResult> Partial<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a, T2 b, T3 c)
        {
            return (d, e) => f(a, b, c, d, e);
        }

        public static Action Partial<T1, T2, T3>(this Action<T1, T2, T3> f, T1 a, T2 b, T3 c)
        {
            return () => f(a, b, c);
        }

        public static Action<T4> Partial<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T1 a, T2 b, T3 c)
        {
            return d => f(a, b, c, d);
        }

        public static Action<T4, T5> Partial<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a, T2 b, T3 c)
        {
            return (d, e) => f(a, b, c, d, e);
        }

        #endregion


        #region Four-param Partial Application
        public static Func<TResult> Partial<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T1 a, T2 b, T3 c, T4 d)
        {
            return () => f(a, b, c, d);
        }

        public static Func<T5, TResult> Partial<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a, T2 b, T3 c, T4 d)
        {
            return e => f(a, b, c, d, e);
        }

        public static Action Partial<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T1 a, T2 b, T3 c, T4 d)
        {
            return () => f(a, b, c, d);
        }

        public static Action<T5> Partial<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a, T2 b, T3 c, T4 d)
        {
            return e => f(a, b, c, d, e);
        }

        #endregion


        #region Five-param Partial Application

        public static Func<TResult> Partial<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a, T2 b, T3 c, T4 d, T5 e)
        {
            return () => f(a, b, c, d, e);
        }

        public static Action Partial<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a, T2 b, T3 c, T4 d, T5 e)
        {
            return () => f(a, b, c, d, e);
        }

        #endregion



        #region One-param Right-Partial Application

        public static Func<TResult> PartialRight<T1, TResult>(this Func<T1, TResult> f, T1 a)
        {
            return () => f(a);
        }

        public static Func<T1, TResult> PartialRight<T1, T2, TResult>(this Func<T1, T2, TResult> f, T2 b)
        {
            return a => f(a, b);
        }

        public static Func<T1, T2, TResult> PartialRight<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T3 c)
        {
            return (a, b) => f(a, b, c);
        }

        public static Func<T1, T2, T3, TResult> PartialRight<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T4 d)
        {
            return (a, b, c) => f(a, b, c, d);
        }

        public static Func<T1, T2, T3, T4, TResult> PartialRight<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T5 e)
        {
            return (a, b, c, d) => f(a, b, c, d, e);
        }

        public static Action PartialRight<T1>(this Action<T1> f, T1 a)
        {
            return () => f(a);
        }

        public static Action<T1> PartialRight<T1, T2>(this Action<T1, T2> f, T2 b)
        {
            return a => f(a, b);
        }

        public static Action<T1, T2> PartialRight<T1, T2, T3>(this Action<T1, T2, T3> f, T3 c)
        {
            return (a, b) => f(a, b, c);
        }

        public static Action<T1, T2, T3> PartialRight<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T4 d)
        {
            return (a, b, c) => f(a, b, c, d);
        }

        public static Action<T1, T2, T3, T4> PartialRight<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T5 e)
        {
            return (a, b, c, d) => f(a, b, c, d, e);
        }
        #endregion


        #region Two-param Right-Partial Application

        public static Func<TResult> PartialRight<T1, T2, TResult>(this Func<T1, T2, TResult> f, T1 a, T2 b)
        {
            return () => f(a, b);
        }

        public static Func<T1, TResult> PartialRight<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T2 b, T3 c)
        {
            return a => f(a, b, c);
        }

        public static Func<T1, T2, TResult> PartialRight<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T3 c, T4 d)
        {
            return (a, b) => f(a, b, c, d);
        }

        public static Func<T1, T2, T3, TResult> PartialRight<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T4 d, T5 e)
        {
            return (a, b, c) => f(a, b, c, d, e);
        }

        public static Action PartialRight<T1, T2>(this Action<T1, T2> f, T1 a, T2 b)
        {
            return () => f(a, b);
        }

        public static Action<T1> PartialRight<T1, T2, T3>(this Action<T1, T2, T3> f, T2 b, T3 c)
        {
            return a => f(a, b, c);
        }

        public static Action<T1, T2> PartialRight<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T3 c, T4 d)
        {
            return (a, b) => f(a, b, c, d);
        }

        public static Action<T1, T2, T3> PartialRight<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T4 d, T5 e)
        {
            return (a, b, c) => f(a, b, c, d, e);
        }

        #endregion


        #region Three-param Right-Partial Application

        public static Func<TResult> PartialRight<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f, T1 a, T2 b, T3 c)
        {
            return () => f(a, b, c);
        }

        public static Func<T1, TResult> PartialRight<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T2 b, T3 c, T4 d)
        {
            return a => f(a, b, c, d);
        }

        public static Func<T1, T2, TResult> PartialRight<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T3 c, T4 d, T5 e)
        {
            return (a, b) => f(a, b, c, d, e);
        }

        public static Action PartialRight<T1, T2, T3>(this Action<T1, T2, T3> f, T1 a, T2 b, T3 c)
        {
            return () => f(a, b, c);
        }

        public static Action<T1> PartialRight<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T2 b, T3 c, T4 d)
        {
            return a => f(a, b, c, d);
        }

        public static Action<T1, T2> PartialRight<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T3 c, T4 d, T5 e)
        {
            return (a, b) => f(a, b, c, d, e);
        }

        #endregion


        #region Four-param Right-Partial Application
        public static Func<TResult> PartialRight<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f, T1 a, T2 b, T3 c, T4 d)
        {
            return () => f(a, b, c, d);
        }

        public static Func<T1, TResult> PartialRight<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T2 b, T3 c, T4 d, T5 e)
        {
            return a => f(a, b, c, d, e);
        }

        public static Action PartialRight<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f, T1 a, T2 b, T3 c, T4 d)
        {
            return () => f(a, b, c, d);
        }

        public static Action<T1> PartialRight<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T2 b, T3 c, T4 d, T5 e)
        {
            return a => f(a, b, c, d, e);
        }

        #endregion


        #region Five-param Right-Partial Application

        public static Func<TResult> PartialRight<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f, T1 a, T2 b, T3 c, T4 d, T5 e)
        {
            return () => f(a, b, c, d, e);
        }

        public static Action PartialRight<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f, T1 a, T2 b, T3 c, T4 d, T5 e)
        {
            return () => f(a, b, c, d, e);
        }

        #endregion

    }
}
