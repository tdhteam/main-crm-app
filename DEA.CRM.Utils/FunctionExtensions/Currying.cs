﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DEA.CRM.Utils.FunctionExtensions
{
    public static class Currying
    {
        public static Func<T1, Func<T2, TResult>> Curry<T1, T2, TResult>(this Func<T1, T2, TResult> f)
        {
            return a => b => f(a, b);
        }

        public static Func<T1, Func<T2, Func<T3, TResult>>> Curry<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> f)
        {
            return a => b => c => f(a, b, c);
        }

        public static Func<T1, Func<T2, Func<T3, Func<T4, TResult>>>> Curry<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f)
        {
            return a => b => c => d => f(a, b, c, d);
        }

        public static Func<T1, Func<T2, Func<T3, Func<T4, Func<T5, TResult>>>>> Curry<T1, T2, T3, T4, T5, TResult>(this Func<T1, T2, T3, T4, T5, TResult> f)
        {
            return a => b => c => d => e => f(a, b, c, d, e);
        }

        public static Func<T1, Action<T2>> Curry<T1, T2>(this Action<T1, T2> f)
        {
            return a => b => f(a, b);
        }

        public static Func<T1, Func<T2, Action<T3>>> Curry<T1, T2, T3>(this Action<T1, T2, T3> f)
        {
            return a => b => c => f(a, b, c);
        }

        public static Func<T1, Func<T2, Func<T3, Action<T4>>>> Curry<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> f)
        {
            return a => b => c => d => f(a, b, c, d);
        }

        public static Func<T1, Func<T2, Func<T3, Func<T4, Action<T5>>>>> Curry<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> f)
        {
            return a => b => c => d => e => f(a, b, c, d, e);
        }
    }
}
