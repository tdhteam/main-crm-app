﻿using System.Collections.Generic;

namespace DEA.CRM.Utils
{
    /// <remarks>
    /// This implementation is not thread-safe by design.
    /// Client code must enforce its own locking mechanism if thread safety is required
    /// </remarks>
    public class LRUCache<TKey, TValue>
    {
        private readonly int _capacity;

        private readonly Dictionary<TKey, LinkedListNode<LRUCacheItem>> _cacheMap;

        private readonly LinkedList<LRUCacheItem> _lruList;

        public LRUCache(int capacity)
        {
            _capacity = capacity;
            _cacheMap = new Dictionary<TKey, LinkedListNode<LRUCacheItem>>();
            _lruList = new LinkedList<LRUCacheItem>();
        }

        public TValue this[TKey key]
        {
            get => Get(key);
            set
            {
                Remove(key);
                Add(key, value);
            }
        }

        public bool Remove(TKey key)
        {
            if (!_cacheMap.TryGetValue(key, out LinkedListNode<LRUCacheItem> node)) return false;
            _lruList.Remove(node);
            _cacheMap.Remove(key);
            return true;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (_cacheMap.TryGetValue(key, out LinkedListNode<LRUCacheItem> node))
            {
                value = node.Value.Value;
                _lruList.Remove(node);
                _lruList.AddLast(node);
                return true;
            }

            value = default(TValue);
            return false;
        }

        public TValue Get(TKey key)
        {
            TryGetValue(key, out TValue value);
            return value;
        }

        public void Add(TKey key, TValue val)
        {
            if (_cacheMap.Count >= _capacity)
            {
                RemoveFirst();
            }
            var cacheItem = new LRUCacheItem(key, val);
            var node = new LinkedListNode<LRUCacheItem>(cacheItem);
            _lruList.AddLast(node);
            _cacheMap.Add(key, node);
        }

        public void Clear()
        {
            _cacheMap.Clear();
            _lruList.Clear();
        }

        private void RemoveFirst()
        {
            // Remove from LRUPriority
            var node = _lruList.First;
            _lruList.RemoveFirst();

            // Remove from cache
            _cacheMap.Remove(node.Value.Key);
        }

        private class LRUCacheItem
        {
            public LRUCacheItem(TKey k, TValue v)
            {
                Key = k;
                Value = v;
            }

            public readonly TKey Key;
            public readonly TValue Value;
        }
    }
}