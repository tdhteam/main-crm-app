﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace DEA.CRM.Utils
{
    public class ExpressionUtils
    {
        public static MemberExpression GetMemberExpression<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            // See if there's a conversion to Object type or it's actually a member that returns Object
            var body = expression.Body is UnaryExpression unaryExp ? unaryExp.Operand : expression.Body;
            return body is MemberExpression memberExp
                ? memberExp
                : throw new InvalidOperationException("Only property access is allowed.");
        }

        public static Accessor<T, TValue> GetAccessor<T, TValue>(Expression<Func<T, TValue>> expression)
        {
            var memberExpression = GetMemberExpression(expression);
            var valueParam = Expression.Parameter(typeof(TValue));
            var assign = Expression.Lambda<Action<T, TValue>>
                ( Expression.Assign(memberExpression, valueParam)
                , expression.Parameters.First()
                , valueParam
                );
            return new Accessor<T, TValue>(expression.Compile(), assign.Compile());
        }

        public static Func<T, T> BuildMutator<T, TValue>(Func<TValue, TValue> transformFn, params Expression<Func<T, TValue>>[] selectorExpressions)
        {
            var accessors = selectorExpressions.Select(GetAccessor).ToArray();
            return x =>
            {
                foreach (var accessor in accessors)
                {
                    accessor.Set(x, transformFn(accessor.Get(x)));
                }
                return x;
            };
        }
    }

    public class Accessor<T, Prop>
    {
        public Func<T, Prop> Get { get; }
        public Action<T, Prop> Set { get; }

        public Accessor(Func<T, Prop> getter, Action<T, Prop> setter)
        {
            Get = getter;
            Set = setter;
        }
    }
}
