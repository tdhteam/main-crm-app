﻿using System;
using System.Threading.Tasks;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    public static class MaybeExtensions
    {
        public static TResult With<T, TResult>(this T input, Func<T, TResult> evaluator)
            where T: class
            where TResult: class
        {
            return input == null ? null : evaluator(input);
        }

        public static T If<T>(this T input, Func<T, bool> predicate) where T: class
        {
            if (input == null) return null;
            return predicate(input) ? input : null;
        }

        public static T Do<T>(this T input, Action<T> action) where T: class
        {
            if (input == null) return null;
            action(input);
            return input;
        }

        public static Maybe<T> AsMaybe<T>(this T value)
        {
            return value;
        }

        public static Maybe<T> AsMaybe<T>(this T? value) where T: struct
        {
            return value ?? Maybe<T>.Nothing;
        }

        public static Maybe<string> AsMaybe(this string value)
        {
            return string.IsNullOrEmpty(value) ? Maybe<string>.Nothing : value;
        }

        public static Maybe<U> Select<T, U>(this Maybe<T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static Maybe<U> SelectMany<T, U>(this Maybe<T> self, Func<T, Maybe<U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            return self.Bind(bind);
        }

        public static Maybe<V> SelectMany<T, U, V>(this Maybe<T> self, Func<T, Maybe<U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }

        public static Maybe<T> Where<T>(this Maybe<T> self, Func<T, bool> predicate)
        {
            if (!self.HasValue) return self;
            return predicate(self.Value) ? self : Maybe<T>.Nothing;
        }

        public static Maybe<T> OnNoValueRaiseError<T>(this Maybe<T> mb, string message)
        {
            if (!mb.HasValue) throw new ApplicationException(message);
            return mb;
        }

        public static Maybe<T> OnNoValueRaiseError<T, TException>(this Maybe<T> mb, Func<TException> exceptionFactory) where TException : Exception
        {
            if (!mb.HasValue) throw exceptionFactory();
            return mb;
        }

        public static T Unwrap<T>(this Maybe<T> mb, T defaultValue = default(T))
        {
            return mb.HasValue ? mb.Value : defaultValue;
        }

        public static T Unwrap<T>(this Maybe<T> mb, Func<T> defaultValueFactory)
        {
            return mb.HasValue ? mb.Value : defaultValueFactory();
        }

        public static U? MapAsNullable<T, U>(this Maybe<T> mb, Func<T, U> map) where U : struct
        {
            return mb.HasValue ? map(mb.Value) : (U?)null;
        }

        public static Maybe<bool> AsJustTrue(this bool target)
        {
            return target ? true : Maybe<bool>.Nothing;
        }

        public static Maybe<bool> AsJustFalse(this bool target)
        {
            return target ? Maybe<bool>.Nothing : false;
        }

        public static Maybe<T> AsJustTrue<T>(this bool target, Func<T> ifTrue)
        {
            return target ? ifTrue() : Maybe<T>.Nothing;
        }

        public static Maybe<T> AsJustFalse<T>(this bool target, Func<T> ifFalse)
        {
            return target ? Maybe<T>.Nothing : ifFalse();
        }
    }

    public struct Maybe<T> : IEquatable<Maybe<T>>
    {
        private readonly T _value;

        public static readonly Maybe<T> Nothing = new Maybe<T>();

        public T Value
        {
            get
            {
                if (!HasValue) throw new InvalidOperationException("Cannot access Value of Nothing");
                return _value;
            }
        }

        public bool HasValue { get; }

        private Maybe(T value)
        {
            _value = value;
            HasValue = true;
        }

        public static Maybe<T> Of(T value)
        {
            return value != null ? new Maybe<T>(value) : Nothing;
        }

        public Maybe<U> Map<U>(Func<T, U> map)
        {
            return HasValue ? map(Value) : Maybe<U>.Nothing;
        }

        public Maybe<U> Bind<U>(Func<T, Maybe<U>> bind)
        {
            return HasValue ? bind(Value) : Maybe<U>.Nothing;
        }

        public async Task<Maybe<U>> MapAsync<U>(Func<T, Task<U>> map)
        {
            return HasValue ? await map(Value).ConfigureAwait(false) : Maybe<U>.Nothing;
        }

        public Task<Maybe<U>> BindAsync<U>(Func<T, Task<Maybe<U>>> bind)
        {
            return HasValue ? bind(Value) : Task.FromResult(Maybe<U>.Nothing);
        }

        public Maybe<T> If(Func<T, bool> predicate)
        {
            if (!HasValue) return Nothing;
            return predicate(Value) ? this : Nothing;
        }

        public Maybe<T> Unless(Func<T, bool> predicate)
        {
            if (!HasValue) return Nothing;
            return predicate(Value) ? Nothing : this;
        }

        public Maybe<T> Do(Action<T> action)
        {
            if (HasValue) action(Value);
            return this;
        }

        public Maybe<T> Or(Func<Maybe<T>> other)
        {
            return HasValue ? this : other();
        }

        public static implicit operator Maybe<T>(T value) => Of(value);

        public static bool operator ==(T value, Maybe<T> maybe)
        {
            return maybe.HasValue && maybe.Value.Equals(value);
        }

        public static bool operator !=(T value, Maybe<T> maybe)
        {
            return !(value == maybe);
        }

        public static bool operator ==(Maybe<T> maybe, T value)
        {
            return maybe.HasValue && maybe.Value.Equals(value);
        }

        public static bool operator !=(Maybe<T> maybe, T value)
        {
            return !(maybe == value);
        }

        public static bool operator ==(Maybe<T> first, Maybe<T> second)
        {
            return first.Equals(second);
        }

        public static bool operator !=(Maybe<T> first, Maybe<T> second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Maybe<T>)) return false;

            var other = (Maybe<T>) obj;
            return Equals(other);
        }

        public bool Equals(Maybe<T> other)
        {
            if (!HasValue && !other.HasValue) return true;
            if (!HasValue || !other.HasValue) return false;

            return _value.Equals(other._value);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return HasValue ? _value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return HasValue ? Value.ToString() : "Nothing";
        }
    }
}