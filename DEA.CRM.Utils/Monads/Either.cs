﻿using System;
using System.Collections.Generic;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    public static class EitherExtensions
    {
        public static Either<L, R> AsLeft<L, R>(this L left)
        {
            return left;
        }

        public static Either<L, R> AsRight<L, R>(this R right)
        {
            return right;
        }

        public static Either<L, Unit> AsUnit<L, R>(this Either<L, R> self)
        {
            return self.Map(_ => Unit.Default);
        }

        public static Either<L, R> OnLeft<L, R>(this Either<L, R> self, Action<L> onLeft)
        {
            if (self.IsLeft) onLeft(self.LeftValue);
            return self;
        }

        public static Either<L, R> OnRight<L, R>(this Either<L, R> self, Action<R> onRight)
        {
            if (self.IsRight) onRight(self.RightValue);
            return self;
        }

        public static Either<L, R1> Select<L, R, R1>(this Either<L, R> self, Func<R, R1> map)
        {
            return self.Map(map);
        }

        public static Either<L, R1> SelectMany<L, R, R1>(this Either<L, R> self, Func<R, Either<L, R1>> bind)
        {
            return self.Bind(bind);
        }

        public static Either<L, R2> SelectMany<L, R, R1, R2>(this Either<L, R> self, Func<R, Either<L, R1>> bind, Func<R, R1, R2> project)
        {
            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }
    }

    public struct Either<L, R> : IEquatable<Either<L, R>>
    {
        private readonly L _left;
        private readonly R _right;

        public bool IsLeft { get; }
        public bool IsRight => !IsLeft;

        private Either(bool isLeft, L left, R right)
        {
            IsLeft = isLeft;
            _left = left;
            _right = right;
        }

        public static implicit operator Either<L, R>(R right) => Right(right);
        public static implicit operator Either<L, R>(L left) => Left(left);

        public static Either<L, R> Left(L left)
        {
            return new Either<L, R>(true, left, default(R));
        }

        public static Either<L, R> Right(R right)
        {
            return new Either<L, R>(false, default(L), right);
        }

        public static implicit operator Either<L, Unit>(Either<L, R> res) => res.AsUnit();

        public Either<L, R1> Map<R1>(Func<R, R1> map)
        {
            return IsLeft ? Either<L, R1>.Left(LeftValue) : map(RightValue);
        }

        public Either<L, R1> Bind<R1>(Func<R, Either<L, R1>> bind)
        {
            return IsLeft ? Either<L, R1>.Left(LeftValue) : bind(RightValue);
        }

        public L LeftValue
        {
            get
            {
                if (!IsLeft) throw new InvalidOperationException("Left value is not accessible.");
                return _left;
            }
        }

        public R RightValue
        {
            get
            {
                if (!IsRight) throw new InvalidOperationException("Right value is not accessible.");
                return _right;
            }
        }

        public R Unwrap(Func<L, R> valueFactory)
        {
            return IsRight ? RightValue : valueFactory(LeftValue);
        }

        public bool Equals(Either<L, R> other)
        {
            return
                IsLeft && other.IsLeft && EqualityComparer<L>.Default.Equals(LeftValue, other.LeftValue)
                || IsRight && other.IsRight && EqualityComparer<R>.Default.Equals(RightValue, other.RightValue);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (!(obj is Either<L, R> other)) return false;
            return Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked { return (EqualityComparer<L>.Default.GetHashCode(_left) * 397) ^ EqualityComparer<R>.Default.GetHashCode(_right); }
        }
    }
}
