using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    public static class RecoverableResultExtensions
    {
        public static RecoverableResult<U> Select<T, U>(this RecoverableResult<T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static RecoverableResult<U> SelectMany<T, U>(this RecoverableResult<T> self, Func<T, RecoverableResult<U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            return self.Bind(bind);
        }

        public static RecoverableResult<V> SelectMany<T, U, V>(this RecoverableResult<T> self, Func<T, RecoverableResult<U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }

        public static RecoverableResult<T> OnContinuable<T>(this RecoverableResult<T> self, Action<T> action)
        {
            if (!self.IsCritical) action(self.Value);
            return self;
        }

        public static RecoverableResult<T> OnRecoverable<T>(this RecoverableResult<T> self, Action<(string Error, T Value)> action)
        {
            if (self.IsRecoverable) action((self.ErrorMessage, self.Value));
            return self;
        }

        public static RecoverableResult<T> OnSuccess<T>(this RecoverableResult<T> self, Action<T> action)
        {
            if (self.IsSuccess) action(self.Value);
            return self;
        }

        public static RecoverableResult<T> OnCritical<T>(this RecoverableResult<T> self, Action<IEnumerable<string>> action)
        {
            if (self.IsCritical) action(self.Errors);
            return self;
        }

        public static RecoverableResult<T> OnCriticalRaiseError<T, TException>(this RecoverableResult<T> self, Func<string, TException> exceptionFactory) where TException: Exception
        {
            if (self.IsCritical) throw exceptionFactory(string.Join("\n", self.Errors));
            return self;
        }

        public static RecoverableResult<Unit> AsUnit<T>(this RecoverableResult<T> self)
        {
            return self.Map(_ => Unit.Default);
        }

        public static RecoverableResult<T> Finally<T>(this RecoverableResult<T> self, Action action)
        {
            action();
            return self;
        }
    }

    public static class RecoverableResult
    {
        public static RecoverableResult<Unit> Success()
        {
            return RecoverableResult<Unit>.Success(Unit.Default);
        }

        public static RecoverableResult<Unit> Recoverable(params string[] messages)
        {
            return RecoverableResult<Unit>.Recoverable(Unit.Default, messages);
        }

        public static RecoverableResult<Unit> Critical(params string[] messages)
        {
            return RecoverableResult<Unit>.Critical(messages);
        }

        public static RecoverableResult<T> Success<T>(T value)
        {
            return RecoverableResult<T>.Success(value);
        }

        public static RecoverableResult<T> Recoverable<T>(T value, params string[] messages)
        {
            return RecoverableResult<T>.Recoverable(value, messages);
        }

        public static RecoverableResult<T> Critical<T>(params string[] messages)
        {
            return RecoverableResult<T>.Critical(messages);
        }
    }


    public struct RecoverableResult<T>
    {
        private readonly T _value;

        public T Value
        {
            get
            {
                if (IsCritical) throw new InvalidOperationException(string.Join("; ", Errors));
                return _value;
            }
        }

        public bool IsSuccess { get; }
        public bool IsRecoverable { get; }
        public bool IsCritical { get; }

        public string ErrorMessage => string.Join(";\n", Errors.Distinct());

        public IEnumerable<string> Errors { get; }

        public static implicit operator RecoverableResult<Unit>(RecoverableResult<T> res) => res.AsUnit();

        public static RecoverableResult<T> Critical(params string[] messages)
        {
            return new RecoverableResult<T>(default(T), false, false, messages);
        }

        public static RecoverableResult<T> Recoverable(T fallbackValue, params string[] messages)
        {
            return new RecoverableResult<T>(fallbackValue, false, true, messages);
        }


        public static RecoverableResult<T> Success(T value)
        {
            return new RecoverableResult<T>(value, true, false, null);
        }

        public RecoverableResult<U> Map<U>(Func<T, U> map)
        {
            if (IsCritical) return RecoverableResult<U>.Critical(Errors.ToArray());
            return IsSuccess
                ? RecoverableResult<U>.Success(map(Value))
                : RecoverableResult<U>.Recoverable(map(Value), Errors.ToArray());
        }

        public RecoverableResult<U> Bind<U>(Func<T, RecoverableResult<U>> bind)
        {
            if (IsCritical) return RecoverableResult<U>.Critical(Errors.ToArray());
            if (IsSuccess) return bind(Value);

            var bound = bind(Value);
            return bound.IsCritical
                ? RecoverableResult<U>.Critical(Errors.Concat(bound.Errors).ToArray())
                : RecoverableResult<U>.Recoverable(bound.Value, Errors.Concat(bound.Errors).ToArray());
        }

        private RecoverableResult(T value, bool isSuccess, bool isRecoverable, IEnumerable<string> errors)
        {
            _value = value;
            IsSuccess = isSuccess;
            IsRecoverable = isRecoverable;
            IsCritical = !IsSuccess && !IsRecoverable;
            Errors = errors ?? new string[0];
        }
    }
}
