﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DEA.CRM.Utils.Extensions;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    public static class ResultExtensions
    {
        public static Result<T> AsResult<T>(this T value, string errorMessage) where T : class
        {
            return value == null ? Result<T>.Fail(errorMessage) : Result.Success(value);
        }

        public static Result<T> AsResult<T>(this T? nullable, string errorMessage) where T : struct
        {
            return nullable.HasValue ? Result.Success(nullable.Value) : Result<T>.Fail(errorMessage);
        }

        public static Result<Unit> AsSuccessIfTrue(this bool value, string errorMessage)
        {
            return value ? Result.Success() : Result.Fail(errorMessage);
        }

        public static Result<T> AsSuccessIfTrue<T>(this bool value, Func<T> onSuccess, string errorMessage)
        {
            return value ? Result.Success(onSuccess()) : Result.Fail<T>(errorMessage);
        }

        public static Result<string> AsResult(this string stringValue, string errorMessage)
        {
            return string.IsNullOrEmpty(stringValue)
                ? Result<string>.Fail(errorMessage)
                : Result.Success(stringValue);
        }

        public static Result<T> OnFailure<T>(this Result<T> result, Action<string> action)
        {
            if (result.IsFailure) action(result.Error);
            return result;
        }

        public static Result<T> OnFailureRaiseError<T, TException>(this Result<T> input, Func<string, TException> exceptionFactory)
            where TException : Exception
        {
            if (input.IsFailure) throw exceptionFactory(input.Error);
            return input;
        }

        public static Result<T> Ensure<T>(this Result<T> result, Func<T, bool> predicate, string errorMessage)
        {
            if (result.IsFailure) return result;

            return predicate(result.Value) ? result : Result<T>.Fail(errorMessage);
        }

        public static Result<T> Ensure<T>(this Result<T> result, Func<T, bool> predicate, Func<T, string> errorMessage)
        {
            if (result.IsFailure) return result;

            return predicate(result.Value) ? result : Result<T>.Fail(errorMessage(result.Value));
        }

        public static Result<T> OnSuccess<T>(this Result<T> result, Action<T> action)
        {
            if (result.IsSuccess) action(result.Value);
            return result;
        }

        public static Result<Unit> AsUnit<T>(this Result<T> result)
        {
            return result.Map(_ => Unit.Default);
        }

        public static U OnBoth<T, U>(this Result<T> result, Func<Result<T>, U> func)
        {
            return func(result);
        }


        public static Result<U> Select<T, U>(this Result<T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static Result<U> SelectMany<T, U>(this Result<T> self, Func<T, Result<U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));

            return self.Bind(bind);
        }

        public static Result<V> SelectMany<T, U, V>(this Result<T> self, Func<T, Result<U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));
            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }
    }


    public static class AsyncResultExtensions
    {
        public static async Task<Result<T>> AsAsyncResult<T>(this Task<T> task, string errorMessage)
        {
            var value = await task.ConfigureAwait(false);
            return value != null
                ? Result.Success(value)
                : Result<T>.Fail(errorMessage);
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Result<T> result, Func<T, Task> action)
        {
            if (result.IsSuccess) await action(result.Value).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Result<T> result, Func<T, Task<U>> map)
        {
            return result.IsFailure
                ? Result<U>.Fail(result.Error)
                : Result.Success(await map(result.Value).ConfigureAwait(false));
        }

        /// <summary>
        /// Alias of Bind
        /// </summary>
        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Result<T> result, Func<T, Task<Result<U>>> bind)
        {
            return result.IsFailure
                ? Result<U>.Fail(result.Error)
                : await bind(result.Value).ConfigureAwait(false);
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Task<Result<T>> taskResult, Action<T> action)
        {
            var result = await taskResult.ConfigureAwait(false);
            if (result.IsSuccess) action(result.Value);
            return result;
        }

        public static async Task<Result<T>> OnSuccessAsync<T>(this Task<Result<T>> taskResult, Func<T, Task> action)
        {
            var result = await taskResult.ConfigureAwait(false);
            if (result.IsSuccess) await action(result.Value).ConfigureAwait(false);
            return result;
        }

        public static Task<Result<Unit>> AsUnitAsync<T>(this Task<Result<T>> taskResult)
        {
            return taskResult.OnSuccessAsync(_ => Unit.Default);
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Task<Result<T>> taskResult, Func<T, U> map)
        {
            var result = await taskResult.ConfigureAwait(false);
            if (result.IsFailure) return Result<U>.Fail(result.Error);
            var kResult = map(result.Value);
            return Result.Success(kResult);
        }

        /// <summary>
        /// Alias of Map
        /// </summary>
        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Task<Result<T>> taskResult, Func<T, Task<U>> map)
        {
            var result = await taskResult.ConfigureAwait(false);
            if (result.IsFailure) return Result<U>.Fail(result.Error);
            var kResult = await map(result.Value).ConfigureAwait(false);
            return Result.Success(kResult);
        }

        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Task<Result<T>> task, Func<T, Result<U>> bind)
        {
            var result = await task.ConfigureAwait(false);
            return result.IsFailure
                ? Result<U>.Fail(result.Error)
                : bind(result.Value);
        }

        public static async Task<Result<U>> OnSuccessAsync<T, U>(this Task<Result<T>> task, Func<T, Task<Result<U>>> bind)
        {
            var result = await task.ConfigureAwait(false);
            if (result.IsFailure) return Result<U>.Fail(result.Error);
            return await bind(result.Value).ConfigureAwait(false);
        }

        public static async Task<Result<T>> OnFailureRaiseErrorAsync<T, TException>(this Task<Result<T>> task, Func<string, TException> exceptionFactory)
            where TException : Exception
        {
            var result = await task.ConfigureAwait(false);
            if (result.IsFailure) throw exceptionFactory(result.Error);
            return result;
        }

        public static async Task<T> UnwrapAsync<T>(this Task<Result<T>> task)
        {
            try
            {
                var result = await task.ConfigureAwait(false);
                return result.Value;
            }
            catch (Exception e)
            {
                e.ReThrow();
            }
            return default(T);
        }
    }

    /// <summary>
    /// aka Either
    /// </summary>
    public static class Result
    {
        public static Result<T> Fail<T>(string message)
        {
            return Result<T>.Fail(message);
        }

        public static Result<Unit> Fail(string message)
        {
            return Result<Unit>.Fail(message);
        }

        public static Result<Unit> Fail(string messagePrefix, string message)
        {
            return Result<Unit>.Fail($"{messagePrefix}: {message}");
        }

        public static Result<T> Fail<T>(string messagePrefix, string message)
        {
            return Result<T>.Fail($"{messagePrefix}: {message}");
        }

        public static Result<Unit> Success()
        {
            return Result<Unit>.Success(Unit.Default);
        }

        public static Result<T> Success<T>(T value)
        {
            return Result<T>.Success(value);
        }
    }

    /// <summary>
    /// aka Either
    /// </summary>
    public struct Result<T> : IEquatable<Result<T>>
    {
        private readonly T _value;

        public bool IsSuccess { get; }
        public string Error { get; }
        public bool IsFailure => !IsSuccess;

        public T Value
        {
            get
            {
                if (!IsSuccess) throw new InvalidOperationException(Error);
                return _value;
            }
        }

        public static implicit operator Result<Unit>(Result<T> res) => res.AsUnit();

        public static Result<T> Fail(string message)
        {
            return new Result<T>(default(T), false, message);
        }

        public static Result<T> Success(T value)
        {
            return new Result<T>(value, true, string.Empty);
        }

        public Result<U> Map<U>(Func<T, U> func)
        {
            return IsFailure
                ? Result<U>.Fail(Error)
                : Result<U>.Success(func(Value));
        }

        public Result<U> Bind<U>(Func<T, Result<U>> func)
        {
            return IsFailure
                ? Result<U>.Fail(Error)
                : func(Value);
        }

        public T Unwrap(T defaultValue)
        {
            return IsSuccess ? _value : defaultValue;
        }

        public T Unwrap(Func<string, T> valueFactory)
        {
            return IsSuccess ? _value : valueFactory(Error);
        }

        private Result(T value, bool isSuccess, string error)
        {
            _value = value;
            Error = error;
            IsSuccess = isSuccess;
        }

        public override bool Equals(object obj)
        {
            return obj is Result<T> res && Equals(res);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<T>.Default.GetHashCode(_value);
        }

        public bool Equals(Result<T> other)
        {
            if (IsFailure && other.IsFailure && Error.Equals(other.Error)) return true;
            if (IsFailure || other.IsFailure) return false;

            return _value.Equals(other._value);
        }
    }
}
