﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DEA.CRM.Utils.Monads
{
    public static class ListExtensions
    {
        public static IEnumerable<T> DoEach<T>(this IEnumerable<T> self, Action<T> action)
        {
            foreach (var elem in self)
            {
                action(elem);
                yield return elem;
            }
        }

        public static TLst DoEach<T, TLst>(this TLst self, Action<T> action) where TLst: ICollection<T>
        {
            foreach (var elem in self)
            {
                action(elem);
            }
            return self;
        }

        public static IEnumerable<U> Bind<T, U>(this IEnumerable<T> self, Func<T, IEnumerable<U>> bind)
        {
            return self.SelectMany(bind);
        }

        public static IEnumerable<T> AsLst<T>(this T self)
        {
            yield return self;
        }

        public static Maybe<T> FirstOrNothing<T>(this IEnumerable<T> self, Func<T, bool> predicate = null)
        {
            predicate = predicate ?? Functions.Lambda((T x) => true);
            foreach (var element in self)
            {
                if (predicate(element)) return element;
            }
            return Maybe<T>.Nothing;
        }

        public static Maybe<T> FirstOrNothing<T>(this IQueryable<T> self, Expression<Func<T, bool>> predicate = null)
        {
            var query = predicate == null ? self : self.Where(predicate);
            var result = query.Take(1).ToArray();
            return result.Any() ? result.First() : Maybe<T>.Nothing;
        }

        public static Result<T> FirstOrFailure<T>(this IEnumerable<T> self, string errorMessage, Func<T, bool> predicate = null)
        {
            predicate = predicate ?? Functions.Lambda((T x) => true);
            foreach (var element in self)
            {
                if (predicate(element)) return Result.Success(element);
            }
            return Result.Fail<T>(errorMessage);
        }

        public static Result<T> FirstOrFailure<T>(this IQueryable<T> self, string errorMessage, Expression<Func<T, bool>> predicate = null)
        {
            var query = predicate == null ? self : self.Where(predicate);
            var result = query.Take(1).ToArray();
            return result.Any() ? Result.Success(result.First()) : Result.Fail<T>(errorMessage);
        }
    }
}
