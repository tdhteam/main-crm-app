﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    public static class TaskM
    {
        private static readonly TaskFactory MyTaskFactory =
            new TaskFactory
                ( CancellationToken.None
                , TaskCreationOptions.None
                , TaskContinuationOptions.None
                , TaskScheduler.Default
                );

        /// <summary>
        /// Copied from Microsoft.AspNet.Identity.AsyncHelper
        /// </summary>
        public static TResult RunSync<TResult>(Func<Task<TResult>> func)
        {
            var cultureUi = CultureInfo.CurrentUICulture;
            var culture = CultureInfo.CurrentCulture;
            return MyTaskFactory.StartNew(() =>
            {
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = cultureUi;
                return func();
            }).Unwrap().GetAwaiter().GetResult();
        }

        public static void RunSync(Func<Task> func)
        {
            var cultureUi = CultureInfo.CurrentUICulture;
            var culture = CultureInfo.CurrentCulture;
            MyTaskFactory.StartNew(() =>
            {
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = cultureUi;
                return func();
            }).Unwrap().GetAwaiter().GetResult();
        }

        public static B[] ForkJoin<A, B>(this IEnumerable<A> sequence, Func<A, Task<B>> fork)
        {
            return ForkJoin(sequence, fork, Functions.Identity);
        }

        public static TResult ForkJoin<A, B, TResult>(this IEnumerable<A> sequence, Func<A, Task<B>> fork, Func<B[], TResult> project)
        {
            return RunSync(() => Join(sequence.Select(fork), project));
        }

        public static Task<TResult> Join<T, TResult>(IEnumerable<Task<T>> tasks, Func<T[], TResult> project)
        {
            return Task.WhenAll(tasks).Map(project);
        }

        public static Task<TResult> Join<T1, T2, TResult>(Task<T1> task1, Task<T2> task2, Func<T1, T2, TResult> project)
        {
            return Task.WhenAll(task1.Wrap(), task2.Wrap()).Map(arr => project((T1) arr[0], (T2) arr[1]));
        }

        public static Task<TResult> Join<T1, T2, T3, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Func<T1, T2, T3, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    ));
        }

        public static Task<TResult> Join<T1, T2, T3, T4, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Task<T4> task4
            , Func<T1, T2, T3, T4, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    , task4.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    , (T4) arr[3]
                    ));
        }

        public static Task<TResult> Join<T1, T2, T3, T4, T5, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Task<T4> task4
            , Task<T5> task5
            , Func<T1, T2, T3, T4, T5, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    , task4.Wrap()
                    , task5.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    , (T4) arr[3]
                    , (T5) arr[4]
                    ));
        }

        public static Task<TResult> Join<T1, T2, T3, T4, T5, T6, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Task<T4> task4
            , Task<T5> task5
            , Task<T6> task6
            , Func<T1, T2, T3, T4, T5, T6, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    , task4.Wrap()
                    , task5.Wrap()
                    , task6.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    , (T4) arr[3]
                    , (T5) arr[4]
                    , (T6) arr[5]
                    ));
        }

        public static Task<TResult> Join<T1, T2, T3, T4, T5, T6, T7, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Task<T4> task4
            , Task<T5> task5
            , Task<T6> task6
            , Task<T7> task7
            , Func<T1, T2, T3, T4, T5, T6, T7, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    , task4.Wrap()
                    , task5.Wrap()
                    , task6.Wrap()
                    , task7.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    , (T4) arr[3]
                    , (T5) arr[4]
                    , (T6) arr[5]
                    , (T7) arr[6]
                    ));
        }

        public static Task<TResult> Join<T1, T2, T3, T4, T5, T6, T7, T8, TResult>
            ( Task<T1> task1
            , Task<T2> task2
            , Task<T3> task3
            , Task<T4> task4
            , Task<T5> task5
            , Task<T6> task6
            , Task<T7> task7
            , Task<T8> task8
            , Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> project
            )
        {
            return Task
                .WhenAll
                    ( task1.Wrap()
                    , task2.Wrap()
                    , task3.Wrap()
                    , task4.Wrap()
                    , task5.Wrap()
                    , task6.Wrap()
                    , task7.Wrap()
                    , task8.Wrap()
                    )
                .Map(arr => project
                    ( (T1) arr[0]
                    , (T2) arr[1]
                    , (T3) arr[2]
                    , (T4) arr[3]
                    , (T5) arr[4]
                    , (T6) arr[5]
                    , (T7) arr[6]
                    , (T8) arr[7]
                    ));
        }

        private static async Task<object> Wrap<T>(this Task<T> task)
        {
            return await task.ConfigureAwait(false);
        }

        public static Task<B> Map<A, B>(this Task<A> task, Func<A, B> map)
        {
            return task == NoOp<A>.Instance.Value
                ? NoOp<B>.Instance.Value
                : MapInternal(task, map);
        }

        private static async Task<B> MapInternal<A, B>(this Task<A> task, Func<A, B> map)
        {
            return map(await task.ConfigureAwait(false));
        }

        public static Task<A> Then<A>(this Task<A> task, Action<A> action)
        {
            return task.Map(x =>
            {
                action(x);
                return x;
            });
        }

        public static Task<B> Bind<A, B>(this Task<A> task, Func<A, Task<B>> bind)
        {
            return task == NoOp<A>.Instance.Value
                ? NoOp<B>.Instance.Value
                : BindInternal(task, bind);
        }
        private static async Task<B> BindInternal<A, B>(this Task<A> task, Func<A, Task<B>> bind)
        {
            return await bind(await task.ConfigureAwait(false)).ConfigureAwait(false);
        }

        public static async Task<Unit> Bind(this Task task, Func<Task> bind)
        {
            await task.ConfigureAwait(false);
            await bind().ConfigureAwait(false);
            return Unit.Default;
        }

        public static async Task<Unit> Map(this Task task, Action action)
        {
            await task.ConfigureAwait(false);
            action();
            return Unit.Default;
        }

        public static Task<A> AsTask<A>(this A target)
        {
            return Task.FromResult(target);
        }

        public static Task<B> Select<A, B>(this Task<A> self, Func<A, B> map)
        {
            return self.Map(map);
        }

        public static Task<B> SelectMany<A, B>(this Task<A> self, Func<A, Task<B>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            return self.Bind(bind);
        }

        public static Task<C> SelectMany<A, B, C>(this Task<A> self, Func<A, Task<B>> bind, Func<A, B, C> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }

        public static Task<A> IfTrue<A>(this Task<A> self, Func<A, bool> predicate, Action<A> then)
        {
            return self == NoOp<A>.Instance.Value
                ? self
                : IfTrueInternal(self, predicate, then);
        }

        private static async Task<A> IfTrueInternal<A>(Task<A> self, Func<A, bool> predicate, Action<A> then)
        {
            var value = await self.ConfigureAwait(false);
            if (predicate(value)) then(value);
            return value;
        }

        public static Task<A> IfFalse<A>(this Task<A> self, Func<A, bool> predicate, Action<A> then)
        {
            return self == NoOp<A>.Instance.Value
                ? self
                : IfFalseInternal(self, predicate, then);
        }

        private static async Task<A> IfFalseInternal<A>(Task<A> self, Func<A, bool> predicate, Action<A> then)
        {
            var value = await self.ConfigureAwait(false);
            if (!predicate(value)) then(value);
            return value;
        }

        /// <remarks>
        /// WARNING: blocks the task to evaluate the predicate and returns a NoOp to
        /// replace the original task if the predicate evaluates to false.
        /// This implies a possible deadlock if the original task was not configured
        /// with a proper awaiter
        /// </remarks>
        public static Task<A> Where<A>(this Task<A> self, Func<A, bool> predicate)
        {
            if (self == NoOp<A>.Instance.Value) return self;
            return predicate(RunSync(() => self)) ? self : NoOp<A>.Instance.Value;
        }

        public static Task<A> AsTaskIfTrue<A>(this bool value, Func<A> ifTrue)
        {
            return value ? ifTrue().AsTask() : NoOp<A>.Instance.Value;
        }

        private static class NoOp<A>
        {
            public static readonly Lazy<Task<A>> Instance = new Lazy<Task<A>>(() => default(A).AsTask());
        }
    }
}
