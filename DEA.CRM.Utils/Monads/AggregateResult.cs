﻿using System;
using System.Linq;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Utils.Monads
{
    /// <summary>
    /// This is a child of RecoverableResult and Either.
    /// It's similar to Either for having a dedicated type parameter for Left.
    /// It's nearly identical to RecoverableResult in that it has three states and an error aggregation mechanism.
    /// </summary>
    /// <typeparam name="E">The error type parameter</typeparam>
    /// <typeparam name="T">The value</typeparam>
    public struct AggregateResult<E, T>
    {
        private readonly T _value;

        public T Value
        {
            get
            {
                if (IsCritical) throw new InvalidOperationException(string.Join("; ", Errors.Select(x => x.ToString()).Distinct()));
                return _value;
            }
        }

        public bool IsSuccess { get; }
        public bool IsRecoverable { get; }
        public bool IsCritical { get; }

        public E[] Errors { get; }

        public static implicit operator AggregateResult<E, T>(T value) => Success(value);
        public static implicit operator AggregateResult<E, T>(E error) => Critical(error);

        public static AggregateResult<E, T> Critical(params E[] errors)
        {
            return new AggregateResult<E, T>(default(T), false, false, errors);
        }

        public static AggregateResult<E, T> Recoverable(T fallbackValue, params E[] errors)
        {
            return new AggregateResult<E, T>(fallbackValue, false, true, errors);
        }

        public static AggregateResult<E, T> Success(T value)
        {
            return new AggregateResult<E, T>(value, true, false, new E[0]);
        }

        public AggregateResult<E, U> Map<U>(Func<T, U> map)
        {
            if (IsCritical) return AggregateResult<E, U>.Critical(Errors);
            return IsSuccess
                ? AggregateResult<E, U>.Success(map(Value))
                : AggregateResult<E, U>.Recoverable(map(Value), Errors);
        }

        public AggregateResult<E, U> Bind<U>(Func<T, AggregateResult<E, U>> bind)
        {
            if (IsCritical) return AggregateResult<E, U>.Critical(Errors);
            if (IsSuccess) return bind(Value);

            var bound = bind(Value);
            return bound.IsCritical
                ? AggregateResult<E, U>.Critical(Errors.Concat(bound.Errors).ToArray())
                : AggregateResult<E, U>.Recoverable(bound.Value, Errors.Concat(bound.Errors).ToArray());
        }

        private AggregateResult(T value, bool isSuccess, bool isRecoverable, E[] errors)
        {
            _value = value;
            IsSuccess = isSuccess;
            IsRecoverable = isRecoverable;
            IsCritical = !IsSuccess && !IsRecoverable;
            Errors = errors ?? new E[0];
        }
    }


    public static class AggregateResult
    {
        public static AggregateResult<E, T> Recoverable<E, T>(T value, params E[] errors)
        {
            return AggregateResult<E, T>.Recoverable(value, errors);
        }
    }

    public static class AggregateResultExtensions
    {
        public static AggregateResult<E, U> Select<E, T, U>(this AggregateResult<E, T> self, Func<T, U> map)
        {
            return self.Map(map);
        }

        public static AggregateResult<E, U> SelectMany<E, T, U>(this AggregateResult<E, T> self, Func<T, AggregateResult<E, U>> bind)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            return self.Bind(bind);
        }

        public static AggregateResult<E, V> SelectMany<E, T, U, V>(this AggregateResult<E, T> self, Func<T, AggregateResult<E, U>> bind, Func<T, U, V> project)
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            return self.Bind(value =>
            {
                var proj = project.Partial(value);
                return bind(value).Map(proj);
            });
        }

        public static AggregateResult<E, T> OnContinuable<E, T>(this AggregateResult<E, T> self, Action<T> action)
        {
            if (!self.IsCritical) action(self.Value);
            return self;
        }

        public static AggregateResult<E, T> OnSuccess<E, T>(this AggregateResult<E, T> self, Action<T> action)
        {
            if (self.IsSuccess) action(self.Value);
            return self;
        }

        public static AggregateResult<E, T> OnCritical<E, T>(this AggregateResult<E, T> self, Action<E[]> action)
        {
            if (self.IsCritical) action(self.Errors);
            return self;
        }

        public static AggregateResult<E, T> OnCriticalRaiseError<E, T, TException>(this AggregateResult<E, T> self, Func<string, TException> exceptionFactory)
            where TException : Exception
        {
            if (self.IsCritical) throw exceptionFactory(string.Join("\n", self.Errors.Select(x => x.ToString()).Distinct()));
            return self;
        }

        public static AggregateResult<E, T> Finally<E, T>(this AggregateResult<E, T> self, Action action)
        {
            action();
            return self;
        }
    }
}
