﻿using System;
using System.Collections.Generic;

namespace DEA.CRM.Utils.Concurrency.Synchronization
{
    /// <summary>
    /// Access lock mechanism
    /// </summary>
    public interface IAccessLock
    {
        /// <summary>
        /// The unique ID of a session
        /// </summary>
        string SessionId { get; set; }

        /// <summary>
        /// Time to wait until the lock is acquired, usually a TimeOutExcetion will be thrown if the lock cannot be acquired before the defined timeout 
        /// </summary>
        TimeSpan Timeout { get; set; }

        /// <summary>
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        IEnumerable<TResult> PerformAction<TResult>(Func<IEnumerable<TResult>> action);

        /// <summary>
        ///  </summary>
        /// <param name="action"></param>
        TResult PerformAction<TResult>(Func<TResult> action);
    }
}
