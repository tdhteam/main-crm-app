﻿using System;
using System.Collections.Concurrent;

namespace DEA.CRM.Utils.Concurrency.Synchronization
{
    public interface IKeyBaseSync<TKey>
    {
        TResult PerformAction<TResult>(TKey key, Func<TKey, TResult> action);
        void PerformAction(TKey key, Action<TKey> action);
    }

    public class KeyBasedSynchronization
    {
        public static IKeyBaseSync<TKey> Get<TKey>()
        {
            return new LocalMachineKeyBaseSync<TKey>();
        }
    }

    public class LocalMachineKeyBaseSync<TKey> : IKeyBaseSync<TKey>
    {
        private readonly ConcurrentDictionary<TKey, object> _keyLocks = new ConcurrentDictionary<TKey, object>();

        public TResult PerformAction<TResult>(TKey key, Func<TKey, TResult> action)
        {
            try
            {
                var keyLock = _keyLocks.GetOrAdd(key, new object());
                TResult result;
                lock (keyLock) result = action(key);
                return result;
            }
            finally
            {
                _keyLocks.TryRemove(key, out object _);
            }
        }

        public void PerformAction(TKey key, Action<TKey> action)
        {
            PerformAction(key, k =>
            {
                action(k);
                return true;
            });
        }
    }
}