﻿using System;
using System.Collections.Generic;

namespace DEA.CRM.Utils.Concurrency.Synchronization
{
    public class NoLock : IAccessLock
    {
        public string SessionId { get; set; }
        public TimeSpan Timeout { get; set; }

        public IEnumerable<TResult> PerformAction<TResult>(Func<IEnumerable<TResult>> action)
        {
            return action();
        }

        public TResult PerformAction<TResult>(Func<TResult> action)
        {
            return action();
        }
    }
}
