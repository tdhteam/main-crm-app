﻿using System;
using System.Collections.Concurrent;

namespace DEA.CRM.Utils.Concurrency
{
    public static class ConcurrentDictionaryExt
    {
        /// <summary>
        /// Thread-safe GetOrAdd method for ConcurrentDictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="valueFactory"></param>
        /// <returns></returns>
        public static TValue GetOrAddSafe<TKey, TValue>(this ConcurrentDictionary<TKey, Lazy<TValue>> dictionary,
            TKey key,
            Func<TKey, TValue> valueFactory)
        {
            Lazy<TValue> lazy = dictionary.GetOrAdd(key, new Lazy<TValue>(() => valueFactory(key)));
            return lazy.Value;
        }

        /// <summary>
        /// Thread-safe AddOrUpdate method for ConcurrentDictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="addValueFactory"></param>
        /// <param name="updateValueFactory"></param>
        /// <returns></returns>
        public static TValue AddOrUpdateSafe<TKey, TValue>(this ConcurrentDictionary<TKey, Lazy<TValue>> dictionary,
            TKey key,
            Func<TKey, TValue> addValueFactory,
            Func<TKey, TValue, TValue> updateValueFactory)
        {
            Lazy<TValue> lazy = dictionary.AddOrUpdate(key,
                new Lazy<TValue>(() => addValueFactory(key)),
                (k, oldValue) => new Lazy<TValue>(() => updateValueFactory(k, oldValue.Value)));
            return lazy.Value;
        }
    }
}
