﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DEA.CRM.Utils.Concurrency
{
    /// <summary>
    /// Provides a convenient and less error-prone way to generate singleton of a provided type
    /// <para>Singletons can be identified by their own types for simple types or using a unique ID for complex types such as IEnumerable, dictionary, generics...</para>
    /// <remarks>
    /// Despite being more convenient it does come with a performance cost.
    /// <para>Under testing condition, SingletonGenerator is able to respond to approximately 5,000,000 requests per second
    ///  - which is usually good enough</para>
    /// <para>However comparing to a simple locking mechanism, it is still 2 to 5 times slower</para>
    /// <para>If highest performance is absolutely needed, consider looking into a manual locking strategy</para>
    /// </remarks>
    /// </summary>
    public static class SingletonGenerator
    {
        private static readonly ConcurrentDictionary<string, object> Instances = new ConcurrentDictionary<string, object>();

        static SingletonGenerator()
        {
        }

        /// <summary>
        /// Get a singleton of the provided type.
        /// If the instance is not yet available, create it using the valueFactory function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">If T is a complex type e.g. array, dictionary, list, generics... it is necessary to use a UNIQUE ID to identify the singleton</param>
        /// <param name="valueFactory">The function to create the actual instance of the singleton in case it is not yet available</param>
        /// <returns>The singleton</returns>
        private static T GetInstanceById<T>(string id, Func<T> valueFactory)
        {
            var lazy = (Lazy<T>)Instances.GetOrAdd(id, new Lazy<T>(valueFactory));
            return lazy.Value;
        }

        public static T GetInstanceById<T>(Guid id, Func<T> valueFactory)
        {
            return GetInstanceById(id.ToString(), valueFactory);
        }

        /// <summary>
        /// Get singleton instance of a dictionary-based type
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="id">Unique ID of the provided type</param>
        /// <param name="dictionaryFactory"></param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> GetInstanceOf<TKey, TValue>(
            Guid id,
            Func<IDictionary<TKey, TValue>> dictionaryFactory)
        {
            return GetInstanceById(id.ToString(), dictionaryFactory);
        }

        /// <summary>
        /// Get singleton instance of a IEnumerable based type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">Unique ID of the provided type</param>
        /// <param name="valueFactory"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetInstanceOf<T>(Guid id, Func<IEnumerable<T>> valueFactory)
        {
            return GetInstanceById(id.ToString(), valueFactory);
        }

        /// <summary>
        /// Get singleton instance of a simple type
        /// </summary>
        /// <typeparam name="T">This should be a simple type</typeparam>
        /// <param name="valueFactory"></param>
        /// <returns></returns>
        public static T GetInstanceOf<T>(Func<T> valueFactory)
        {
            return GetInstanceById(typeof(T).FullName, valueFactory);
        }
    }
}
