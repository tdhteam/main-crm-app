﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Utils.Extensions
{
    public static class StringExtensions
    {
        public static bool EqualsIgnoreCase(this string source, string target)
        {            
            return string.Compare(source, target, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
        
        public static string Slice(this string source, int start, int end)
        {
            if (end < 0) // Keep this for negative end support
            {
                end = source.Length + end;
            }
            int len = end - start;               // Calculate length
            return source.Substring(start, len); // Return Substring of length
        }

        /// <summary>
        /// Check string contain sub-string with ignoring case senitive
        /// </summary>
        /// <param name="s"></param>
        /// <param name="subStringCaseInsensitive"></param>
        /// <returns></returns>
        public static bool ContainIgnoreCase(this string s, string subStringCaseInsensitive)
        {
            //Should in string extention method ?!
            return s.IndexOf(subStringCaseInsensitive, StringComparison.OrdinalIgnoreCase) > -1;
        }

        public static int ExtractNum(this string source)
        {
            if (string.IsNullOrEmpty(source)) throw new ArgumentException("Extracting Number failed: source cannot be null");
            var result = new StringBuilder();
            for (int i = source.Length - 1; i >= 0; i--)
            {
                var c = source[i];
                if (c >= '0' && c <= '9')
                {
                    result.Insert(0, c);
                }
                else
                {
                    break;
                }
            }
            result.Insert(0, '0');
            return int.Parse(result.ToString());
        }

        /// <summary>
        /// Adds a char to end of given string if it does not ends with the char.
        /// </summary>
        public static string EnsureEndsWith(this string str, char c)
        {
            return EnsureEndsWith(str, c, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Adds a char to end of given string if it does not ends with the char.
        /// </summary>
        public static string EnsureEndsWith(this string str, char c, StringComparison comparisonType)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.EndsWith(c.ToString(CultureInfo.InvariantCulture), comparisonType))
            {
                return str;
            }

            return str + c;
        }

        /// <summary>
        /// Adds a char to end of given string if it does not ends with the char.
        /// </summary>
        public static string EnsureEndsWith(this string str, char c, bool ignoreCase, CultureInfo culture)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.EndsWith(c.ToString(culture), ignoreCase, culture))
            {
                return str;
            }

            return str + c;
        }

        /// <summary>
        /// Adds a char to beginning of given string if it does not starts with the char.
        /// </summary>
        public static string EnsureStartsWith(this string str, char c)
        {
            return EnsureStartsWith(str, c, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Adds a char to beginning of given string if it does not starts with the char.
        /// </summary>
        public static string EnsureStartsWith(this string str, char c, StringComparison comparisonType)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.StartsWith(c.ToString(CultureInfo.InvariantCulture), comparisonType))
            {
                return str;
            }

            return c + str;
        }

        /// <summary>
        /// Adds a char to beginning of given string if it does not starts with the char.
        /// </summary>
        public static string EnsureStartsWith(this string str, char c, bool ignoreCase, CultureInfo culture)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.StartsWith(c.ToString(culture), ignoreCase, culture))
            {
                return str;
            }

            return c + str;
        }

        /// <summary>
        /// Indicates whether this string is null or an System.String.Empty string.
        /// </summary>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// indicates whether this string is null, empty, or consists only of white-space characters.
        /// </summary>
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        /// <summary>
        /// Gets a substring of a string from beginning of the string.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="str"/> is null</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="len"/> is bigger that string's length</exception>
        public static string Left(this string str, int len)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.Length < len)
            {
                throw new ArgumentException("len argument can not be bigger than given string's length!");
            }

            return str.Substring(0, len);
        }

        /// <summary>
        /// Converts line endings in the string to <see cref="Environment.NewLine"/>.
        /// </summary>
        public static string NormalizeLineEndings(this string str)
        {
            return str.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", Environment.NewLine);
        }

        /// <summary>
        /// Gets index of nth occurence of a char in a string.
        /// </summary>
        /// <param name="str">source string to be searched</param>
        /// <param name="c">Char to search in <see cref="str"/></param>
        /// <param name="n">Count of the occurence</param>
        public static int NthIndexOf(this string str, char c, int n)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            var count = 0;
            for (var i = 0; i < str.Length; i++)
            {
                if (str[i] != c)
                {
                    continue;
                }

                if ((++count) == n)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets a substring of a string from end of the string.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="str"/> is null</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="len"/> is bigger that string's length</exception>
        public static string Right(this string str, int len)
        {
            if (str == null)
            {
                throw new ArgumentNullException("str");
            }

            if (str.Length < len)
            {
                throw new ArgumentException("len argument can not be bigger than given string's length!");
            }

            return str.Substring(str.Length - len, len);
        }

        /// <summary>
        /// Uses string.Split method to split given string by given separator.
        /// </summary>
        public static string[] Split(this string str, string separator)
        {
            return str.Split(new[] { separator }, StringSplitOptions.None);
        }

        /// <summary>
        /// Uses string.Split method to split given string by given separator.
        /// </summary>
        public static string[] Split(this string str, string separator, StringSplitOptions options)
        {
            return str.Split(new[] { separator }, options);
        }

        /// <summary>
        /// Uses string.Split method to split given string by <see cref="Environment.NewLine"/>.
        /// </summary>
        public static string[] SplitToLines(this string str)
        {
            return str.Split(Environment.NewLine);
        }

        /// <summary>
        /// Uses string.Split method to split given string by <see cref="Environment.NewLine"/>.
        /// </summary>
        public static string[] SplitToLines(this string str, StringSplitOptions options)
        {
            return str.Split(Environment.NewLine, options);
        }

        public static string FirstLine(this string str)
        {
            var lines = str.SplitToLines(StringSplitOptions.RemoveEmptyEntries);
            return lines != null && lines.Length > 0 ? lines[0] : str;
        }

        /// <summary>
        /// Converts PascalCase string to camelCase string.
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <returns>camelCase of the string</returns>
        public static string ToCamelCase(this string str)
        {
            return str.ToCamelCase(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts PascalCase string to camelCase string in specified culture.
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <param name="culture">An object that supplies culture-specific casing rules</param>
        /// <returns>camelCase of the string</returns>
        public static string ToCamelCase(this string str, CultureInfo culture)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }

            if (str.Length == 1)
            {
                return str.ToLower(culture);
            }

            return char.ToLower(str[0], culture) + str.Substring(1);
        }

        /// <summary>
        /// Converts string to enum value.
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="value">String value to convert</param>
        /// <returns>Returns enum object</returns>
        public static T ToEnum<T>(this string value)
            where T : struct
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            return (T)Enum.Parse(typeof(T), value);
        }

        /// <summary>
        /// Converts string to enum value.
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="value">String value to convert</param>
        /// <param name="ignoreCase">Ignore case</param>
        /// <returns>Returns enum object</returns>
        public static T ToEnum<T>(this string value, bool ignoreCase)
            where T : struct
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        /// <summary>
        /// Converts camelCase string to PascalCase string.
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <returns>PascalCase of the string</returns>
        public static string ToPascalCase(this string str)
        {
            return str.ToPascalCase(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts camelCase string to PascalCase string in specified culture.
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <param name="culture">An object that supplies culture-specific casing rules</param>
        /// <returns>PascalCase of the string</returns>
        public static string ToPascalCase(this string str, CultureInfo culture)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }

            if (str.Length == 1)
            {
                return str.ToUpper(culture);
            }

            return char.ToUpper(str[0], culture) + str.Substring(1);
        }

        /// <summary>
        /// Gets a substring of a string from beginning of the string if it exceeds maximum length.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="str"/> is null</exception>
        public static string Truncate(this string str, int maxLength)
        {
            if (str == null)
            {
                return null;
            }

            if (str.Length <= maxLength)
            {
                return str;
            }

            return str.Left(maxLength);
        }

        /// <summary>
        /// Gets a substring of a string from beginning of the string if it exceeds maximum length.
        /// It adds a "..." postfix to end of the string if it's truncated.
        /// Returning string can not be longer than maxLength.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="str"/> is null</exception>
        public static string TruncateWithPostfix(this string str, int maxLength)
        {
            return TruncateWithPostfix(str, maxLength, "...");
        }

        /// <summary>
        /// Gets a substring of a string from beginning of the string if it exceeds maximum length.
        /// It adds given <paramref name="postfix"/> to end of the string if it's truncated.
        /// Returning string can not be longer than maxLength.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="str"/> is null</exception>
        public static string TruncateWithPostfix(this string str, int maxLength, string postfix)
        {
            if (str == null)
            {
                return null;
            }

            if (str == string.Empty || maxLength == 0)
            {
                return string.Empty;
            }

            if (str.Length <= maxLength)
            {
                return str;
            }

            if (maxLength <= postfix.Length)
            {
                return postfix.Left(maxLength);
            }

            return str.Left(maxLength - postfix.Length) + postfix;
        }

        private static readonly List<string> UnsafeCharacterListForUrl = new List<string>
        {
            ":", ";", "@", "&", "=", "<", ">", "#", "{", "}", "|", @"\", "^", "~", "[", "]", "`"
        };

        public static string RemoveWildcards(this string source)
        {
            return source?.Replace("%", "").Replace("?", "");
        }

        /// <summary>
        /// Check length of input string (and required it)
        /// </summary>
        public static bool InMaxLength(this string source, int length = 256, bool required = false)
        {
            return (required && IsNotNullOrEmpty(source) && source.Trim().Length <= length)
                   || (!required && source.SafeLength() <= length);
        }

        public static int SafeLength(this string target)
        {
            return IsNotNullOrEmpty(target) ? target.Length : 0;
        }

        /// <summary>
        /// Check string include special characters by array of string
        /// </summary>
        public static bool HasCharacters(this string source, string[] chars)
        {
            return chars.Any(c => source.IndexOf(c, StringComparison.Ordinal) >= 0);
        }

        /// <summary>
        /// Check if a string includes any special characters
        /// </summary>
        public static bool HasAnyCharacters(this string source, char[] chars)
        {
            return chars.Any(c => source.IndexOf(c) >= 0);
        }

        /// <summary>
        /// the source string is not null or empty
        /// </summary>
        public static bool IsNotNullOrEmpty(this string source)
        {
            return !string.IsNullOrEmpty(source);
        }

        /// <returns>either the passed in string, or if the string is null, the default value</returns>
        public static string DefaultIfNullOrEmpty(this string source, string defaultValue)
        {
            return string.IsNullOrEmpty(source) ? defaultValue : source;
        }

        /// <returns>either the passed in string, or if the string is null, an empty string</returns>
        public static string DefaultString(this string source)
        {
            return DefaultIfNullOrEmpty(source, string.Empty);
        }

        public static string SubstringSafe(this string source, int startIndex, int length)
        {
            return new string(source.DefaultString().Skip(startIndex).Take(length).ToArray());
        }


        public static string TrimEnd(this string source, string value)
        {
            var s = source;
            while (s.EndsWith(value))
            {
                s = s.Remove(s.LastIndexOf(value, StringComparison.Ordinal));
            }
            return s;
        }

        public static string SafeReplace(this string source, string oldString, string newString)
        {
            return string.IsNullOrEmpty(source) ? source : source.Replace(oldString, newString);
        }

        public static string SafeStringForUrl(this string source)
        {
            UnsafeCharacterListForUrl.ForEach(c => source = source.SafeReplace(c, string.Empty));
            return source;
        }

        /// <summary>
        /// Make a long string become shorter with max length and the remain characters is replaced with ...
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string MakeShortString(this string source, int maxLength)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            if (source.Length > maxLength) return source.SubstringSafe(0, maxLength) + "...";
            return source;
        }

        /// <summary>
        /// Split the string using the separator and remove empty entries
        /// </summary>
        public static string[] SplitBy(this string source, params string[] separator)
        {
            return source.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Similar to LIKE operator in SQL
        /// </summary>
        public static bool SqlLike(this string source, string other)
        {
            return source?.ToUpper().Contains(other.DefaultString().ToUpper().Trim('%')) == true;
        }

        public static bool ContainsIgnoreCase(this string source, string other)
        {
            if (string.IsNullOrEmpty(source)) return false;
            return source.IndexOf(other, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public static bool ValidHttpImageUrl(this string source)
        {
            if (string.IsNullOrEmpty(source)) return false;
            var result = Uri.TryCreate(source, UriKind.Absolute, out var uriResult)
                         && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            return result;
        }

        public static bool ContainsUnsafeCharactersForUrl(this string source)
        {
            if (string.IsNullOrEmpty(source)) return false;
            return UnsafeCharacterListForUrl.Any(source.Contains);
        }

        /// <summary>
        /// Converts the target string into a byte array
        /// </summary>
        public static byte[] GetBytes(this string source)
        {
            if (source == null) return null;
            var bytes = new byte[source.Length * sizeof(char)];
            Buffer.BlockCopy(source.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static byte[] GetBytesFromHex(this string hex)
        {
            var input = hex
                .Replace("-", "")
                .Replace(" ", "");
            var result = new byte[hex.Length / 2];
            int index = 0;
            for (var i = 0; i < input.Length; i += 2) // every two hex characters are converted into 1 byte
            {
                result[index++] = Convert.ToByte(input.Substring(i, 2), 16);
            }
            return result;
        }

        public static string ToHexString(this byte[] data, bool binaryFormat = false)
        {
            var hex = new StringBuilder(data.Length * 2); // a hex string needs 2 characters to represent a byte
            var separator = binaryFormat ? " " : "";
            foreach (byte b in data)
            {
                hex.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
                hex.Append(separator);
            }
            return hex.ToString().TrimEnd();
        }

        public static string ToCharSequence(this byte[] data)
        {
            var hex = new StringBuilder(data.Length);
            foreach (var b in data)
            {
                hex.Append(Convert.ToChar(b));
            }
            return hex.ToString();
        }

        public static Maybe<int> ParseInt(this string data)
        {
            return int.TryParse(data, out int parseResult)
                ? parseResult
                : Maybe<int>.Nothing;
        }

        public static WildcardedString EscapeWildcard(this string source, char wildcard, Func<string, string> action)
        {
            return WildcardedString.From(source, wildcard).Mutate(action);
        }

        public static bool IsBitValue(this string source, out bool boolValue)
        {
            if (!"0".Equals(source) && !"1".Equals(source))
            {
                boolValue = false;
                return false;
            }
            boolValue = "1".Equals(source);
            return true;
        }

        /// <summary>
        /// Extension method for string to check if a string value is equal to Boolean value true or false
        /// </summary>
        /// <param name="source">A case insensive string with value Y/N Yes/No true/false, T/F 0/1</param>
        /// <returns></returns>
        public static bool IsTrueValue(this string source)
        {
            if (string.IsNullOrEmpty(source)) return false;
            var lowerCaseStr = source.ToLower();
            if (lowerCaseStr.Equals("y") || lowerCaseStr.Equals("yes") || lowerCaseStr.Equals("1") ||
                lowerCaseStr.Equals("true") || lowerCaseStr.Equals("t")) return true;
            return false;
        }

        public static bool IsValueInRange(this string source, params string[] valueRanges)
        {
            return valueRanges.ToList().IndexOf(source) > -1;
        }

        public static string UserNameWithoutDomain(this string sourceUserName)
        {
            if (string.IsNullOrEmpty(sourceUserName))
            {
                return string.Empty;
            }
            var tokens = sourceUserName.Split("\\".ToCharArray());
            if (tokens.Length == 2)
            {
                return tokens[1].Trim();
            }
            return sourceUserName;
        }

        /// <summary>
        /// Convert a possible long user name to a short user name (max default 20 chars - limit by E7) to be compatible with some name fields in Epicor
        /// </summary>
        /// <param name="target"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string ToSafeLengthName(this string target, int maxLength = 20)
        {
            if (string.IsNullOrEmpty(target)) return string.Empty;

            if (target.Length <= maxLength) return target;

            var nameWithoutDomain = target.UserNameWithoutDomain();

            if (nameWithoutDomain.Length > maxLength)
            {
                throw new ArgumentException($"The name {target} exceeds maximum length of {maxLength} characters.");
            }

            return nameWithoutDomain;
        }

        /// <summary>
        /// Convert a path with backward slash (\) to a path with forward slash (/)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToUrlPath(this string source)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            return source.Replace("\\", "/");
        }

        public static string AppendString(this string source, string appendedString)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : $"{source}{appendedString}";
        }

        public class WildcardedString
        {
            private bool _atStart;
            private bool _atEnd;
            private char _wildcard;
            private string _value;

            public string Unescape()
            {
                var result = _value;
                if (_atStart) result = _wildcard + result;
                if (_atEnd) result += _wildcard;
                return result;
            }

            public override string ToString()
            {
                return _value;
            }

            internal static WildcardedString From(string source, char wildcard)
            {
                if (source == null) return Empty();
                var result = new WildcardedString
                {
                    _atStart = source.Length > 0 && source[0] == wildcard,
                    _atEnd = source.Length > 1 && source[source.Length - 1] == wildcard,
                    _wildcard = wildcard,
                    _value = source.Trim(wildcard)
                };
                return result;
            }

            internal WildcardedString Mutate(Func<string, string> action)
            {
                if (_value != null) _value = action(_value);
                return this;
            }

            private static WildcardedString Empty()
            {
                return new WildcardedString { _value = null };
            }
        }
    }
}
