﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DEA.CRM.Utils.Extensions
{
    public static class DecimalExtensions
    {
        private const decimal RoundingThreshold = 0.00000001m;

        public static bool IsWhole(this decimal source)
        {
            return Math.Abs(source - decimal.Truncate(source)) <= RoundingThreshold;
        }

        public static bool Is(this decimal source, decimal target)
        {
            return Math.Abs(source - target) <= RoundingThreshold;
        }

        public static bool IsNot(this decimal source, decimal target)
        {
            return !source.Is(target);
        }

        public static void RoundAndAdjust<T>
        (this ICollection<T> sequence
            , decimal target
            , int roundingDecimals
            , Expression<Func<T, decimal>> selector
        ) where T : class
        {
            var accessor = ExpressionUtils.GetAccessor(selector);
            var round = Functions.Lambda((decimal x) => Math.Round(x, roundingDecimals));

            var roundedSum = sequence.Sum(x => round(accessor.Get(x)));
            var diff = target - roundedSum;

            Func<T, bool> matchFn = target < 0
                ? e => accessor.Get(e) + diff < 0
                : Functions.Lambda((T e) => accessor.Get(e) + diff >= 0);

            T itemToAdjust = null;
            var minVariance = decimal.MaxValue;

            foreach (var element in sequence)
            {
                var originalValue = accessor.Get(element);
                var roundedValue = round(originalValue);
                accessor.Set(element, roundedValue); // mutate the element with rounded value
                if (matchFn(element))
                {
                    // find the element that has the smallest variance compared to its original value
                    var variance = Math.Abs(roundedValue + diff - originalValue);
                    if (variance < minVariance)
                    {
                        itemToAdjust = element;
                        minVariance = variance;
                    }
                }
            }

            // perform adjustment on the item with the smallest variance
            if (itemToAdjust != null)
            {
                accessor.Set(itemToAdjust, accessor.Get(itemToAdjust) + diff);
            }
        }
    }
}
