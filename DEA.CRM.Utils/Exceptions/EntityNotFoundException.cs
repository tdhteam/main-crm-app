﻿using System;
using System.Runtime.Serialization;

namespace DEA.CRM.Utils.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message) : base(message)
        {            
        }
    }
    
    public class AppNotificationException : Exception
    {
        public AppNotificationException()
        {
        }

        public AppNotificationException(string message) : base(message)
        {
        }

        public AppNotificationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AppNotificationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class BusinessException : Exception
    {
        public BusinessException()
        {
        }

        public BusinessException(string message) : base(message)
        {
        }

        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BusinessException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
