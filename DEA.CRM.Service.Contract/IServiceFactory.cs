﻿namespace DEA.CRM.Service.Contract
{
    public interface IServiceFactory
    {
        TService GetService<TService>();
    }
}
