﻿using System;

namespace DEA.CRM.Service.Contract.HandOverContract
{
    public class HandOverContractDto
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactType { get; set; }
        public string CustomerType { get; set; }
        public string ContractPersonnelFullName { get; set; }
        public string CompanyName { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public DateTime? ContractDate { get; set; }
        public string SalesUserKey { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryPriority { get; set; }
        public decimal? PaymentInitialAmount { get; set; }
        public decimal? PaymentCashRemainAmount { get; set; }
        public decimal? PaymentBankInstallmentAmount { get; set; }
        public decimal? PaymentInstallmentAmount { get; set; }

        public static readonly Func<DEA.CRM.Model.HandOverContract, HandOverContractDto> EntityToDtoMapper =
           entity => entity == null
               ? null
               : new HandOverContractDto
               {
                   Id = entity.Id,
                   CustomerId = entity.CustomerId,
                   FirstName = entity.FirstName,
                   LastName = entity.LastName,
                   Title = entity.Title,
                   ContactType = entity.ContactType,
                   CustomerType = entity.CustomerType,
                   ProductName = entity.ProductName,
                   ProductType = entity.ProductType,
                   ContractDate = entity.ContractDate,
                   SalesUserKey = entity.SalesUserKey,
                   DeliveryDate = entity.DeliveryDate,
                   DeliveryPriority = entity.DeliveryPriority,
                   PaymentInitialAmount = entity.PaymentInitialAmount,
                   PaymentCashRemainAmount = entity.PaymentCashRemainAmount,
                   PaymentBankInstallmentAmount = entity.PaymentBankInstallmentAmount,
                   PaymentInstallmentAmount = entity.PaymentInstallmentAmount
               };
    }
}
