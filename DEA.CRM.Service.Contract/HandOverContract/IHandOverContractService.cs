﻿using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.HandOverContract
{

    public class SearchHandOverContractCommand : Paging
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
    }

    public class CreateOrUpdateHandOverContractCommand : HandOverContractDto
    {

    }


    /// <summary>
    /// Service to handle CRUD Contact, retrieve data for front site to use
    /// </summary>
    public interface IHandOverContractService
    {
        PagingResult<HandOverContractDto> SearchHandOverContracts(SearchHandOverContractCommand command);
        Result<HandOverContractDto> GetById(int handovercontractId);
        Result<HandOverContractDto> Update(CreateOrUpdateHandOverContractCommand command);
    }
}
