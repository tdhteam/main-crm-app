﻿using AutoMapper;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.UserDocument.Dto;

namespace DEA.CRM.Service.Contract.Helpers
{
    public static class MapperConfigurator
    {
        public static void ConfigureMappers()
        {
            Mapper.Initialize(cfg => {
                // userInfo: dto -> entity
                cfg.CreateMap<UserDetailDto, UserInfo>();
                cfg.CreateMap<BasicUserDetailDto, UserInfo>();
                cfg.CreateMap<UserDetailSearchResultDto, UserInfo>();
                cfg.CreateMap<UserInfoToRoleDto, UserInfoToRole>();
                cfg.CreateMap<UserDashboardDto, UserDashboard>();
                cfg.CreateMap<UserInfoToCompanyDto, UserInfoToCompany>()
                    .ForMember(m => m.UserKeyNavigation, opt => opt.Ignore())
                    .ForMember(m => m.Company, opt => opt.Ignore());
                cfg.CreateMap<UserInfoToDealerDto, UserInfoToDealer>()
                    .ForMember(m => m.UserKeyNavigation, opt => opt.Ignore())
                    .ForMember(m => m.Dealer, opt => opt.Ignore());
                cfg.CreateMap<UserInfoToShowroomDto, UserInfoToShowroom>()
                    .ForMember(m => m.UserKeyNavigation, opt => opt.Ignore())
                    .ForMember(m => m.Showroom, opt => opt.Ignore());

                // userInfo: entity -> dto
                cfg.CreateMap<UserInfo, UserDetailDto>();
                cfg.CreateMap<UserInfo, BasicUserDetailDto>();
                cfg.CreateMap<UserInfo, UserDetailSearchResultDto>();
                cfg.CreateMap<UserInfoSearch, UserDetailSearchResultDto>();
                cfg.CreateMap<UserInfoToRole, UserInfoToRoleDto>();
                cfg.CreateMap<UserDashboard, UserDashboardDto>();
                cfg.CreateMap<UserInfoToCompany, UserInfoToCompanyDto>()
                    .ForMember(m => m.CompanyName,
                        opt => opt.MapFrom(e => e.Company != null ? e.Company.Name : string.Empty));

                cfg.CreateMap<UserInfoToDealer, UserInfoToDealerDto>()
                    .ForMember(m => m.DealerName,
                        opt => opt.MapFrom(e => e.Dealer != null ? e.Dealer.Name : string.Empty));

                cfg.CreateMap<UserInfoToShowroom, UserInfoToShowroomDto>()
                    .ForMember(m => m.ShowroomName,
                        opt => opt.MapFrom(e => e.Showroom != null ? e.Showroom.Name : string.Empty));

                cfg.CreateMap<Model.Contact, ContactDto>();
                cfg.CreateMap<Model.Contact, ContactDetailDto>();
                cfg.CreateMap<ContactDto, Model.Contact>();
                cfg.CreateMap<ContactDetailDto, Model.Contact>();
                cfg.CreateMap<ContactMeetingProgressDto, ContactMeetingProgress>();
                cfg.CreateMap<ContactMeetingProgress, ContactMeetingProgressDto>();
                cfg.CreateMap<ContactComment, ContactCommentDto>();
                cfg.CreateMap<ContactCommentDto, ContactComment>();
                cfg.CreateMap<ContactCommentAttachment, ContactCommentAttachmentDto>();
                cfg.CreateMap<ContactCommentAttachmentDto, ContactCommentAttachment>();
                cfg.CreateMap<Model.Attachment, AttachmentDto>();
                cfg.CreateMap<AttachmentDto, Model.Attachment>();

                cfg.CreateMap<UserInfoToShowroom, UserInfoToShowroomDto>();

                // Company, Dealer, Showroom
                cfg.CreateMap<CompanyDto, Company>().ForMember(m => m.Dealer, opt => opt.Ignore());
                cfg.CreateMap<CreateOrUpdateCompanyCommand, Company>().ForMember(m => m.Dealer, opt => opt.Ignore());
                cfg.CreateMap<Company, CompanyDto>().ForMember(m => m.Dealers, opt => opt.Ignore());

                cfg.CreateMap<DealerDto, Dealer>().ForMember(m => m.Showrooms, opt => opt.Ignore());
                cfg.CreateMap<Dealer, DealerDto>().ForMember(m => m.Showrooms, opt => opt.Ignore());

                cfg.CreateMap<ShowroomDto, Showroom>()
                    .ForMember(m => m.Dealer, opt => opt.Ignore())
                    .ForMember(m => m.UserInfoToShowroom, opt => opt.Ignore());
                cfg.CreateMap<Showroom, ShowroomDto>();

                // Activity dto -> entity
                cfg.CreateMap<ActivityDto, Activity>();
                cfg.CreateMap<ActivityAssignmentDto, ActivityAssignment>();
                cfg.CreateMap<ActivityPriorityDto, ActivityPriority>();
                cfg.CreateMap<ActivityStatusDto, ActivityStatus>();

                // Activity entity -> dto
                cfg.CreateMap<Activity, ActivityDto>();
                cfg.CreateMap<ActivityAssignment, ActivityAssignmentDto>();
                cfg.CreateMap<ActivityPriority, ActivityPriorityDto>();
                cfg.CreateMap<ActivityStatus, ActivityStatusDto>();

                // CalendarEvent dto -> entity
                cfg.CreateMap<CalendarEventDto, CalendarEvent>();
                cfg.CreateMap<CalendarEventTypeDto, CalendarEventType>();
                cfg.CreateMap<EventInviteeDto, EventInvitee>();
                cfg.CreateMap<EventRecurrenceDto, EventRecurrence>();
                cfg.CreateMap<EventReminderDto, EventReminder>();
                cfg.CreateMap<EventStatusDto, EventStatus>();
                cfg.CreateMap<CalendarEventTypeDto, CalendarEventType>();

                // CalendarEvent entity -> dto
                cfg.CreateMap<CalendarEvent, CalendarEventDto>();
                cfg.CreateMap<CalendarEventType, CalendarEventTypeDto>();
                cfg.CreateMap<EventInvitee, EventInviteeDto>();
                cfg.CreateMap<EventRecurrence, EventRecurrenceDto>();
                cfg.CreateMap<EventReminder, EventReminderDto>();
                cfg.CreateMap<EventStatus, EventStatusDto>();
                cfg.CreateMap<CalendarEventType, CalendarEventTypeDto>();

                cfg.CreateMap<Activity, CalendarItemDto>();
                cfg.CreateMap<CalendarEvent, CalendarItemDto>();

                cfg.CreateMap<CalendarEventListViewSearchResult, CalendarEventListViewSearchResultDto>();

                cfg.CreateMap<Setting, SettingDto>();
                cfg.CreateMap<SettingDto, Setting>();

                cfg.CreateMap<Model.UserDocument, UserDocumentDto>();
                cfg.CreateMap<DocumentFolder, DocumentFolderDto>();

                cfg.CreateMap<UserDocumentDto, Model.UserDocument>();
                cfg.CreateMap<DocumentFolderDto, DocumentFolder>();
            });
        }
    }
}
