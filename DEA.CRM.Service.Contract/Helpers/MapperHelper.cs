﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Timing;
using _ = DEA.CRM.Utils.Functions;

namespace DEA.CRM.Service.Contract.Helpers
{
    public static class MapperHelper
    {
        public static Func<TFrom, TResult> AutoMap<TFrom, TResult>() where TFrom : class where TResult : class
        {
            return _.Lambda<TFrom, TResult>(Mapper.Map<TResult>);
        }

        public static Func<T, T> NormalizeDates<T>(params Expression<Func<T, DateTime>>[] dateSelectors)
        {
            return ExpressionUtils.BuildMutator(Clock.Normalize, dateSelectors);
        }

        public static Func<T, T> NormalizeNegativeOne<T>(params Expression<Func<T, int?>>[] selectors)
        {
            return ExpressionUtils.BuildMutator(x => x == -1 ? null : x, selectors);
        }
    }
}
