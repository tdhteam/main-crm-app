﻿using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Contract.Product.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Product
{

    public class SearchProductCommand : Paging
    {
        public string Name { get; set; }
        public string Vin { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string Title { get; set; }
    }


    public interface IProductService
    {
        PagingResult<ProductDto> GetAllProducts();
        Result<ProductDto> GetProductById(int productId);
        ProductDto CreateOrUpdate(CreateOrUpdateProductCommand command);
        ResponseDto DeleteProduct(int productId);
        PagingResult<ProductDto> SearchProducts(SearchProductCommand command);
    }
}
