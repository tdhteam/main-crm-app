﻿namespace DEA.CRM.Service.Contract.Product.Dto
{
    public class CarMakeDto
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
