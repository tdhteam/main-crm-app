﻿using System;

namespace DEA.CRM.Service.Contract.Product.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string ProductNo { get; set; }
        public string Name { get; set; }
        public bool ProductActive { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? PercentVat { get; set; }
        public decimal? PercentService { get; set; }
        public decimal? PercentSales { get; set; }
        public decimal? QuantityInStock { get; set; }
        public int ProductCategoryId { get; set; }
        public string Vin { get; set; }
        public string EngineNumber { get; set; }
        public bool InActiveForSale { get; set; }
        public string WarehouseName { get; set; }
        public string Status { get; set; }
        public string Color { get; set; }
        public string ProductType { get; set; }
        public DateTime? DateEnteredStock { get; set; }
        public DateTime? DateRelease { get; set; }
        public string TransmissionType { get; set; }
        public DateTime? ProductionDate { get; set; }
        public int? NumberOfSeats { get; set; }
        public string Engine { get; set; }
        public string Uom { get; set; }
        public string HandlerUserKey { get; set; }
        public double? InStockAge { get; set; }
        public int? ContractId { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string BankOfGuarantee { get; set; }
        public string PaymentStatus { get; set; }
        public string Description { get; set; }
        public string PlateNumber { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public static readonly Func<DEA.CRM.Model.Product, ProductDto> EntityToDtoMapper =
          entity => entity == null
              ? null
              : new ProductDto
              {
                  Id = entity.Id,
                  ProductNo = entity.ProductNo,
                  Name = entity.Name,
                  ProductActive = entity.ProductActive,
                  UnitPrice = entity.UnitPrice,
                  PercentVat = entity.PercentVat,
                  PercentService = entity.PercentService,
                  PercentSales = entity.PercentSales,
                  QuantityInStock = entity.QuantityInStock,
                  ProductCategoryId = entity.ProductCategoryId,
                  Vin = entity.Vin,
                  EngineNumber = entity.EngineNumber,
                  InActiveForSale = entity.InActiveForSale,
                  WarehouseName = entity.WarehouseName,
                  Status = entity.Status,
                  Color = entity.Color,
                  ProductType = entity.ProductType,
                  DateEnteredStock = entity.DateEnteredStock,
                  DateRelease = entity.DateRelease,
                  TransmissionType = entity.TransmissionType,
                  ProductionDate = entity.ProductionDate,
                  Engine = entity.Engine,
                  Uom = entity.Uom,
                  HandlerUserKey = entity.HandlerUserKey,
                  InStockAge = entity.InStockAge,
                  ContractId = entity.ContractId,
                  PurchaseOrderNo = entity.PurchaseOrderNo,
                  BankOfGuarantee = entity.BankOfGuarantee,
                  PaymentStatus = entity.PaymentStatus,
                  Description = entity.Description,
                  PlateNumber = entity.PlateNumber,
                  CompanyId = entity.CompanyId,
                  DealerId = entity.DealerId,
                  ShowroomId = entity.ShowroomId
              };
    }
}
