﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Core
{
    public interface IPickListService
    {
        IList<IPickListDto> GetCompaniesAsPickListForUser(UserDetailDto user);
        
        IList<IPickListDto> GetDealersAsPickListForUser(UserDetailDto user);

        IList<IPickListDto> GetDealersAsPickList();
        
        IList<IPickListDto> GetShowroomAsPickListForUser(UserDetailDto user);

        IList<ActivityPriorityDto> GetActivityPriorityList();

        IList<ActivityStatusDto> GetActivityStatusList();
        
        IList<EventStatusDto> GetEventStatusList();
        
        IList<CalendarEventTypeDto> GetEventTypeList();
        
        IList<IPickListDto> GetAllCurrenciesList();
        
        IList<IPickListDto> GetAllLanguagesList();

        IList<IPickListDto> GetAllCities();

        IList<IPickListDto> GetAllDistricts(IList<IPickListDto> allCities);

        IList<IPickListDto> GetAllTimeZoneList();

        IList<IPickListDto> GetAllSupportedDateFormatList();

        IList<IPickListDto> GetSalutationList();
        
        IList<LeadSourceDto> GetLeadSources();

        IList<IPickListDto> GetCarMakeList();
    }
}
