﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core.Dto;

namespace DEA.CRM.Service.Contract.Core
{
    public class UpdateSettingValueCommand
    {
        public int SettingId { get; set; }
        public string SettingValue { get; set; }
    }

    public interface ISettingService
    {
        /// <summary>
        /// Get all setting groups
        /// </summary>
        /// <returns></returns>
        List<string> Groups();

        /// <summary>
        /// Get all setting by setting group
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        List<SettingDto> ByGroup(string groupName);

        string GetValue(SettingIds setting);

        bool IsAffirmative(SettingIds setting);

        SettingDto UpdateSettingValue(UpdateSettingValueCommand command, UserDetailDto currentUser);

        void RefreshSettingCache(int settingId);

        SettingDto Get(int settingId, bool includeValueList);

        List<SettingDto> Get(int[] settingIds);

        FileSystemAttachmentConfigurationDto GetLocalFileSystemAttachmentConfiguration();

        AzureStorageConfigurationDto GetAzureStorageConfiguration();

        S3BucketConfigurationDto GetS3BucketConfiguration();
    }
}
