﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Core
{
    public class FindUserByNameQuery
    {
        public string UserName { get; set; }
        public bool IsActive { get; set; }

        public static FindUserByNameQuery ActiveUserQuery(string userName)
            => new FindUserByNameQuery
            {
                IsActive = true,
                UserName = userName
            };
    }

    public class FindUserByKeyQuery
    {
        public string UserKey { get; set; }
        public bool IsActive { get; set; }

        public static FindUserByKeyQuery ActiveUserQuery(string userKey)
            => new FindUserByKeyQuery
            {
                IsActive = true,
                UserKey = userKey
            };
    }

    public class SearchUserQuery : KeywordSearchQuery
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public bool? IsAdmin { get; set; }
        public string Status { get; set; }
    }

    public class BaseUserCommand
    {
        public string UserKey { get; set; }
    }

    public class UpdateUserCommand : UserDetailDto
    {
        public string UserAvatarDataUrl { get; set; }
        public AttachmentUploadInfo UserAvatarDataFileInfo { get; set; }
    }

    public class DeactivateUserCommand : BaseUserCommand
    {
        public string TransferOwnershipToUserKey { get; set; }
    }

    public interface IUserInfoService
    {
        PagingResult<UserDetailSearchResultDto> SearchUsers(SearchUserQuery query);

        /// <summary>
        /// Get list of all users that current user can view
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Result<List<UserDetailSearchResultDto>> FindAllUsersForAssignedUser(UserDetailDto currentUser);

        Task<UserDetailDto> CreateOrUpdateAsync(UpdateUserCommand command, UserDetailDto currentUserDto);

        BasicUserDetailDto GetBasicUserDetailByUserName(FindUserByNameQuery query, bool refresh);

        BasicUserDetailDto GetBasicUserDetailByUserKey(FindUserByKeyQuery query, bool refresh);

        UserDetailDto GetFullUserDetailByUserName(FindUserByNameQuery query, bool refresh);

        UserDetailDto GetFullUserDetailByUserKey(FindUserByKeyQuery query, bool refresh);

        UserDetailDto GetDefaultForNewUser();

        Dictionary<string, IList<IPickListDto>> GetPickListsForUserDetailForm(UserDetailDto currentUserDto); 

        Result<ResponseDto> DeactivateUser(DeactivateUserCommand command);

        string GetUserFullName(string userKey);
    }
}
