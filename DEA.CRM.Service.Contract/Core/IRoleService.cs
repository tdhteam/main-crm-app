﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Core
{
    public class CreateOrUpdateRoleCommand
    {
        public CreateOrUpdateRoleCommand()
        {
            ModulePermissions = new HashSet<ModulePermissionUpdateCommand>();
            CustomPermissions = new HashSet<RoleAssignPermissionDto>();
        }

        public string RoleId { get; set; }
        public string Name { get; set; }
        public string ParentRoleId { get; set; }
        public ICollection<ModulePermissionUpdateCommand> ModulePermissions { get; set; }
        public ICollection<RoleAssignPermissionDto> CustomPermissions { get; set; }
        
    }

    public class ModulePermissionUpdateCommand
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool CanView { get; set; }
        public bool CanCreate { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
    }

    public class DeleteRoleCommand
    {
        public string RoleId { get; set; }
        
        public string TransferOwnershipToRoleId { get; set; }
    }

    public interface IRoleService
    {
        PagingResult<RoleDetailDto> GetAllRoles();

        RoleTreeDto GetRoleTree();

        Result<RoleDetailDto> CreateOrUpdate(CreateOrUpdateRoleCommand roleDetailDto);

        Result<RoleDetailDto> GetDefaultNewRole(string parentRoleId);

        Result<RoleDetailFormDto> CreateRoleDetailFormSchema(string parentRoleId); 

        Result<RoleDetailDto> GetRoleById(string roleId);

        ResponseDto DeleteRole(DeleteRoleCommand command);
    }
}
