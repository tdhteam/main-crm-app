﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Contract.Core
{
    public class CreateOrUpdateProfileCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DirectlyRelatedToRole { get; set; }
        public ICollection<ModulePermissionUpdateCommand> ModulePermissions;
    }
    
    public interface IProfileService
    {
        List<ProfileDto> GetAll();

        Result<ProfileDto> GetById(int id);
        
        Result<ProfileDto> CreateOrUpdate(CreateOrUpdateProfileCommand command);
        
        ResponseDto DeleteProfile(int id);
    }
}
