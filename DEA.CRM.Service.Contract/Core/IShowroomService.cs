﻿using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Core
{
    public class SearchShowroomCommand : Paging
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
    }

    public interface IShowroomService
    {
        PagingResult<ShowroomDto> SearchShowrooms(SearchShowroomCommand command);
        ShowroomDetailFormDto GetDefaultShowroomForNew();
        ShowroomDetailFormDto GetShowroomById(int showroomId);
        ShowroomDetailFormDto CreateOrUpdate(ShowroomDto showroomDto); 
    }
}
