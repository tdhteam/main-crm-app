﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;

namespace DEA.CRM.Service.Contract.Core
{
    public interface ITabService
    {
        List<TabInfoDto> GetAll();
    }
}
