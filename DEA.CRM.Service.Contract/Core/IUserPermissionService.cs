﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;

namespace DEA.CRM.Service.Contract.Core
{
    public enum PermissionOperation
    {
        View = 0,
        Create = 1,
        Edit = 2,
        Delete = 3,
        ViewAll = 4,
        EditAll = 7
    }

    public enum PermissionType
    {
        // Custom Permission map to Id of record in Permission table

        ImportContact = 1,
        ExportContact = 2,
        ImportProduct = 3,
        ExportProduct = 4
    }

    public enum MatchOption
    {
        Some,
        All
    }

    public interface IOptionMatchable<out T>
    {
        T[] ItemsToMatch { get; }
        MatchOption MatchOption { get; }
    }

    public static class OptionMatchReducer
    {
        /// <summary>
        /// Takes a target IOptionMatchable and a predicate/evaluator
        /// then reduces the target's internal items to a boolean value
        /// </summary>
        /// <remarks>
        /// Match :: IOptionMatchable m => m a -> (a, Bool) -> Bool
        /// </remarks>
        public static bool Match<T>(this IOptionMatchable<T> target, Func<T, bool> evaluator)
        {
            var reduce = GetOptionMatchReducer<T>(target.MatchOption);
            return reduce(evaluator, target.ItemsToMatch);
        }

        /// <summary>
        /// Takes a match option, a predicate and a sequence of type T then reduces the sequence to a boolean value.
        /// </summary>
        /// <typeparam name="T">Type of the sequence's item</typeparam>
        /// <param name="opt">Match option</param>
        /// <remarks>
        /// GetOptionMatchReducer :: IEnumerable seq => MatchOption -> ((a -> bool), seq a) -> bool
        /// </remarks>
        private static Func<Func<T, bool>, IEnumerable<T>, bool> GetOptionMatchReducer<T>(MatchOption opt)
        {
            Func<IEnumerable<T>, Func<T, bool>, bool> reducer;
            if (opt == MatchOption.Some)
                reducer = Enumerable.Any;
            else
                reducer = Enumerable.All;

            return Functions.Flip(reducer);
        }
    }

    public class PermissionCheckOpts : IOptionMatchable<PermissionType>
    {
        public PermissionType[] ItemsToMatch { get; private set; }
        public PermissionType[] PermissionsToCheck => ItemsToMatch;

        public MatchOption MatchOption { get; private set; }
        /// <summary>
        /// This prop is reserve for permission delegation feature
        /// </summary>
        public bool IncludeDelegation { get; private set; }

        /// <summary>
        /// Returns a permission check option with MatchOptions defaults to Some and WithDelegation defaults to TRUE
        /// </summary>
        public static implicit operator PermissionCheckOpts(PermissionType permission)
        {
            return IncludeDelegate(permission);
        }

        /// <summary>
        /// Returns a permission check option with MatchOptions defaults to Some and WithDelegation defaults to TRUE
        /// </summary>
        public static PermissionCheckOpts IncludeDelegate(params PermissionType[] permissions)
        {
            return Check(MatchOption.Some, true, permissions);
        }

        /// <summary>
        /// Returns a permission check option with WithDelegation defaults to TRUE
        /// </summary>
        public static PermissionCheckOpts IncludeDelegate(MatchOption matchOption, params PermissionType[] permissions)
        {
            return Check(matchOption, true, permissions);
        }


        public static PermissionCheckOpts ExcludeDelegate(params PermissionType[] permissions)
        {
            return Check(MatchOption.Some, false, permissions);
        }

        public static PermissionCheckOpts ExcludeDelegate(MatchOption matchOption, params PermissionType[] permissions)
        {
            return Check(matchOption, false, permissions);
        }

        public static PermissionCheckOpts Check(MatchOption matchOption, bool withDelegation, params PermissionType[] permissions)
        {
            return new PermissionCheckOpts
            {
                MatchOption = matchOption,
                ItemsToMatch = permissions,
                IncludeDelegation = withDelegation
            };
        }
    }

    public interface IUserPermissionService
    {
        bool HasBasicPermission(IUserDetailDto userDetailDto, string moduleName, PermissionOperation operation);

        bool HasCustomPermission(IUserDetailDto userDetailDto, PermissionCheckOpts opts);
    }

    
}
