﻿using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Contract.Core
{
    public class CreateOrUpdateCompanyCommand: CompanyDto
    {
    }

    public interface ICompanyService
    {
        Result<CompanyDto> GetCompanyById(int id);
        Result<CompanyDto> GetFirstCompany();
        Result<CompanyDto> CreateOrUpdate(CreateOrUpdateCompanyCommand companyDto);
    }
}
