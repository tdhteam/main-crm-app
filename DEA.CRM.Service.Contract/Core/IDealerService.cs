﻿using System.Threading.Tasks;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Core
{
    public class SearchDealerCommand  : Paging
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
    }

    public interface IDealerService
    {
        PagingResult<DealerDto> SearchDealers(SearchDealerCommand command);
        DealerDto GetDealerById(int dealerId);
        DealerDto GetDefaultDealer();
        Task<DealerDto> CreateOrUpdateAsync(DealerDto dealerDto); 
    }
} 
