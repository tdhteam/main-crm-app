﻿using System;
using System.Collections.Generic;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class PermissionDto
    {
        public PermissionDto()
        {
            ProfileToCustomPermissions = new HashSet<ProfileToCustomPermissionDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }

        public ICollection<ProfileToCustomPermissionDto> ProfileToCustomPermissions { get; set; }

        public static readonly Func<Permission, PermissionDto> EntityToDtoMapper = entity =>
            new PermissionDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder
            };

        public static readonly Func<PermissionDto, Permission> DtoToEntityMapper = dto =>
            new Permission
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                DisplayOrder = dto.DisplayOrder
            };
    }

    public class ProfileToCustomPermissionDto
    {
        public int ProfileId { get; set; }
        public int PermissionId { get; set; }

        public ProfileDto Profile { get; set; }
        public PermissionDto Permission { get; set; }
    }

}
