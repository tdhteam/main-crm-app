﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserInfoToShowroomDto
    {
        public string UserKey { get; set; }
        public int ShowroomId { get; set; }
        public string ShowroomName { get; set; }
        public int ShowroomDealerId { get; set; }
    }
}
