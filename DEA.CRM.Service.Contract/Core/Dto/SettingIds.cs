﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public enum SettingIds
    {
        FileSystemAttachmentRootPath = 1,

        AzureStorageConnectionString = 2,
        AzureBlobStorageRootContainerName = 3,

        S3RegionEndpointName = 4,
        S3AccessKeyId = 5,
        S3SecretAccessKey = 6,
        S3RootBucketName = 7,

        AppApiBaseUrl = 100
    }
}
