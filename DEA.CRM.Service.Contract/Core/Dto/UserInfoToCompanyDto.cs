﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserInfoToCompanyDto
    {
        public string UserKey { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}
