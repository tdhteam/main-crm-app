﻿using System;
using System.Collections.Generic;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class DealerDto
    {
        public DealerDto()
        {
            Showrooms = new HashSet<ShowroomDto>();
        }

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string LogoRelativeUrl { get; set; }
        public string CompanyCode { get; set; }

        public ICollection<ShowroomDto> Showrooms { get; set; }

        public static readonly Func<Dealer, DealerDto> EntityToDtoMapper = entity =>
            MapperHelper.AutoMap<Dealer, DealerDto>()(entity);

        public static readonly Func<DealerDto, Dealer> DtoToEntityMapper = entity =>
            MapperHelper.AutoMap<DealerDto, Dealer>()(entity);
    }
}
