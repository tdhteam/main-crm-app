﻿using System;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserInfoToRoleDto
    {
        public string UserKey { get; set; }
        public string RoleId { get; set; }

        public static Func<UserInfoToRole, UserInfoToRoleDto> EntityToDtoMapper = entity =>
            new UserInfoToRoleDto
            {
                UserKey = entity.UserKey,
                RoleId = entity.RoleId
            };
    }
}
