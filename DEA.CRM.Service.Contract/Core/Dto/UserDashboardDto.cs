﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserDashboardDto
    {
        public int Id { get; set; }
        public string UserKey { get; set; }
        public string Name { get; set; }
        public string WidgetSettings { get; set; }
    }
}
