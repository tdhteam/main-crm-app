﻿using System;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ProfileToStandardPermissionDto
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int TabId { get; set; }
        public int Operation { get; set; }
        public bool Permission { get; set; }

        public static readonly Func<ProfileToStandardPermission, ProfileToStandardPermissionDto> EntityToDtoMapper =
            entity =>
                new ProfileToStandardPermissionDto
                {
                    Id = entity.Id,
                    ProfileId = entity.ProfileId,
                    TabId = entity.TabId,
                    Operation = entity.Operation,
                    Permission = entity.Permission
                };
    }
}
