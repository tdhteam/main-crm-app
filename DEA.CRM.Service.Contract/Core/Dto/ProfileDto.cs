﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ProfileDto
    {
        public ProfileDto()
        {
            ProfileToStandardPermissions = new HashSet<ProfileToStandardPermissionDto>();
            ProfileToTabs = new HashSet<ProfileToTabDto>();
            RoleToProfiles = new HashSet<RoleToProfileDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DirectlyRelatedToRole { get; set; }

        public ICollection<ProfileToStandardPermissionDto> ProfileToStandardPermissions{ get; set; }
        public ICollection<ProfileToTabDto> ProfileToTabs { get; set; }
        public ICollection<RoleToProfileDto> RoleToProfiles { get; set; }
        
        public static readonly Func<Profile, ProfileDto> EntityToDtoMapper = entity =>
            new ProfileDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                DirectlyRelatedToRole = entity.DirectlyRelatedToRole,
                ProfileToStandardPermissions = entity.ProfileToStandardPermissions
                        .Select(ProfileToStandardPermissionDto.EntityToDtoMapper).ToList(),
                RoleToProfiles = entity.RoleToProfiles.Select(RoleToProfileDto.EntityToDtoMapper).ToList()
            };
    }
}
