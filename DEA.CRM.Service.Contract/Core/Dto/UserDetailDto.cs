﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public interface IBasicUserDetailDto
    {
        string UserKey { get; set; }
        string UserName { get; set; }
        string FullName { get; }
        bool IsAdmin { get; set; }
    }

    public interface IUserDetailDto : IBasicUserDetailDto
    {
        IDictionary<string, ModulePermissionDto> StandardModulePermissions { get; set; }
        ICollection<PermissionDto> CustomPermissions { get; set; }
        bool HasPermission(PermissionType permission);
        bool HasAnyGivenPermissions(params PermissionType[] permissions);
        bool HasAllGivenPermissions(params PermissionType[] permissions);
    }

    public class BasicUserDetailDto : IBasicUserDetailDto
    {
        public string UserKey { get; set; }
        public string UserName { get; set; }
        public string CalendarColor { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => string.IsNullOrEmpty(FirstName) ? LastName : $"{FirstName} {LastName}";
        public string ReportToUserKey { get; set; }
        public bool IsAdmin { get; set; }
        public int CurrencyId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Department { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string OtherPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }

        public bool InActive => GlobalConstants.StatusInActive == Status;

        public static readonly Func<BasicUserDetailDto, UserInfo> BasicDtoToEntityMapper =
            MapperHelper.AutoMap<BasicUserDetailDto, UserInfo>();

        public static readonly Func<UserInfo, BasicUserDetailDto> BasicEntityToDtoMapper =
            MapperHelper.AutoMap<UserInfo, BasicUserDetailDto>();
    }

    public class UserDetailSearchResultDto : BasicUserDetailDto
    {
        public new string FullName { get; set; }

        public static readonly Func<UserDetailSearchResultDto, UserInfo> DtoToEntityMapper =
            MapperHelper.AutoMap<UserDetailSearchResultDto, UserInfo>();

        public static readonly Func<UserInfo, UserDetailSearchResultDto> EntityToDtoMapper =
            MapperHelper.AutoMap<UserInfo, UserDetailSearchResultDto>();

        public static readonly Func<UserInfoSearch, UserDetailSearchResultDto> SearchEntityToDtoMapper =
            MapperHelper.AutoMap<UserInfoSearch, UserDetailSearchResultDto>();
    }

    public class UserDetailDto : BasicUserDetailDto, IUserDetailDto
    {
        public UserDetailDto()
        {
            UserDashboard = new HashSet<UserDashboardDto>();
            UserInfoToCompany = new HashSet<UserInfoToCompanyDto>();
            UserInfoToDealer = new HashSet<UserInfoToDealerDto>();
            UserInfoToShowroom =new HashSet<UserInfoToShowroomDto>();
            CustomPermissions = new HashSet<PermissionDto>();
            StandardModulePermissions = new Dictionary<string, ModulePermissionDto>();
        }

        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedByUserKey { get; set; }
        public string ModifiedByUserFullName { get; set; }
        public string AddressStreet { get; set; }
        public string AddressDistrict { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressPostalCode { get; set; }
        public string DateFormat { get; set; }
        public string HourFotmat { get; set; }
        public bool Deleted { get; set; }
        public int? ReminderIntervalInSeconds { get; set; }
        public string Language { get; set; }
        public string TimeZone { get; set; }
        public string CurrencyGroupingPattern { get; set; }
        public string CurrencyDecimalSeparator { get; set; }
        public string CurrencyGroupingSeparator { get; set; }
        public string CurrencySymbolPlacement { get; set; }
        public int NumberOfCurrencyDecimal { get; set; }
        public string AvatarRelativeUrl { get; set; }
        public bool AccessAllCompanies { get; set; }
        public bool AccessAllDealers { get; set; }
        public bool AccessAllShowrooms { get; set; }
        public int? DefaultCompanyId { get; set; }
        public int? DefaultDealerId { get; set; }
        public int? DefaultShowroomId { get; set; }

        // Computed properties
        public bool NeedFirstTimeLoginSetup =>
            string.IsNullOrEmpty(Language)
            || string.IsNullOrEmpty(DateFormat)
            || string.IsNullOrEmpty(TimeZone)
            || !DefaultCompanyId.HasValue
            || !DefaultDealerId.HasValue
            || !DefaultShowroomId.HasValue;

        public UserInfoToRoleDto UserRole { get; set; }
        public ICollection<UserDashboardDto> UserDashboard { get; set; }
        public ICollection<UserInfoToCompanyDto> UserInfoToCompany { get; set; }
        public ICollection<UserInfoToDealerDto> UserInfoToDealer { get; set; }
        public ICollection<UserInfoToShowroomDto> UserInfoToShowroom { get; set; }
        public IDictionary<string, ModulePermissionDto> StandardModulePermissions { get; set; }
        public ICollection<PermissionDto> CustomPermissions { get; set; }

        public static readonly Func<UserDetailDto, UserInfo> DtoToEntityMapper =
            MapperHelper.AutoMap<UserDetailDto, UserInfo>();

        public static readonly Func<UserInfo, UserDetailDto> EntityToDtoMapper =
            MapperHelper.AutoMap<UserInfo, UserDetailDto>();

        /// <summary>
        /// Check to see if the user has a specific permission
        /// </summary>
        public bool HasPermission(PermissionType permission)
        {
            return IsAdmin || CustomPermissions.Any(p => p.Id == (int)permission);
        }

        /// <summary>
        /// Check to see if the user has any permission in a given permission list
        /// </summary>
        public bool HasAnyGivenPermissions(params PermissionType[] permissions)
        {
            return IsAdmin || 
                permissions
                .Cast<int>()
                .Intersect(CustomPermissions?.Select(p => p.Id) ?? new int[0])
                .Any();
        }

        /// <summary>
        /// Check to see if the user has all permissions in a given permission list
        /// </summary>
        public bool HasAllGivenPermissions(params PermissionType[] permissions)
        {
            var givenPermissions = new HashSet<int>(permissions.Cast<int>());
            return IsAdmin || 
                    givenPermissions
                       .Intersect(CustomPermissions.Select(p => p.Id))
                       .Count() == givenPermissions.Count;
        }
    }
}
