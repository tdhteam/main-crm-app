﻿using System;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ShowroomDto
    {
        public int Id { get; set; }
        public int DealerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string DealerName { get; set; }

        public static readonly Func<Showroom, ShowroomDto> EntityToDtoMapper = entity =>
            MapperHelper.AutoMap<Showroom, ShowroomDto>()(entity);

        public static readonly Func<ShowroomDto, Showroom> DtoToEntityMapper = entity =>
            MapperHelper.AutoMap<ShowroomDto, Showroom>()(entity);
    }
}
