﻿using System;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    /// <summary>
    /// aka business Module in CRM
    /// </summary>
    public class TabInfoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public string Label { get; set; }
        public int? ParentTabId { get; set; }
        public string ParentTabLabel { get; set; }

        public static readonly Func<Tab, TabInfoDto> EntityToDtoMapper = entity 
            => new TabInfoDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Sequence = entity.Sequence,
                Label = entity.Label,
                ParentTabId = entity.ParentTabId,
                ParentTabLabel = entity.ParentTabLabel
            };
    }
}
