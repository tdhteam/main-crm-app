﻿using System;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class RoleToProfileDto
    {
        public string RoleId { get; set; }
        public int ProfileId { get; set; }

        public static Func<RoleToProfile, RoleToProfileDto> EntityToDtoMapper = entity =>
            new RoleToProfileDto
            {
                RoleId = entity.RoleId,
                ProfileId = entity.ProfileId
            };
    }
}
