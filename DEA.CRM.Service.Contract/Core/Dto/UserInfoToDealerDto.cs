﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserInfoToDealerDto
    {
        public string UserKey { get; set; }
        public int DealerId { get; set; }
        public string DealerName { get; set; }
        public int DealerCompanyId { get; set; }
    }
}
