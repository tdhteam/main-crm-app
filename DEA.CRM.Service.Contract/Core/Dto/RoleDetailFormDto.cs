﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    /// <summary>
    /// aka the entire role detail form
    /// </summary>
    public class RoleDetailFormDto : FormResponseDto<RoleDetailDto>
    {
        public RoleDetailFormDto()
        {
            FormData = new RoleDetailDto();
            PickListValues = new Dictionary<string, IList<IPickListDto>>();
        }
    }
}
