﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserDetailFormDto : FormResponseDto<UserDetailDto>
    {
        public UserDetailFormDto()
        {
            FormData = new UserDetailDto();
            PickListValues = new Dictionary<string, IList<IPickListDto>>();
        }
    }
}
