﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class RoleTreeNodeDto
    {
        public RoleTreeNodeDto()
        {
            Children = new List<RoleTreeNodeDto>();
        }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("parentkey")]
        public string ParentKey { get; set; }

        /// <summary>
        /// The primary label for the node
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Secondary label for the node
        /// </summary>
        [JsonProperty("subtitle")]
        public string SubTitle { get; set; }

        [JsonProperty("expanded")] public bool Expanded { get; set; } = true;

        [JsonProperty("children")]
        public IList<RoleTreeNodeDto> Children { get; set; }

        public static readonly Func<RoleDetailDto, RoleTreeNodeDto> RoleDtoToRoleNodeMapper = dto =>
            new RoleTreeNodeDto
            {
                Key = dto.RoleId,
                Title = dto.Name,
                SubTitle = dto.TreePath
            };
    }

    public class RoleTreeDto
    {
        /// <summary>
        /// A dictionary keep track of all nodes in the tree for fast access to the node
        /// </summary>
        private readonly Dictionary<string, RoleTreeNodeDto> _allNodes;

        public RoleTreeDto()
        {
            RootNodes = new List<RoleTreeNodeDto>();
            _allNodes = new Dictionary<string, RoleTreeNodeDto>();
        }

        /// <summary>
        /// List of root node prepresent the tree
        /// </summary>
        [JsonProperty("rootNodes")]
        public IList<RoleTreeNodeDto> RootNodes { get; }

        public RoleTreeNodeDto AddRoot(RoleTreeNodeDto nodeToAdd)
        {
            RootNodes.Add(nodeToAdd);
            _allNodes.TryAdd(nodeToAdd.Key, nodeToAdd);
            return nodeToAdd;
        }

        public RoleTreeNodeDto AddNode(RoleTreeNodeDto nodeToAdd, string parentNodeKey)
        {
            if (_allNodes.TryGetValue(parentNodeKey, out RoleTreeNodeDto parentNode))
            {
                parentNode.Children.Add(nodeToAdd);
                _allNodes.TryAdd(nodeToAdd.Key, nodeToAdd);
            }
            return nodeToAdd;
        }
    }
}
