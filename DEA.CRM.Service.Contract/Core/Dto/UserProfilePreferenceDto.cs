﻿using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class UserProfilePreferenceDto
    {
        public string UserKey { get; set; }
        public string TimeZone { get; set; }
        public string Language { get; set; }
        public string DateFormat { get; set; }
        public string CurrencyCode { get; set; }
        public int? DefaultCompanyId { get; set; }
        public int? DefaultDealerId { get; set; }
        public int? DefaultShowroomId { get; set; }
    }

    public class UserProfilePreferenceFormDto : FormResponseDto<UserProfilePreferenceDto>
    {
    }
}
