﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ShowroomDetailFormDto: FormResponseDto<ShowroomDto>
    {
        public ShowroomDetailFormDto()
        {
            FormData = new ShowroomDto();
            PickListValues = new Dictionary<string, IList<IPickListDto>>();
        }
    }
}
