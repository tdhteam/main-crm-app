﻿using System;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class SettingDto
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SettingValue { get; set; }
        public string SettingGroup { get; set; }
        public int? PickListId { get; set; }

        public static readonly Func<Setting, SettingDto> EntityToDtoMapper =
            MapperHelper.AutoMap<Setting, SettingDto>();
    }
}
