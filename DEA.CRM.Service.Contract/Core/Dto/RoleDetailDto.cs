﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Model;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class RoleAssignPermissionDto : PermissionDto
    {
        public bool IsAssigned { get; set; }

        public static readonly Func<Permission, RoleAssignPermissionDto> NonAssignedMapper = entity =>
            new RoleAssignPermissionDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                IsAssigned = false
            };
    }

    public class RoleDetailDto
    {
        public RoleDetailDto()
        {
            RoleToProfiles = new HashSet<RoleToProfileDto>();
            UserInfoToRoles = new HashSet<UserInfoToRoleDto>();
            ModulePermissions = new HashSet<ModulePermissionDto>();
            CustomPermissions = new HashSet<RoleAssignPermissionDto>();
        }

        public string RoleId { get; set; }
        public string Name { get; set; }
        public string ParentRoleId { get; set; }
        public string ParentRoleName { get; set; }
        public int Depth { get; set; }
        public string TreePath { get; set; }

        public ICollection<RoleToProfileDto> RoleToProfiles { get; set; }
        public ICollection<UserInfoToRoleDto> UserInfoToRoles { get; set; }
        public ICollection<ModulePermissionDto> ModulePermissions { get; set; }
        public ICollection<RoleAssignPermissionDto> CustomPermissions { get; set; }

        public static readonly Func<Role, RoleDetailDto> EntityToDtoMapper = entity =>
            new RoleDetailDto
            {
                RoleId = entity.RoleId,
                Name = entity.Name,
                ParentRoleId = entity.ParentRoleId,
                ParentRoleName = entity.ParentRoleName,
                Depth = entity.Depth,
                TreePath = entity.TreePath,
                RoleToProfiles = entity.RoleToProfile.Select(RoleToProfileDto.EntityToDtoMapper).ToList(),
                UserInfoToRoles = entity.UserInfoToRole.Select(UserInfoToRoleDto.EntityToDtoMapper).ToList()
            };

        public static RoleDetailDto NewDefaultRoleDto(string parentRoleId, 
                string parentRoleName, 
                IList<TabInfoDto> moduleList,
                IList<RoleAssignPermissionDto> allPermissions)
            => new RoleDetailDto
            {
                ParentRoleId = parentRoleId,
                ParentRoleName = parentRoleName,
                ModulePermissions = ModulePermissionDto.BuildPermissionForModules(moduleList),
                CustomPermissions = allPermissions
            };
    }
}
