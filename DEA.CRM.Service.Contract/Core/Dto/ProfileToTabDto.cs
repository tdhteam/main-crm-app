﻿namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ProfileToTabDto
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int TabId { get; set; }
        public bool Permission { get; set; }
    }
}
