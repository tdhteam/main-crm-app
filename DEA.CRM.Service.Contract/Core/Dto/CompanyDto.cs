﻿using System;
using System.Collections.Generic;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class CompanyDto
    {
        public CompanyDto()
        {
            Dealers = new HashSet<DealerDto>();
        }

        public int Id { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string LogoRelativeUrl { get; set; }

        public ICollection<DealerDto> Dealers { get; set; }

        public static readonly Func<Company, CompanyDto> EntityToDtoMapper = entity =>
            MapperHelper.AutoMap<Company, CompanyDto>()(entity);

        public static readonly Func<CompanyDto, Company> DtoToEntityMapper = entity =>
            MapperHelper.AutoMap<CompanyDto, Company>()(entity);

    }
}
