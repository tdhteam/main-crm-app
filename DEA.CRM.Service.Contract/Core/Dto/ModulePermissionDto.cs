﻿using System.Collections.Generic;
using System.Linq;

namespace DEA.CRM.Service.Contract.Core.Dto
{
    public class ModulePermissionDto
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool CanAccessModule => CanView && CanCreate && CanEdit && CanDelete;
        public bool CanView { get; set; }
        public bool CanCreate { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }

        public static ICollection<ModulePermissionDto> BuildPermissionForModules(IList<TabInfoDto> moduleList)
        {
            return moduleList?.Select(m => AllPermission(m.Id, m.Name)).ToList();
        }
        
        public static ICollection<ModulePermissionDto> BuildNoAccessPermissionForModules(IList<TabInfoDto> moduleList)
        {
            return moduleList?.Select(m => NoPermission(m.Id, m.Name)).ToList();
        }

        public static ModulePermissionDto AllPermission(int moduleId, string moduleName)
            => new ModulePermissionDto
            {
                ModuleId = moduleId,
                ModuleName = moduleName,
                CanView = true,
                CanCreate = true,
                CanEdit = true,
                CanDelete = true
            };

        public static ModulePermissionDto NoPermission(int moduleId, string moduleName)
            => new ModulePermissionDto
            {
                ModuleId = moduleId,
                ModuleName = moduleName,
                CanView = false,
                CanCreate = false,
                CanEdit = false,
                CanDelete = false
            };
    }
}