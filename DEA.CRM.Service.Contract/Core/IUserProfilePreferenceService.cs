﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Contract.Core
{
    /// <summary>
    /// Business service to deal with personal user preference
    /// </summary>
    public interface IUserProfilePreferenceService
    {
        Result<UserProfilePreferenceFormDto> GetCurrentUserRequiredPreference(UserDetailDto currentUser);

        Result<UserDetailDto> SaveBasicProfilePreference(UserProfilePreferenceDto profilePreferenceDto,
            UserDetailDto currentUserDto);
    }
}
