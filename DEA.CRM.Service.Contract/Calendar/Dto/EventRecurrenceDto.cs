﻿using System;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class EventRecurrenceDto
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public DateTime RecurringDate { get; set; }
        public string RecurringType { get; set; }
        public int RecurringFrequency { get; set; }
        public string RecurringInfo { get; set; }
        public DateTime RecurringEndDate { get; set; }
    }
}
