﻿using System;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class CalendarEventListViewSearchResultDto
    {
        public string UniqueKey { get; set; }
        public int Id { get; set; }
        public string EntityType { get; set; } // Activity or CalendarEvent
        public string ActivityType { get; set; }
        public string Subject { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? DueDateTime { get; set; }
        public int? DurationHours { get; set; }
        public int? DurationMinutues { get; set; }
        public string Status { get; set; }
        public string EventStatus { get; set; }
        public string Priority { get; set; }
        public string Location { get; set; }
        public string Visibility { get; set; }
        public string OwnerUserKey { get; set; }
        public string OwnerUserFullName { get; set; }
        public int? RelatedToContactId { get; set; }
        public int? RelatedToHandoverContractId { get; set; }
        public int? RelatedToCustomerServiceId { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int? DealerId { get; set; }
        public string DealerName { get; set; }
        public int? ShowroomId { get; set; }
        public string ShowroomName { get; set; }

        public static readonly Func<CalendarEventListViewSearchResult, CalendarEventListViewSearchResultDto>
            EntityToDtoMapper = MapperHelper
                .AutoMap<CalendarEventListViewSearchResult, CalendarEventListViewSearchResultDto>();

    }
}
