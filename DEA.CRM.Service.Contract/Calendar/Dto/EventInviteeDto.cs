﻿namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class EventInviteeDto
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string AssigneeUserKey { get; set; }
        public string AssigneeUserFullName { get; set; }
    }
}
