﻿namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class ActivityReminderDto
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public string AssigneeUserKey { get; set; }
        public string AssigneeUserFullName { get; set; }
    }
}
