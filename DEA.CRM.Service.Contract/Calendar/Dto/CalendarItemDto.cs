﻿namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    /// <summary>
    /// Calendar item represent calendar event or activity
    /// </summary>
    public class CalendarItemDto : BaseCalendarEventLikeDto
    {
    }
}
