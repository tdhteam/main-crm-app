﻿using System;
using System.Collections.Generic;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class ActivityDto : BaseCalendarEventLikeDto
    {
        public ActivityDto()
        {
            ActivityAssignment = new HashSet<ActivityAssignmentDto>();
        }
        
        public ICollection<ActivityAssignmentDto> ActivityAssignment { get; set; }

        public static readonly Func<ActivityDto, Activity> DtoToEntityMapper =
            MapperHelper.AutoMap<ActivityDto, Activity>();

        public static readonly Func<Activity, ActivityDto, Activity> DtoToEntityMapperForUpdate = (entity, dto) =>
        {
            entity.Subject = dto.Subject;
            entity.IsRecurring = dto.IsRecurring;
            entity.RecurringType = dto.RecurringType;
            entity.StartDateTime = dto.StartDateTime;
            entity.DueDateTime = dto.DueDateTime;
            entity.SendNotification = dto.SendNotification;
            entity.DurationHours = dto.DurationHours;
            entity.DurationMinutues = dto.DurationMinutues;
            entity.Status = dto.Status;
            entity.EventStatus = dto.EventStatus;
            entity.Priority = dto.Priority;
            entity.Location = dto.Location;
            entity.SendReminder = dto.SendReminder;
            entity.ReminderBeforeDay = dto.ReminderBeforeDay;
            entity.ReminderBeforeHour = dto.ReminderBeforeHour;
            entity.ReminderBeforeMinute = dto.ReminderBeforeMinute;
            entity.Visibility = dto.Visibility;
            entity.OwnerUserKey = dto.OwnerUserKey;
            entity.RelatedToContactId = dto.RelatedToContactId;
            entity.RelatedToHandoverContractId = dto.RelatedToHandoverContractId;
            entity.RelatedToCustomerServiceId = dto.RelatedToCustomerServiceId;
            entity.Description = dto.Description;
            entity.Tags = dto.Tags;
            entity.CompanyId = dto.CompanyId;
            entity.DealerId = dto.DealerId;
            entity.ShowroomId = dto.ShowroomId;

            return entity;
        };

        public static readonly Func<Activity, ActivityDto> EntityToDtoMapper =
            MapperHelper.AutoMap<Activity, ActivityDto>();
    }
}
