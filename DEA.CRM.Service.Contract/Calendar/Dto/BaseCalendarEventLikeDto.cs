﻿using System;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public abstract class BaseCalendarEventLikeDto
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public bool IsRecurring { get; set; }
        public string RecurringType { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? DueDateTime { get; set; }
        public bool? SendNotification { get; set; }
        public int? DurationHours { get; set; }
        public int? DurationMinutues { get; set; }
        public string Status { get; set; }
        public string EventStatus { get; set; }
        public string Priority { get; set; }
        public string Location { get; set; }
        public bool? SendReminder { get; set; }
        public int? ReminderBeforeDay { get; set; }
        public int? ReminderBeforeHour { get; set; }
        public int? ReminderBeforeMinute { get; set; }
        public string Visibility { get; set; }
        public string OwnerUserKey { get; set; }
        public string OwnerUserFullName { get; set; }
        public int? RelatedToContactId { get; set; }
        public int? RelatedToHandoverContractId { get; set; }
        public int? RelatedToCustomerServiceId { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }
    }
}