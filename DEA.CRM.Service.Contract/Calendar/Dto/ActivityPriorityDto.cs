﻿using System;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class ActivityPriorityDto : NameLookupDto, IBasicLookupDto
    {
        public static readonly Func<ActivityPriority, ActivityPriorityDto> EntityToDtoMapper =
            MapperHelper.AutoMap<ActivityPriority, ActivityPriorityDto>();
    }

    public class ActivityStatusDto : NameLookupDto, IBasicLookupDto
    {
        public static readonly Func<ActivityStatus, ActivityStatusDto> EntityToDtoMapper =
            MapperHelper.AutoMap<ActivityStatus, ActivityStatusDto>();
    }

    public class EventStatusDto : NameLookupDto, IBasicLookupDto
    {
        public static readonly Func<EventStatus, EventStatusDto> EntityToDtoMapper =
            MapperHelper.AutoMap<EventStatus, EventStatusDto>();
    }
}
