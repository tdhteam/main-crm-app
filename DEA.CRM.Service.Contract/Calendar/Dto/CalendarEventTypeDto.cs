﻿using System;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class CalendarEventTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
        public string Color { get; set; }

        public static readonly Func<CalendarEventType, CalendarEventTypeDto> EntityToDtoMapper =
            MapperHelper.AutoMap<CalendarEventType, CalendarEventTypeDto>();
    }
}
