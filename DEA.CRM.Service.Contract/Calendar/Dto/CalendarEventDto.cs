﻿using System;
using System.Collections.Generic;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class CalendarEventDto : BaseCalendarEventLikeDto
    {
        public CalendarEventDto()
        {
            EventInvitee = new HashSet<EventInviteeDto>();
            EventRecurrence = new HashSet<EventRecurrenceDto>();
            EventReminder = new HashSet<EventReminderDto>();
        }
        
        public int? CalendarEventTypeId { get; set; }

        public CalendarEventTypeDto CalendarEventType { get; set; }
        public ICollection<EventInviteeDto> EventInvitee { get; set; }
        public ICollection<EventRecurrenceDto> EventRecurrence { get; set; }
        public ICollection<EventReminderDto> EventReminder { get; set; }
        
        public static readonly Func<CalendarEventDto, CalendarEvent> DtoToEntityMapper =
            MapperHelper.AutoMap<CalendarEventDto, CalendarEvent>();

        public static readonly Func<CalendarEvent, CalendarEventDto, CalendarEvent> DtoToEntityMapperForUpdate =
            (entity, dto) =>
            {
                entity.Subject = dto.Subject;
                entity.CalendarEventTypeId = dto.CalendarEventTypeId;
                entity.IsRecurring = dto.IsRecurring;
                entity.RecurringType = dto.RecurringType;
                entity.StartDateTime = dto.StartDateTime;
                entity.DueDateTime = dto.DueDateTime;
                entity.SendNotification = dto.SendNotification;
                entity.DurationHours = dto.DurationHours;
                entity.DurationMinutues = dto.DurationMinutues;
                entity.Status = dto.Status;
                entity.EventStatus = dto.EventStatus;
                entity.Priority = dto.Priority;
                entity.Location = dto.Location;
                entity.SendReminder = dto.SendReminder;
                entity.Visibility = dto.Visibility;
                entity.OwnerUserKey = dto.OwnerUserKey;
                entity.RelatedToContactId = dto.RelatedToContactId;
                entity.RelatedToHandoverContractId = dto.RelatedToHandoverContractId;
                entity.RelatedToCustomerServiceId = dto.RelatedToCustomerServiceId;
                entity.Description = dto.Description;
                entity.Tags = dto.Tags;
                entity.CompanyId = dto.CompanyId;
                entity.DealerId = dto.DealerId;
                entity.ShowroomId = dto.ShowroomId;

                return entity;
            };

        public static readonly Func<CalendarEvent, CalendarEventDto> EntityToDtoMapper =
            MapperHelper.AutoMap<CalendarEvent, CalendarEventDto>(); 
    }
}
