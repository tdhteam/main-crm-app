﻿namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class EventReminderDto
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int ReminderTime { get; set; }
        public int? ReminderSent { get; set; }
        public int? RecurringId { get; set; }
        public string ReminderType { get; set; }
    }
}
