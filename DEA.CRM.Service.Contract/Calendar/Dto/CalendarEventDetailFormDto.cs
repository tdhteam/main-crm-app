﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class CalendarEventDetailFormDto : FormResponseDto<CalendarEventDto>
    {
        public IList<ActivityPriorityDto> PriorityList { get; set; }
        public IList<EventStatusDto> EventStatusList { get; set; }
        public IList<CalendarEventTypeDto> EventTypeList { get; set; }
        public IList<UserDetailSearchResultDto> AssignedToUserList { get; set; }
    }
}
