﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Calendar.Dto
{
    public class ActivityDetailFormDto : FormResponseDto<ActivityDto>
    {
        public IList<ActivityPriorityDto> PriorityList { get; set; }
        public IList<ActivityStatusDto> ActivityStatusList { get; set; }
        public IList<UserDetailSearchResultDto> AssignedToUserList { get; set; }
    }
}
