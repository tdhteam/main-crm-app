﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Calendar
{   
    public class UpdateActivityCommand : ActivityDto
    {
    }

    public class UpdateCalendarEventCommand : CalendarEventDto
    {
    }

    public class SearchCalendarItemQuery : Paging
    {
    }

    public class GetCalendarQuery
    {
        public int Month { get; set; }
    }

    public interface ICalendarEventService
    {
        Result<ActivityDto> CreateOrUpdateActivity(UpdateActivityCommand command, UserDetailDto currentUser);

        Result<ActivityDto> GetNewDefaultActivity(UserDetailDto currentUser);

        Result<ActivityDto> GetActivityById(int id, UserDetailDto currentUser);

        Result<ActivityDetailFormDto> BuildActivityDetailFormSchema(int id, UserDetailDto currentUser);

        ResponseDto DeleteActivityById(int id, UserDetailDto currentUser);

        Result<CalendarEventDto> CreateOrUpdateCalendarEvent(UpdateCalendarEventCommand command, UserDetailDto currentUser);

        Result<CalendarEventDetailFormDto> BuildCalendarEventDetailFormSchema(int id, UserDetailDto currentUser);

        Result<CalendarEventDto> GetNewDefaultCalendarEvent(UserDetailDto currentUser);

        Result<CalendarEventDto> GetCalendarEventById(int id, UserDetailDto currentUser);

        ResponseDto DeleteCalendarEventById(int id, UserDetailDto currentUser);

        PagingResult<CalendarEventListViewSearchResultDto> SearchCalendarItems(SearchCalendarItemQuery query, UserDetailDto currentUser);

        List<EventItemDto> GetCalendarItemsByMonth(GetCalendarQuery query, UserDetailDto currentUser);
    }

}
