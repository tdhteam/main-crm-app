﻿using DEA.CRM.Model;
using System;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactCommentAttachmentDto
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int ContactCommentId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public AttachmentDto Attachment { get; set; }

        public static readonly Func<ContactCommentAttachment, ContactCommentAttachmentDto> EntityToDtoMapper =
        MapperHelper.AutoMap<Model.ContactCommentAttachment, ContactCommentAttachmentDto>();

        public static readonly Func<ContactCommentAttachmentDto, ContactCommentAttachment> DtoToEntityMapper =
          MapperHelper.AutoMap<ContactCommentAttachmentDto, ContactCommentAttachment>();

        
    }
}
