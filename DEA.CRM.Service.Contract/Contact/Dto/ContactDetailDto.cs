﻿using System;
using System.Collections.Generic;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactDetailDto : ContactDto
    {
        public ContactDetailDto()
        {
            ContactMeetingProgresses = new HashSet<ContactMeetingProgressDto>();
            ContactComments = new HashSet<ContactCommentDto>();
        }

        public ICollection<ContactMeetingProgressDto> ContactMeetingProgresses { get; set; }
        public ICollection<ContactCommentDto> ContactComments { get; set; }

        public static readonly Func<ContactDetailDto, Model.Contact> DetailDtoToEntityMapper =
            MapperHelper.AutoMap<ContactDetailDto, Model.Contact>();

        public static readonly Func<Model.Contact, ContactDetailDto> EntityToDetailDtoMapper =
            entity =>
            {
                if (entity == null) return null;
                var contactDto = MapperHelper.AutoMap<Model.Contact, ContactDetailDto>()(entity);
                return contactDto;
            };
    }
}
