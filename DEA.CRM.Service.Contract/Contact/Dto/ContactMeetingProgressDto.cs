﻿using DEA.CRM.Model;
using System;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactMeetingProgressDto
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string CarOfInterest { get; set; }
        public string CarColorOfInterest { get; set; }
        public string Description { get; set; }
        public string MeetingStatus { get; set; }
        public DateTime? DateOfMeeting { get; set; }
        public string OtherNote { get; set; }

        public static readonly Func<Model.ContactMeetingProgress, ContactMeetingProgressDto> EntityToDtoMapper =
          MapperHelper.AutoMap<Model.ContactMeetingProgress, ContactMeetingProgressDto>();

        public static readonly Func<ContactMeetingProgressDto, ContactMeetingProgress> DtoToEntityMapper =
          MapperHelper.AutoMap<ContactMeetingProgressDto, ContactMeetingProgress>();

    }
}
