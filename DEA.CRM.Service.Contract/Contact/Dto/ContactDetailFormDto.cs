﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactDetailFormDto : FormResponseDto<ContactDto>
    {
        public IList<IPickListDto> SalutationList { get; set; }
        public IList<LeadSourceDto> LeadSources { get; set; }
        public IList<UserDetailSearchResultDto> AssignedToUserList { get; set; }
        public IList<IPickListDto> CarMakeList { get; set; }
    }
}
