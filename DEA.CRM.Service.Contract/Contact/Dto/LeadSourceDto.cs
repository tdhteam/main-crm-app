﻿using DEA.CRM.Model;
using System;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class LeadSourceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }

        public static readonly Func<LeadSource, LeadSourceDto> EntityToDtoMapper =
            MapperHelper.AutoMap<LeadSource, LeadSourceDto>();

    }
}
