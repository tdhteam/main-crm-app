﻿using System;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactDto
    {
        public int Id { get; set; }
        public string ContactNo { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public string OfficePhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string ApproachingType { get; set; }
        public string AddressStreet { get; set; }
        public string AddressDistrict { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressPostalCode { get; set; }
        public string Occupation { get; set; }
        public string Title { get; set; }
        public string PaymentType { get; set; }
        public string CarIsUsing { get; set; }
        public string ContactSource { get; set; }
        public DateTime? PlanToBuyDate { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }
        public DateTime? FirstMeetingDate { get; set; }
        public string HandlerUserKey { get; set; }
        public string ContactType { get; set; }
        public string CompanyName { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyOwner { get; set; }
        public string CompanyWebSite { get; set; }
        public string CompanyEmail { get; set; }
        public string ContactNote { get; set; }
        public string Tags { get; set; }

        // Computed properties
        public string HandlerUserFullName { get; set; }

        public static readonly Func<ContactDto, Model.Contact> DtoToEntityMapper =
            MapperHelper.AutoMap<ContactDto, Model.Contact>();

        public static readonly Func<Model.Contact, ContactDto> EntityToDtoMapper =
            entity =>
            {
                if (entity == null) return null;
                var contactDto = MapperHelper.AutoMap<Model.Contact, ContactDto>()(entity);
                return contactDto;
            };
    }
}
