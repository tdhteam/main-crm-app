﻿using DEA.CRM.Model;
using System;
using System.Collections.Generic;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.Contact.Dto
{
    public class ContactCommentDto
    {
        public int Id { get; set; }
        public string CommentContent { get; set; }
        public int ContactId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static readonly Func<ContactComment, ContactCommentDto> EntityToDtoMapper =
          MapperHelper.AutoMap<Model.ContactComment, ContactCommentDto>();

        public static readonly Func<ContactCommentDto, ContactComment> DtoToEntityMapper =
          MapperHelper.AutoMap<ContactCommentDto, ContactComment>();

        public ContactCommentDto()
        {
            ContactCommentAttachments = new HashSet<ContactCommentAttachmentDto>();
        }

        public ICollection<ContactCommentAttachmentDto> ContactCommentAttachments { get; set; }

    }
}
