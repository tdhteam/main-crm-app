﻿using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Contact
{
    public interface ILeadSourceService
    {
        PagingResult<LeadSourceDto> GetAllLeadSources();
    }
}
