﻿using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core.Dto;

namespace DEA.CRM.Service.Contract.Contact
{

    public class CreateOrUpdateContactCommand: ContactDetailDto
    {
    }

    public class CreateOrUpdateContactMeetingCommand : ContactMeetingProgressDto
    {
    }

    public class CreateOrUpdateContactCommentCommand : ContactCommentDto
    {
    }

    public class CreateOrUpdateContactCommentAttachmentCommand : ContactCommentAttachmentDto
    {
    }

    public class SearchContactCommand : Paging
    {
        public string UserKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string Title { get; set; }
    }

    /// <summary>
    /// Service to handle CRUD Contact, retrieve data for front site to use
    /// </summary>
    public interface IContactService
    {
        PagingResult<ContactDto> SearchContacts(SearchContactCommand command);
        Result<ContactDetailDto> Update(CreateOrUpdateContactCommand command);
        Result<ContactDetailDto> UpdateContactMeetingProgress(CreateOrUpdateContactMeetingCommand command);

        Result<ContactDetailDto> GetById(int id);
        Result<ContactDetailFormDto> BuildContactDetailFormSchema(int id, UserDetailDto currentUser);

        Result<ContactCommentDto> UpdateContactComment(CreateOrUpdateContactCommentCommand command);
        Result<ContactCommentAttachmentDto> UpdateContactCommentAttachment(CreateOrUpdateContactCommentAttachmentCommand command);
    }
}
