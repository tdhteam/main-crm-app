﻿using Newtonsoft.Json;

namespace DEA.CRM.Service.Contract.Dto
{
    public class ResponseDto<T> : ResponseDto
    {
        [JsonProperty("viewModel")]
        public T ViewModel { get; set; }

        public static ResponseDto<T> OkWithData(T viewModel)
            => new ResponseDto<T>
            {
                CustomStatusCode = 200,
                Success = true,
                ViewModel = viewModel
            };
    }
    
    public class ResponseDto
    {
        [JsonProperty("statusCode")]
        public int CustomStatusCode { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errors")]
        public ErrorResponseDto Errors { get; set; }

        public static ResponseDto OkNoContent() => new ResponseDto
        {
            CustomStatusCode = 200,
            Success = true
        };
        
        public static ResponseDto ErrorContent(string error) => new ResponseDto
        {
            CustomStatusCode = 10000,
            Success = false,
            Errors = new ErrorResponseDto
            {
                Source = "api",
                Title = error,
                Detail = error
            }
        };
    }

    public class ErrorResponseDto
    {
        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("detail")]
        public string Detail { get; set; }

        public static ErrorResponseDto ToError(string source, string title, string detail)
        {
            return new ErrorResponseDto
            {
                Source = source,
                Title = title,
                Detail = detail
            };
        }
    }
}
