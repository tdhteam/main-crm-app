﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DEA.CRM.Service.Contract.Dto
{
    public class FormResponseDto<TModel> where TModel : class 
    {
        [JsonProperty("formData")]
        public TModel FormData { get; set; }
        
        [JsonProperty("pickListValues")]
        public Dictionary<string, IList<IPickListDto>> PickListValues { get; set; }
    }
}
