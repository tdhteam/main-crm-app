﻿using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Dto
{
    public class KeywordSearchQuery : Paging
    {
        public string Keyword { get; set; }
    }
}
