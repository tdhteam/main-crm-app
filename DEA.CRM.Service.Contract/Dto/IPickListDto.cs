﻿namespace DEA.CRM.Service.Contract.Dto
{
    public interface IPickListDto
    {
        string Key { get; set; }
        string Value { get; set; }
        string ParentKey { get; set; }
    }

    public class KeyValuePickListDto : IPickListDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string ParentKey { get; set; }

        public static IPickListDto Of(string key, string value)
        {
            return new KeyValuePickListDto
            {
                Key = key,
                Value = value
            };
        }
        
        public static IPickListDto Of(string key, string value, string parentKey)
        {
            return new KeyValuePickListDto
            {
                Key = key,
                Value = value,
                ParentKey = parentKey
            };
        }
    }

    public interface IBasicLookupDto
    {
        int Id { get; set; }
        string Name { get; set; }
        bool Presence { get; set; }
        int SortOrderId { get; set; }
    }

    public abstract class NameLookupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
    }
}
