﻿namespace DEA.CRM.Service.Contract.Dto
{
    public class LookUpDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}