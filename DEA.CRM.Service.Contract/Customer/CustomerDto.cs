﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DEA.CRM.Service.Customer
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string CustomerNo { get; set; }
        public int? ConvertedFromContactId { get; set; }
        public string ContactType { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Occupation { get; set; }
        public string OfficePhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string CustomerType { get; set; }
        public string PaymentType { get; set; }
        public string CustomerSource { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public static readonly Func<DEA.CRM.Model.Customer, CustomerDto> EntityToDtoMapper =
            entity => entity == null
                ? null
                : new CustomerDto
                {
                    Id = entity.Id,
                    CustomerNo =  entity.CustomerNo,
                    ConvertedFromContactId = entity.ConvertedFromContactId,
                    ContactType = entity.ContactType,
                    CompanyName = entity.CompanyName,
                    Salutation = entity.Salutation,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    Gender = entity.Gender,
                    DateOfBirth = entity.DateOfBirth,
                    Occupation = entity.Occupation,
                    OfficePhone = entity.OfficePhone,
                    HomePhone = entity.HomePhone,
                    MobilePhone = entity.MobilePhone,
                    CustomerType = entity.CustomerType,
                    PaymentType = entity.PaymentType,
                    CustomerSource = entity.CustomerSource,
                    CompanyId = entity.ConvertedFromContactId,
                    DealerId = entity.ConvertedFromContactId,
                    ShowroomId = entity.ConvertedFromContactId,
                };
    }
}
