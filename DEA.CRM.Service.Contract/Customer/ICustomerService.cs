﻿using DEA.CRM.Utils.Pagination;


namespace DEA.CRM.Service.Customer
{

    public class SearchCustomerCommand : Utils.Pagination.Paging
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public interface ICustomerService
    {
        PagingResult<CustomerDto> SearchCustomers(SearchCustomerCommand command);
    }
}
