﻿namespace DEA.CRM.Service.Contract.UserDocument.Dto
{
    public class DocumentFolderDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
