﻿using System;
using DEA.CRM.Service.Contract.Helpers;

namespace DEA.CRM.Service.Contract.UserDocument.Dto
{
    public class UserDocumentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }        
        public string AttachmentId { get; set; }
        public string DocumentLink { get; set; }
        public string OwnerUserKey { get; set; }
        public string OwnerUserFullName { get; set; }        
        public string FolderName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedByUserKey { get; set; }
        public string ModifiedByUserFullName { get; set; }

        public static readonly Func<Model.UserDocument, UserDocumentDto> EntityToDtoMapper =
            MapperHelper.AutoMap<Model.UserDocument, UserDocumentDto>();

        public static Model.UserDocument DtoToEntityMapper(UserDocumentDto userDocumentDto)
        {
            return new Model.UserDocument
            {
                Name = userDocumentDto.Name,
                Description = userDocumentDto.Description,
                AttachmentId = userDocumentDto.AttachmentId,
                DocumentLink = userDocumentDto.DocumentLink,
                OwnerUserKey = userDocumentDto.OwnerUserKey,
                OwnerUserFullName = userDocumentDto.OwnerUserFullName,
                FolderName = userDocumentDto.FolderName,
                CreatedDate = userDocumentDto.CreatedDate,
                ModifiedDate = userDocumentDto.ModifiedDate,
                ModifiedByUserKey = userDocumentDto.ModifiedByUserKey,
                ModifiedByUserFullName = userDocumentDto.ModifiedByUserFullName
            };
        }
    }
}
