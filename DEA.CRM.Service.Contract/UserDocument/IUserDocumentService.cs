﻿using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.UserDocument.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.UserDocument
{
    public class SearchUserDocumentQuery : Paging
    {
        
    }
    
    public interface IUserDocumentService
    {
        Result<PagingResult<UserDocumentDto>> SearchUserDocuments(SearchUserDocumentQuery query);

        Result<UserDocumentDto> CreateOrUpdate(UserDocumentDto userDocumentDto, UserDetailDto currentUser);
    }
}
