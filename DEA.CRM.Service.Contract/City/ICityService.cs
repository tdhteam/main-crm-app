﻿using System.Collections.Generic;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.Contract.City
{
    public interface ICityService
    {
        IEnumerable<LookUpDto> GetAllCities();
        IEnumerable<LookUpDto> GetDistrictsByCity(string city);
    }
}
