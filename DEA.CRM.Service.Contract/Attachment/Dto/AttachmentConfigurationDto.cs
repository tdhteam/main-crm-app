﻿namespace DEA.CRM.Service.Contract.Attachment.Dto
{
    public class BaseAttachmentConfigurationDto
    {
        public string ThumbnailDirectoryName { get; set; } // Name of directory under [ThumbnailDirectoryPath] to store thumbnail for images
        public int PreferedThumbnailWidth { get; set; }
        public int PreferedThumbnailHeight { get; set; }

        public BaseAttachmentConfigurationDto()
        {
            ThumbnailDirectoryName = "thumbnail";
            PreferedThumbnailWidth = 150;
            PreferedThumbnailHeight = 150;
        }
    }

    public class FileSystemAttachmentConfigurationDto : BaseAttachmentConfigurationDto
    {
        public string AttachmentRootDirectoryPath { get; set; } // File system path to root directory store attachment
        public string ThumbnailRootDirectoryPath { get; set; } // File system path to root directory store thumbnail for images

        public static FileSystemAttachmentConfigurationDto BasicConfiguration(string attachmentRootDirectoryPath)
        {
            return new FileSystemAttachmentConfigurationDto
            {
                AttachmentRootDirectoryPath = attachmentRootDirectoryPath,
                ThumbnailRootDirectoryPath = attachmentRootDirectoryPath,
                ThumbnailDirectoryName = "thumbnail",
                PreferedThumbnailWidth = 150,
                PreferedThumbnailHeight = 150
            };
        }
    }

    public class AzureStorageConfigurationDto : BaseAttachmentConfigurationDto
    {
        public string AzureStorageConnectionString { get; set; }
        public string RootContainerName { get; set; }
    }

    public class S3BucketConfigurationDto : BaseAttachmentConfigurationDto
    {
        public string RegionEndpointName { get; set; }
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
        public string RootBucketName { get; set; }
        public string PublicEndpoint => $"http://s3.{RegionEndpointName}.amazonaws.com/{RootBucketName}/";
    }
}
