﻿namespace DEA.CRM.Service.Contract.Attachment.Dto
{
    public class AttachmentUploadInfo
    {
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public string ContentType { get; set; }
    }
}
