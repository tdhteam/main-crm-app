﻿using System;

namespace DEA.CRM.Service.Contract.Attachment.Dto
{
    public class AttachmentDtoBase
    {
        public AttachmentDtoBase()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string CreatedByUserKey { get; set; }
        public DateTime? DateAttached { get; set; }
        public long FileSize { get; set; }
        public string ContentType { get; set; }
        public string FileUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string DownloadFileUrl { get; set; }
        public string DownloadThumbnailUrl { get; set; }
    }

    public class AttachmentDto : AttachmentDtoBase
    {
        public byte[] FileContentBytes { get; set; }
        public byte[] ThumbnailFileContentBytes { get; set; }
        public int ThumbnailWidth { get; set; }
        public int ThumbnailHeight { get; set; }

        public static readonly Func<Model.Attachment, AttachmentDto> Mapper = entity => new AttachmentDto
        {
            Id = entity.Id,
            FileName = entity.FileName,
            Description = entity.Description,
            CreatedByUserKey = entity.CreatedByUserKey,
            DateAttached = entity.DateAttached,
            FileSize = entity.FileSize,
            FileUrl = entity.FileUrl,
            ContentType = entity.ContentType,
            ThumbnailUrl = entity.ThumbnailUrl,
            DownloadFileUrl = entity.DownloadFileUrl,
            DownloadThumbnailUrl = entity.DownloadThumbnailUrl
        };

        public static readonly Func<AttachmentDto, Model.Attachment> DtoToEntityMapper = dto => new Model.Attachment
        {
            Id = dto.Id,
            FileName = dto.FileName,
            Description = dto.Description,
            CreatedByUserKey = dto.CreatedByUserKey,
            DateAttached = dto.DateAttached,
            FileSize = dto.FileSize,
            FileUrl = dto.FileUrl,
            ContentType = dto.ContentType,
            ThumbnailUrl = dto.ThumbnailUrl,
            DownloadFileUrl = dto.DownloadFileUrl,
            DownloadThumbnailUrl = dto.DownloadThumbnailUrl
        };
        
        public static readonly Func<AttachmentDto, AttachmentDto> ToFileMetaResponse = dto =>
            new AttachmentDto
            {
                Id = dto.Id,
                FileName = dto.FileName,
                Description = dto.Description,
                ContentType = dto.ContentType,
                CreatedByUserKey = dto.CreatedByUserKey,
                DateAttached = dto.DateAttached,
                FileSize = dto.FileSize,
                FileUrl = dto.FileUrl,
                ThumbnailUrl = dto.ThumbnailUrl,
                DownloadFileUrl = dto.DownloadFileUrl,
                DownloadThumbnailUrl = dto.DownloadThumbnailUrl
            };
    }
}
