using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Attachment
{
    public interface IAttachmentMetadataService
    {
        AttachmentDto SaveAttachmentMetadata(AttachmentDto attachmentDto);
        Result<Unit> RemoveAttachmentMetadata(string id);
        Result<AttachmentDto> GetAttachmentMetadata(string id);
        PagingResult<AttachmentDto> FindAttachmentMetadataList(FindAttachmentCommand command, UserDetailDto currentUser);
    }
}