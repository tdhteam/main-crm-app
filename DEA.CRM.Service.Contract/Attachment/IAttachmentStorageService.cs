﻿using System.IO;
using System.Threading.Tasks;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Contract.Attachment
{
    public interface IAttachmentStorageService
    {
        Task<Result<AttachmentDto>> PutStreamToStorageAsync(Stream dataStream, AttachmentDto attachmentMetaDto, int blockNumber);
        
        Task<Result<AttachmentDto>> CommitStreamToStorageAsync(AttachmentDto attachmentMetaDto);
        
        Task<Result<AttachmentDto>> SaveAttachmentContentToStorageAsync(AttachmentDto attachmentDto);
        
        Task<Result<AttachmentDto>> LoadAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto);
        
        Task<Result<Unit>> RemoveAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto);
    }
}
