﻿using System;
using System.Threading.Tasks;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Contract.Attachment
{
    public class FindAttachmentCommand : Paging
    {
        public string AttachmentId { get; set; }
        public string Description { get; set; }
        public DateTime DataAttached { get; set; }
        public string FilePath { get; set; }
    }

    public interface IAttachmentManager
    {
        Task<Result<AttachmentDto>> SaveAttachmentAsync(AttachmentDto attachmentDto, UserDetailDto currentUser);
        Task<Result<AttachmentDto>> LoadAttachmentAsync(string id, UserDetailDto currentUser, bool includeFileContent = false);
        Task<Result<AttachmentDto>> LoadAttachmentAsync(string id, bool includeFileContent = false);
        Task<Result<Unit>> RemoveAttachmentAsync(string id, UserDetailDto currentUser, bool flushChanges = true);
        Result<PagingResult<AttachmentDto>> FindAttachmentMetadataList(FindAttachmentCommand command, UserDetailDto currentUser);
        string GetFileDownloadUrl(string attachmentId);
        string GetFileDownloadThumbnailUrl(string attachmentId);
    }
}
