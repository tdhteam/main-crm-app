﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ProductCategoryGroup")]
    public partial class ProductCategoryGroup
    {
        public ProductCategoryGroup()
        {
            ProductCategory = new HashSet<ProductCategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<ProductCategory> ProductCategory { get; set; }
    }
}
