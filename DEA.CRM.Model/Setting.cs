﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Setting")]
    public partial class Setting
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SettingValue { get; set; }
        public string SettingGroup { get; set; }
        public int? PickListId { get; set; }
    }
}
