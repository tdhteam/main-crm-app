﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("EventStatus")]
    public partial class EventStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
    }
}
