﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserInfoToShowroom")]
    public partial class UserInfoToShowroom
    {
        public string UserKey { get; set; }
        public int ShowroomId { get; set; }

        public Showroom Showroom { get; set; }
        public UserInfo UserKeyNavigation { get; set; }
    }
}
