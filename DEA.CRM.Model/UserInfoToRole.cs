﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserInfoToRole")]
    public partial class UserInfoToRole
    {
        public string UserKey { get; set; }
        public string RoleId { get; set; }

        public Role Role { get; set; }
        public UserInfo UserInfo { get; set; }
    }
}
