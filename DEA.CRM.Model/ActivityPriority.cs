﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("ActivityPriority")]
    public partial class ActivityPriority : IBasicLookupEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
    }

    public class ActivityPriorityEntityTypeConfiguration : IEntityTypeConfiguration<ActivityPriority>
    {
        public void Configure(EntityTypeBuilder<ActivityPriority> builder)
        {
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}
