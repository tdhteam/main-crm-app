﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("HandOverContractLine")]
    public partial class HandOverContractLine
    {
        public int Id { get; set; }
        public int HandOverContractId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitCost { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? FinalCost { get; set; }

        public HandOverContract HandOverContract { get; set; }
    }
}
