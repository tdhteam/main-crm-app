﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ContactAttachment")]
    public partial class ContactAttachment
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int ContactId { get; set; }
    }
}
