﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("HandOverContractCommentAttachment")]
    public partial class HandOverContractCommentAttachment
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int HandOverContractCommentId { get; set; }
    }
}
