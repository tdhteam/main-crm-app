﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Showroom")]
    public partial class Showroom
    {
        public Showroom()
        {
            UserInfoToShowroom = new HashSet<UserInfoToShowroom>();
        }

        public int Id { get; set; }
        public int DealerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public Dealer Dealer { get; set; }
        public ICollection<UserInfoToShowroom> UserInfoToShowroom { get; set; }
    }
}
