﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ContactMeetingProgress")]
    public partial class ContactMeetingProgress
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string CarOfInterest { get; set; }
        public string CarColorOfInterest { get; set; }
        public string Description { get; set; }
        public string MeetingStatus { get; set; }
        public DateTime? DateOfMeeting { get; set; }
        public string OtherNote { get; set; }

        public Contact Contact { get; set; }
    }
}
