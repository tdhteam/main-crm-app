﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ParentTab")]
    public partial class ParentTab
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public int Sequence { get; set; }
        public bool Visible { get; set; }
    }
}
