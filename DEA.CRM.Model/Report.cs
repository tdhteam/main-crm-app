﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Report")]
    public partial class Report
    {
        public int Id { get; set; }
        public int FolderId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReportType { get; set; }
        public string AccessRoute { get; set; }

        public ReportFolder Folder { get; set; }
    }
}
