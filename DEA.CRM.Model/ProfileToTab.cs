﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ProfileToTab")]
    public partial class ProfileToTab
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int TabId { get; set; }
        public bool Permission { get; set; }

        public Profile Profile { get; set; }
        public Tab Tab { get; set; }
    }
}
