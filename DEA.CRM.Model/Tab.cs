﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Tab")]
    public partial class Tab
    {
        public Tab()
        {
            ProfileToStandardPermission = new HashSet<ProfileToStandardPermission>();
            ProfileToTab = new HashSet<ProfileToTab>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public string Label { get; set; }
        public int? ParentTabId { get; set; }
        public string ParentTabLabel { get; set; }

        public ICollection<ProfileToStandardPermission> ProfileToStandardPermission { get; set; }
        public ICollection<ProfileToTab> ProfileToTab { get; set; }
    }
}
