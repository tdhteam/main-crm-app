﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("EventReminder")]
    public partial class EventReminder
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int ReminderTime { get; set; }
        public int? ReminderSent { get; set; }
        public int? RecurringId { get; set; }
        public string ReminderType { get; set; }

        public CalendarEvent Event { get; set; }
    }
}
