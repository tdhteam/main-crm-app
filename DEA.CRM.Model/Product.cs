﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Product")]
    public partial class Product
    {
        public int Id { get; set; }
        public string ProductNo { get; set; }
        public string Name { get; set; }
        public bool ProductActive { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? PercentVat { get; set; }
        public decimal? PercentService { get; set; }
        public decimal? PercentSales { get; set; }
        public decimal? QuantityInStock { get; set; }
        public int ProductCategoryId { get; set; }
        public string Vin { get; set; }
        public string EngineNumber { get; set; }
        public bool InActiveForSale { get; set; }
        public string WarehouseName { get; set; }
        public string Status { get; set; }
        public string Color { get; set; }
        public string ProductType { get; set; }
        public DateTime? DateEnteredStock { get; set; }
        public DateTime? DateRelease { get; set; }
        public string TransmissionType { get; set; }
        public DateTime? ProductionDate { get; set; }
        public int? NumberOfSeats { get; set; }
        public string Engine { get; set; }
        public string Uom { get; set; }
        public string HandlerUserKey { get; set; }
        public double? InStockAge { get; set; }
        public int? ContractId { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string BankOfGuarantee { get; set; }
        public string PaymentStatus { get; set; }
        public string Description { get; set; }
        public string PlateNumber { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public ProductCategory ProductCategory { get; set; }
    }
}
