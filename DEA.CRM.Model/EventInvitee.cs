﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("EventInvitee")]
    public partial class EventInvitee
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string AssigneeUserKey { get; set; }
        public string AssigneeUserFullName { get; set; }

        public CalendarEvent Event { get; set; }
    }
}
