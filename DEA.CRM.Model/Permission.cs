﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Permission")]
    public partial class Permission
    {
        public Permission()
        {
            ProfileToCustomPermissions = new HashSet<ProfileToCustomPermission>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }

        public ICollection<ProfileToCustomPermission> ProfileToCustomPermissions { get; set; }
    }
}
