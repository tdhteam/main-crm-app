﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("Currency")]
    public class Currency
    {
        public string CurrencyCode { get; set; }
        public string Description { get; set; }
        public short Precision { get; set; }
        public string Symbol { get; set; }
        public bool BaseCurrency { get; set; }
        public bool IsActive { get; set; }
    }

    public class CurrencyEntityTypeConfiguration : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.HasKey(p => p.CurrencyCode);
            builder.Property(p => p.Description).IsRequired().HasMaxLength(50);
            builder.Property(p => p.Precision).IsRequired();
            builder.Property(p => p.Symbol).IsRequired().HasMaxLength(8).IsUnicode();
            builder.Property(p => p.BaseCurrency).IsRequired().HasDefaultValue(false);
            builder.Property(p => p.IsActive).IsRequired().HasDefaultValue(true);
        }
    }
}
