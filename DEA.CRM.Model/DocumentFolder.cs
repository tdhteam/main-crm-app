﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{    
    [Table("DocumentFolder")]
    public partial class DocumentFolder
    {        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class DocumentFolderEntityTypeConfiguration : IEntityTypeConfiguration<DocumentFolder>
    {
        public void Configure(EntityTypeBuilder<DocumentFolder> builder)
        {
            builder.Property(p => p.Name).IsRequired().HasMaxLength(255).IsUnicode();
            
            builder.Property(p => p.Description).HasMaxLength(255).IsUnicode();
        }
    }
}
