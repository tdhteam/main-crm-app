﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("PickList")]
    public partial class PickList
    {
        public int PickListId { get; set; }
        public string Name { get; set; }
        public string Query { get; set; }
        public bool System { get; set; }
    }
}
