﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("GeneralAuditTrail")]
    public partial class GeneralAuditTrail
    {
        public int Id { get; set; }
        public string UserKey { get; set; }
        public string UserFullName { get; set; }
        public string Entity { get; set; }
        public string Module { get; set; }
        public string Action { get; set; }
        public string RecordId { get; set; }
        public DateTime ActionDate { get; set; }
    }
}
