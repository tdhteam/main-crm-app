﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserDashboard")]
    public partial class UserDashboard
    {
        public int Id { get; set; }
        public string UserKey { get; set; }
        public string Name { get; set; }
        public string WidgetSettings { get; set; }

        public UserInfo UserKeyNavigation { get; set; }
    }
}
