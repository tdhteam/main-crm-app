﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("CustomerService")]
    public partial class CustomerService
    {
        public CustomerService()
        {
            CustomerServiceComment = new HashSet<CustomerServiceComment>();
        }

        public int Id { get; set; }
        public DateTime ServiceDate { get; set; }
        public int? ContactReferenceId { get; set; }
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Category { get; set; }
        public string Priority { get; set; }
        public string PhoneNumber { get; set; }
        public string ServiceUserKey { get; set; }
        public string ServicePersonnel { get; set; }
        public string ProductModel { get; set; }
        public string PlateNumber { get; set; }
        public DateTime? ProductionDate { get; set; }
        public string VehicleRunKm { get; set; }
        public string ReasonToService { get; set; }
        public bool? UseTransportation { get; set; }
        public DateTime? PlanServiceCompleteDate { get; set; }
        public string ReminderPersonnelUserKey { get; set; }
        public string ReminderPersonnelFullName { get; set; }
        public DateTime? ContractDate { get; set; }
        public string SalesUserKey { get; set; }
        public string SalesPersonnelFullName { get; set; }
        public string AppointedSalesUserKey { get; set; }
        public string AppointedSalesPersonnelFullName { get; set; }
        public string Description { get; set; }
        public string ServiceResult { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public ICollection<CustomerServiceComment> CustomerServiceComment { get; set; }
    }
}
