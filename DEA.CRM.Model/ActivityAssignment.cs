﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("ActivityAssignment")]
    public partial class ActivityAssignment
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public string AssigneeUserKey { get; set; }
        public string AssigneeUserFullName { get; set; }

        public Activity Activity { get; set; }
    }

    public class ActivityAssignmentEntityTypeConfiguration : IEntityTypeConfiguration<ActivityAssignment>
    {
        public void Configure(EntityTypeBuilder<ActivityAssignment> builder)
        {
            builder.Property(e => e.AssigneeUserFullName)
                .IsRequired()
                .HasMaxLength(256);

            builder.Property(e => e.AssigneeUserKey)
                .IsRequired()
                .HasMaxLength(128);

            builder.HasOne(d => d.Activity)
                .WithMany(p => p.ActivityAssignment)
                .HasForeignKey(d => d.ActivityId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ActivityAssignment_Activity");
        }
    }
}
