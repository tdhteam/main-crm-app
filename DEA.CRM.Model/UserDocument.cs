﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("UserDocument")]
    public partial class UserDocument
    {        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }        
        public string AttachmentId { get; set; }
        public string DocumentLink { get; set; }
        public string OwnerUserKey { get; set; }
        public string OwnerUserFullName { get; set; }        
        public string FolderName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedByUserKey { get; set; }
        public string ModifiedByUserFullName { get; set; }
    }

    public class UserDocumentEntityTypeConfiguration : IEntityTypeConfiguration<UserDocument>
    {
        public void Configure(EntityTypeBuilder<UserDocument> builder)
        {
            builder.Property(p => p.Name).IsRequired().HasMaxLength(255).IsUnicode();
            
            builder.Property(p => p.Description).HasMaxLength(500).IsUnicode();

            builder.Property(p => p.AttachmentId).HasMaxLength(36);
            
            builder.Property(p => p.DocumentLink).HasMaxLength(255);
            
            builder.Property(p => p.OwnerUserKey).IsRequired().HasMaxLength(128);
            
            builder.Property(p => p.OwnerUserFullName).IsRequired().HasMaxLength(255).IsUnicode();
            
            builder.Property(p => p.FolderName).IsRequired().HasMaxLength(255).IsUnicode();

            builder.Property(p => p.CreatedDate).IsRequired();
            
            builder.Property(p => p.ModifiedDate).IsRequired();

            builder.Property(p => p.ModifiedByUserKey).IsRequired().HasMaxLength(128);
            
            builder.Property(p => p.ModifiedByUserFullName).IsRequired().HasMaxLength(255).IsUnicode();
        }
    }
}
