﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("CustomerServiceCommentAttachment")]
    public partial class CustomerServiceCommentAttachment
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int CustomerServiceCommentId { get; set; }
    }
}
