﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("City")]
    public partial class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Description { get; set; }
    }
}
