﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Company")]
    public partial class Company
    {
        public Company()
        {
            Dealer = new HashSet<Dealer>();
            UserInfoToCompany = new HashSet<UserInfoToCompany>();
        }

        public int Id { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string LogoRelativeUrl { get; set; }

        public ICollection<Dealer> Dealer { get; set; }
        public ICollection<UserInfoToCompany> UserInfoToCompany { get; set; }
    }
}
