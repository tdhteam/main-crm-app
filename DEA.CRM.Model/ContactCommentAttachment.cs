﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ContactCommentAttachment")]
    public partial class ContactCommentAttachment
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int ContactCommentId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Attachment Attachment { get; set; }
    }
}
