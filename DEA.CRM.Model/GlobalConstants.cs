﻿namespace DEA.CRM.Model
{
    public static class GlobalConstants
    {
        public static string StatusActive = "Active";
        public static string StatusInActive = "InActive";
        
        public static string NewStringId = "New";
        public static string DefaultImageExt = "png";
        public static string DefaultImageContentType = "image/png";

        public class ModuleNames
        {
            public static string Dashboard = "Dashboard";
            public static string Contacts = "Contacts";
            public static string Products = "Products";
            public static string HandoverContracts = "HandoverContracts";
            public static string CustomerServices = "CustomerServices";            
            public static string Calendar = "Calendar";
            public static string Reports = "Reports";
            public static string Comments = "Comments";
            public static string Attachments = "Attachments";                        
        }
    }    
}
