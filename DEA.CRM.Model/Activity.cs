﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("Activity")]
    public partial class Activity
    {
        public Activity()
        {
            ActivityAssignment = new HashSet<ActivityAssignment>();
        }

        public int Id { get; set; }
        public string Subject { get; set; }
        public bool IsRecurring { get; set; }
        public string RecurringType { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? DueDateTime { get; set; }
        public bool? SendNotification { get; set; }
        public int? DurationHours { get; set; }
        public int? DurationMinutues { get; set; }
        public string Status { get; set; }
        public string EventStatus { get; set; }
        public string Priority { get; set; }
        public string Location { get; set; }
        public bool? SendReminder { get; set; }
        public int? ReminderBeforeDay { get; set; }
        public int? ReminderBeforeHour { get; set; }
        public int? ReminderBeforeMinute { get; set; }
        public string Visibility { get; set; }
        public string OwnerUserKey { get; set; }
        public int? RelatedToContactId { get; set; }
        public int? RelatedToHandoverContractId { get; set; }
        public int? RelatedToCustomerServiceId { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public ICollection<ActivityAssignment> ActivityAssignment { get; set; }
    }

    public class ActivityEntityTypeConfiguration : IEntityTypeConfiguration<Activity>
    {
        public void Configure(EntityTypeBuilder<Activity> builder)
        {
            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.DueDateTime).HasColumnType("datetime");

            builder.Property(e => e.EventStatus).HasMaxLength(50);

            builder.Property(e => e.Location).HasMaxLength(255);

            builder.Property(e => e.OwnerUserKey).HasMaxLength(128);

            builder.Property(e => e.Priority).HasMaxLength(50);

            builder.Property(e => e.RecurringType).HasMaxLength(255);

            builder.Property(e => e.StartDateTime).HasColumnType("datetime");

            builder.Property(e => e.Status).HasMaxLength(50);

            builder.Property(e => e.Subject)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(e => e.Tags).HasMaxLength(255);

            builder.Property(e => e.Visibility).HasMaxLength(50);
        }
    }
}
