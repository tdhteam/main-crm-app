﻿namespace DEA.CRM.Model
{
    public interface IBasicLookupEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        bool Presence { get; set; }
        int SortOrderId { get; set; }
    }
}
