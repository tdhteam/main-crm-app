﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("FreeTag")]
    public partial class FreeTag
    {
        public int Id { get; set; }
        public string Tag { get; set; }
        public string RawTag { get; set; }
        public string Visibility { get; set; }
        public string OwnerUserKey { get; set; }
    }
}
