﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserInfo")]
    public partial class UserInfo
    {
        public UserInfo()
        {
            UserDashboard = new HashSet<UserDashboard>();
            UserInfoToCompany = new HashSet<UserInfoToCompany>();
            UserInfoToDealer = new HashSet<UserInfoToDealer>();
            UserInfoToShowroom = new HashSet<UserInfoToShowroom>();
        }

        public string UserKey { get; set; }
        public string UserName { get; set; }
        public string CalendarColor { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ReportToUserKey { get; set; }
        public bool IsAdmin { get; set; }
        public string CurrencyCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedByUserKey { get; set; }
        public string ModifiedByUserFullName { get; set; }
        public string Title { get; set; }
        public string Department { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string OtherPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string AddressStreet { get; set; }
        public string AddressDistrict { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressPostalCode { get; set; }
        public string DateFormat { get; set; }
        public string HourFotmat { get; set; }
        public bool Deleted { get; set; }
        public int? ReminderIntervalInSeconds { get; set; }
        public string Language { get; set; }
        public string TimeZone { get; set; }
        public string CurrencyGroupingPattern { get; set; }
        public string CurrencyDecimalSeparator { get; set; }
        public string CurrencyGroupingSeparator { get; set; }
        public string CurrencySymbolPlacement { get; set; }
        public int NumberOfCurrencyDecimal { get; set; }
        public string AvatarRelativeUrl { get; set; }
        public bool AccessAllCompanies { get; set; }
        public bool AccessAllDealers { get; set; }
        public bool AccessAllShowrooms { get; set; }
        public int? DefaultCompanyId { get; set; }
        public int? DefaultDealerId { get; set; }
        public int? DefaultShowroomId { get; set; }

        public UserInfoToRole UserRole { get; set; }
        public ICollection<UserDashboard> UserDashboard { get; set; }
        public ICollection<UserInfoToCompany> UserInfoToCompany { get; set; }
        public ICollection<UserInfoToDealer> UserInfoToDealer { get; set; }
        public ICollection<UserInfoToShowroom> UserInfoToShowroom { get; set; }
    }
}
