﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("RoleToProfile")]
    public partial class RoleToProfile
    {
        public string RoleId { get; set; }
        public int ProfileId { get; set; }

        public Profile Profile { get; set; }
        public Role Role { get; set; }
    }
}
