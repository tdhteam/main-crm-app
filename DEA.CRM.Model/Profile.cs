﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Profile")]
    public partial class Profile
    {
        public Profile()
        {
            ProfileToStandardPermissions = new HashSet<ProfileToStandardPermission>();
            ProfileToTabs = new HashSet<ProfileToTab>();
            RoleToProfiles = new HashSet<RoleToProfile>();
            ProfileToCustomPermissions = new HashSet<ProfileToCustomPermission>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DirectlyRelatedToRole { get; set; }

        public ICollection<ProfileToStandardPermission> ProfileToStandardPermissions { get; set; }
        public ICollection<ProfileToTab> ProfileToTabs { get; set; }
        public ICollection<RoleToProfile> RoleToProfiles { get; set; }
        public ICollection<ProfileToCustomPermission> ProfileToCustomPermissions { get; set; }
    }
}
