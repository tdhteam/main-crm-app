﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ProfileToStandardPermission")]
    public partial class ProfileToStandardPermission
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int TabId { get; set; }
        public int Operation { get; set; }
        public bool Permission { get; set; }

        public Profile Profile { get; set; }
        public Tab Tab { get; set; }
    }
}
