﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserInfoToCompany")]
    public partial class UserInfoToCompany
    {
        public string UserKey { get; set; }
        public int CompanyId { get; set; }

        public Company Company { get; set; }
        public UserInfo UserKeyNavigation { get; set; }
    }
}
