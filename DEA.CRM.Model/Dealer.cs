﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Dealer")]
    public partial class Dealer
    {
        public Dealer()
        {
            Showrooms = new HashSet<Showroom>();
            UserInfoToDealer = new HashSet<UserInfoToDealer>();
        }

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string LogoRelativeUrl { get; set; }

        public Company Company { get; set; }
        public ICollection<Showroom> Showrooms { get; set; }
        public ICollection<UserInfoToDealer> UserInfoToDealer { get; set; }
    }
}
