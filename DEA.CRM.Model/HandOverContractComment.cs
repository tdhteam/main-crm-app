﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("HandOverContractComment")]
    public partial class HandOverContractComment
    {
        public int Id { get; set; }
        public string CommentContent { get; set; }
        public int HandOverContractId { get; set; }

        public HandOverContract HandOverContract { get; set; }
    }
}
