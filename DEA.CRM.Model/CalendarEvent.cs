﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("CalendarEvent")]
    public partial class CalendarEvent
    {
        public CalendarEvent()
        {
            EventInvitee = new HashSet<EventInvitee>();
            EventRecurrence = new HashSet<EventRecurrence>();
            EventReminder = new HashSet<EventReminder>();
        }

        public int Id { get; set; }
        public string Subject { get; set; }
        public int? CalendarEventTypeId { get; set; }
        public bool IsRecurring { get; set; }
        public string RecurringType { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? DueDateTime { get; set; }
        public bool? SendNotification { get; set; }
        public int? DurationHours { get; set; }
        public int? DurationMinutues { get; set; }
        public string Status { get; set; }
        public string EventStatus { get; set; }
        public string Priority { get; set; }
        public string Location { get; set; }
        public bool? SendReminder { get; set; }
        public int? ReminderBeforeDay { get; set; }
        public int? ReminderBeforeHour { get; set; }
        public int? ReminderBeforeMinute { get; set; }
        public string Visibility { get; set; }
        public string OwnerUserKey { get; set; }
        public int? RelatedToContactId { get; set; }
        public int? RelatedToHandoverContractId { get; set; }
        public int? RelatedToCustomerServiceId { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public CalendarEventType CalendarEventType { get; set; }
        public ICollection<EventInvitee> EventInvitee { get; set; }
        public ICollection<EventRecurrence> EventRecurrence { get; set; }
        public ICollection<EventReminder> EventReminder { get; set; }
    }
}
