﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Role")]
    public partial class Role
    {
        public Role()
        {
            RoleToProfile = new HashSet<RoleToProfile>();
            UserInfoToRole = new HashSet<UserInfoToRole>();
        }

        public string RoleId { get; set; }
        public string Name { get; set; }
        public string ParentRoleId { get; set; }
        public string ParentRoleName { get; set; }
        public int Depth { get; set; }
        public string TreePath { get; set; }

        public ICollection<RoleToProfile> RoleToProfile { get; set; }
        public ICollection<UserInfoToRole> UserInfoToRole { get; set; }
    }
}
