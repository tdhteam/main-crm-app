﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Attachment")]
    public partial class Attachment
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string CreatedByUserKey { get; set; }
        public DateTime? DateAttached { get; set; }
        public long FileSize { get; set; }
        public string ContentType { get; set; }
        public string FileUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string DownloadFileUrl { get; set; }
        public string DownloadThumbnailUrl { get; set; }
    }
}
