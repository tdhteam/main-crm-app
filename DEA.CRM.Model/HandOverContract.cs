﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("HandOverContract")]
    public partial class HandOverContract
    {
        public HandOverContract()
        {
            HandOverContractComment = new HashSet<HandOverContractComment>();
            HandOverContractLine = new HashSet<HandOverContractLine>();
        }

        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactType { get; set; }
        public string CustomerType { get; set; }
        public string ContractPersonnelFullName { get; set; }
        public string CompanyName { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public DateTime? ContractDate { get; set; }
        public string SalesUserKey { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryPriority { get; set; }
        public decimal? PaymentInitialAmount { get; set; }
        public decimal? PaymentCashRemainAmount { get; set; }
        public decimal? PaymentBankInstallmentAmount { get; set; }
        public decimal? PaymentInstallmentAmount { get; set; }
        public DateTime? CustomerRequestDeliveryDate { get; set; }
        public DateTime? ContractSubmittedDate { get; set; }
        public DateTime? ContractApprovalDate { get; set; }
        public DateTime? ProductRequestToFactoryDate { get; set; }
        public DateTime? ProductFactoryDeliveryDate { get; set; }
        public string Vin { get; set; }
        public DateTime? PdirequestDate { get; set; }
        public DateTime? PdiinstallationRequestDate { get; set; }
        public DateTime? PdicompletedDate { get; set; }
        public DateTime? PdiinstallationCompletedtDate { get; set; }
        public DateTime? PdssaleDate { get; set; }
        public DateTime? PdsplanRegistrationDate { get; set; }
        public DateTime? PdsactualRegistrationDate { get; set; }
        public DateTime? PdsactualDeliveryDate { get; set; }
        public string ContractDescription { get; set; }
        public string WarrantyDescription { get; set; }
        public string WarrantyDurationInMonths { get; set; }
        public string WarrantyInKm { get; set; }
        public string AddressStreet { get; set; }
        public string AddressDistrict { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressPostalCode { get; set; }
        public string TermAndCondition { get; set; }
        public decimal? DepositAmount { get; set; }
        public decimal? RegistrationTax { get; set; }
        public decimal? RegistrationFee { get; set; }
        public decimal? AnnualRoadTax { get; set; }
        public decimal? NewVehicleRegistrationFee { get; set; }
        public decimal? RegistrationServiceFee { get; set; }
        public decimal? VehicleInsurance { get; set; }
        public decimal? AccidentInsurance { get; set; }
        public decimal? PackageAndTransportationFee { get; set; }
        public decimal? AdjustmentAmount { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }
        public string Tags { get; set; }

        public Customer Customer { get; set; }
        public ICollection<HandOverContractComment> HandOverContractComment { get; set; }
        public ICollection<HandOverContractLine> HandOverContractLine { get; set; }
    }
}
