﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ProductAttachment")]
    public partial class ProductAttachment
    {
        public int Id { get; set; }
        public string AttachmentId { get; set; }
        public int ProductId { get; set; }
    }
}
