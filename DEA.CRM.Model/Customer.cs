﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("Customer")]
    public partial class Customer
    {
        public Customer()
        {
            HandOverContract = new HashSet<HandOverContract>();
        }

        public int Id { get; set; }
        public string CustomerNo { get; set; }
        public int? ConvertedFromContactId { get; set; }
        public string ContactType { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Occupation { get; set; }
        public string OfficePhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string CustomerType { get; set; }
        public string PaymentType { get; set; }
        public string CustomerSource { get; set; }
        public int? CompanyId { get; set; }
        public int? DealerId { get; set; }
        public int? ShowroomId { get; set; }

        public ICollection<HandOverContract> HandOverContract { get; set; }
    }
}
