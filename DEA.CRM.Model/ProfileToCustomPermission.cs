﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ProfileToCustomPermission")]
    public partial class ProfileToCustomPermission
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int PermissionId { get; set; }

        public Profile Profile { get; set; }
        public Permission Permission { get; set; }
    }
}
