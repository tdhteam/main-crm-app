﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DEA.CRM.Model
{
    [Table("ActivityStatus")]
    public partial class ActivityStatus : IBasicLookupEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
    }

    public class ActivityStatusEntityTypeConfiguration : IEntityTypeConfiguration<ActivityStatus>
    {
        public void Configure(EntityTypeBuilder<ActivityStatus> builder)
        {
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}
