﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ReportFolder")]
    public partial class ReportFolder
    {
        public ReportFolder()
        {
            Report = new HashSet<Report>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Report> Report { get; set; }
    }
}
