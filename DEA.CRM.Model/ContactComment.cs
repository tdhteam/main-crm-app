﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("ContactComment")]
    public partial class ContactComment
    {

        public ContactComment()
        {
            ContactCommentAttachments = new HashSet<ContactCommentAttachment>();
        }

        public int Id { get; set; }
        public string CommentContent { get; set; }
        public int ContactId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public ICollection<ContactCommentAttachment> ContactCommentAttachments { get; set; }

        public Contact Contact { get; set; }
    }
}
