﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("UserInfoToDealer")]
    public partial class UserInfoToDealer
    {
        public string UserKey { get; set; }
        public int DealerId { get; set; }

        public Dealer Dealer { get; set; }
        public UserInfo UserKeyNavigation { get; set; }
    }
}
