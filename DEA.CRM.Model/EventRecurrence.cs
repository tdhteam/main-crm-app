﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("EventRecurrence")]
    public partial class EventRecurrence
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public DateTime RecurringDate { get; set; }
        public string RecurringType { get; set; }
        public int RecurringFrequency { get; set; }
        public string RecurringInfo { get; set; }
        public DateTime RecurringEndDate { get; set; }

        public CalendarEvent Event { get; set; }
    }
}
