﻿-- noinspection SqlNoDataSourceInspectionForFile
USE [CRMApp]
GO

/* Modify schema for Activity and Calendar Event*/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]') AND type in (N'U'))
BEGIN
	DROP TABLE ActivityAssignment
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityReminder]') AND type in (N'U'))
  BEGIN
    DROP TABLE ActivityReminder
  END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Activity]') AND type in (N'U'))
BEGIN
	DROP TABLE Activity
END
GO

/****** Object:  Table [dbo].[Activity]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Activity]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Activity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[IsRecurring] [bit] NOT NULL,
	[RecurringType] [nvarchar](255) NULL,
	[StartDateTime] [datetime] NULL,
	[DueDateTime] [datetime] NULL,
	[SendNotification] [bit] NULL,
	[DurationHours] [int] NULL,
	[DurationMinutues] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EventStatus] [nvarchar](50) NULL,
	[Priority] [nvarchar](50) NULL,
	[Location] [nvarchar](255) NULL,
	[SendReminder] [bit] NULL,
	[ReminderBeforeDay] [int] NULL,
	[ReminderBeforeHour] [int] NULL,
	[ReminderBeforeMinute] [int] NULL,
	[Visibility] [nvarchar](50) NULL,
	[OwnerUserKey] [nvarchar](128) NULL,
	[RelatedToContactId] [int] NULL,
	[RelatedToHandoverContractId] [int] NULL,
	[RelatedToCustomerServiceId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[Tags] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ActivityAssignment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityAssignment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[AssigneeUserKey] [nvarchar](128) NOT NULL,
	[AssigneeUserFullName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ActivityAssignment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActivityAssignment_Activity]') AND parent_object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]'))
	ALTER TABLE [dbo].[ActivityAssignment]  WITH CHECK ADD  CONSTRAINT [FK_ActivityAssignment_Activity] FOREIGN KEY([ActivityId])
	REFERENCES [dbo].[Activity] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActivityAssignment_Activity]') AND parent_object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]'))
	ALTER TABLE [dbo].[ActivityAssignment] CHECK CONSTRAINT [FK_ActivityAssignment_Activity]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventInvitee]') AND type in (N'U'))
BEGIN
	DROP TABLE EventInvitee
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventReminder]') AND type in (N'U'))
BEGIN
	DROP TABLE EventReminder
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventRecurrence]') AND type in (N'U'))
BEGIN
	DROP TABLE EventRecurrence
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalendarEvent]') AND type in (N'U'))
BEGIN
	DROP TABLE CalendarEvent
END
GO
/****** Object:  Table [dbo].[CalendarEvent]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalendarEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CalendarEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[CalendarEventTypeId] [int] NULL,
	[IsRecurring] [bit] NOT NULL,
	[RecurringType] [nvarchar](255) NULL,
	[StartDateTime] [datetime] NULL,
	[DueDateTime] [datetime] NULL,
	[SendNotification] [bit] NULL,
	[DurationHours] [int] NULL,
	[DurationMinutues] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EventStatus] [nvarchar](50) NULL,
	[Priority] [nvarchar](50) NULL,
	[Location] [nvarchar](255) NULL,
	[SendReminder] [bit] NULL,
	[ReminderBeforeDay] [int] NULL,
	[ReminderBeforeHour] [int] NULL,
	[ReminderBeforeMinute] [int] NULL,
	[Visibility] [nvarchar](50) NULL,
	[OwnerUserKey] [nvarchar](128) NULL,
	[RelatedToContactId] [int] NULL,
	[RelatedToHandoverContractId] [int] NULL,
	[RelatedToCustomerServiceId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[Tags] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_CalendarEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[EventInvitee]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventInvitee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventInvitee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[AssigneeUserKey] [nvarchar](128) NULL,
	[AssigneeUserFullName] [nvarchar](256) NULL,
 CONSTRAINT [PK_EventInvitee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventRecurrence]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventRecurrence]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventRecurrence](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[RecurringDate] [datetime] NOT NULL,
	[RecurringType] [nvarchar](100) NOT NULL,
	[RecurringFrequency] [int] NOT NULL,
	[RecurringInfo] [nvarchar](100) NULL,
	[RecurringEndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EventRecurrence] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventReminder]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventReminder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventReminder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[ReminderTime] [int] NOT NULL,
	[ReminderSent] [int] NULL,
	[RecurringId] [int] NULL,
	[ReminderType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EventReminder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CalendarEvent_CalendarEventType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CalendarEvent]'))
ALTER TABLE [dbo].[CalendarEvent]  WITH CHECK ADD  CONSTRAINT [FK_CalendarEvent_CalendarEventType] FOREIGN KEY([CalendarEventTypeId])
REFERENCES [dbo].[CalendarEventType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CalendarEvent_CalendarEventType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CalendarEvent]'))
ALTER TABLE [dbo].[CalendarEvent] CHECK CONSTRAINT [FK_CalendarEvent_CalendarEventType]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventInvitee_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventInvitee]'))
ALTER TABLE [dbo].[EventInvitee]  WITH CHECK ADD  CONSTRAINT [FK_EventInvitee_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventInvitee_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventInvitee]'))
ALTER TABLE [dbo].[EventInvitee] CHECK CONSTRAINT [FK_EventInvitee_CalendarEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventRecurrence_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventRecurrence]'))
ALTER TABLE [dbo].[EventRecurrence]  WITH CHECK ADD  CONSTRAINT [FK_EventRecurrence_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventRecurrence_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventRecurrence]'))
ALTER TABLE [dbo].[EventRecurrence] CHECK CONSTRAINT [FK_EventRecurrence_CalendarEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventReminder_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventReminder]'))
ALTER TABLE [dbo].[EventReminder]  WITH CHECK ADD  CONSTRAINT [FK_EventReminder_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventReminder_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventReminder]'))
ALTER TABLE [dbo].[EventReminder] CHECK CONSTRAINT [FK_EventReminder_CalendarEvent]
GO

/****Currency table****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](8) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Precision] [smallint] NOT NULL,
	[Symbol] [nvarchar](8) NOT NULL,
	[BaseCurrency] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****City table****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[City]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[City](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [Name] [nvarchar](255) NOT NULL,
    [Description] [nvarchar](255) NULL,
   CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
  (
    [Id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

/****UserInfo****/
IF EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserInfo') and name = 'CurrencyId')
	ALTER TABLE UserInfo DROP COLUMN CurrencyId  
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserInfo') and name = 'CurrencyCode')
  ALTER TABLE UserInfo ADD CurrencyCode nvarchar(10) NULL
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.UserInfo') and name = 'TimeZone')
  ALTER TABLE UserInfo ALTER COLUMN TimeZone nvarchar(50) NOT NULL
GO

/****ContactComment****/
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.ContactComment') and name = 'ModifiedDate')
  ALTER TABLE ContactComment ADD ModifiedDate datetime NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.ContactComment') and name = 'ModifiedBy')
  ALTER TABLE ContactComment ADD ModifiedBy nvarchar(50) NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.ContactCommentAttachment') and name = 'ModifiedDate')
  ALTER TABLE ContactCommentAttachment ADD ModifiedDate datetime NULL
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.ContactCommentAttachment') and name = 'ModifiedBy')
  ALTER TABLE ContactCommentAttachment ADD ModifiedBy nvarchar(50) NULL
GO
