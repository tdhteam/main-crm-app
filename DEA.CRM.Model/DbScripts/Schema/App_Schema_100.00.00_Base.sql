﻿-- noinspection SqlNoDataSourceInspectionForFile

USE [master]
GO
/****** Object:  Database [CRMApp]    Script Date: 4/23/2018 5:38:02 PM ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'CRMApp')
BEGIN
	CREATE DATABASE [CRMApp] 
END

GO
ALTER DATABASE [CRMApp] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CRMApp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CRMApp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CRMApp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CRMApp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CRMApp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CRMApp] SET ARITHABORT OFF 
GO
ALTER DATABASE [CRMApp] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CRMApp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CRMApp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CRMApp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CRMApp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CRMApp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CRMApp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CRMApp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CRMApp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CRMApp] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CRMApp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CRMApp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CRMApp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CRMApp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CRMApp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CRMApp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CRMApp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CRMApp] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CRMApp] SET  MULTI_USER 
GO
ALTER DATABASE [CRMApp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CRMApp] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CRMApp] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CRMApp] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CRMApp] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CRMApp]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Activity]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Activity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[IsRecurring] [bit] NOT NULL,
	[RecurringType] [nvarchar](255) NOT NULL,
	[StartDateTime] [datetime] NULL,
	[DueDateTime] [datetime] NULL,
	[SendNotification] [bit] NULL,
	[DurationHours] [int] NULL,
	[DurationMinutues] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EventStatus] [nvarchar](50) NULL,
	[Priority] [nvarchar](50) NULL,
	[Location] [nvarchar](255) NULL,
	[SendReminder] [bit] NULL,
	[ReminderBeforeDay] [int] NULL,
	[ReminderBeforeHour] [int] NULL,
	[ReminderBeforeMinute] [int] NULL,
	[Visibility] [nvarchar](50) NULL,
	[OwnerUserKey] [nvarchar](128) NULL,
	[RelatedToContactId] [int] NULL,
	[RelatedToHandoverContractId] [int] NULL,
	[RelatedToCustomerServiceId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[Tags] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ActivityAssignment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityAssignment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[AssigneeUserKey] [nvarchar](128) NOT NULL,
	[AssigneeUserFullName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ActivityAssignment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ActivityPriority]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityPriority]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityPriority](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Presence] [bit] NOT NULL,
	[SortOrderId] [int] NOT NULL,
 CONSTRAINT [PK_ActivityPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ActivityStatus]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Presence] [bit] NOT NULL,
	[SortOrderId] [int] NOT NULL,
 CONSTRAINT [PK_ActivityStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Attachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Attachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Attachment](
	[Id] [varchar](36) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[CreatedByUserKey] [nvarchar](128) NOT NULL,
	[DateAttached] [datetime] NULL,
	[FileSize] [bigint] NOT NULL,
	[ContentType] [nvarchar](100) NULL,
	[FileUrl] [nvarchar](255) NULL,
	[ThumbnailUrl] [nvarchar](255) NULL,
	[DownloadFileUrl] [nvarchar](255) NULL,
	[DownloadThumbnailUrl] [nvarchar](255) NULL,
 CONSTRAINT [PK_Attachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[BankInfo]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BankInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BankInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_BankInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CalendarEvent]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalendarEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CalendarEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[CalendarEventTypeId] [int] NULL,
	[IsRecurring] [bit] NOT NULL,
	[RecurringType] [nvarchar](255) NULL,
	[StartDateTime] [datetime] NULL,
	[DueDateTime] [datetime] NULL,
	[SendNotification] [bit] NULL,
	[DurationHours] [int] NULL,
	[DurationMinutues] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EventStatus] [nvarchar](50) NULL,
	[Priority] [nvarchar](50) NULL,
	[Location] [nvarchar](255) NULL,
	[SendReminder] [bit] NULL,
	[ReminderBeforeDay] [int] NULL,
	[ReminderBeforeHour] [int] NULL,
	[ReminderBeforeMinute] [int] NULL,
	[Visibility] [nvarchar](50) NULL,
	[OwnerUserKey] [nvarchar](128) NULL,
	[RelatedToContactId] [int] NULL,
	[RelatedToHandoverContractId] [int] NULL,
	[RelatedToCustomerServiceId] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[Tags] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_CalendarEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CalendarEventType]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalendarEventType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CalendarEventType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Presence] [bit] NOT NULL,
	[SortOrderId] [int] NOT NULL,
	[Color] [nvarchar](10) NULL,
 CONSTRAINT [PK_CalendarEventType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Company]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyCode] [varchar](20) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NULL,
	[Fax] [nvarchar](20) NULL,
	[WebSite] [nvarchar](255) NULL,
	[LogoRelativeUrl] [nvarchar](255) NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContactNo] [nvarchar](50) NOT NULL,
	[Salutation] [nvarchar](50) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[Gender] [nvarchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[Email] [nvarchar](255) NULL,
	[OfficePhone] [nvarchar](20) NULL,
	[HomePhone] [nvarchar](20) NULL,
	[MobilePhone] [nvarchar](20) NULL,
	[ApproachingType] [nvarchar](255) NULL,
	[AddressStreet] [nvarchar](255) NULL,
	[AddressDistrict] [nvarchar](255) NULL,
	[AddressCity] [nvarchar](255) NULL,
	[AddressState] [nvarchar](255) NULL,
	[AddressCountry] [nvarchar](50) NULL,
	[AddressPostalCode] [nvarchar](20) NULL,
	[Occupation] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[PaymentType] [nvarchar](255) NULL,
	[CarIsUsing] [nvarchar](255) NULL,
	[ContactSource] [nvarchar](255) NULL,
	[PlanToBuyDate] [datetime] NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
	[FirstMeetingDate] [datetime] NULL,
	[HandlerUserKey] [nvarchar](128) NULL,
	[ContactType] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[CompanyTaxCode] [nvarchar](50) NULL,
	[CompanyOwner] [nvarchar](255) NULL,
	[CompanyWebSite] [nvarchar](255) NULL,
	[CompanyEmail] [nvarchar](255) NULL,
	[ContactNote] [nvarchar](2000) NULL,
	[Tags] [nvarchar](255) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactAttachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactAttachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [varchar](36) NOT NULL,
	[ContactId] [int] NOT NULL,
 CONSTRAINT [PK_ContactAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactComment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactComment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommentContent] [nvarchar](4000) NOT NULL,
	[ContactId] [int] NOT NULL,
 CONSTRAINT [PK_ContactComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactCommentAttachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactCommentAttachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactCommentAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [varchar](36) NOT NULL,
	[ContactCommentId] [int] NOT NULL,
 CONSTRAINT [PK_ContactCommentAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactMeetingProgress]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactMeetingProgress]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactMeetingProgress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContactId] [int] NULL,
	[CarOfInterest] [nvarchar](255) NULL,
	[CarColorOfInterest] [nvarchar](50) NULL,
	[Description] [nvarchar](2000) NULL,
	[MeetingStatus] [nvarchar](50) NULL,
	[DateOfMeeting] [datetime] NULL,
	[OtherNote] [nvarchar](2000) NULL,
 CONSTRAINT [PK_ContactMeetingProgress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNo] [nvarchar](50) NOT NULL,
	[ConvertedFromContactId] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Salutation] [nvarchar](50) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[Gender] [nvarchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[Occupation] [nvarchar](255) NULL,
	[OfficePhone] [nvarchar](20) NULL,
	[HomePhone] [nvarchar](20) NULL,
	[MobilePhone] [nvarchar](20) NULL,
	[CustomerType] [nvarchar](100) NULL,
	[PaymentType] [nvarchar](100) NULL,
	[CustomerSource] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerService]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerService]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerService](
	[Id] [int] NOT NULL,
	[ServiceDate] [datetime] NOT NULL,
	[ContactReferenceId] [int] NULL,
	[CustomerId] [int] NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[Category] [nvarchar](255) NULL,
	[Priority] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[ServiceUserKey] [nvarchar](128) NULL,
	[ServicePersonnel] [nvarchar](255) NULL,
	[ProductModel] [nvarchar](255) NULL,
	[PlateNumber] [nvarchar](30) NULL,
	[ProductionDate] [datetime] NULL,
	[VehicleRunKm] [nvarchar](30) NULL,
	[ReasonToService] [nvarchar](500) NULL,
	[UseTransportation] [bit] NULL,
	[PlanServiceCompleteDate] [datetime] NULL,
	[ReminderPersonnelUserKey] [nvarchar](128) NULL,
	[ReminderPersonnelFullName] [nvarchar](255) NULL,
	[ContractDate] [datetime] NULL,
	[SalesUserKey] [nvarchar](128) NULL,
	[SalesPersonnelFullName] [nvarchar](255) NULL,
	[AppointedSalesUserKey] [nvarchar](128) NULL,
	[AppointedSalesPersonnelFullName] [nvarchar](255) NULL,
	[Description] [nvarchar](2000) NULL,
	[ServiceResult] [nvarchar](2000) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_CustomerService] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerServiceComment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerServiceComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerServiceComment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommentContent] [nvarchar](4000) NOT NULL,
	[CustomerServiceId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerServiceComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerServiceCommentAttachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerServiceCommentAttachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerServiceCommentAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [varchar](36) NOT NULL,
	[CustomerServiceCommentId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerServiceCommentAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DatabaseVersion]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatabaseVersion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatabaseVersion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Major] [int] NOT NULL,
	[Minor] [int] NOT NULL,
	[Build] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DatabaseVersion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Dealer]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dealer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Dealer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NULL,
	[Fax] [nvarchar](20) NULL,
	[WebSite] [nvarchar](255) NULL,
	[LogoRelativeUrl] [nvarchar](255) NULL,
 CONSTRAINT [PK_Dealer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventInvitee]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventInvitee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventInvitee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[AssigneeUserKey] [nvarchar](128) NULL,
	[AssigneeUserFullName] [nvarchar](256) NULL,
 CONSTRAINT [PK_EventInvitee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventRecurrence]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventRecurrence]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventRecurrence](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[RecurringDate] [datetime] NOT NULL,
	[RecurringType] [nvarchar](100) NOT NULL,
	[RecurringFrequency] [int] NOT NULL,
	[RecurringInfo] [nvarchar](100) NULL,
	[RecurringEndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EventRecurrence] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventReminder]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventReminder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventReminder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NOT NULL,
	[ReminderTime] [int] NOT NULL,
	[ReminderSent] [int] NULL,
	[RecurringId] [int] NULL,
	[ReminderType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EventReminder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventStatus]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Presence] [bit] NOT NULL,
	[SortOrderId] [int] NOT NULL,
 CONSTRAINT [PK_EventStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[FreeTag]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FreeTag]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FreeTag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tag] [nvarchar](50) NOT NULL,
	[RawTag] [nvarchar](50) NULL,
	[Visibility] [nvarchar](20) NOT NULL,
	[OwnerUserKey] [nvarchar](128) NULL,
 CONSTRAINT [PK_FreeTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[GeneralAuditTrail]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralAuditTrail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralAuditTrail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserKey] [nvarchar](128) NOT NULL,
	[UserFullName] [nvarchar](255) NOT NULL,
	[Entity] [nvarchar](255) NOT NULL,
	[Module] [nvarchar](255) NOT NULL,
	[Action] [nvarchar](255) NOT NULL,
	[RecordId] [nvarchar](100) NOT NULL,
	[ActionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GeneralAuditTrail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[GlAccount]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GlAccount]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GlAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountCode] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_GlAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[HandOverContract]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HandOverContract]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HandOverContract](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](255) NOT NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[ContactType] [nvarchar](50) NOT NULL,
	[CustomerType] [nvarchar](255) NULL,
	[ContractPersonnelFullName] [nvarchar](255) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[ProductName] [nvarchar](255) NULL,
	[ProductType] [nvarchar](255) NULL,
	[ContractDate] [datetime] NULL,
	[SalesUserKey] [nvarchar](128) NULL,
	[DeliveryDate] [datetime] NULL,
	[DeliveryPriority] [nvarchar](50) NULL,
	[PaymentInitialAmount] [decimal](18, 2) NULL,
	[PaymentCashRemainAmount] [decimal](18, 2) NULL,
	[PaymentBankInstallmentAmount] [decimal](18, 2) NULL,
	[PaymentInstallmentAmount] [decimal](18, 2) NULL,
	[CustomerRequestDeliveryDate] [datetime] NULL,
	[ContractSubmittedDate] [datetime] NULL,
	[ContractApprovalDate] [datetime] NULL,
	[ProductRequestToFactoryDate] [datetime] NULL,
	[ProductFactoryDeliveryDate] [datetime] NULL,
	[VIN] [nvarchar](50) NULL,
	[PDIRequestDate] [datetime] NULL,
	[PDIInstallationRequestDate] [datetime] NULL,
	[PDICompletedDate] [datetime] NULL,
	[PDIInstallationCompletedtDate] [datetime] NULL,
	[PDSSaleDate] [datetime] NULL,
	[PDSPlanRegistrationDate] [datetime] NULL,
	[PDSActualRegistrationDate] [datetime] NULL,
	[PDSActualDeliveryDate] [datetime] NULL,
	[ContractDescription] [nvarchar](2000) NULL,
	[WarrantyDescription] [nvarchar](2000) NULL,
	[WarrantyDurationInMonths] [nvarchar](50) NULL,
	[WarrantyInKm] [nvarchar](30) NULL,
	[AddressStreet] [nvarchar](255) NULL,
	[AddressDistrict] [nvarchar](255) NULL,
	[AddressCity] [nvarchar](255) NULL,
	[AddressState] [nvarchar](255) NULL,
	[AddressCountry] [nvarchar](50) NULL,
	[AddressPostalCode] [nvarchar](20) NULL,
	[TermAndCondition] [nvarchar](4000) NULL,
	[DepositAmount] [decimal](18, 2) NULL,
	[RegistrationTax] [decimal](18, 2) NULL,
	[RegistrationFee] [decimal](18, 2) NULL,
	[AnnualRoadTax] [decimal](18, 2) NULL,
	[NewVehicleRegistrationFee] [decimal](18, 2) NULL,
	[RegistrationServiceFee] [decimal](18, 2) NULL,
	[VehicleInsurance] [decimal](18, 2) NULL,
	[AccidentInsurance] [decimal](18, 2) NULL,
	[PackageAndTransportationFee] [decimal](18, 2) NULL,
	[AdjustmentAmount] [decimal](18, 2) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
	[Tags] [nvarchar](255) NULL,
 CONSTRAINT [PK_Contract] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[HandOverContractComment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HandOverContractComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HandOverContractComment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommentContent] [nvarchar](4000) NOT NULL,
	[HandOverContractId] [int] NOT NULL,
 CONSTRAINT [PK_HandOverContractComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[HandOverContractCommentAttachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HandOverContractCommentAttachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HandOverContractCommentAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [varchar](36) NOT NULL,
	[HandOverContractCommentId] [int] NOT NULL,
 CONSTRAINT [PK_HandOverContractCommentAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[HandOverContractLine]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HandOverContractLine]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HandOverContractLine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HandOverContractId] [int] NOT NULL,
	[ProductName] [nvarchar](255) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[UnitCost] [decimal](18, 2) NOT NULL,
	[DiscountAmount] [decimal](18, 2) NULL,
	[DiscountPercent] [decimal](18, 2) NULL,
	[FinalCost] [decimal](18, 2) NULL,
 CONSTRAINT [PK_ContractLine] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[LeadSource]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeadSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LeadSource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_LeadSource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ParentTab]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParentTab]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ParentTab](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Label] [nvarchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Visible] [bit] NOT NULL,
 CONSTRAINT [PK_ParentTab] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Permission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Permission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PickList]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PickList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PickList](
	[PickListId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Query] [nvarchar](2048) NULL,
	[System] [bit] NOT NULL,
 CONSTRAINT [PK_PickList] PRIMARY KEY CLUSTERED 
(
	[PickListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PickListValue]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PickListValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PickListValue](
	[PickListId] [int] NOT NULL,
	[Value] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[SortOrder] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductNo] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ProductActive] [bit] NOT NULL,
	[UnitPrice] [decimal](18, 8) NULL,
	[PercentVat] [decimal](18, 2) NULL,
	[PercentService] [decimal](18, 2) NULL,
	[PercentSales] [decimal](18, 2) NULL,
	[QuantityInStock] [decimal](18, 2) NULL,
	[ProductCategoryId] [int] NOT NULL,
	[VIN] [nvarchar](50) NULL,
	[EngineNumber] [nvarchar](50) NULL,
	[InActiveForSale] [bit] NOT NULL,
	[WarehouseName] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Color] [nvarchar](50) NULL,
	[ProductType] [nvarchar](255) NULL,
	[DateEnteredStock] [datetime] NULL,
	[DateRelease] [datetime] NULL,
	[TransmissionType] [nvarchar](255) NULL,
	[ProductionDate] [datetime] NULL,
	[NumberOfSeats] [int] NULL,
	[Engine] [nvarchar](255) NULL,
	[UOM] [nvarchar](100) NULL,
	[HandlerUserKey] [nvarchar](128) NULL,
	[InStockAge] [float] NULL,
	[ContractId] [int] NULL,
	[PurchaseOrderNo] [nvarchar](50) NULL,
	[BankOfGuarantee] [nvarchar](255) NULL,
	[PaymentStatus] [nvarchar](2000) NULL,
	[Description] [nvarchar](2000) NULL,
	[PlateNumber] [nvarchar](20) NULL,
	[CompanyId] [int] NULL,
	[DealerId] [int] NULL,
	[ShowroomId] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductAttachment]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductAttachment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [varchar](36) NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_ProductAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[CategoryGroupId] [int] NOT NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductCategoryGroup]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductCategoryGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductCategoryGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_ProductCategoryGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductNamePool]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductNamePool]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductNamePool](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ProductNamePool] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Profile]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Profile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[DirectlyRelatedToRole] [bit] NOT NULL,
 CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProfileToCustomPermission]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileToCustomPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProfileToCustomPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_ProfileToCustomPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProfileToStandardPermission]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileToStandardPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProfileToStandardPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[TabId] [int] NOT NULL,
	[Operation] [int] NOT NULL,
	[Permission] [bit] NOT NULL,
 CONSTRAINT [PK_ProfileToStandardPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProfileToTab]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfileToTab]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProfileToTab](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[TabId] [int] NOT NULL,
	[Permission] [bit] NOT NULL,
 CONSTRAINT [PK_ProfileToTab] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RecurringType]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecurringType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RecurringType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Presence] [bit] NOT NULL,
	[SortOrderId] [int] NOT NULL,
	[Color] [nvarchar](10) NULL,
 CONSTRAINT [PK_RecurringType_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Report]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FolderId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ReportType] [nvarchar](50) NOT NULL,
	[AccessRoute] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ReportFolder]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportFolder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ReportFolder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_ReportFolder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Role]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Role](
	[RoleId] [nvarchar](36) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ParentRoleId] [nvarchar](36) NULL,
	[ParentRoleName] [nvarchar](255) NULL,
	[Depth] [int] NOT NULL,
	[TreePath] [nvarchar](500) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RoleSeq]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleSeq]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoleSeq](
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_RoleSeq] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RoleToProfile]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleToProfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoleToProfile](
	[RoleId] [nvarchar](36) NOT NULL,
	[ProfileId] [int] NOT NULL,
 CONSTRAINT [PK_RoleToProfile] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Setting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Setting](
	[SettingId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[SettingValue] [nvarchar](1024) NULL,
	[SettingGroup] [nvarchar](50) NOT NULL,
	[PickListId] [int] NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[SettingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Showroom]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Showroom]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Showroom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DealerId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NOT NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NULL,
	[Fax] [nvarchar](20) NULL,
 CONSTRAINT [PK_Showroom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Tab]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tab]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tab](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Label] [nvarchar](50) NOT NULL,
	[ParentTabId] [int] NULL,
	[ParentTabLabel] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tab] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UnitOfMeasure]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UnitOfMeasure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UnitOfMeasure](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UnitOfMeasure] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserDashboard]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDashboard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDashboard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserKey] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[WidgetSettings] [text] NULL,
 CONSTRAINT [PK_UserDashboard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInfo](
	[UserKey] [nvarchar](128) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[CalendarColor] [varchar](25) NOT NULL CONSTRAINT [DF_UserInfo_CalendarColor]  DEFAULT ('#E6FAD8'),
	[Salutation] [nvarchar](50) NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NULL,
	[ReportToUserKey] [nvarchar](128) NULL,
	[IsAdmin] [bit] NOT NULL CONSTRAINT [DF_UserInfo_IsAdmin]  DEFAULT ((0)),
	[CurrencyId] [int] NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedByUserKey] [nvarchar](128) NOT NULL,
	[ModifiedByUserFullName] [nvarchar](255) NOT NULL,
	[Title] [nchar](10) NULL,
	[Department] [nvarchar](255) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[WorkPhone] [nvarchar](50) NULL,
	[MobilePhone] [nvarchar](50) NULL,
	[OtherPhone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Status] [nvarchar](25) NULL,
	[AddressStreet] [nvarchar](255) NULL,
	[AddressDistrict] [nvarchar](255) NULL,
	[AddressCity] [nvarchar](255) NULL,
	[AddressState] [nvarchar](255) NULL,
	[AddressCountry] [nvarchar](50) NULL,
	[AddressPostalCode] [nvarchar](25) NULL,
	[DateFormat] [nvarchar](20) NOT NULL,
	[HourFotmat] [nvarchar](20) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ReminderIntervalInSeconds] [int] NULL,
	[Language] [nvarchar](10) NOT NULL,
	[TimeZone] [nvarchar](50) NOT NULL,
	[CurrencyGroupingPattern] [varchar](30) NOT NULL,
	[CurrencyDecimalSeparator] [varchar](2) NOT NULL,
	[CurrencyGroupingSeparator] [varchar](2) NOT NULL,
	[CurrencySymbolPlacement] [varchar](10) NOT NULL,
	[NumberOfCurrencyDecimal] [int] NOT NULL,
	[AvatarRelativeUrl] [nvarchar](255) NULL,
	[AccessAllCompanies] [bit] NOT NULL,
	[AccessAllDealers] [bit] NOT NULL,
	[AccessAllShowrooms] [bit] NOT NULL,
	[DefaultCompanyId] [int] NULL,
	[DefaultDealerId] [int] NULL,
	[DefaultShowroomId] [int] NULL,
 CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInfoToCompany]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfoToCompany]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInfoToCompany](
	[UserKey] [nvarchar](128) NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_UserInfoToCompany] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC,
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserInfoToDealer]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfoToDealer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInfoToDealer](
	[UserKey] [nvarchar](128) NOT NULL,
	[DealerId] [int] NOT NULL,
 CONSTRAINT [PK_UserInfoToDealer] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC,
	[DealerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserInfoToRole]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfoToRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInfoToRole](
	[UserKey] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](36) NOT NULL,
 CONSTRAINT [PK_UserInfoToRole] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserInfoToShowroom]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfoToShowroom]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserInfoToShowroom](
	[UserKey] [nvarchar](128) NOT NULL,
	[ShowroomId] [int] NOT NULL,
 CONSTRAINT [PK_UserInfoToShowroom] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC,
	[ShowroomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vendor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Vendor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VendorCode] [nvarchar](100) NOT NULL,
	[VendorName] [nvarchar](255) NOT NULL,
	[PrimaryEmail] [nvarchar](255) NULL,
	[PrimaryPhone] [nvarchar](255) NULL,
	[Status] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Warehouse]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Warehouse]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Warehouse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Warehouse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActivityAssignment_Activity]') AND parent_object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]'))
ALTER TABLE [dbo].[ActivityAssignment]  WITH CHECK ADD  CONSTRAINT [FK_ActivityAssignment_Activity] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activity] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ActivityAssignment_Activity]') AND parent_object_id = OBJECT_ID(N'[dbo].[ActivityAssignment]'))
ALTER TABLE [dbo].[ActivityAssignment] CHECK CONSTRAINT [FK_ActivityAssignment_Activity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CalendarEvent_CalendarEventType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CalendarEvent]'))
ALTER TABLE [dbo].[CalendarEvent]  WITH CHECK ADD  CONSTRAINT [FK_CalendarEvent_CalendarEventType] FOREIGN KEY([CalendarEventTypeId])
REFERENCES [dbo].[CalendarEventType] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CalendarEvent_CalendarEventType]') AND parent_object_id = OBJECT_ID(N'[dbo].[CalendarEvent]'))
ALTER TABLE [dbo].[CalendarEvent] CHECK CONSTRAINT [FK_CalendarEvent_CalendarEventType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactComment_Contact]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactComment]'))
ALTER TABLE [dbo].[ContactComment]  WITH CHECK ADD  CONSTRAINT [FK_ContactComment_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactComment_Contact]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactComment]'))
ALTER TABLE [dbo].[ContactComment] CHECK CONSTRAINT [FK_ContactComment_Contact]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactMeetingProgress_Contact]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactMeetingProgress]'))
ALTER TABLE [dbo].[ContactMeetingProgress]  WITH CHECK ADD  CONSTRAINT [FK_ContactMeetingProgress_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContactMeetingProgress_Contact]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContactMeetingProgress]'))
ALTER TABLE [dbo].[ContactMeetingProgress] CHECK CONSTRAINT [FK_ContactMeetingProgress_Contact]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerServiceComment_CustomerService]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerServiceComment]'))
ALTER TABLE [dbo].[CustomerServiceComment]  WITH CHECK ADD  CONSTRAINT [FK_CustomerServiceComment_CustomerService] FOREIGN KEY([CustomerServiceId])
REFERENCES [dbo].[CustomerService] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerServiceComment_CustomerService]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerServiceComment]'))
ALTER TABLE [dbo].[CustomerServiceComment] CHECK CONSTRAINT [FK_CustomerServiceComment_CustomerService]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Dealer_Company]') AND parent_object_id = OBJECT_ID(N'[dbo].[Dealer]'))
ALTER TABLE [dbo].[Dealer]  WITH CHECK ADD  CONSTRAINT [FK_Dealer_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Dealer_Company]') AND parent_object_id = OBJECT_ID(N'[dbo].[Dealer]'))
ALTER TABLE [dbo].[Dealer] CHECK CONSTRAINT [FK_Dealer_Company]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventInvitee_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventInvitee]'))
ALTER TABLE [dbo].[EventInvitee]  WITH CHECK ADD  CONSTRAINT [FK_EventInvitee_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventInvitee_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventInvitee]'))
ALTER TABLE [dbo].[EventInvitee] CHECK CONSTRAINT [FK_EventInvitee_CalendarEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventRecurrence_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventRecurrence]'))
ALTER TABLE [dbo].[EventRecurrence]  WITH CHECK ADD  CONSTRAINT [FK_EventRecurrence_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventRecurrence_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventRecurrence]'))
ALTER TABLE [dbo].[EventRecurrence] CHECK CONSTRAINT [FK_EventRecurrence_CalendarEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventReminder_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventReminder]'))
ALTER TABLE [dbo].[EventReminder]  WITH CHECK ADD  CONSTRAINT [FK_EventReminder_CalendarEvent] FOREIGN KEY([EventId])
REFERENCES [dbo].[CalendarEvent] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EventReminder_CalendarEvent]') AND parent_object_id = OBJECT_ID(N'[dbo].[EventReminder]'))
ALTER TABLE [dbo].[EventReminder] CHECK CONSTRAINT [FK_EventReminder_CalendarEvent]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContract_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContract]'))
ALTER TABLE [dbo].[HandOverContract]  WITH CHECK ADD  CONSTRAINT [FK_HandOverContract_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContract_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContract]'))
ALTER TABLE [dbo].[HandOverContract] CHECK CONSTRAINT [FK_HandOverContract_Customer]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContractComment_HandOverContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContractComment]'))
ALTER TABLE [dbo].[HandOverContractComment]  WITH CHECK ADD  CONSTRAINT [FK_HandOverContractComment_HandOverContract] FOREIGN KEY([HandOverContractId])
REFERENCES [dbo].[HandOverContract] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContractComment_HandOverContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContractComment]'))
ALTER TABLE [dbo].[HandOverContractComment] CHECK CONSTRAINT [FK_HandOverContractComment_HandOverContract]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContractLine_HandOverContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContractLine]'))
ALTER TABLE [dbo].[HandOverContractLine]  WITH CHECK ADD  CONSTRAINT [FK_HandOverContractLine_HandOverContract] FOREIGN KEY([HandOverContractId])
REFERENCES [dbo].[HandOverContract] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HandOverContractLine_HandOverContract]') AND parent_object_id = OBJECT_ID(N'[dbo].[HandOverContractLine]'))
ALTER TABLE [dbo].[HandOverContractLine] CHECK CONSTRAINT [FK_HandOverContractLine_HandOverContract]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Product_ProductCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Product]'))
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY([ProductCategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Product_ProductCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Product]'))
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductCategory_ProductCategoryGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductCategory]'))
ALTER TABLE [dbo].[ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_ProductCategory_ProductCategoryGroup] FOREIGN KEY([CategoryGroupId])
REFERENCES [dbo].[ProductCategoryGroup] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductCategory_ProductCategoryGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductCategory]'))
ALTER TABLE [dbo].[ProductCategory] CHECK CONSTRAINT [FK_ProductCategory_ProductCategoryGroup]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToCustomPermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToCustomPermission]'))
ALTER TABLE [dbo].[ProfileToCustomPermission]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToCustomPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToCustomPermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToCustomPermission]'))
ALTER TABLE [dbo].[ProfileToCustomPermission] CHECK CONSTRAINT [FK_ProfileToCustomPermission_Permission]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToCustomPermission_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToCustomPermission]'))
ALTER TABLE [dbo].[ProfileToCustomPermission]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToCustomPermission_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToCustomPermission_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToCustomPermission]'))
ALTER TABLE [dbo].[ProfileToCustomPermission] CHECK CONSTRAINT [FK_ProfileToCustomPermission_Profile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToStandardPermission_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToStandardPermission]'))
ALTER TABLE [dbo].[ProfileToStandardPermission]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToStandardPermission_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToStandardPermission_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToStandardPermission]'))
ALTER TABLE [dbo].[ProfileToStandardPermission] CHECK CONSTRAINT [FK_ProfileToStandardPermission_Profile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToStandardPermission_Tab]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToStandardPermission]'))
ALTER TABLE [dbo].[ProfileToStandardPermission]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToStandardPermission_Tab] FOREIGN KEY([TabId])
REFERENCES [dbo].[Tab] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToStandardPermission_Tab]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToStandardPermission]'))
ALTER TABLE [dbo].[ProfileToStandardPermission] CHECK CONSTRAINT [FK_ProfileToStandardPermission_Tab]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToTab_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToTab]'))
ALTER TABLE [dbo].[ProfileToTab]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToTab_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToTab_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToTab]'))
ALTER TABLE [dbo].[ProfileToTab] CHECK CONSTRAINT [FK_ProfileToTab_Profile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToTab_Tab]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToTab]'))
ALTER TABLE [dbo].[ProfileToTab]  WITH CHECK ADD  CONSTRAINT [FK_ProfileToTab_Tab] FOREIGN KEY([TabId])
REFERENCES [dbo].[Tab] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileToTab_Tab]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileToTab]'))
ALTER TABLE [dbo].[ProfileToTab] CHECK CONSTRAINT [FK_ProfileToTab_Tab]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Report_ReportFolder]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report]'))
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_ReportFolder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[ReportFolder] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Report_ReportFolder]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report]'))
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_ReportFolder]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoleToProfile_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoleToProfile]'))
ALTER TABLE [dbo].[RoleToProfile]  WITH CHECK ADD  CONSTRAINT [FK_RoleToProfile_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoleToProfile_Profile]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoleToProfile]'))
ALTER TABLE [dbo].[RoleToProfile] CHECK CONSTRAINT [FK_RoleToProfile_Profile]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoleToProfile_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoleToProfile]'))
ALTER TABLE [dbo].[RoleToProfile]  WITH CHECK ADD  CONSTRAINT [FK_RoleToProfile_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoleToProfile_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoleToProfile]'))
ALTER TABLE [dbo].[RoleToProfile] CHECK CONSTRAINT [FK_RoleToProfile_Role]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Showroom_Dealer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Showroom]'))
ALTER TABLE [dbo].[Showroom]  WITH CHECK ADD  CONSTRAINT [FK_Showroom_Dealer] FOREIGN KEY([DealerId])
REFERENCES [dbo].[Dealer] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Showroom_Dealer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Showroom]'))
ALTER TABLE [dbo].[Showroom] CHECK CONSTRAINT [FK_Showroom_Dealer]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserDashboard_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserDashboard]'))
ALTER TABLE [dbo].[UserDashboard]  WITH CHECK ADD  CONSTRAINT [FK_UserDashboard_UserInfo] FOREIGN KEY([UserKey])
REFERENCES [dbo].[UserInfo] ([UserKey])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserDashboard_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserDashboard]'))
ALTER TABLE [dbo].[UserDashboard] CHECK CONSTRAINT [FK_UserDashboard_UserInfo]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfo_UserInfoToRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfo]'))
ALTER TABLE [dbo].[UserInfo]  WITH CHECK ADD  CONSTRAINT [FK_UserInfo_UserInfoToRole] FOREIGN KEY([UserKey])
REFERENCES [dbo].[UserInfoToRole] ([UserKey])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfo_UserInfoToRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfo]'))
ALTER TABLE [dbo].[UserInfo] CHECK CONSTRAINT [FK_UserInfo_UserInfoToRole]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToCompany_Company]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToCompany]'))
ALTER TABLE [dbo].[UserInfoToCompany]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToCompany_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToCompany_Company]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToCompany]'))
ALTER TABLE [dbo].[UserInfoToCompany] CHECK CONSTRAINT [FK_UserInfoToCompany_Company]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToCompany_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToCompany]'))
ALTER TABLE [dbo].[UserInfoToCompany]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToCompany_UserInfo] FOREIGN KEY([UserKey])
REFERENCES [dbo].[UserInfo] ([UserKey])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToCompany_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToCompany]'))
ALTER TABLE [dbo].[UserInfoToCompany] CHECK CONSTRAINT [FK_UserInfoToCompany_UserInfo]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToDealer_Dealer]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToDealer]'))
ALTER TABLE [dbo].[UserInfoToDealer]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToDealer_Dealer] FOREIGN KEY([DealerId])
REFERENCES [dbo].[Dealer] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToDealer_Dealer]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToDealer]'))
ALTER TABLE [dbo].[UserInfoToDealer] CHECK CONSTRAINT [FK_UserInfoToDealer_Dealer]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToDealer_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToDealer]'))
ALTER TABLE [dbo].[UserInfoToDealer]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToDealer_UserInfo] FOREIGN KEY([UserKey])
REFERENCES [dbo].[UserInfo] ([UserKey])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToDealer_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToDealer]'))
ALTER TABLE [dbo].[UserInfoToDealer] CHECK CONSTRAINT [FK_UserInfoToDealer_UserInfo]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToRole_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToRole]'))
ALTER TABLE [dbo].[UserInfoToRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToRole_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToRole]'))
ALTER TABLE [dbo].[UserInfoToRole] CHECK CONSTRAINT [FK_UserInfoToRole_Role]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToShowroom_Showroom]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToShowroom]'))
ALTER TABLE [dbo].[UserInfoToShowroom]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToShowroom_Showroom] FOREIGN KEY([ShowroomId])
REFERENCES [dbo].[Showroom] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToShowroom_Showroom]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToShowroom]'))
ALTER TABLE [dbo].[UserInfoToShowroom] CHECK CONSTRAINT [FK_UserInfoToShowroom_Showroom]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToShowroom_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToShowroom]'))
ALTER TABLE [dbo].[UserInfoToShowroom]  WITH CHECK ADD  CONSTRAINT [FK_UserInfoToShowroom_UserInfo] FOREIGN KEY([UserKey])
REFERENCES [dbo].[UserInfo] ([UserKey])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserInfoToShowroom_UserInfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserInfoToShowroom]'))
ALTER TABLE [dbo].[UserInfoToShowroom] CHECK CONSTRAINT [FK_UserInfoToShowroom_UserInfo]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ProductNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Product Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'* Tên Xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ProductActive'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Đang kinh doanh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductActive'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'UnitPrice'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Đơn giá' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'UnitPrice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PercentVat'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT(%)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PercentVat'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PercentService'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Service(%)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PercentService'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PercentSales'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales(%)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PercentSales'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'QuantityInStock'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Số lượng tồn kho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'QuantityInStock'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ProductCategoryId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dòng xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductCategoryId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'VIN'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Số khung' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'VIN'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'EngineNumber'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Số máy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'EngineNumber'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'InActiveForSale'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Đang kinh doanh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'InActiveForSale'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'WarehouseName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tên kho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'WarehouseName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'Status'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trạng thái' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Status'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'Color'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Màu xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Color'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ProductType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loại' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'DateEnteredStock'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngày nhập kho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'DateEnteredStock'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'DateRelease'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngày xuất kho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'DateRelease'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'TransmissionType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hộp số' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'TransmissionType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ProductionDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Năm sản xuất' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ProductionDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'NumberOfSeats'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Số chỗ ngồi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'NumberOfSeats'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'Engine'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Động cơ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Engine'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'UOM'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Đơn vị tính' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'UOM'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'HandlerUserKey'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'* Người xử lý' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'HandlerUserKey'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'InStockAge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tuổi kho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'InStockAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'ContractId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hợp đồng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'ContractId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PurchaseOrderNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Số đơn hàng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PurchaseOrderNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'BankOfGuarantee'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NH bảo lãnh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'BankOfGuarantee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PaymentStatus'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tình trạng thanh toán' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PaymentStatus'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'Description'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mô tả' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'Description'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Product', N'COLUMN',N'PlateNumber'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Biển số xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Product', @level2type=N'COLUMN',@level2name=N'PlateNumber'
GO
USE [master]
GO
ALTER DATABASE [CRMApp] SET  READ_WRITE 
GO
