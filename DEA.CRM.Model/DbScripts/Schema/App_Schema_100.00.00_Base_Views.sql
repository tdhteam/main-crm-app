﻿-- noinspection SqlNoDataSourceInspectionForFile
USE [CRMApp]
GO

/*Schema for SQL views*/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllActiveUserInfo]'))
    DROP VIEW [dbo].[vwAllActiveUserInfo]
GO
CREATE VIEW [dbo].[vwAllActiveUserInfo]
AS
	SELECT *
		, LTRIM(IsNull(FirstName, '') + ' ' + IsNull(LastName, '')) as FullName
	FROM UserInfo 
	WHERE Deleted = 0
GO

/* View schema for user info searching */
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwUserInfoSearch]'))
    DROP VIEW [dbo].[vwUserInfoSearch]
GO
CREATE VIEW [dbo].[vwUserInfoSearch]
AS
	SELECT u.UserKey
		, u.UserName
		, u.FirstName
		, u.LastName
		, LTRIM(IsNull(FirstName, '') + ' ' + IsNull(LastName, '')) as FullName
		, u.IsAdmin
		, u.Email
		, r.RoleId
		, r.Name AS RoleName
		, u.Status
	FROM UserInfo u INNER JOIN UserInfoToRole ur ON u.UserKey = ur.UserKey
		INNER JOIN [Role] r ON ur.RoleId = r.RoleId
	WHERE u.Deleted = 0
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAllCalendarEvent]'))
    DROP VIEW [dbo].[vwAllCalendarEvent]
GO
CREATE VIEW [dbo].[vwAllCalendarEvent]
AS
    SELECT [Id]
      ,[Subject]
      ,[IsRecurring]
      ,[RecurringType]
      ,[StartDateTime]
      ,[DueDateTime]
      ,[SendNotification]
      ,[DurationHours]
      ,[DurationMinutues]
      ,[Status]
      ,[EventStatus]
      ,[Priority]
      ,[Location]
      ,[SendReminder]
      ,[Visibility]
      ,[OwnerUserKey]
      ,[RelatedToContactId]
      ,[RelatedToHandoverContractId]
      ,[RelatedToCustomerServiceId]
      ,[Description]
      ,[Tags]
      ,[CompanyId]
      ,[DealerId]
      ,[ShowroomId]
	  ,'activity' AS EntityType
	FROM Activity
	UNION
	SELECT [Id]
      ,[Subject]
      ,[IsRecurring]
      ,[RecurringType]
      ,[StartDateTime]
      ,[DueDateTime]
      ,[SendNotification]
      ,[DurationHours]
      ,[DurationMinutues]
      ,[Status]
      ,[EventStatus]
      ,[Priority]
      ,[Location]
      ,[SendReminder]
      ,[Visibility]
      ,[OwnerUserKey]
      ,[RelatedToContactId]
      ,[RelatedToHandoverContractId]
      ,[RelatedToCustomerServiceId]
      ,[Description]
      ,[Tags]
      ,[CompanyId]
      ,[DealerId]
      ,[ShowroomId]
	  ,'calendar event' AS EntityType
	FROM CalendarEvent
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCalendarEventListViewSearchResult]'))
    DROP VIEW [dbo].[vwCalendarEventListViewSearchResult]
GO
CREATE VIEW [dbo].[vwCalendarEventListViewSearchResult]
AS
    SELECT 
		a.[Id]
		,'activity_' + cast(a.Id as varchar) AS UniqueKey
		,'activity' AS EntityType
		, 'Task' as ActivityType
		,Subject
		,RecurringType
		,StartDateTime
		,DueDateTime
		,DurationHours
		,DurationMinutues
		,a.Status
		,EventStatus
		,Priority
		,Location
		,Visibility
		,OwnerUserKey
		,u.FullName AS OwnerUserFullName
		,RelatedToContactId
		,RelatedToHandoverContractId
		,RelatedToCustomerServiceId
		,a.Description
		,Tags
		,a.CompanyId
		,c.Name AS CompanyName
		,a.DealerId
		,d.Name AS DealerName
		,a.ShowroomId
		,s.Name AS ShowroomName
	FROM 
		Activity a INNER JOIN vwAllActiveUserInfo u ON a.OwnerUserKey = u.UserKey
		LEFT JOIN Company c ON a.CompanyId = c.Id
		LEFT JOIN Dealer d ON a.CompanyId = d.CompanyId AND a.DealerId = d.Id
		LEFT JOIN Showroom s ON d.Id = s.DealerId and a.ShowroomId = s.Id	
	UNION
	SELECT 
		e.[Id]
		,'calendar_event_' + cast(e.Id as varchar) AS UniqueKey
		,'calendar event' AS EntityType
		, cet.Name as ActivityType
		,Subject
		,RecurringType
		,StartDateTime
		,DueDateTime
		,DurationHours
		,DurationMinutues
		,e.Status
		,EventStatus
		,Priority
		,Location
		,Visibility
		,OwnerUserKey
		,u.FullName AS OwnerUserFullName
		,RelatedToContactId
		,RelatedToHandoverContractId
		,RelatedToCustomerServiceId
		,e.Description
		,Tags
		,e.CompanyId
		,c.Name AS CompanyName
		,e.DealerId
		,d.Name AS DealerName
		,e.ShowroomId
		,s.Name AS ShowroomName
	FROM 
		CalendarEvent e INNER JOIN vwAllActiveUserInfo u ON e.OwnerUserKey = u.UserKey
		INNER JOIN CalendarEventType cet ON e.CalendarEventTypeId = cet.Id
		LEFT JOIN Company c ON e.CompanyId = c.Id
		LEFT JOIN Dealer d ON e.CompanyId = d.CompanyId AND e.DealerId = d.Id
		LEFT JOIN Showroom s ON d.Id = s.DealerId and e.ShowroomId = s.Id
	WHERE cet.Presence = 1
GO
