﻿-- noinspection SqlNoDataSourceInspectionForFile

-- noinspection SqlNoDataSourceInspection
CREATE TABLE [__EFMigrationsHistory]
(
  MigrationId    NVARCHAR(150) NOT NULL
    CONSTRAINT PK___EFMigrationsHistory
    PRIMARY KEY,
  ProductVersion NVARCHAR(32)  NOT NULL
)
GO

CREATE TABLE AspNetRoles
(
  Id               NVARCHAR(450) NOT NULL
    CONSTRAINT PK_AspNetRoles
    PRIMARY KEY,
  ConcurrencyStamp NVARCHAR(MAX),
  Name             NVARCHAR(256),
  NormalizedName   NVARCHAR(256)
)
GO

CREATE UNIQUE INDEX RoleNameIndex
  ON AspNetRoles (NormalizedName)
GO

CREATE TABLE AspNetUsers
(
  Id                   NVARCHAR(450) NOT NULL
    CONSTRAINT PK_AspNetUsers
    PRIMARY KEY,
  AccessFailedCount    INT           NOT NULL,
  ConcurrencyStamp     NVARCHAR(MAX),
  Email                NVARCHAR(256),
  EmailConfirmed       BIT           NOT NULL,
  LockoutEnabled       BIT           NOT NULL,
  LockoutEnd           DATETIMEOFFSET,
  NormalizedEmail      NVARCHAR(256),
  NormalizedUserName   NVARCHAR(256),
  PasswordHash         NVARCHAR(MAX),
  PhoneNumber          NVARCHAR(MAX),
  PhoneNumberConfirmed BIT           NOT NULL,
  SecurityStamp        NVARCHAR(MAX),
  TwoFactorEnabled     BIT           NOT NULL,
  UserName             NVARCHAR(256)
)
GO

CREATE INDEX EmailIndex
  ON AspNetUsers (NormalizedEmail)
GO

CREATE UNIQUE INDEX UserNameIndex
  ON AspNetUsers (NormalizedUserName)
GO

CREATE TABLE AspNetRoleClaims
(
  Id         INT IDENTITY
    CONSTRAINT PK_AspNetRoleClaims
    PRIMARY KEY,
  ClaimType  NVARCHAR(MAX),
  ClaimValue NVARCHAR(MAX),
  RoleId     NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetRoleClaims_AspNetRoles_RoleId
    REFERENCES AspNetRoles
)
GO

CREATE INDEX IX_AspNetRoleClaims_RoleId
  ON AspNetRoleClaims (RoleId)
GO

CREATE TABLE AspNetUserClaims
(
  Id         INT IDENTITY
    CONSTRAINT PK_AspNetUserClaims
    PRIMARY KEY,
  ClaimType  NVARCHAR(MAX),
  ClaimValue NVARCHAR(MAX),
  UserId     NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId
    REFERENCES AspNetUsers
)
GO

CREATE INDEX IX_AspNetUserClaims_UserId
  ON AspNetUserClaims (UserId)
GO

CREATE TABLE AspNetUserLogins
(
  LoginProvider       NVARCHAR(450) NOT NULL,
  ProviderKey         NVARCHAR(450) NOT NULL,
  ProviderDisplayName NVARCHAR(MAX),
  UserId              NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetUserLogins_AspNetUsers_UserId
    REFERENCES AspNetUsers,
  CONSTRAINT PK_AspNetUserLogins
  PRIMARY KEY (LoginProvider, ProviderKey)
)
GO

CREATE INDEX IX_AspNetUserLogins_UserId
  ON AspNetUserLogins (UserId)
GO

CREATE TABLE AspNetUserRoles
(
  UserId NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetUserRoles_AspNetUsers_UserId
    REFERENCES AspNetUsers,
  RoleId NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetUserRoles_AspNetRoles_RoleId
    REFERENCES AspNetRoles,
  CONSTRAINT PK_AspNetUserRoles
  PRIMARY KEY (UserId, RoleId)
)
GO

CREATE INDEX IX_AspNetUserRoles_RoleId
  ON AspNetUserRoles (RoleId)
GO

CREATE TABLE AspNetUserTokens
(
  UserId        NVARCHAR(450) NOT NULL
    CONSTRAINT FK_AspNetUserTokens_AspNetUsers_UserId
    REFERENCES AspNetUsers,
  LoginProvider NVARCHAR(450) NOT NULL,
  Name          NVARCHAR(450) NOT NULL,
  Value         NVARCHAR(MAX),
  CONSTRAINT PK_AspNetUserTokens
  PRIMARY KEY (UserId, LoginProvider, Name)
)
GO

