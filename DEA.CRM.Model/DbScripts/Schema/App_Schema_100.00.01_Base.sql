-- noinspection SqlNoDataSourceInspectionForFile
USE [CRMApp]
GO

/****** Object:  Table [dbo].[DocumentFolder]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentFolder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DocumentFolder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_DocumentFolder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT 1 FROM DocumentFolder WHERE Name='Default')
INSERT INTO DocumentFolder(Name, [Description]) VALUES('Default', 'Default folder')
GO

IF NOT EXISTS(SELECT 1 FROM DocumentFolder WHERE Name='Dropbox')
INSERT INTO DocumentFolder(Name, [Description]) VALUES('Dropbox', 'Document from Dropbox')
GO

IF NOT EXISTS(SELECT 1 FROM DocumentFolder WHERE Name='Google Drive')
INSERT INTO DocumentFolder(Name, [Description]) VALUES('Google Drive', 'Document from Google Drive')
GO


/****** Object:  Table [dbo].[UserDocument]    Script Date: 4/23/2018 5:38:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDocument]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDocument](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](500) NULL,
    [AttachmentId] [varchar](36) NULL,
    [DocumentLink] [varchar](255) NULL,
    [OwnerUserKey] [nvarchar](128) NOT NULL,
    [OwnerUserFullName] [nvarchar](255) NOT NULL,
    [FolderName] [nvarchar](255) NOT NULL,
    [CreatedDate] [datetime] NOT NULL,
    [ModifiedDate] [datetime] NOT NULL,
    [ModifiedByUserKey] [nvarchar](128) NOT NULL,
    [ModifiedByUserFullName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_UserDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

