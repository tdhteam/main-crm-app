﻿-- noinspection SqlNoDataSourceInspectionForFile

USE [CRMApp]
GO

/*******************************Role**********************************************/
IF NOT EXISTS (SELECT 1 FROM [Role] WHERE RoleId = 'H1')
BEGIN
  INSERT [dbo].[Role] ([RoleId], [Name], [ParentRoleId], [ParentRoleName], [Depth], [TreePath]) 
  VALUES (N'H1', N'Organization', NULL, NULL, 0, N'H1')
END
GO

IF NOT EXISTS (SELECT 1 FROM [Role] WHERE RoleId = 'H2')
BEGIN
  INSERT [dbo].[Role] ([RoleId], [Name], [ParentRoleId], [ParentRoleName], [Depth], [TreePath]) 
  VALUES (N'H2', N'CEO', N'H1', N'Organization', 1, N'H1::H2')
END
GO

IF NOT EXISTS (SELECT 1 FROM [Role] WHERE RoleId = 'H3')
BEGIN
  INSERT [dbo].[Role] ([RoleId], [Name], [ParentRoleId], [ParentRoleName], [Depth], [TreePath]) 
  VALUES (N'H3', N'Sales Manager', N'H2', N'CEO', 2, N'H1::H2::H3')
END
GO

IF NOT EXISTS (SELECT 1 FROM [Role] WHERE RoleId = 'H4')
BEGIN
  INSERT [dbo].[Role] ([RoleId], [Name], [ParentRoleId], [ParentRoleName], [Depth], [TreePath]) 
  VALUES (N'H4', N'Marketing Manager', N'H2', N'CEO', 2, N'H1::H2::H4')
END
GO

/***************************************Profile**************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[Profile] WHERE [Id]=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[Profile] ON 
  
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (1, N'Administrator', N'Admin Profile', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (2, N'Dealer Profile', N'Profile Related to Dealer', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (3, N'GM Profile', N'Profile Related to Dealer GM', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (4, N'SM Profile', N'Profile Related to Dealer SM', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (5, N'Sales Profile', N'Profile Related to Sales', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (6, N'Support Profile', N'Profile Related to Support', 1)
  INSERT [dbo].[Profile] ([Id], [Name], [Description], [DirectlyRelatedToRole]) VALUES (7, N'Sales Manager Profile', N'Default profile assigned direct to role Sales Manager', 1)
  
  SET IDENTITY_INSERT [dbo].[Profile] OFF
  
END
GO

/********************************RoleToProfile*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[RoleToProfile] WHERE [RoleId]=N'H2' AND [ProfileId]=1)
BEGIN
  INSERT [dbo].[RoleToProfile] ([RoleId], [ProfileId]) VALUES (N'H2', 1)
END
GO


/********************************Permission*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[Permission] WHERE [Id]=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[Permission] ON 
  
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (1, N'Import Contact', N'User can import Contact from external data source', 1)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (2, N'Export Contact', N'Export Contact data to spreadsheet format', 2)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (3, N'Import Product', N'User can import Product from external data source', 3)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (4, N'Export Product', N'Export Product data to spreadsheet format', 4)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (5, N'Export Customer', N'Export Customer data to spreadsheet format', 5)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (6, N'Export Contract', N'Export Contract data to spreadsheet format', 6)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (8, N'Export Customer Service', N'Export Customer Service data to spreadsheet format', 7)
  INSERT [dbo].[Permission] ([Id], [Name], [Description], [DisplayOrder]) VALUES (9, N'Export Reports', N'Export Report data to spreadsheet format', 8)
  
  SET IDENTITY_INSERT [dbo].[Permission] OFF
END
GO

/********************************ParentTab*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[ParentTab] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[ParentTab] ON 
  
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (1, N'Home', 1, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (2, N'Marketing', 2, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (3, N'Sales', 3, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (4, N'Support', 4, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (5, N'Inventory', 5, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (6, N'Reports', 6, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (7, N'Tools', 7, 1)
  INSERT [dbo].[ParentTab] ([Id], [Label], [Sequence], [Visible]) VALUES (8, N'Settings', 8, 1)
  
  SET IDENTITY_INSERT [dbo].[ParentTab] OFF

END
GO

/********************************Tab*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[Tab] WHERE [Id]=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[Tab] ON 
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (1, N'Dashboard', 1, N'Dashboard', 6, N'Reports')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (2, N'Contacts', -1, N'Contacts', 3, N'Sales')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (3, N'Products', -1, N'Products', 3, N'Sales')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (4, N'HandoverContracts', -1, N'Handover Contracts', 3, N'Sales')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (5, N'CustomerServices', -1, N'Customer Services', 4, N'Support')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (6, N'Calendar', -1, N'Calendar', 7, N'Tools')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (7, N'Reports', -1, N'Reports', 6, N'Reports')
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (8, N'Comments', -1, N'Comments', NULL, NULL)
  
  INSERT [dbo].[Tab] ([Id], [Name], [Sequence], [Label], [ParentTabId], [ParentTabLabel]) 
  VALUES (9, N'Attachments', -1, N'Attachments', NULL, NULL)
  
  SET IDENTITY_INSERT [dbo].[Tab] OFF

END
GO


/************************************UserInfoToRole*****************************************/


IF NOT EXISTS (SELECT 1 FROM [dbo].[UserInfoToRole] WHERE [UserKey]=N'e8229596-8c18-44ef-9269-c16d4bdcdaf7' AND [RoleId]=N'H2')
BEGIN 
  INSERT [dbo].[UserInfoToRole] ([UserKey], [RoleId]) VALUES (N'e8229596-8c18-44ef-9269-c16d4bdcdaf7', N'H2')
END
GO

/********************************UserInfo*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[UserInfo] WHERE UserName='admin')
BEGIN
  INSERT [dbo].[UserInfo] (
    [UserKey], 
    [UserName], 
    [CalendarColor], 
    [Salutation], 
    [FirstName], 
    [LastName], 
    [ReportToUserKey], 
    [IsAdmin],  
    [Description], 
    [CreatedDate], 
    [ModifiedDate], 
    [ModifiedByUserKey], 
    [ModifiedByUserFullName], 
    [Title], 
    [Department], 
    [HomePhone], 
    [WorkPhone], 
    [MobilePhone], 
    [OtherPhone], 
    [Fax], 
    [Email], 
    [Status], 
    [AddressStreet], 
    [AddressDistrict], 
    [AddressCity], 
    [AddressState], 
    [AddressCountry], 
    [AddressPostalCode], 
    [DateFormat], 
    [HourFotmat], 
    [Deleted], 
    [ReminderIntervalInSeconds], [Language], [TimeZone], [CurrencyGroupingPattern], [CurrencyDecimalSeparator], [CurrencyGroupingSeparator], [CurrencySymbolPlacement], [NumberOfCurrencyDecimal], [AvatarRelativeUrl], [AccessAllCompanies], [AccessAllDealers], [AccessAllShowrooms], [DefaultCompanyId], [DefaultDealerId], [DefaultShowroomId]) 
  VALUES (
    N'e8229596-8c18-44ef-9269-c16d4bdcdaf7', 
    N'admin', 
    N'#FF00EE', 
    NULL, 
    N'Admin', 
    N'CRM', 
    NULL, 
    1, 
    NULL, 
    CAST(N'2018-04-04 11:40:35.570' AS DateTime), 
    CAST(N'2018-04-04 11:40:35.570' AS DateTime), 
    N'a3ffae5c-e2eb-4370-8c43-3de692389776', 
    N'system', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Active', NULL, NULL, NULL, NULL, NULL, NULL, N'dd/MM/yyyy', N'hh:mm', 0, NULL, N'en', N'UTC', N'123,456.50', N'.', N',', N'VND', 2, NULL, 1, 1, 1, NULL, NULL, NULL)
END
GO

/********************************Company*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[Company] WHERE Id=1)
BEGIN
  SET IDENTITY_INSERT [dbo].[Company] ON 

  INSERT [dbo].[Company] ([Id], [CompanyCode], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax], [WebSite], [LogoRelativeUrl]) 
  VALUES (1, N'DEA', N'CTY TNHH DEA Việt Nam', N'', N'', N'', N'', N'', N'', N'', N'', N'')

  SET IDENTITY_INSERT [dbo].[Company] OFF

END
GO

/********************************Dealer*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[Dealer] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[Dealer] ON 

  INSERT [dbo].[Dealer] ([Id], [CompanyId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax], [WebSite], [LogoRelativeUrl]) 
  VALUES (1, 1, N'Dealer 1', N'', N'HN', N'', N'', N'', N'', N'', N'', N'')

  INSERT [dbo].[Dealer] ([Id], [CompanyId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax], [WebSite], [LogoRelativeUrl]) 
  VALUES (2, 1, N'Dealer 2', N'', N'HCM', N'', N'', N'', N'', N'', N'', N'')

  INSERT [dbo].[Dealer] ([Id], [CompanyId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax], [WebSite], [LogoRelativeUrl]) 
  VALUES (3, 1, N'Dealer 3', N'', N'DN', N'', N'', N'', N'', N'', N'', N'')

  SET IDENTITY_INSERT [dbo].[Dealer] OFF
  
END
GO

/********************************Showroom*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[Showroom] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[Showroom] ON 

  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (1, 1, N'Showroom 1', N'', N'', N'', N'', N'', N'', N'')
  
  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (2, 1, N'Showroom 2', N'', N'', N'', N'', N'', N'', N'')
  
  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (3, 1, N'Showroom 3', N'', N'', N'', N'', N'', N'', N'')
  
  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (4, 2, N'Showroom 4', N'', N'', N'', N'', N'', N'', N'')
  
  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (5, 2, N'Showroom 5', N'', N'', N'', N'', N'', N'', N'')
  
  INSERT [dbo].[Showroom] ([Id], [DealerId], [Name], [Address], [City], [State], [Country], [PostalCode], [Phone], [Fax]) 
  VALUES (6, 3, N'Showroom 6', N'', N'', N'', N'', N'', N'', N'')
  
  SET IDENTITY_INSERT [dbo].[Showroom] OFF

END
GO

/********************************CalendarEventType*********************************************/

IF NOT EXISTS (SELECT 1 FROM [dbo].[CalendarEventType] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[CalendarEventType] ON 

  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (1, N'Call', 1, 1, N'#EC7063')
  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (2, N'Meeting', 1, 2, N'#9B59B6')
  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (3, N'Mobile Call', 1, 3, N'#5DADE2')
  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (4, N'Onsite Meeting', 1, 4, N'#F5B041')
  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (5, N'Onsite Service', 1, 5, N'#58D68D')
  INSERT [dbo].[CalendarEventType] ([Id], [Name], [Presence], [SortOrderId], [Color]) VALUES (6, N'Group Event', 1, 6, N'#F5CBA7')

  SET IDENTITY_INSERT [dbo].[CalendarEventType] OFF

END
GO

/********************************ProductCategoryGroup*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[ProductCategoryGroup] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[ProductCategoryGroup] ON 
  INSERT [dbo].[ProductCategoryGroup] ([Id], [Name], [Description]) VALUES (1, N'Xe', N'Tất cả các loại xe')
  SET IDENTITY_INSERT [dbo].[ProductCategoryGroup] OFF
  
END
GO

/********************************ProductCategory*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[ProductCategory] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[ProductCategory] ON 
  
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (1, N'Attrage', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (2, N'Lancer Hatchback', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (3, N'Lancer Sedan', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (4, N'Mirage Hatchback', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (5, N'Outlander Sport Utility', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (6, N'Outlander Sport Sport Utility', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (7, N'Pajero', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (8, N'Pajero Sport', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (9, N'Triton', NULL, 1)
  INSERT [dbo].[ProductCategory] ([Id], [Name], [Description], [CategoryGroupId]) VALUES (10, N'i-MiEv Hatchback', NULL, 1)
  
  SET IDENTITY_INSERT [dbo].[ProductCategory] OFF

END
GO

/********************************ActivityPriority*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[ActivityPriority] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[ActivityPriority] ON
   
  INSERT [dbo].[ActivityPriority] ([Id], [Name], [Presence], [SortOrderId]) VALUES (1, N'High', 1, 1)
  INSERT [dbo].[ActivityPriority] ([Id], [Name], [Presence], [SortOrderId]) VALUES (2, N'Medium', 1, 2)
  INSERT [dbo].[ActivityPriority] ([Id], [Name], [Presence], [SortOrderId]) VALUES (3, N'Low', 1, 3)

  SET IDENTITY_INSERT [dbo].[ActivityPriority] OFF

END 
GO

/********************************ActivityStatus*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[ActivityStatus] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[ActivityStatus] ON 
  
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (1, N'Not Started', 1, 1)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (2, N'In Progress', 1, 2)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (3, N'Completed', 1, 3)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (4, N'Pending Input', 1, 4)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (5, N'Deferred', 1, 5)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (6, N'Planned', 1, 6)
  INSERT [dbo].[ActivityStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (7, N'Cancelled', 1, 7)
  
  SET IDENTITY_INSERT [dbo].[ActivityStatus] OFF

END
GO

/********************************BankInfo*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[BankInfo] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[BankInfo] ON 

  INSERT [dbo].[BankInfo] ([Id], [Code], [Name]) VALUES (1, N'VCB', N'Vietcombank')
  INSERT [dbo].[BankInfo] ([Id], [Code], [Name]) VALUES (2, N'TCB', N'Techcombank')
  INSERT [dbo].[BankInfo] ([Id], [Code], [Name]) VALUES (3, N'HSBC', N'HSBC')
  INSERT [dbo].[BankInfo] ([Id], [Code], [Name]) VALUES (4, N'CITI', N'Citi bank')
  
  SET IDENTITY_INSERT [dbo].[BankInfo] OFF

END
GO

/********************************EventStatus*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[EventStatus] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[EventStatus] ON 
  
  INSERT [dbo].[EventStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (1, N'Planned', 1, 1)
  INSERT [dbo].[EventStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (2, N'Held', 1, 2)
  INSERT [dbo].[EventStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (3, N'Not Held', 1, 3)
  INSERT [dbo].[EventStatus] ([Id], [Name], [Presence], [SortOrderId]) VALUES (4, N'Cancelled', 1, 4)
  
  SET IDENTITY_INSERT [dbo].[EventStatus] OFF

END
GO

/********************************GlAccount*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[GlAccount] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[GlAccount] ON 
  
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (1, N'300', N'Bán phần mềm')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (2, N'301', N'Bán phần cứng')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (3, N'302', N'Thu nhập cho thuê')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (4, N'303', N'Thu nhập từ lãi')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (5, N'304', N'Bán dịch vụ hỗ trợ phần mềm')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (6, N'305', N'Bán mặt hàng khác')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (7, N'306', N'Bán hàng trực tuyến')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (8, N'307', N'Dịch vụ phần cứng')
  INSERT [dbo].[GlAccount] ([Id], [AccountCode], [Description]) VALUES (9, N'308', N'Bán xe')
  
  SET IDENTITY_INSERT [dbo].[GlAccount] OFF

END
GO

/********************************LeadSource*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[LeadSource] WHERE Id=1)
BEGIN

  SET IDENTITY_INSERT [dbo].[LeadSource] ON 
  
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (1, N'Đến showroom', 1)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (2, N'Giới thiệu', 2)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (3, N'Sự kiện', 3)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (4, N'Website', 4)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (5, N'Ban giám đốc', 5)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (6, N'Khách hàng VIP', 6)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (7, N'Dịch vụ', 7)
  INSERT [dbo].[LeadSource] ([Id], [Name], [SortOrder]) VALUES (8, N'Khác', 8)
  
  SET IDENTITY_INSERT [dbo].[LeadSource] OFF

END
GO

/********************************RoleSeq*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[RoleSeq] WHERE Id=4)
BEGIN

  SET IDENTITY_INSERT [dbo].[RoleSeq] ON 
  INSERT [dbo].[RoleSeq] ([Id]) VALUES (4)
  SET IDENTITY_INSERT [dbo].[RoleSeq] OFF
  
END
GO

/********************************UnitOfMeasure*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[UnitOfMeasure] WHERE Id=1)
BEGIN
  SET IDENTITY_INSERT [dbo].[UnitOfMeasure] ON 
  INSERT [dbo].[UnitOfMeasure] ([Id], [Name], [Description], [IsActive]) VALUES (1, N'Chiếc', N'Chiếc', 1)
  SET IDENTITY_INSERT [dbo].[UnitOfMeasure] OFF
END
GO

/********************************Vendor*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[Vendor] WHERE Id=1)
BEGIN
  SET IDENTITY_INSERT [dbo].[Vendor] ON 
  INSERT [dbo].[Vendor] ([Id], [VendorCode], [VendorName], [PrimaryEmail], [PrimaryPhone], [Status], [IsActive]) VALUES (1, N'V1', N'CTY CP Ô TÔ SAO TÂY NAM', NULL, NULL, N'Approved', 1)
  INSERT [dbo].[Vendor] ([Id], [VendorCode], [VendorName], [PrimaryEmail], [PrimaryPhone], [Status], [IsActive]) VALUES (2, N'V2', N'Công ty CP Huyndai Thành Công Việt Nam', NULL, NULL, N'Approved', 1)
  INSERT [dbo].[Vendor] ([Id], [VendorCode], [VendorName], [PrimaryEmail], [PrimaryPhone], [Status], [IsActive]) VALUES (3, N'V3', N'CÔNG TY TNHH Ô TÔ HYUNDAI HOÀNG VIỆT', NULL, NULL, N'Approved', 1)
  SET IDENTITY_INSERT [dbo].[Vendor] OFF
END
GO

/********************************Warehouse*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[Warehouse] WHERE Id=1)
BEGIN
  SET IDENTITY_INSERT [dbo].[Warehouse] ON 
  INSERT [dbo].[Warehouse] ([Id], [Code], [Name]) VALUES (1, N'HCM', N'HCM')
  INSERT [dbo].[Warehouse] ([Id], [Code], [Name]) VALUES (2, N'VT', N'BR-VT')
  INSERT [dbo].[Warehouse] ([Id], [Code], [Name]) VALUES (3, N'DN', N'DN')
  INSERT [dbo].[Warehouse] ([Id], [Code], [Name]) VALUES (4, N'HN', N'HN')
  SET IDENTITY_INSERT [dbo].[Warehouse] OFF
END
GO

/********************************Currency*********************************************/
IF NOT EXISTS (SELECT 1 FROM [dbo].[Currency] WHERE CurrencyCode='VND')
BEGIN
	INSERT [dbo].[Currency] ([CurrencyCode], [Description], [Precision], [Symbol], [BaseCurrency], [IsActive]) VALUES (N'EUR', N'Euro', 2, N'EUR', 0, 1)
	INSERT [dbo].[Currency] ([CurrencyCode], [Description], [Precision], [Symbol], [BaseCurrency], [IsActive]) VALUES (N'GBP', N'Pound Sterling', 2, N'GBP', 0, 1)
	INSERT [dbo].[Currency] ([CurrencyCode], [Description], [Precision], [Symbol], [BaseCurrency], [IsActive]) VALUES (N'USD', N'Dollar', 2, N'$', 0, 1)
	INSERT [dbo].[Currency] ([CurrencyCode], [Description], [Precision], [Symbol], [BaseCurrency], [IsActive]) VALUES (N'VND', N'Vietnam Dong', 0, N'VND', 1, 1)
	INSERT [dbo].[Currency] ([CurrencyCode], [Description], [Precision], [Symbol], [BaseCurrency], [IsActive]) VALUES (N'YEN', N'Yen', 2, N'JPY', 0, 1)
END
GO
