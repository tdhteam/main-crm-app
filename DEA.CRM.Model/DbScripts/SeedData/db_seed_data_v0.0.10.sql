-- noinspection SqlNoDataSourceInspectionForFile


USE [CRMApp]
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[City] WHERE Id = 1)
BEGIN

  SET IDENTITY_INSERT [dbo].[City] ON 

  Insert into City (Id, [Name], [Description]) values (1,N'An Giang',N'An Giang')
  Insert into City (Id, [Name], [Description]) values (2,N'Bà Rịa - Vũng Tàu',N'Bà Rịa - Vũng Tàu')
  Insert into City (Id, [Name], [Description]) values (3,N'Bắc Giang',N'Bắc Giang')
  Insert into City (Id, [Name], [Description]) values (4,N'Bắc Kạn',N'Bắc Kạn')
  Insert into City (Id, [Name], [Description]) values (5,N'Bạc Liêu',N'Bạc Liêu')
  Insert into City (Id, [Name], [Description]) values (6,N'Bắc Ninh',N'Bắc Ninh')
  Insert into City (Id, [Name], [Description]) values (7,N'Bến Tre',N'Bến Tre')
  Insert into City (Id, [Name], [Description]) values (8,N'Bình Định',N'Bình Định')
  Insert into City (Id, [Name], [Description]) values (9,N'Bình Dương',N'Bình Dương')
  Insert into City (Id, [Name], [Description]) values (10,N'Bình Phước',N'Bình Phước')
  Insert into City (Id, [Name], [Description]) values (11,N'Bình Thuận',N'Bình Thuận')
  Insert into City (Id, [Name], [Description]) values (12,N'Cà Mau',N'Cà Mau')
  Insert into City (Id, [Name], [Description]) values (13,N'Cần Thơ',N'Cần Thơ')
  Insert into City (Id, [Name], [Description]) values (14,N'Cao Bằng',N'Cao Bằng')
  Insert into City (Id, [Name], [Description]) values (15,N'Đà Nẵng',N'Đà Nẵng')
  Insert into City (Id, [Name], [Description]) values (16,N'Đắk Lắk',N'Đắk Lắk')
  Insert into City (Id, [Name], [Description]) values (17,N'Đắk Nông',N'Đắk Nông')
  Insert into City (Id, [Name], [Description]) values (18,N'Điện Biên',N'Điện Biên')
  Insert into City (Id, [Name], [Description]) values (19,N'Đồng Nai',N'Đồng Nai')
  Insert into City (Id, [Name], [Description]) values (20,N'Đồng Tháp',N'Đồng Tháp')
  Insert into City (Id, [Name], [Description]) values (21,N'Gia Lai',N'Gia Lai')
  Insert into City (Id, [Name], [Description]) values (22,N'Hà Giang',N'Hà Giang')
  Insert into City (Id, [Name], [Description]) values (23,N'Hà Nam',N'Hà Nam')
  Insert into City (Id, [Name], [Description]) values (24,N'Hà Nội',N'Hà Nội')
  Insert into City (Id, [Name], [Description]) values (25,N'Hà Tĩnh',N'Hà Tĩnh')
  Insert into City (Id, [Name], [Description]) values (26,N'Hải Dương',N'Hải Dương')
  Insert into City (Id, [Name], [Description]) values (27,N'Hải Phòng',N'Hải Phòng')
  Insert into City (Id, [Name], [Description]) values (28,N'Hậu Giang',N'Hậu Giang')
  Insert into City (Id, [Name], [Description]) values (29,N'Hòa Bình',N'Hòa Bình')
  Insert into City (Id, [Name], [Description]) values (30,N'Hưng Yên',N'Hưng Yên')
  Insert into City (Id, [Name], [Description]) values (31,N'Khánh Hòa',N'Khánh Hòa')
  Insert into City (Id, [Name], [Description]) values (32,N'Kiên Giang',N'Kiên Giang')
  Insert into City (Id, [Name], [Description]) values (33,N'Kon Tum',N'Kon Tum')
  Insert into City (Id, [Name], [Description]) values (34,N'Lai Châu',N'Lai Châu')
  Insert into City (Id, [Name], [Description]) values (35,N'Lâm Đồng',N'Lâm Đồng')
  Insert into City (Id, [Name], [Description]) values (36,N'Lạng Sơn',N'Lạng Sơn')
  Insert into City (Id, [Name], [Description]) values (37,N'Lào Cai',N'Lào Cai')
  Insert into City (Id, [Name], [Description]) values (38,N'Long An',N'Long An')
  Insert into City (Id, [Name], [Description]) values (39,N'Nam Định',N'Nam Định')
  Insert into City (Id, [Name], [Description]) values (40,N'Nghệ An',N'Nghệ An')
  Insert into City (Id, [Name], [Description]) values (41,N'Ninh Bình',N'Ninh Bình')
  Insert into City (Id, [Name], [Description]) values (42,N'Ninh Thuận',N'Ninh Thuận')
  Insert into City (Id, [Name], [Description]) values (43,N'Phú Thọ',N'Phú Thọ')
  Insert into City (Id, [Name], [Description]) values (44,N'Phú Yên',N'Phú Yên')
  Insert into City (Id, [Name], [Description]) values (45,N'Quảng Bình',N'Quảng Bình')
  Insert into City (Id, [Name], [Description]) values (46,N'Quảng Nam',N'Quảng Nam')
  Insert into City (Id, [Name], [Description]) values (47,N'Quảng Ngãi',N'Quảng Ngãi')
  Insert into City (Id, [Name], [Description]) values (48,N'Quảng Ninh',N'Quảng Ninh')
  Insert into City (Id, [Name], [Description]) values (49,N'Quảng Trị',N'Quảng Trị')
  Insert into City (Id, [Name], [Description]) values (50,N'Sóc Trăng',N'Sóc Trăng')
  Insert into City (Id, [Name], [Description]) values (51,N'Sơn La',N'Sơn La')
  Insert into City (Id, [Name], [Description]) values (52,N'Tây Ninh',N'Tây Ninh')
  Insert into City (Id, [Name], [Description]) values (53,N'Thái Bình',N'Thái Bình')
  Insert into City (Id, [Name], [Description]) values (54,N'Thái Nguyên',N'Thái Nguyên')
  Insert into City (Id, [Name], [Description]) values (55,N'Thanh Hóa',N'Thanh Hóa')
  Insert into City (Id, [Name], [Description]) values (56,N'Thừa Thiên Huế',N'Thừa Thiên Huế')
  Insert into City (Id, [Name], [Description]) values (57,N'Tiền Giang',N'Tiền Giang')
  Insert into City (Id, [Name], [Description]) values (58,N'TP. Hồ Chí Minh',N'TP. Hồ Chí Minh')
  Insert into City (Id, [Name], [Description]) values (59,N'Trà Vinh',N'Trà Vinh')
  Insert into City (Id, [Name], [Description]) values (60,N'Tuyên Quang',N'Tuyên Quang')
  Insert into City (Id, [Name], [Description]) values (61,N'Vĩnh Long',N'Vĩnh Long')
  Insert into City (Id, [Name], [Description]) values (62,N'Vĩnh Phúc',N'Vĩnh Phúc')
  Insert into City (Id, [Name], [Description]) values (63,N'Yên Bái',N'Yên Bái')

  SET IDENTITY_INSERT [dbo].[City] OFF 

END
GO 
