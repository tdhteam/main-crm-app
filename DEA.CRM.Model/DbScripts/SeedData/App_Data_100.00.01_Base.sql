-- noinspection SqlNoDataSourceInspectionForFile

USE [CRMApp]
GO

/*UserInfoToCompany for admin user*/
DECLARE @companyId int 
SELECT @companyId=Id FROM Company
INSERT INTO UserInfoToCompany(UserKey, CompanyId)
SELECT UserKey, @companyId  FROM UserInfo WHERE IsAdmin=1
GO

/*UserInfoToDealer for admin user*/

DECLARE @dealerId INT
DECLARE curDealer CURSOR FOR SELECT Id FROM Dealer

OPEN curDealer
FETCH NEXT FROM curDealer INTO @dealerId
WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO UserInfoToDealer(UserKey, DealerId)
			SELECT UserKey, @dealerId FROM UserInfo WHERE IsAdmin = 1
		FETCH NEXT FROM curDealer INTO @dealerId
	END
CLOSE curDealer
DEALLOCATE curDealer

GO

/*UserInfoToShowroom for admin user*/
DECLARE @showroomId INT
DECLARE curShowroom CURSOR FOR SELECT Id FROM Showroom

OPEN curShowroom
FETCH NEXT FROM curShowroom INTO @showroomId
WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO UserInfoToShowroom(UserKey, ShowroomId)
			SELECT UserKey, @showroomId FROM UserInfo WHERE IsAdmin = 1
		FETCH NEXT FROM curShowroom INTO @showroomId
	END
CLOSE curShowroom
DEALLOCATE curShowroom

GO