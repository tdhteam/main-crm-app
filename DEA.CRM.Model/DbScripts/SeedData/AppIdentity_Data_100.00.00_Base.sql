﻿-- noinspection SqlNoDataSourceInspectionForFile

USE [CRMAppIdentity]
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[AspNetUsers] WHERE [UserName]='admin')
BEGIN
  INSERT [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) 
  VALUES (N'e8229596-8c18-44ef-9269-c16d4bdcdaf7', 0, N'd151d87d-1b91-485a-b6da-c1856b493382', NULL, 1, 0, NULL, NULL, N'ADMIN', N'AQAAAAEAACcQAAAAEN/qx61qwMYEzNccH/GUUWd7SQKt3UnvvFUMLNSK6SOZmFPO585AWEARu+zq7/e/mw==', NULL, 1, N'13f19d17-d3d7-408b-8efa-837a8958a532', 0, N'admin')
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[__EFMigrationsHistory] WHERE [MigrationId]='20180402081434_CreateIdentityDbMigration')
BEGIN
  INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) 
  VALUES (N'20180402081434_CreateIdentityDbMigration', N'2.0.2-rtm-10011')
END
GO
