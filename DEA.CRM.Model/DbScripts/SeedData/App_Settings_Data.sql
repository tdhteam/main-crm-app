﻿-- noinspection SqlNoDataSourceInspectionForFile

USE [CRMApp]
GO

IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingId = 1)
BEGIN
	INSERT INTO [dbo].[Setting]
           ([SettingId]
           ,[Name]
           ,[Description]
           ,[SettingValue]
           ,[SettingGroup])
     VALUES
           (1
           ,'FileSystemAttachmentRootPath'
           ,'Root path to the directory where file attachment is stored for file system provider'
           ,'F:\www\CRM'
           ,'Attachment')
END
GO


--AppApiBaseUrl=100
IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingId = 100)
BEGIN
	INSERT INTO [dbo].[Setting]
           ([SettingId]
           ,[Name]
           ,[Description]
           ,[SettingValue]
           ,[SettingGroup])
     VALUES
           (100
           ,'AppApiBaseUrl'
           ,'Base url to the app base url'
           ,'http://localhost:5001/api/'
           ,'URL')
END
GO

--AzureStorageConnectionString = 2
IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingId = 2)
BEGIN
	INSERT INTO [dbo].[Setting]
           ([SettingId]
           ,[Name]
           ,[Description]
           ,[SettingValue]
           ,[SettingGroup])
     VALUES
           (2
           ,'Azure Storage Connection String'
           ,'Connection string to the Azure storage service'
           ,'DefaultEndpointsProtocol=https;AccountName=storagesample;AccountKey=<account-key>'
           ,'Azure')
END
GO

--AzureBlobStorageRootContainerName = 3
IF NOT EXISTS(SELECT 1 FROM Setting WHERE SettingId = 3)
BEGIN
	INSERT INTO [dbo].[Setting]
           ([SettingId]
           ,[Name]
           ,[Description]
           ,[SettingValue]
           ,[SettingGroup])
     VALUES
           (3
           ,'Azure Blob Storage Root Container Name'
           ,'Root container name for Azure blob storage'
           ,'crm-attachment'
           ,'Azure')
END
GO
