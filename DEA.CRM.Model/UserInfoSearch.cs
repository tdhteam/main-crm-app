﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DEA.CRM.Model
{
    /// <summary>
    /// Basic info for user searching
    /// </summary>
    [Table("vwUserInfoSearch")]
    public class UserInfoSearch
    {
        [Key]
        public string UserKey { get; set; }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public bool IsAdmin { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
