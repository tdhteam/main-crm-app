﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("CustomerServiceComment")]
    public partial class CustomerServiceComment
    {
        public int Id { get; set; }
        public string CommentContent { get; set; }
        public int CustomerServiceId { get; set; }

        public CustomerService CustomerService { get; set; }
    }
}
