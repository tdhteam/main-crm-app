﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DEA.CRM.Model
{
    [Table("CalendarEventType")]
    public partial class CalendarEventType
    {
        public CalendarEventType()
        {
            CalendarEvent = new HashSet<CalendarEvent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Presence { get; set; }
        public int SortOrderId { get; set; }
        public string Color { get; set; }

        public ICollection<CalendarEvent> CalendarEvent { get; set; }
    }
}
