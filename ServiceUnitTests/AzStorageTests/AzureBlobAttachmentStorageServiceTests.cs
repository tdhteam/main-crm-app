using System.Threading.Tasks;
using Autofac;
using DEA.CRM.Service.Contract.Attachment;
using ServiceUnitTests.TestSetup;
using Xunit;

namespace ServiceUnitTests.AzStorageTests
{
    public class AzureBlobAttachmentStorageServiceTests : TestBase
    {
        public AzureBlobAttachmentStorageServiceTests(AutofacFixture fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task SaveStreamToStorageAsync_Should_Create_Container_If_Not_Exists()
        {
            var service = autofacFixture.Container.Resolve<IAttachmentStorageService>();
            Assert.NotNull(service);
        }
    }
}
