﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using August.ImageProcessingService.Thumbnail;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils.Monads;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Xunit;

namespace ServiceUnitTests.AzStorageTests
{
    public class AzBlobStorageTests
    {
        private CloudBlobContainer _blobContainer;

        private async Task InitStorage()
        {
            // Create a blob client for interacting with the blob service.
            var azureStorageConnectionString =
                @"DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;BlobEndpoint=http://127.0.0.1:10000/devstoreaccount1;";
            var rootContainerName = "attachment-test-container";

            var storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();

            _blobContainer = blobClient.GetContainerReference(rootContainerName);
            await _blobContainer.CreateIfNotExistsAsync(
                BlobContainerPublicAccessType.Blob,
                new BlobRequestOptions(),
                new OperationContext()
            );
        }

        [Fact]
        public async Task InitStorageClient_Test_Should_Be_Able_To_Init()
        {
            await InitStorage();
            Assert.NotNull(_blobContainer);
        }

        [Fact]
        public async Task SaveStreamToStorageAsync_Test_Save_Multi_Streams_To_Blob_Blocks()
        {
            var blockNumber = 0;
            var blockSize = 1024 * 1024;
            //var fileToUploadPath = @"E:\dea-crm-app\main-crm-app\ServiceUnitTests\TestData\Files\Fullstack React.pdf";            
            var fileToUploadPath = @"/Users/thinh/code/mitsu-crm/crm-app/CrmApp/ServiceUnitTests/TestData/Files/Fullstack React.pdf";            
            
            var attachmentMeta = new AttachmentDto
            {
                FileName = "Fullstack React.pdf",
                ContentType = "application/pdf"
            };
            
            using (var fs = File.OpenRead(fileToUploadPath))
            {
                using (var reader = new BinaryReader(fs))
                {
                    var streamBuffer = reader.ReadBytes(blockSize);
                    while (streamBuffer.Length > 0)
                    {
                        var memStream = new MemoryStream(streamBuffer);
                        await SaveStreamToStorageAsync(memStream, attachmentMeta, blockNumber);                        
                        streamBuffer = reader.ReadBytes(blockSize);
                        Debug.WriteLine($"Put block number {blockNumber} - Stream position : {fs.Position}");
                        blockNumber++;                        
                    }
                }                
            }

            var attResult = await CommitChangesForBlockListToBlob(attachmentMeta, blockIds);
            
            Debug.WriteLine(attResult.Value.FileUrl);
            Debug.WriteLine(attResult.Value.DownloadFileUrl);
            
            Assert.NotNull(attResult.Value.FileUrl);
            Assert.NotNull(attResult.Value.DownloadFileUrl);
        }

        private List<string> blockIds = new List<string>();        

        private async Task<Result<AttachmentDto>> SaveStreamToStorageAsync(Stream dataStream, AttachmentDto attachmentMetaDto, int blockNumber)
        {
            if (dataStream == null)
                throw new ArgumentNullException(nameof(dataStream));

            if (attachmentMetaDto == null)
                throw new ArgumentNullException(nameof(attachmentMetaDto));

            await InitStorage();

            var blockId = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("BlockId{0}", blockNumber.ToString("0000000"))));
            blockIds.Add(blockId);

            // Calculate the MD5 hash of the buffer.
            var bytesToWrite = StreamHelper.ReadToEnd(dataStream);            
            var blockHash = CryptoHelper.Md5ComputeHashBytes(bytesToWrite);
            var md5Content = Convert.ToBase64String(blockHash, 0, 16);

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetaDto.Id}/{attachmentMetaDto.FileName}");
            await blockBlob.PutBlockAsync(blockId: blockId, blockData: dataStream, contentMD5: md5Content);
            
            return Result.Success(attachmentMetaDto);
        }

        private async Task<Result<AttachmentDto>> CommitChangesForBlockListToBlob(AttachmentDto attachmentMetaDto, List<string> blockIds)
        {
            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetaDto.Id}/{attachmentMetaDto.FileName}");
            await blockBlob.PutBlockListAsync(blockIds);
            
            attachmentMetaDto.FileUrl = blockBlob.Uri.ToString();
            attachmentMetaDto.DownloadFileUrl = blockBlob.Uri.ToString();
            blockIds = new List<string>();
            
            return Result.Success(attachmentMetaDto);
        }

        #region SaveAttachmentContentToStorageAsync_Test

        [Fact]
        public async Task SaveAttachmentContentToStorageAsync_Test_Image_Upload()
        {
            var testFilePath = @"E:\dea-crm-app\main-crm-app\ServiceUnitTests\TestData\Files\dashboard-1.jpg";
            var attachmentMeta = new AttachmentDto
            {
                FileName = "dashboard-1.jpg",
                FileContentBytes = File.ReadAllBytes(testFilePath),
                ContentType = "image/jpg"
            };

            var attRes = await SaveAttachmentContentToStorageAsync(attachmentMeta);
            Assert.True(attRes.IsSuccess);
            Assert.NotNull(attRes.Value.FileUrl);
            Assert.NotNull(attRes.Value.DownloadFileUrl);
            Assert.NotNull(attRes.Value.ThumbnailUrl);
            Assert.NotNull(attRes.Value.DownloadThumbnailUrl);
        }

        [Fact]
        public async Task SaveAttachmentContentToStorageAsync_Test_Normal_File_Upload()
        {
            var testFilePath = @"E:\dea-crm-app\main-crm-app\ServiceUnitTests\TestData\Files\TextFile1.txt";
            var attachmentMeta = new AttachmentDto
            {
                FileName = "TextFile1.txt",
                FileContentBytes = File.ReadAllBytes(testFilePath),
                ContentType = "text/plain"
            };

            var attRes = await SaveAttachmentContentToStorageAsync(attachmentMeta);
            Assert.True(attRes.IsSuccess);
            Assert.NotNull(attRes.Value.FileUrl);
            Assert.NotNull(attRes.Value.DownloadFileUrl);
            Assert.Null(attRes.Value.ThumbnailUrl);
            Assert.Null(attRes.Value.DownloadThumbnailUrl);
        }

        private async Task<Result<AttachmentDto>> SaveAttachmentContentToStorageAsync(AttachmentDto attachmentDto)
        {
            await InitStorage();

            var azureStorageConfig = new AzureStorageConfigurationDto();

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentDto.Id}/{attachmentDto.FileName}");
            using (var memStream = new MemoryStream(attachmentDto.FileContentBytes))
            {
                await blockBlob.UploadFromStreamAsync(memStream);

                blockBlob.Properties.ContentType = attachmentDto.ContentType;
                await blockBlob.SetPropertiesAsync();

                attachmentDto.FileUrl = blockBlob.Uri.ToString();
                attachmentDto.DownloadFileUrl = blockBlob.Uri.ToString();
            }

            var hasThumbnail = FileHelper.IsImageFileContentType(attachmentDto.ContentType);

            if (hasThumbnail)
            {
                var thumbnailMaker = new ImageSharpThumbnailMakerService();
                var thumbnailBytes = thumbnailMaker.Resize(attachmentDto.FileContentBytes, azureStorageConfig.PreferedThumbnailWidth);

                var blockBlobFormThumbnail = _blobContainer.GetBlockBlobReference($"{attachmentDto.Id}/thumbnail/{attachmentDto.FileName}");
                using (var memStream = new MemoryStream(thumbnailBytes))
                {
                    await blockBlobFormThumbnail.UploadFromStreamAsync(memStream);

                    blockBlobFormThumbnail.Properties.ContentType = attachmentDto.ContentType;
                    await blockBlobFormThumbnail.SetPropertiesAsync();

                    attachmentDto.ThumbnailUrl = blockBlobFormThumbnail.Uri.ToString();
                    attachmentDto.DownloadThumbnailUrl = blockBlobFormThumbnail.Uri.ToString();
                }
                attachmentDto.ThumbnailFileContentBytes = thumbnailBytes;
            }

            return Result<AttachmentDto>.Success(attachmentDto);
        }

        #endregion

        #region CreateNestedBlockBlobsAsync_Test

        [Fact]
        public async Task CreateNestedBlockBlobsAsync_Test()
        {
            await InitStorage();

            await CreateNestedBlockBlobsAsync(_blobContainer, 3, 3);
        }

        private static async Task CreateNestedBlockBlobsAsync(CloudBlobContainer container, short numberOfLevels, short numberOfBlobsPerLevel)
        {
            try
            {
                CloudBlockBlob blob;
                MemoryStream msWrite;
                string blobName = string.Empty;
                string virtualDirName = string.Empty;

                // Create a nested blob structure.
                for (int i = 1; i <= numberOfLevels; i++)
                {
                    // Construct the virtual directory name, which becomes part of the blob name.
                    virtualDirName += string.Format("level{0}{1}", i, container.ServiceClient.DefaultDelimiter);
                    for (int j = 1; j <= numberOfBlobsPerLevel; j++)
                    {
                        // Construct string for blob name.
                        blobName = virtualDirName + string.Format("{0}-{1}.txt", i, j.ToString("00000"));

                        // Get a reference to the blob.
                        blob = container.GetBlockBlobReference(blobName);

                        // Write the blob URI to its contents.
                        msWrite = new MemoryStream(Encoding.UTF8.GetBytes("Absolute URI to blob: " + blob.StorageUri.PrimaryUri + "."));
                        msWrite.Position = 0;
                        using (msWrite)
                        {
                            await blob.UploadFromStreamAsync(msWrite);
                        }

                        // Set a property on the blob.
                        blob.Properties.ContentType = "text/html";
                        await blob.SetPropertiesAsync();

                        // Set some metadata on the blob.
                        blob.Metadata.Add("DateCreated", DateTime.UtcNow.ToLongDateString());
                        blob.Metadata.Add("TimeCreated", DateTime.UtcNow.ToLongTimeString());
                        await blob.SetMetadataAsync();
                    }
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        #endregion

        #region UploadBlobInBlocksAsync_Test

        [Fact]
        public async Task UploadBlobInBlocksAsync_Test()
        {
            await InitStorage();

            await UploadBlobInBlocksAsync(_blobContainer);
        }

        /// <summary>
        /// Uploads the blob as a set of 256 KB blocks.
        /// </summary>
        /// <param name="container">A CloudBlobContainer object.</param>
        /// <returns>A Task object.</returns>
        private static async Task UploadBlobInBlocksAsync(CloudBlobContainer container)
        {
            // Create an array of random bytes, of the specified size.
            byte[] randomBytes = new byte[5 * 1024 * 1024];
            Random rnd = new Random();
            rnd.NextBytes(randomBytes);

            // Get a reference to a new block blob.
            CloudBlockBlob blob = container.GetBlockBlobReference("sample-blob-" + Guid.NewGuid());

            // Specify the block size as 256 KB.
            int blockSize = 256 * 1024;

            MemoryStream msWrite = null;

            // Create new stream of bytes.
            msWrite = new MemoryStream(randomBytes);
            msWrite.Position = 0;
            using (msWrite)
            {
                long streamSize = msWrite.Length;

                // Create a list of block IDs.
                List<string> blockIDs = new List<string>();

                // Indicate the starting block number.
                int blockNumber = 1;

                try
                {
                    // The number of bytes read so far.
                    int bytesRead = 0;

                    // The number of bytes left to read and upload.
                    long bytesLeft = streamSize;

                    // Loop until all of the bytes in the stream have been uploaded.
                    while (bytesLeft > 0)
                    {
                        int bytesToRead;

                        // Check whether the remaining bytes constitute at least another block.
                        if (bytesLeft >= blockSize)
                        {
                            // Read another whole block.
                            bytesToRead = blockSize;
                        }
                        else
                        {
                            // There's less than a whole block left, so read the rest of it.
                            bytesToRead = (int)bytesLeft;
                        }

                        // Create a block ID from the block number, and add it to the block ID list.
                        // Note that the block ID is a base64 string.
                        string blockId = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("BlockId{0}", blockNumber.ToString("0000000"))));
                        blockIDs.Add(blockId);

                        // Set up a new buffer for writing the block, and read that many bytes into it .
                        byte[] bytesToWrite = new byte[bytesToRead];
                        msWrite.Read(bytesToWrite, 0, bytesToRead);

                        // Calculate the MD5 hash of the buffer.
                        MD5 md5 = new MD5CryptoServiceProvider();
                        byte[] blockHash = md5.ComputeHash(bytesToWrite);
                        string md5Hash = Convert.ToBase64String(blockHash, 0, 16);

                        // Upload the block with the hash.
                        await blob.PutBlockAsync(blockId, new MemoryStream(bytesToWrite), md5Hash);

                        // Increment and decrement the counters.
                        bytesRead += bytesToRead;
                        bytesLeft -= bytesToRead;
                        blockNumber++;
                    }

                    // Read the block list that we just created. The blocks will all be uncommitted at this point.
                    await ReadBlockListAsync(blob);

                    // Commit the blocks to a new blob.
                    await blob.PutBlockListAsync(blockIDs);

                    // Read the block list again. Now all blocks will be committed.
                    await ReadBlockListAsync(blob);
                }
                catch (StorageException e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                    throw;
                }
            }
        }

        /// <summary>
        /// Reads the blob's block list, and indicates whether the blob has been committed.
        /// </summary>
        /// <param name="blob">A CloudBlockBlob object.</param>
        /// <returns>A Task object.</returns>
        private static async Task ReadBlockListAsync(CloudBlockBlob blob)
        {
            // Get the blob's block list.
            foreach (var listBlockItem in await blob.DownloadBlockListAsync(BlockListingFilter.All, null, null, null))
            {
                if (listBlockItem.Committed)
                {
                    Console.WriteLine(
                        "Block {0} has been committed to block list. Block length = {1}",
                        listBlockItem.Name,
                        listBlockItem.Length);
                }
                else
                {
                    Console.WriteLine(
                        "Block {0} is uncommitted. Block length = {1}",
                        listBlockItem.Name,
                        listBlockItem.Length);
                }
            }

            Console.WriteLine();
        }

        #endregion
    }
}
