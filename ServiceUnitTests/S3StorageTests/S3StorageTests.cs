﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;
using Xunit;
using Xunit.Abstractions;

namespace ServiceUnitTests.S3StorageTests
{        
    public class S3StorageTests
    {
        private readonly ITestOutputHelper _output;
        
        private IAmazonS3 _client;

        public S3StorageTests(ITestOutputHelper output)
        {
            this._output = output;
        }
        
        private static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            return config;
        }

        private IAmazonS3 CreateS3Client()
        {
            return new AmazonS3Client(
                "AKIAJXXUSIPHE36FRZGQ",
                "M5zgqYc73QmJ08XSuuwdUBlTz3jMRpStTqCTYjpG",
                RegionEndpoint.APSoutheast1);
        }

        [Fact]
        public async Task TestBasicOperation_S3()
        {            
            using (_client = CreateS3Client())
            {                
                await CreateABucket($"test-bucket-{DateTime.Now:yy-MM-dd}");
                await ListingBuckets();
            }
        }

        [Fact]
        public async Task Test_S3_Upload_Multipart()
        {
            var bucketName = $"test-bucket-{DateTime.Now:yy-MM-dd}";
            //var testFile = @"/Users/thinh/code/mitsu-crm/crm-app/CrmApp/ServiceUnitTests/TestData/Files/Fullstack React.pdf";
            var testFile = @"/Users/thinh/books/making_things_move.pdf";
            using (_client = CreateS3Client())
            {
                await CreateABucket(bucketName);

                try
                {
                    var uploadResponse = await UploadUsingMultiPartAPIAsync(_client, bucketName, testFile); 
                    _output.WriteLine("BucketName: " + uploadResponse.BucketName);
                    _output.WriteLine("Location: " + uploadResponse.Location);
                    _output.WriteLine("VersionId: " + uploadResponse.VersionId);
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                         amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        _output.WriteLine("Please check the provided AWS Credentials.");
                        _output.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                    }
                    else
                    {
                        _output.WriteLine("An Error, number {0} -> {1} ", amazonS3Exception.ErrorCode, amazonS3Exception.Message);
                    }
                }                                
            }            
        }
        
        private async Task ListingBuckets()
        {
            try
            {
                ListBucketsResponse response = await _client.ListBucketsAsync();
                foreach (S3Bucket bucket in response.Buckets)
                {
                    _output.WriteLine("You own Bucket with name: {0}", bucket.BucketName);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                     amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    _output.WriteLine("Please check the provided AWS Credentials.");
                    _output.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                }
                else
                {
                    _output.WriteLine("An Error, number {0}, occurred when listing buckets with the message '{1}",
                        amazonS3Exception.ErrorCode, amazonS3Exception.Message);
                }
            }
        }
        
        private async Task CreateABucket(string bucketName)
        {
            try
            {
                PutBucketRequest request = new PutBucketRequest();
                request.BucketName = bucketName;
                await _client.PutBucketAsync(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Please check the provided AWS Credentials.");
                    Console.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine("An Error, number {0}, occurred when creating a bucket with the message '{1}", amazonS3Exception.ErrorCode, amazonS3Exception.Message);
                }
            }
        }
        
        #region Multi Part Upload Code
        static readonly int ONE_MEG = (int)Math.Pow(2, 20);
        /// <summary>
        /// Sample code to contrast uploading a file using Amazon S3's Multi-Part Upload API
        /// </summary>
        /// <param name="s3Client"></param>
        /// <param name="bucketName"></param>
        /// <param name="filePath"></param>
        private async Task<CompleteMultipartUploadResponse> UploadUsingMultiPartAPIAsync(IAmazonS3 s3Client, string bucketName, string filePath)
        {
            var objectKey = $"multipart/{Path.GetFileName(filePath)}";
            //var objectKey = $"multipart1/object1";            

            // tell S3 we're going to upload an object in multiple parts and receive an upload ID
            // in return
            var initializeUploadRequest = new InitiateMultipartUploadRequest
            {
                BucketName = bucketName,
                Key = objectKey
            };
            var initializeUploadResponse = await s3Client.InitiateMultipartUploadAsync(initializeUploadRequest);

            // this ID must accompany all parts and the final 'completed' call
            var uploadId = initializeUploadResponse.UploadId;

            // Send the file (synchronously) using 4*5MB parts - note we pass the upload id
            // with each call. For each part we need to log the returned etag value to pass
            // to the completion call
            var partETags = new List<PartETag>();
            var partSize = 5 * ONE_MEG; // this is the minimum part size allowed
            
            // calculate number of part which is multiple of 5M each part
            var fileInfo = new FileInfo(filePath);
            var fileSizeInMb = fileInfo.Length / partSize;
            if (fileInfo.Length % partSize > 0)
                fileSizeInMb++;

            for (var partNumber = 0; partNumber < fileSizeInMb; partNumber++)
            {
                // part numbers must be between 1 and 1000
                var logicalPartNumber = partNumber + 1;
                var uploadPartRequest = new UploadPartRequest
                {
                    BucketName = bucketName,
                    Key = objectKey,
                    UploadId = uploadId,
                    PartNumber = logicalPartNumber,
                    PartSize = partSize,
                    FilePosition = partNumber * partSize,
                    FilePath = filePath
                };

                var partUploadResponse = await s3Client.UploadPartAsync(uploadPartRequest);
                partETags.Add(new PartETag { PartNumber = logicalPartNumber, ETag = partUploadResponse.ETag });
            }

            var completeUploadRequest = new CompleteMultipartUploadRequest
            {
                BucketName = bucketName,
                Key = objectKey,
                UploadId = uploadId,
                PartETags = partETags
            };

            return await s3Client.CompleteMultipartUploadAsync(completeUploadRequest);
        }

        #endregion
    }
}
