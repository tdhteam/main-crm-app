﻿using System;
using System.Collections.Generic;
using System.Text;
using August.ImageProcessingService.Thumbnail;
using Autofac;
using DEA.CRM.Service.Attachment;
using DEA.CRM.Service.Calendar;
using DEA.CRM.Service.City;
using DEA.CRM.Service.Contact;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Calendar;
using DEA.CRM.Service.Contract.City;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.HandOverContract;
using DEA.CRM.Service.Contract.Product;
using DEA.CRM.Service.Contract.UserDocument;
using DEA.CRM.Service.Core;
using DEA.CRM.Service.Customer;
using DEA.CRM.Service.HandOverContract;
using DEA.CRM.Service.Product;
using DEA.CRM.Service.UserDocument;
using DEA.CRM.Utils.Extensions;

namespace ServiceUnitTests.TestSetup
{
    public class TestAutofacModule : Module
    {
        private readonly string _hostingEnv;

        public TestAutofacModule(string hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestAppServiceFactory>().As<IServiceFactory>().SingleInstance();

            builder.RegisterType<DefaultUserPermissionService>().As<IUserPermissionService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultPermissionService>().As<IPermissionService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultUserInfoService>().As<IUserInfoService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultRoleService>().As<IRoleService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultProfileService>().As<IProfileService>().InstancePerLifetimeScope();

            builder.RegisterType<DefaultTabService>().As<ITabService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultSettingService>().As<ISettingService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultCompanyService>().As<ICompanyService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultDealerService>().As<IDealerService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultShowroomService>().As<IShowroomService>().InstancePerLifetimeScope();

            builder.RegisterType<DefaultCalendarEventService>().As<ICalendarEventService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultContactService>().As<IContactService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultLeadSourceService>().As<ILeadSourceService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultHandOverContractService>().As<IHandOverContractService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultProductService>().As<IProductService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultCustomerService>().As<ICustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultCityService>().As<ICityService>().InstancePerLifetimeScope();

            builder.RegisterType<DefaultPickListService>().As<IPickListService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultSettingService>().As<ISettingService>().InstancePerLifetimeScope();

            // Attachment            
            builder.RegisterType<ImageSharpThumbnailMakerService>().As<IThumbnailMakerService>().InstancePerLifetimeScope();

            if ("Azure".EqualsIgnoreCase(_hostingEnv))
            {
                builder.RegisterType<AzureBlobAttachmentMetadataService>().As<IAttachmentMetadataService>().InstancePerLifetimeScope();
                builder.RegisterType<AzureBlobAttachmentStorageService>().As<IAttachmentStorageService>().InstancePerLifetimeScope();
            }
            else if ("AWS".EqualsIgnoreCase(_hostingEnv))
            {
                builder.RegisterType<S3BucketAttachmentMetadataService>().As<IAttachmentMetadataService>().InstancePerLifetimeScope();
                builder.RegisterType<S3BucketAttachmentStorageService>().As<IAttachmentStorageService>().InstancePerLifetimeScope();
            }
            else
            {
                builder.RegisterType<FileSystemAttachmentMetadataService>().As<IAttachmentMetadataService>().InstancePerLifetimeScope();
                builder.RegisterType<FileSystemAttachmentStorageService>().As<IAttachmentStorageService>().InstancePerLifetimeScope();
            }

            builder.RegisterType<DefaultAttachmentManager>().As<IAttachmentManager>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultUserDocumentService>().As<IUserDocumentService>().InstancePerLifetimeScope();

        }
    }
}
