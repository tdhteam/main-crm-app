﻿using System;
using DEA.CRM.Service.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace ServiceUnitTests.TestSetup
{
    public class TestAppServiceFactory : IServiceFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public TestAppServiceFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public TService GetService<TService>()
        {
            return _serviceProvider.GetService<TService>();
        }
    }
}
