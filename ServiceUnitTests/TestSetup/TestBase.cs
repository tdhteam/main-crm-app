﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ServiceUnitTests.TestSetup
{
    public class TestBase : IClassFixture<AutofacFixture>
    {
        protected AutofacFixture autofacFixture;

        public TestBase(AutofacFixture fixture)
        {
            autofacFixture = fixture;
        }
    }
}
