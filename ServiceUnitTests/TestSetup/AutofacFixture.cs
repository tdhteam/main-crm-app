﻿using System;
using Autofac;

namespace ServiceUnitTests.TestSetup
{
    public class AutofacFixture : IDisposable
    {
        public IContainer Container { get; set; }

        public AutofacFixture()
        {
            var hostingEnv = "Azure";
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule(new TestAutofacModule(hostingEnv));
            Container = containerBuilder.Build();
        }

        public void Dispose()
        {
            Container?.Dispose();
        }
    }
}
