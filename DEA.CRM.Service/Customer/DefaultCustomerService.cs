﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Pagination;
using System.Linq;
using DEA.CRM.Service.Contract;

namespace DEA.CRM.Service.Customer
{
    public class DefaultCustomerService : DefaultServiceBase, ICustomerService
    {
        public DefaultCustomerService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        public PagingResult<CustomerDto> SearchCustomers(SearchCustomerCommand command)
        {
            if (command == null)
                return PagingResult<CustomerDto>.Empty();
            var predicate =
                Functions.Lambda<Model.Customer, bool>(
                    c => (
                        (command.FirstName == null || c.FirstName.Contains(command.FirstName))
                        && (command.LastName == null || c.LastName.Contains(command.LastName))
                    ));

            var customersQuery = DbContext.Customers.Where(predicate).AsQueryable(); ;

            if (!string.IsNullOrEmpty(command.OrderBy))
            {
                customersQuery = customersQuery.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = customersQuery.Count();
            var contacts = customersQuery.Paginate(command);

            return new PagingResult<CustomerDto>(contacts.Select(CustomerDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }
    }
}
