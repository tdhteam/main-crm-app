﻿using System;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Contract.Product;
using DEA.CRM.Service.Contract.Product.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Product
{
    public class DefaultProductService : DefaultServiceBase, IProductService
    {
        public DefaultProductService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        public PagingResult<ProductDto> GetAllProducts()
        {
            var allProducts = DbContext.Products.Select(ProductDto.EntityToDtoMapper).ToList();
            return PagingResult<ProductDto>.AllResult(allProducts);
        }

        public Result<ProductDto> GetProductById(int productId)
        {
            var productEntity = DbContext.Products.Find(productId).AsResult($"Cannot find Product with id {productId}");
            return Result<ProductDto>.Success(ProductDto.EntityToDtoMapper(productEntity.Value));
        }

        public ProductDto CreateOrUpdate(CreateOrUpdateProductCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var product = command.Id == null ? Create(command) : Update(command);
            DbContext.SaveChanges();

            return ProductDto.EntityToDtoMapper(product);
        }

        public ResponseDto DeleteProduct(int productId)
        {
            var productEntity = DbContext.Products.Find(productId);
            if (productEntity != null)
                DbContext.Products.Remove(productEntity);

            DbContext.SaveChanges();

            return ResponseDto.OkNoContent();
        }

        private Model.Product Update(CreateOrUpdateProductCommand command)
        {
            var productEntity = DbContext.Products.FirstOrDefault(r => r.Id == command.Id);

            if (productEntity == null)
                throw new EntityNotFoundException("I18N.Product.PRODUCT_WITH_ID_IS_NOT_FOUND");

            productEntity.Name = command.Name;
            productEntity.Id = command.Id.Value;
            productEntity.BankOfGuarantee = command.BankOfGuarantee;
            productEntity.Color = command.Color;
            productEntity.CompanyId = command.CompanyId;
            productEntity.ContractId = command.ContractId;
            productEntity.DateEnteredStock = command.DateEnteredStock;
            productEntity.DateRelease = command.DateRelease;
            productEntity.DealerId = command.DealerId;
            productEntity.Description = command.Description;
            productEntity.Engine = command.Engine;
            productEntity.EngineNumber = command.EngineNumber;
            productEntity.HandlerUserKey = command.HandlerUserKey;
            productEntity.InActiveForSale = command.InActiveForSale;
            productEntity.InStockAge = command.InStockAge;
            productEntity.NumberOfSeats = command.NumberOfSeats;
            productEntity.PaymentStatus = command.PaymentStatus;
            productEntity.PercentSales = command.PercentSales;
            productEntity.PercentService = command.PercentService;
            productEntity.PercentVat = command.PercentVat;
            productEntity.PlateNumber = command.PlateNumber;
            productEntity.ProductActive = command.ProductActive;
            productEntity.ProductCategoryId = command.ProductCategoryId;
            productEntity.ProductNo = command.ProductNo;
            productEntity.ProductType = command.ProductType;
            productEntity.ProductionDate = command.ProductionDate;
            productEntity.PurchaseOrderNo = command.PurchaseOrderNo;
            productEntity.QuantityInStock = command.QuantityInStock;
            productEntity.ShowroomId = command.ShowroomId;
            productEntity.Status = command.Status;
            productEntity.TransmissionType = command.TransmissionType;
            productEntity.UnitPrice = command.UnitPrice;
            productEntity.Uom = command.Uom;
            productEntity.Vin = command.Vin;
            productEntity.WarehouseName = command.WarehouseName;

            return productEntity;
        }

        private Model.Product Create(CreateOrUpdateProductCommand command)
        {
            var productEntityToAdd = new Model.Product
            {
                Name = command.Name,
                BankOfGuarantee = command.BankOfGuarantee,
                Color = command.Color,
                CompanyId = command.CompanyId,
                ContractId = command.ContractId,
                DateEnteredStock = command.DateEnteredStock,
                DateRelease = command.DateRelease,
                DealerId = command.DealerId,
                Description = command.Description,
                Engine = command.Engine,
                EngineNumber = command.EngineNumber,
                HandlerUserKey = command.HandlerUserKey,
                InActiveForSale = command.InActiveForSale,
                InStockAge = command.InStockAge,
                NumberOfSeats = command.NumberOfSeats,
                PaymentStatus = command.PaymentStatus,
                PercentSales = command.PercentSales,
                PercentService = command.PercentService,
                PercentVat = command.PercentVat,
                PlateNumber = command.PlateNumber,
                ProductActive = command.ProductActive,
                ProductCategoryId = command.ProductCategoryId,
                ProductNo = command.ProductNo,
                ProductType = command.ProductType,
                ProductionDate = command.ProductionDate,
                PurchaseOrderNo = command.PurchaseOrderNo,
                QuantityInStock = command.QuantityInStock,
                ShowroomId = command.ShowroomId,
                Status = command.Status,
                TransmissionType = command.TransmissionType,
                UnitPrice = command.UnitPrice,
                Uom = command.Uom,
                Vin = command.Vin,
                WarehouseName = command.WarehouseName,
            };

            DbContext.Products.Add(productEntityToAdd);
            return productEntityToAdd;
        }

        public PagingResult<ProductDto> SearchProducts(SearchProductCommand command)
        {

            if (command == null)
                return PagingResult<ProductDto>.Empty();
            var predicate =
                Functions.Lambda<Model.Product, bool>(
                    c => (
                        (command.Name == null || c.Name.Contains(command.Name))
                        && (command.Vin == null || c.Vin.Contains(command.Vin))
                    ));

            var productsQuery = DbContext.Products.Where(predicate).AsQueryable();

            if (!string.IsNullOrEmpty(command.OrderBy))
            {
                productsQuery = productsQuery.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = productsQuery.Count();
            var products = productsQuery.Paginate(command);

            return new PagingResult<ProductDto>(products.Select(ProductDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };

        }
    }
}
