﻿using System;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.UserDocument;
using DEA.CRM.Service.Contract.UserDocument.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using DEA.CRM.Utils.Timing;

namespace DEA.CRM.Service.UserDocument
{
    public class DefaultUserDocumentService: DefaultServiceBase, IUserDocumentService
    {
        private readonly IUserInfoService _userInfoService;
        
        public DefaultUserDocumentService(CRMAppDbContext dbContext, IServiceFactory serviceFactory, IUserInfoService userInfoService) : base(dbContext, serviceFactory)
        {
            _userInfoService = userInfoService;
        }
        
        public Result<PagingResult<UserDocumentDto>> SearchUserDocuments(SearchUserDocumentQuery query)
        {
            return Result<PagingResult<UserDocumentDto>>.Success(
                PagingResult<UserDocumentDto>.AllResult(DbContext.UserDocuments
                    .Select(UserDocumentDto.EntityToDtoMapper).ToList())
            );
        }

        public Result<UserDocumentDto> CreateOrUpdate(UserDocumentDto userDocumentDto, UserDetailDto currentUser)
        {
            if(userDocumentDto == null)
                throw new ArgumentNullException(nameof(userDocumentDto));
                        
            if(currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (userDocumentDto.Id <= 0)
                return CreateUserDocumentInternal(userDocumentDto, currentUser);
                        
            return UpdateUserDocumentInternal(userDocumentDto, currentUser);

        }

        private Result<UserDocumentDto> UpdateUserDocumentInternal(UserDocumentDto userDocumentDto, UserDetailDto currentUser)
        {
            throw new NotImplementedException();
        }

        private Result<UserDocumentDto> CreateUserDocumentInternal(UserDocumentDto userDocumentDto, UserDetailDto currentUser)
        {
            userDocumentDto.AttachmentId = userDocumentDto.AttachmentId;
            userDocumentDto.DocumentLink = userDocumentDto.DocumentLink;
            userDocumentDto.CreatedDate = Clock.Now;
            userDocumentDto.ModifiedDate = Clock.Now;
            userDocumentDto.OwnerUserKey = currentUser.UserKey;
            userDocumentDto.OwnerUserFullName = currentUser.FullName;
            userDocumentDto.ModifiedByUserKey = currentUser.UserKey;
            userDocumentDto.ModifiedByUserFullName = currentUser.FullName;
            
            var userDocumentEntity = UserDocumentDto.DtoToEntityMapper(userDocumentDto);
            
            DbContext.UserDocuments.Add(userDocumentEntity);
            DbContext.SaveChanges();

            userDocumentDto.Id = userDocumentEntity.Id;
            
            return Result.Success(userDocumentDto);
        }
    }
}
