﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using System.Linq;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.HandOverContract;

namespace DEA.CRM.Service.HandOverContract
{
    public class DefaultHandOverContractService : DefaultServiceBase, IHandOverContractService
    {
        public DefaultHandOverContractService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        public Result<HandOverContractDto> GetById(int handovercontractId)
        {
            return DbContext.HandOverContracts.FirstOrDefault(c => c.Id == handovercontractId)
               .AsResult($"handover Contact with id {handovercontractId} not found.")
               .Bind(
                   entity => HandOverContractDto.EntityToDtoMapper(entity).AsResult("Cannot convert handover contact entity to dto")
               );
        }

        public PagingResult<HandOverContractDto> SearchHandOverContracts(SearchHandOverContractCommand command)
        {
            if (command == null)
                return PagingResult<HandOverContractDto>.Empty();
            var predicate =
                Functions.Lambda<Model.HandOverContract, bool>(
                    c => (
                        (command.FirstName == null || c.FirstName.Contains(command.FirstName))
                        && (command.LastName == null || c.LastName.Contains(command.LastName))
                        && (command.Title == null || c.Title.Contains(command.Title))
                    ));

            var handOverContractsQuery = DbContext.HandOverContracts.Where(predicate).AsQueryable();

            if (!string.IsNullOrEmpty(command.OrderBy))
            {
                handOverContractsQuery = handOverContractsQuery.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = handOverContractsQuery.Count();
            var contacts = handOverContractsQuery.Paginate(command);

            return new PagingResult<HandOverContractDto>(contacts.Select(HandOverContractDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };


        }

        public Result<HandOverContractDto> Update(CreateOrUpdateHandOverContractCommand command)
        {
            throw new System.NotImplementedException();
        }
    }
}
