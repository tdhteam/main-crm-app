﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Calendar;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using DEA.CRM.Utils.Timing;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.Service.Calendar
{
    public class DefaultCalendarEventService : DefaultServiceBase, ICalendarEventService
    {
        private readonly IUserPermissionService _userPermissionService;
        private readonly IPickListService _pickListService;
        private readonly IUserInfoService _userInfoService;

        public DefaultCalendarEventService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            IUserPermissionService userPermissionService, 
            IPickListService pickListService, 
            IUserInfoService userInfoService) : base(dbContext, serviceFactory)
        {
            _userPermissionService = userPermissionService;
            _pickListService = pickListService;
            _userInfoService = userInfoService;
        }

        #region Activity

        public Result<ActivityDto> CreateOrUpdateActivity(UpdateActivityCommand command, UserDetailDto currentUser)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));
            
            if(currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (command.Id <= 0)
            {
                return HandleCreateActivity(command, currentUser);
            }
            
            return HandleUpdateActivity(command, currentUser);
        }

        private Result<ActivityDto> HandleUpdateActivity(UpdateActivityCommand command, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Edit))
            {
                return Result<ActivityDto>.Fail($"User has no edit permission for activity");
            }

            var activityDto = DbContext.Activities
                .Include(x => x.ActivityAssignment)
                .FirstOrNothing(x => x.Id == command.Id)
                .Do(activityEntity =>
                {
                    activityEntity = ActivityDto.DtoToEntityMapperForUpdate(activityEntity, command);
                    DbContext.SaveChanges();
                })
                .Map(e => ActivityDto.EntityToDtoMapper(e))
                .Unwrap();

            return activityDto.AsResult("Failed to update activity");
        }

        private Result<ActivityDto> HandleCreateActivity(UpdateActivityCommand command, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Create))
            {
                return Result<ActivityDto>.Fail($"User has no create permission for activity");
            }

            var activity = ActivityDto.DtoToEntityMapper(command);
            DbContext.Activities.Add(activity);
            DbContext.SaveChanges();

            return ActivityDto.EntityToDtoMapper(activity).AsResult("Failed to create activity");
        }

        /// <summary>
        /// Build a form schema for new activity or edit activity including static data lookup used in the form
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public Result<ActivityDetailFormDto> BuildActivityDetailFormSchema(int id, UserDetailDto currentUser)
        {
            var activityDtoRes =
                id <= 0 ? GetNewDefaultActivity(currentUser) : GetActivityById(id, currentUser);
            
            if (activityDtoRes.IsFailure)
                return Result<ActivityDetailFormDto>.Fail(activityDtoRes.Error);
            
            var allCompanies = _pickListService.GetCompaniesAsPickListForUser(currentUser);
            var allDealers = _pickListService.GetDealersAsPickListForUser(currentUser);
            var allShowrooms = _pickListService.GetShowroomAsPickListForUser(currentUser);
            var priorityList = _pickListService.GetActivityPriorityList();
            var activityStatusList = _pickListService.GetActivityStatusList();

            var activityFormRes = new ActivityDetailFormDto
            {
                FormData = activityDtoRes.Value,
                PickListValues = new Dictionary<string, IList<IPickListDto>>
                {
                    {"Companies", allCompanies},
                    {"Dealers", allDealers},
                    {"Showrooms", allShowrooms}
                },
                PriorityList = priorityList,
                ActivityStatusList = activityStatusList,
                AssignedToUserList = _userInfoService.FindAllUsersForAssignedUser(currentUser).Value
            };
            
            return Result.Success(activityFormRes);
        }

        public Result<ActivityDto> GetActivityById(int id, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.View))
            {
                return Result<ActivityDto>.Fail($"User has no view permission for activity");
            }
            
            var activityDto = DbContext.Activities
                .Include(x => x.ActivityAssignment)
                .FirstOrNothing(x => x.Id == id)
                .Map(e => ActivityDto.EntityToDtoMapper(e))
                .Unwrap();

            activityDto.OwnerUserFullName = _userInfoService.GetUserFullName(activityDto.OwnerUserKey);

            return activityDto.AsResult($"Failed to get activity with id {id}");
        }

        public Result<ActivityDto> GetNewDefaultActivity(UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Create))
            {
                return Result<ActivityDto>.Fail($"User has no create permission for activity");
            }

            return Result.Success(new ActivityDto
            {
                OwnerUserKey = currentUser.UserKey,
                OwnerUserFullName = currentUser.FullName,
                StartDateTime = Clock.Now.Date,
                DueDateTime = Clock.Now.Date,
                Status = GlobalConstants.StatusActive
            });
        }

        public ResponseDto DeleteActivityById(int id, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Delete))
            {
                return ResponseDto.ErrorContent("User has no delete permission for activity");
            }
            
            var activity = DbContext.Activities
                .Include(x => x.ActivityAssignment)
                .FirstOrDefault(x => x.Id == id);

            if (activity != null)
            {
                DbContext.ActivityAssignments.RemoveRange(activity.ActivityAssignment);
                DbContext.Activities.Remove(activity);
                DbContext.SaveChanges();
            }

            return ResponseDto.OkNoContent();
        }

        #endregion

        #region Calendar Event

        public Result<CalendarEventDto> CreateOrUpdateCalendarEvent(UpdateCalendarEventCommand command, UserDetailDto currentUser)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));
            
            if(currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (command.Id <= 0)
            {
                return HandleCreateCalendarEvent(command, currentUser);
            }
            
            return HandleUpdateCalendarEvent(command, currentUser);  
        }

        private Result<CalendarEventDto> HandleUpdateCalendarEvent(UpdateCalendarEventCommand command, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Edit))
            {
                return Result<CalendarEventDto>.Fail($"User has no edit permission for calendar event");
            }

            var eventDto = DbContext.CalendarEvents
                .Include(x => x.CalendarEventType)
                .Include(x => x.EventInvitee)
                .Include(x => x.EventRecurrence)
                .Include(x => x.EventReminder)
                .FirstOrNothing(x => x.Id == command.Id)
                .Do(entity =>
                {
                    entity = CalendarEventDto.DtoToEntityMapperForUpdate(entity, command);
                    DbContext.SaveChanges();
                })
                .Map(e => CalendarEventDto.EntityToDtoMapper(e))
                .Unwrap();

            return eventDto.AsResult("Failed to update activity");
        }

        private Result<CalendarEventDto> HandleCreateCalendarEvent(UpdateCalendarEventCommand command, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Create))
            {
                return Result<CalendarEventDto>.Fail($"User has no create permission for calendar event");
            }

            var eventEntity = CalendarEventDto.DtoToEntityMapper(command);
            DbContext.CalendarEvents.Add(eventEntity);
            DbContext.SaveChanges();

            return CalendarEventDto.EntityToDtoMapper(eventEntity).AsResult("Failed to create calendar event");
        }

        public Result<CalendarEventDetailFormDto> BuildCalendarEventDetailFormSchema(int id, UserDetailDto currentUser)
        {
            var eventDtoRes =
                id <= 0 ? GetNewDefaultCalendarEvent(currentUser) : GetCalendarEventById(id, currentUser);
            
            if (eventDtoRes.IsFailure)
                return Result<CalendarEventDetailFormDto>.Fail(eventDtoRes.Error);
            
            var allCompanies = _pickListService.GetCompaniesAsPickListForUser(currentUser);
            var allDealers = _pickListService.GetDealersAsPickListForUser(currentUser);
            var allShowrooms = _pickListService.GetShowroomAsPickListForUser(currentUser);
            var priorityList = _pickListService.GetActivityPriorityList();
            var eventStatusList = _pickListService.GetEventStatusList();
            var eventTypeList = _pickListService.GetEventTypeList();

            var activityFormRes = new CalendarEventDetailFormDto
            {
                FormData = eventDtoRes.Value,
                PickListValues = new Dictionary<string, IList<IPickListDto>>
                {
                    {"Companies", allCompanies},
                    {"Dealers", allDealers},
                    {"Showrooms", allShowrooms}
                },
                PriorityList = priorityList,
                EventStatusList = eventStatusList,
                EventTypeList = eventTypeList,
                AssignedToUserList = _userInfoService.FindAllUsersForAssignedUser(currentUser).Value
            };
            
            return Result.Success(activityFormRes);
        }

        public Result<CalendarEventDto> GetCalendarEventById(int id, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.View))
            {
                return Result<CalendarEventDto>.Fail($"User has no view permission for calendar event");
            }
            
            var eventDto = DbContext.CalendarEvents
                .Include(x => x.CalendarEventType)
                .Include(x => x.EventInvitee)
                .Include(x => x.EventRecurrence)
                .Include(x => x.EventReminder)
                .FirstOrNothing(x => x.Id == id)
                .Map(e => CalendarEventDto.EntityToDtoMapper(e))
                .Unwrap();

            return eventDto.AsResult($"Failed to get calendar event with id {id}");
        }

        public Result<CalendarEventDto> GetNewDefaultCalendarEvent(UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Create))
            {
                return Result<CalendarEventDto>.Fail($"User has no create permission for calendar event");
            }

            return Result.Success(new CalendarEventDto
            {
                OwnerUserKey = currentUser.UserKey,
                OwnerUserFullName = currentUser.FullName,
                StartDateTime = Clock.Now.Date,
                DueDateTime = Clock.Now.Date
            });
        }

        public ResponseDto DeleteCalendarEventById(int id, UserDetailDto currentUser)
        {
            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Calendar, PermissionOperation.Delete))
            {
                return ResponseDto.ErrorContent("User has no delete permission for calendar event");
            }
            
            var calendarEvent = DbContext.CalendarEvents
                .Include(x => x.CalendarEventType)
                .Include(x => x.EventInvitee)
                .Include(x => x.EventRecurrence)
                .Include(x => x.EventReminder)
                .FirstOrDefault(x => x.Id == id);

            if (calendarEvent != null)
            {
                DbContext.EventInvitees.RemoveRange(calendarEvent.EventInvitee);
                DbContext.EventRecurrences.RemoveRange(calendarEvent.EventRecurrence);
                DbContext.EventReminders.RemoveRange(calendarEvent.EventReminder);
                DbContext.CalendarEvents.Remove(calendarEvent);
                DbContext.SaveChanges();
            }

            return ResponseDto.OkNoContent();
        }

        #endregion

        #region Search activity and calendar events

        /// <summary>
        /// Search calendar items are union result from Activity and CalendarEvent
        /// </summary>
        /// <param name="query"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public PagingResult<CalendarEventListViewSearchResultDto> SearchCalendarItems(SearchCalendarItemQuery query, UserDetailDto currentUser)
        {
            //TODO apply query for search calendar
            return PagingResult<CalendarEventListViewSearchResultDto>.AllResult(
                DbContext.CalendarEventListViewSearchResults.Select(CalendarEventListViewSearchResultDto.EntityToDtoMapper).ToList()
            );
        }

        public List<EventItemDto> GetCalendarItemsByMonth(GetCalendarQuery query, UserDetailDto currentUser)
        {
            return DbContext.CalendarEventListViewSearchResults
                .Where(x => x.StartDateTime.HasValue && x.StartDateTime.Value.Month == query.Month)
                .Select(x => 
                new EventItemDto
                {
                    Title = x.Subject,
                    Start = x.StartDateTime.GetValueOrDefault(DateTime.MinValue),
                    End = x.DueDateTime.GetValueOrDefault(DateTime.MinValue),
                    AllDay = false
                })
                .ToList();

        }

        #endregion
    }
}
