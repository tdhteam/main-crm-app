﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils.Pagination;
using System.Linq;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Contact.Dto;

namespace DEA.CRM.Service.Contact
{
    public class DefaultLeadSourceService : DefaultServiceBase, ILeadSourceService
    {
        public DefaultLeadSourceService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        public PagingResult<LeadSourceDto> GetAllLeadSources()
        {
            var allLeadSources = DbContext.LeadSources.Select(LeadSourceDto.EntityToDtoMapper)
                .OrderBy(x => x.SortOrder)
                .ToList();
            return PagingResult<LeadSourceDto>.AllResult(allLeadSources);
        }
    }
}
