﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Timing;

namespace DEA.CRM.Service.Contact
{
    public class DefaultContactService : DefaultServiceBase, IContactService
    {
        private readonly IPickListService _pickListService;
        private readonly IUserInfoService _userInfoService;

        public DefaultContactService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            IPickListService pickListService, IUserInfoService userInfoService) : base(dbContext, serviceFactory)
        {
            _pickListService = pickListService;
            _userInfoService = userInfoService;
        }

        public PagingResult<ContactDto> SearchContacts(SearchContactCommand command)
        {
            if (command == null)
                return PagingResult<ContactDto>.Empty();

            var dealers = DbContext.UserInfoToDealers.Where(d => d.UserKey == command.UserKey);
            var showrroms = DbContext.UserInfoToShowrooms.Where(s => s.UserKey == command.UserKey);

            var contactsQuery = (from contact in DbContext.Contacts
                join show in showrroms on contact.ShowroomId equals show.ShowroomId
                select contact).Union(from contact in DbContext.Contacts
                join dealer in dealers on contact.DealerId equals dealer.DealerId
                select contact);


            var predicate =
                Functions.Lambda<Model.Contact, bool>(
                    c => (
                        (command.FirstName == null || c.FirstName.Contains(command.FirstName))
                        && (command.LastName == null || c.LastName.Contains(command.LastName))
                        && (command.Email == null || c.Email.Contains(command.Email))
                        && (command.Title == null || c.Title.Contains(command.Title))
                         && (command.HomePhone == null || c.Title.Contains(command.HomePhone))
                    ));

            contactsQuery = contactsQuery.Where(predicate).AsQueryable();

            if(!string.IsNullOrEmpty(command.OrderBy))
            {
                contactsQuery = contactsQuery.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = contactsQuery.Count();
            var contacts = contactsQuery.Paginate(command);

            return new PagingResult<ContactDto>(contacts.Select(ContactDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }

        public Result<ContactDetailDto> Update(CreateOrUpdateContactCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (command.Id == 0)
            {
                var newContact = ContactDetailDto.DetailDtoToEntityMapper(command);
                DbContext.Contacts.Add(newContact);
                DbContext.SaveChanges();
                command.Id = newContact.Id;
            }
            else
            {
                var contact = DbContext.Contacts.Find(command.Id);
                if (contact != null)
                {
                    contact.Title = command.Title;
                    contact.FirstName = command.FirstName;
                    contact.LastName = command.LastName;
                    contact.MobilePhone = command.MobilePhone;
                    contact.HomePhone = command.HomePhone;
                    contact.OfficePhone = command.OfficePhone;
                    contact.Occupation = command.Occupation;
                    contact.AddressCity = command.AddressCity;
                    contact.AddressCountry = command.AddressCountry;
                    contact.AddressDistrict = command.AddressDistrict;
                    contact.AddressPostalCode = command.AddressPostalCode;
                    contact.AddressState = command.AddressState;
                    contact.AddressStreet = command.AddressStreet;
                    contact.ApproachingType = command.ApproachingType;
                    contact.Salutation = command.Salutation;
                    contact.PaymentType = command.PaymentType;
                    contact.PlanToBuyDate = command.PlanToBuyDate;
                    contact.DateOfBirth = command.DateOfBirth;
                    contact.FirstMeetingDate = command.FirstMeetingDate;
                    contact.Email = command.Email;
                    contact.ContactSource = command.ContactSource;
                    DbContext.SaveChanges();
                }
            }
            return Result<ContactDetailDto>.Success(command);
        }

        public Result<ContactDetailDto> UpdateContactMeetingProgress(CreateOrUpdateContactMeetingCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (command.Id == 0)
            {
                var contactMeeting = ContactMeetingProgressDto.DtoToEntityMapper(command);
                DbContext.ContactMeetingProgresses.Add(contactMeeting);
                DbContext.SaveChanges();
                command.Id = contactMeeting.Id;
            }
            else
            {
                var contactMeeting = DbContext.ContactMeetingProgresses.Find(command.Id);
                if (contactMeeting != null)
                {

                    contactMeeting.CarColorOfInterest = command.CarColorOfInterest;
                    contactMeeting.CarOfInterest = command.CarOfInterest;
                    contactMeeting.DateOfMeeting = command.DateOfMeeting;
                    contactMeeting.Description = command.Description;
                    DbContext.SaveChanges();
                }
            }
            return GetById(command.ContactId);
        }

        public Result<ContactDetailDto> GetById(int id)
        {
            return DbContext.Contacts
                .Include(c => c.ContactMeetingProgresses)
                .Include(c => c.ContactComments)
                .ThenInclude(cm => cm.ContactCommentAttachments)
                .ThenInclude(a => a.Attachment)
                .FirstOrDefault(c => c.Id == id)
                .AsResult($"Contact with id {id} not found.")
                .Bind(
                    entity => ContactDetailDto.EntityToDetailDtoMapper(entity)
                        .AsResult("Cannot convert contact entity to dto")
                );
        }

        public Result<ContactDetailFormDto> BuildContactDetailFormSchema(int id, UserDetailDto currentUser)
        {
            var salutationList = _pickListService.GetSalutationList();
            var leadSources = _pickListService.GetLeadSources();
            var assignedToUserList = _userInfoService.FindAllUsersForAssignedUser(currentUser).Value;
            var carMakeList = _pickListService.GetCarMakeList();

            if (id <= 0)
            {
                var newDefaultForm = DefaultNewContactDetailForm(currentUser)
                    .OnSuccess(
                        form =>
                        {
                            form.SalutationList = salutationList;
                            form.LeadSources = leadSources;
                            form.AssignedToUserList = assignedToUserList;
                            form.CarMakeList = carMakeList;
                        }
                    );
                return newDefaultForm;
            }

            var contactDetailDto = GetById(id);

            if (contactDetailDto.IsFailure)
                return Result<ContactDetailFormDto>.Fail(contactDetailDto.Error);

            return Result<ContactDetailFormDto>.Success(new ContactDetailFormDto
            {
                FormData = contactDetailDto.Value,
                SalutationList = salutationList,
                LeadSources = leadSources,
                AssignedToUserList = assignedToUserList,
                CarMakeList = carMakeList
            });
        }

        private Result<ContactDetailFormDto> DefaultNewContactDetailForm(UserDetailDto currentUser)
        {
            return Result<ContactDetailFormDto>.Success(new ContactDetailFormDto
            {
                FormData = new ContactDetailDto
                {
                    FirstMeetingDate = DateTime.Now,
                    HandlerUserKey = currentUser.UserKey,
                    HandlerUserFullName = currentUser.FullName
                }
            });
        }

        public Result<ContactCommentDto> UpdateContactComment(CreateOrUpdateContactCommentCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            var contactCommentDto = command as ContactCommentDto;
            if (command.Id == 0)
            {
                var contactComment = ContactCommentDto.DtoToEntityMapper(contactCommentDto);
                DbContext.ContactComments.Add(contactComment);
                DbContext.SaveChanges();
                contactCommentDto.Id = contactComment.Id;
            }
            else
            {
                var contactComment = DbContext.ContactComments.Find(command.Id);
                if (contactComment != null)
                {
                    contactComment.CommentContent = command.CommentContent;
                    contactComment.ModifiedDate = DateTime.Now;
                    DbContext.SaveChanges();
                }
            }
            return Result.Success(contactCommentDto);
        }

        public Result<ContactCommentAttachmentDto> UpdateContactCommentAttachment(CreateOrUpdateContactCommentAttachmentCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            var attachDto = command as ContactCommentAttachmentDto;
            if (command.Id == 0)
            {
                var attach = ContactCommentAttachmentDto.DtoToEntityMapper(attachDto);
                DbContext.ContactCommentAttachments.Add(attach);
                DbContext.SaveChanges();
                attachDto.Id = attach.Id;
            }
            return Result.Success(attachDto);
        }
    }
}
