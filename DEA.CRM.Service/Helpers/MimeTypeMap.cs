﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DEA.CRM.Service.Helpers
{
    /// <summary>
    /// Reference from https://raw.githubusercontent.com/samuelneff/MimeTypeMap/master/src/MimeTypes/MimeTypeMap.cs
    /// </summary>
    public static class MimeTypeMap
    {
        private static readonly Lazy<IDictionary<string, string>> Mappings = new Lazy<IDictionary<string, string>>(BuildMappings);

        private static IDictionary<string, string> BuildMappings()
        {
            var mappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) {

                #region Big freaking list of mime types
            
                // maps both ways,
                // extension -> mime type
                //   and
                // mime type -> extension
                //
                // any mime types on left side not pre-loaded on right side, are added automatically
                // some mime types can map to multiple extensions, so to get a deterministic mapping,
                // add those to the dictionary specifcially
                //
                // combination of values from Windows 7 Registry and 
                // from C:\Windows\System32\inetsrv\config\applicationHost.config
                // some added, including .7z and .dat
                //
                // Some added based on http://www.iana.org/assignments/media-types/media-types.xhtml
                // which lists mime types, but not extensions
                //                
                {".bmp", "image/bmp"},
                {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
                {".gif", "image/gif"},
                {".ico", "image/x-icon"},
                {".jpe", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".jpg", "image/jpeg"},
                {".js", "application/javascript"},
                {".json", "application/json"},
                {".jsx", "text/jscript"},
                {".pdf", "application/pdf"},
                {".png", "image/png"},
                {".pnz", "image/png"},
                {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
                {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
                {".ppt", "application/vnd.ms-powerpoint"},
                {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
                {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
                {".svg", "image/svg+xml"},
                {".tif", "image/tiff"},
                {".tiff", "image/tiff"},
                {".txt", "text/plain"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
                {".xml", "text/xml"},                
                {".zip", "application/zip"},
                {"image/bmp", ".bmp"},
                {"image/jpeg", ".jpg"},
                {"image/pict", ".pic"},
                {"image/png", ".png"},
                {"image/tiff", ".tiff"},
                {"image/x-macpaint", ".mac"},
                {"image/x-quicktime", ".qti"},
                {"message/rfc822", ".eml"},
                {"text/html", ".html"},
                {"text/plain", ".txt"},
                

                #endregion

                };

            var cache = mappings.ToList(); // need ToList() to avoid modifying while still enumerating

            foreach (var mapping in cache)
            {
                if (!mappings.ContainsKey(mapping.Value))
                {
                    mappings.Add(mapping.Value, mapping.Key);
                }
            }

            return mappings;
        }

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException(nameof(extension));
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            return Mappings.Value.TryGetValue(extension, out var mime) ? mime : "application/octet-stream";
        }

        public static string GetExtension(string mimeType)
        {
             return GetExtension(mimeType, true);
        }
        
        public static string GetExtension(string mimeType, bool throwErrorIfNotFound)
        {
            if (mimeType == null)
            {
                throw new ArgumentNullException(nameof(mimeType));
            }

            if (mimeType.StartsWith("."))
            {
                throw new ArgumentException("Requested mime type is not valid: " + mimeType);
            }

            if (Mappings.Value.TryGetValue(mimeType, out var extension))
            {
                return extension;
            }
            if (throwErrorIfNotFound)
            {
                throw new ArgumentException("Requested mime type is not registered: " + mimeType);
            }
            
            return string.Empty;
        }
    }
}
