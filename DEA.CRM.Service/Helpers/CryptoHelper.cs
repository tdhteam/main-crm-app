﻿using System.Security.Cryptography;
using DEA.CRM.Utils.Extensions;

namespace DEA.CRM.Service.Helpers
{
    public static class CryptoHelper
    {
        public static byte[] Md5ComputeHashBytes(byte[] data)
        {
            // This is one implementation of the abstract class MD5.
            var md5 = new MD5CryptoServiceProvider();

            byte[] result = md5.ComputeHash(data);

            return result;
        }
        
        public static string Md5ComputeHashString(string data)
        {
            var result = CryptoHelper.Md5ComputeHashBytes(data.GetBytes());
            return result.ToHexString(true);
        }
    }
}
