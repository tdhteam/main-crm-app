﻿using System;
using System.IO;
using DEA.CRM.Utils.Extensions;

namespace DEA.CRM.Service.Helpers
{
    public static class FileHelper
    {
        public static string GetFileName(string fileName)
        {
            var indexOfSlash = fileName.LastIndexOf('\\');
            return indexOfSlash < 0
                ? fileName
                : fileName.Substring(indexOfSlash + 1);
        }

        public static string RenameFileIfExists(Func<string, bool> existFunc, string containingDirectory, string originalName)
        {
            var newName = originalName;
            var copyNumber = 1;
            while (existFunc(Path.Combine(containingDirectory, newName)))
            {
                newName = $"{Path.GetFileNameWithoutExtension(originalName)} ({copyNumber++}){Path.GetExtension(originalName)}";
            }
            return Path.Combine(containingDirectory, newName);
        }

        public static bool IsImageFileContentType(string contentType)
        {
            return contentType.IsNotNullOrEmpty() && contentType.ContainsIgnoreCase("image");
        }

        public static bool IsSafeFileExtensionForUploading(string whiteListExtension, string extensionToCheck)
        {
            if (string.IsNullOrEmpty(whiteListExtension) || string.IsNullOrEmpty(extensionToCheck)) return false;
            if ("*".Equals(whiteListExtension)) return true;
            var extArray = whiteListExtension.Split(',', ';', '#', '/');
            var isSafe = false;
            foreach (var ext in extArray)
            {
                if (extensionToCheck.StartsWith(".")) extensionToCheck = extensionToCheck.Replace(".", "");
                var safeExt = ext.StartsWith(".") ? ext.Replace(".", "") : ext;
                isSafe = string.Compare(safeExt, extensionToCheck, StringComparison.InvariantCultureIgnoreCase) == 0;
                if (isSafe) break;
            }
            return isSafe;
        }
    }
}
