﻿using System;
using System.Collections.Generic;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Extensions;

namespace DEA.CRM.Service.Helpers
{
    public enum ErrorPresentationStyle
    {
        Suppress
        , Aggregated
        , Warning
        , Error
    }

    public class ExceptionInfo
    {
        public string ErrorMessage { get; }
        public Exception[] Exceptions { get; }
        public ErrorPresentationStyle PresentationStyle { get; }

        public ExceptionInfo(string errorMessage, ErrorPresentationStyle presentationStyle, params Exception[] exceptions)
        {
            ErrorMessage = errorMessage;
            Exceptions = exceptions;
            PresentationStyle = presentationStyle;
        }
    }

    public class ExceptionHelper
    {
        public static Exception GetInnerMostException(Exception ex)
        {
            var rootEx = ex;
            while (rootEx.InnerException != null)
            {
                rootEx = rootEx.InnerException;
            }
            return rootEx;
        }

        public static ExceptionInfo GetExeptionDetails(Exception ex)
        {
            switch (ex)
            {
                case ApplicationException _:
                case BusinessException _:
                case AppNotificationException _:
                case InvalidOperationException _:
                case ArgumentException _:
                    return new ExceptionInfo(ex.Message, ErrorPresentationStyle.Warning, ex);

                case AggregateException aggE:
                    return new ExceptionInfo(aggE.Message, ErrorPresentationStyle.Aggregated, ProcessAggregateException(aggE).ToArray());

                //case DbEntityValidationException devE:
                //    return new ExceptionInfo(GetErrorMessageFor(devE), ErrorPresentationStyle.Warning, ex);

                //case DbUpdateException duE:
                //    return new ExceptionInfo(GetErrorMessageFor(duE), ErrorPresentationStyle.Warning, GetInnerMostException(ex));

                //case EntityCommandExecutionException eceE:
                //    return new ExceptionInfo(GetErrorMessageFor(eceE), ErrorPresentationStyle.Warning, GetInnerMostException(eceE));
            }
            //if (ex.InnerException is DbEntityValidationException idevE)
            //{
            //    return new ExceptionInfo(GetErrorMessageFor(idevE), ErrorPresentationStyle.Warning, idevE);
            //}

            return new ExceptionInfo(GetErrorMessageFor(ex), ErrorPresentationStyle.Error, GetInnerMostException(ex));
        }

        //public static string GetErrorMessageFor(DbEntityValidationException ex)
        //{
        //    string FormatEntityValidationResult(DbEntityValidationResult e)
        //    {
        //        return $"Entity of type {e.Entry.Entity.GetType().Name} has the following validation errors:\n" +
        //               string.Join("\r\n", e.ValidationErrors.Select(ve => $"{ve.PropertyName}: {ve.ErrorMessage}"));
        //    }
        //    return string.Join("\r\n", ex.EntityValidationErrors.Select(FormatEntityValidationResult));
        //}

        public static string GetErrorMessageFor(Exception ex)
        {
            var rootEx = GetInnerMostException(ex);
            var errorMsg = rootEx.Message;
            return errorMsg;
        }

        //public static string GetErrorMessageFor(EntityCommandExecutionException ex)
        //{
        //    var rootEx = GetInnerMostException(ex);
        //    var errorMsg = rootEx is SqlException exception
        //        ? exception.Message + ". Procedure: " + exception.Procedure
        //        : rootEx.Message;
        //    return errorMsg;
        //}

        private static List<Exception> ProcessAggregateException(AggregateException ex, List<Exception> agg = null)
        {
            if (agg == null) agg = new List<Exception>();
            var innerExceptions = ex.InnerExceptions;

            foreach (var e in innerExceptions)
            {
                if (e is AggregateException aex) ProcessAggregateException(aex, agg);
                else agg.Add(e.GetInnerMostException());
            }
            return agg;
        }
    }
}
