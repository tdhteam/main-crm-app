﻿using System;

namespace DEA.CRM.Service.Helpers
{
    public static class SettingHelper
    {
        public static bool? IsAffirmative(string settingValue)
        {
            if (string.IsNullOrEmpty(settingValue)) return false;

            switch (settingValue.ToUpper())
            {
                case "1":
                case "-1":
                case "ON":
                case "YES":
                case "ACTIVATED":
                case "ENABLED":
                    return true;

                case "0":
                case "OFF":
                case "NO":
                case "DEACTIVATED":
                case "DISABLED":
                    return false;

                default:
                    return null;
            }
        }

        public static float GetFloatValue(string settingValue)
        {
            var val = 0f;
            return float.TryParse(settingValue, out val) ? val : 0;
        }

        public static int GetIntValue(string settingValue)
        {
            var val = 0;
            return int.TryParse(settingValue, out val) ? val : 0;
        }

        public static DateTime GetDateTimeValue(string settingValue)
        {
            var val = DateTime.MinValue;
            return DateTime.TryParse(settingValue, out val) ? val : DateTime.MinValue;
        }

        public static DateTime GetTimeValueFromString(string settingValue)
        {
            var today = DateTime.Today;
            var valInHours = GetFloatValue(settingValue);
            return today.AddHours(valInHours);
        }
    }
}