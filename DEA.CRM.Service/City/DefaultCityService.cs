﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Core;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.City;
using DEA.CRM.Service.Contract.Dto;

namespace DEA.CRM.Service.City
{
    public class DefaultCityService : DefaultServiceBase, ICityService
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public DefaultCityService(CRMAppDbContext dbContext, IServiceFactory serviceFactory, IHostingEnvironment hostingEnvironment) : base(dbContext, serviceFactory)
        {
            _hostingEnvironment = hostingEnvironment;
        }
    

        public IEnumerable<LookUpDto> GetAllCities()
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var jsonText = System.IO.File.ReadAllText(contentRootPath + "/json/vietnam_cities_provinces.json");
            if(jsonText!=string.Empty)
            {
                var list = new List<LookUpDto>();
                var cities = JObject.Parse(jsonText);
                foreach (var property in cities.Properties())
                {
                    list.Add(new LookUpDto
                    {
                        Value = property.Name,
                        Name = property.Children()["name"].First().ToString()
                    });
                }
                return list;
            }

            return null;
        }

        public IEnumerable<LookUpDto> GetDistrictsByCity(string city)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var jsonText = System.IO.File.ReadAllText(contentRootPath + "/json/vietnam_cities_provinces.json");
            if (jsonText != string.Empty)
            {
                var list = new List<LookUpDto>();
                var cities = JObject.Parse(jsonText);
                var c = city.ToUpper();
                var cityJson = cities.Properties().FirstOrDefault(p => p.Name == c);
                if(cityJson != null)
                {
                    foreach (var property in cityJson.Children()["cities"].Children())
                    {
                        var json = ((JProperty)property);
                        list.Add(new LookUpDto
                        {
                            Value = json.Name,
                            Name = json.Value.ToString()
                        });
                    }
                    return list;
                }
            }

            return null;
        }
    }
}
