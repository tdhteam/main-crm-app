﻿using System.Collections.Generic;

namespace DEA.CRM.Service.Localization
{
    /// <summary>
    /// Used for localization configurations.
    /// </summary>
    public class DefaultLocalizationConfiguration : ILocalizationConfiguration
    {
        /// <summary>
        /// Used to set languages available for this application.
        /// </summary>
        public IList<LanguageInfo> Languages { get; private set; }

        /// <summary>
        /// List of localization sources.
        /// </summary>
        public ILocalizationSourceList Sources { get; private set; }

        /// <summary>
        /// Used to enable/disable localization system.
        /// Default: true.
        /// </summary>
        public bool IsEnabled { get; set; }

        public DefaultLocalizationConfiguration()
        {
            Languages = new List<LanguageInfo>();
            Sources = new DefaultLocalizationSourceList();
            IsEnabled = true;
        }
    }
}
