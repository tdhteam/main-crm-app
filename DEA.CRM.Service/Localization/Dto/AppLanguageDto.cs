﻿namespace DEA.CRM.Service.Localization.Dto
{
    public class AppLanguageDto
    {
        public string AppName { get; set; }

        public LoginPage Login { get; set; }

        public DashboardPage Dashboard { get; set; }

        public LocalizedMessageDto Message { get; set; }

        public NavigationText Menu { get; set; }
    }

    public interface ILanguageDto
    {
        string Title { get; set; }
    }

    public class LoginPage : ILanguageDto
    {
        public string Title { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Sign_Me_In { get; set; }
    }

    public class DashboardPage : ILanguageDto
    {
        public string Title { get; set; }
        public string YouCurrentlyHave { get; set; }
    }


    public class LocalizedMessageDto
    {
    }


    public class NavigationText
    {
    }
}
