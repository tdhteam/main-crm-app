﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DEA.CRM.Service.Localization
{
    /// <summary>
    /// Null object pattern for <see cref="ILocalizationSource"/>.
    /// </summary>
    internal class NullLocalizationSource : ILocalizationSource
    {
        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static NullLocalizationSource Instance { get; } = new NullLocalizationSource();

        public string Name => null;

        private readonly IReadOnlyList<LocalizedString> _emptyStringArray = new LocalizedString[0];

        private NullLocalizationSource()
        {

        }

        public void Initialize()
        {

        }

        public string GetString(string name)
        {
            return name;
        }

        public string GetString(string name, CultureInfo culture)
        {
            return name;
        }

        public IReadOnlyList<LocalizedString> GetAllStrings()
        {
            return _emptyStringArray;
        }

        public IReadOnlyList<LocalizedString> GetAllStrings(CultureInfo culture)
        {
            return _emptyStringArray;
        }
    }
}
