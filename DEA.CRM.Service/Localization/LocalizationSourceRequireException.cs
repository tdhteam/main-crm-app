﻿using System;

namespace DEA.CRM.Service.Localization
{
    public class LocalizationSourceRequireException : Exception
    {
        public LocalizationSourceRequireException(string message) : base(message)
        {
        }
    }
}
