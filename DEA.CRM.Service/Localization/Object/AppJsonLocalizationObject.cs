﻿using System;
using System.Globalization;
using DEA.CRM.Service.Localization.Dto;

namespace DEA.CRM.Service.Localization.Object
{

    /// <summary>
    /// Implementation of <see cref="ILocalizationDictionary"/> for specific Agora app
    /// </summary>
    public class AppJsonLocalizationObject : GenericJsonLocalizationObject<AppLanguageDto>
    {
        private AppJsonLocalizationObject(CultureInfo cultureInfo) : base(cultureInfo)
        {
        }

        protected override GenericJsonLocalizationObject<AppLanguageDto> BuildDictionaryFromJsonObject(string cultureName, AppLanguageDto jsonObject)
        {
            if (string.IsNullOrEmpty(cultureName))
            {
                throw new ArgumentNullException(nameof(cultureName), "Culture name is null or empty.");
            }

            if (jsonObject == null)
            {
                throw new ArgumentNullException(nameof(jsonObject), "Parameter jsonObject is null.");
            }

            var dictionary = new AppJsonLocalizationObject(new CultureInfo(cultureName));

            PopulateDictionaryWithObject(dictionary, jsonObject, string.Empty);

            return dictionary;
        }

        private void PopulateDictionaryWithObject(AppJsonLocalizationObject dictionary, object jsonObject, string dictionaryKeyPrefix)
        {
            foreach (var propertyInfo in jsonObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (dictionary.Contains(propertyInfo.Name)) continue;

                    var dictKey = string.IsNullOrEmpty(dictionaryKeyPrefix)
                        ? propertyInfo.Name
                        : string.Format("{0}.{1}", dictionaryKeyPrefix, propertyInfo.Name);
                    var propValue = propertyInfo.GetValue(jsonObject);
                    dictionary[dictKey] = propValue?.ToString() ?? string.Empty;
                }
                else if (propertyInfo.PropertyType.IsPrimitive || propertyInfo.PropertyType == typeof(Decimal))
                {
                    continue;
                }
                else // object type
                {
                    var keyPrefix = string.IsNullOrEmpty(dictionaryKeyPrefix)
                        ? propertyInfo.Name
                        : string.Format("{0}.{1}", dictionaryKeyPrefix, propertyInfo.Name);
                    PopulateDictionaryWithObject(dictionary, propertyInfo.GetValue(jsonObject), keyPrefix);
                }
            }
        }

        public static AppJsonLocalizationObject CreateFromFile(string filePath)
        {
            var obj = new AppJsonLocalizationObject(CultureInfo.CurrentCulture);
            var resultObj = obj.BuildFromFile(filePath);
            return resultObj as AppJsonLocalizationObject;
        }
    }
}
