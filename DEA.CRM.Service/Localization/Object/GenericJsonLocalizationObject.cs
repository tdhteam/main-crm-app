﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using DEA.CRM.Service.Localization.Dictionaries;
using Newtonsoft.Json;

namespace DEA.CRM.Service.Localization.Object
{
    /// <summary>
    /// Implementation for deserialize a generic object from a json file to form an <see cref="ILocalizationDictionary"/>
    /// </summary>
    public abstract class GenericJsonLocalizationObject<T> : LocalizationDictionary
    {
        protected GenericJsonLocalizationObject(CultureInfo cultureInfo) : base(cultureInfo)
        {
        }

        public GenericJsonLocalizationObject<T> BuildFromFile(string filePath)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    throw new ArgumentNullException(nameof(filePath), "File path is null or empty");
                }

                var cultureName = GlobalizationHelper.GetCultureNameFromFileNameWithConvention(Path.GetFileName(filePath));
                using (var textReader = File.OpenText(filePath))
                {
                    using (var jsonTextReader = new JsonTextReader(textReader))
                    {
                        var jsonDiz = new JsonSerializer();
                        var jsonObj = jsonDiz.Deserialize<T>(jsonTextReader);
                        return BuildDictionaryFromJsonObject(cultureName, jsonObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid localization file format! " + filePath, ex);
            }
        }

        protected abstract GenericJsonLocalizationObject<T> BuildDictionaryFromJsonObject(string cultureName, T jsonObject);
    }
}
