﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DEA.CRM.Service.Localization.Object;

namespace DEA.CRM.Service.Localization.Sources.Json
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonFileLocalizationDictionaryProvider : ILocalizationDictionaryProvider
    {
        private readonly string _directoryPath;

        /// <summary>
        /// Creates a new <see cref="JsonFileLocalizationDictionaryProvider"/>.
        /// </summary>
        /// <param name="directoryPath">Path of the dictionary that contains all related XML files</param>
        public JsonFileLocalizationDictionaryProvider(string directoryPath)
        {
            if (!Path.IsPathRooted(directoryPath))
            {
                directoryPath = Path.Combine(DictionaryBasedLocalizationSource.RootDirectoryOfApplication, directoryPath);
            }

            _directoryPath = directoryPath;
        }

        public IEnumerable<LocalizationDictionaryInfo> GetDictionaries(string sourceName)
        {
            var fileNames = Directory.GetFiles(_directoryPath, "*.json", SearchOption.TopDirectoryOnly);

            var dictionaries = new List<LocalizationDictionaryInfo>();

            foreach (var fileName in fileNames.Where(name => !name.EndsWith("languages.json")))
            {
                dictionaries.Add(new LocalizationDictionaryInfo(AppJsonLocalizationObject.CreateFromFile(fileName), isDefault: fileName.EndsWith("en_US.json")));
            }

            return dictionaries;
        }
    }
}
