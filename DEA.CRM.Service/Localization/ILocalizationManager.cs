﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DEA.CRM.Service.Localization
{
    /// <summary>
    /// This interface is used to manage localization system.
    /// </summary>
    public interface ILocalizationManager
    {
        /// <summary>
        /// Initialize the localization manager before using
        /// </summary>
        void Initialize();

        /// <summary>
        /// Reload localization manager with new configuration
        /// </summary>
        /// <param name="configuration">The localization manager configuration</param>
        void Reload(ILocalizationConfiguration configuration);

        /// <summary>
        /// Gets current language for the application.
        /// </summary>
        LanguageInfo CurrentLanguage { get; }

        /// <summary>
        /// Gets all available languages for the application.
        /// </summary>
        /// <returns>List of languages</returns>
        IReadOnlyList<LanguageInfo> GetAllLanguages();

        /// <summary>
        /// Gets a localization source with name.
        /// </summary>
        /// <param name="name">Unique name of the localization source</param>
        /// <returns>The localization source</returns>
        ILocalizationSource GetSource(string name);

        /// <summary>
        /// Gets all registered localization sources.
        /// </summary>
        /// <returns>List of sources</returns>
        IReadOnlyList<ILocalizationSource> GetAllSources();

        /// <summary>
        /// Gets a localized string in current language.
        /// </summary>
        /// <param name="sourceName">Name of the localization source</param>
        /// <param name="name">Key name to get localized string</param>
        /// <returns>Localized string</returns>
        string GetString(string sourceName, string name);

        /// <summary>
        /// Gets a localized string in specified language.
        /// </summary>
        /// <param name="sourceName">Name of the localization source</param>
        /// <param name="name">Key name to get localized string</param>
        /// <param name="culture">culture</param>
        /// <returns>Localized string</returns>
        string GetString(string sourceName, string name, CultureInfo culture);
    }
}
