﻿using System;
using System.Globalization;

namespace DEA.CRM.Service.Localization
{
    public static class GlobalizationHelper
    {
        public static string DefaultLocalizationSourceName = "AppLocalizationSource";

        public static bool IsValidCultureCode(string cultureCode)
        {
            try
            {
                CultureInfo.GetCultureInfo(cultureCode);
                return true;
            }
            catch (CultureNotFoundException)
            {
                return false;
            }
        }

        public static string GetCultureNameFromFileNameWithConvention(string fileName)
        {
            var fileNameParts = fileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
            var cultureName = fileNameParts.Length == 2 ? fileNameParts[0] : fileNameParts.Length == 3 ? fileNameParts[1] : string.Empty;
            return cultureName.Replace("_", "-");
        }

        //public static string GetLocalizedString(string key)
        //{
        //    return IocManager.Instance.GetService<ILocalizationManager>().GetString(DefaultLocalizationSourceName, key);
        //}
    }
}
