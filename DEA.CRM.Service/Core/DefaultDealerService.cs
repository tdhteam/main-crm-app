﻿using System.Linq;
using System.Threading.Tasks;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Core
{
    public class UpdateDealerCommand : DealerDto
    {
        public string LogoDataUrl { get; set; }
        public AttachmentUploadInfo LogoDataFileInfo { get; set; }
    }
    
    public class DefaultDealerService : DefaultServiceBase, IDealerService
    {
        private readonly ISettingService _settingService;
        
        public DefaultDealerService(
            CRMAppDbContext dbContext, 
            ISettingService settingService, 
            IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
            _settingService = settingService;
        }

        public DealerDto GetDealerById(int id)
        {
            var company = DbContext.Companies.FirstOrDefault().AsResult($"There is no company in system.");
            
            var entity = DbContext.Dealers.Find(id).AsResult($"Cannot find dealer with id {id}");

            var dealer = DealerDto.EntityToDtoMapper(entity.Value);
            dealer.CompanyCode = company.Value.CompanyCode;

            if (!string.IsNullOrEmpty(dealer.LogoRelativeUrl))
            {
                var apiBaseUrl = _settingService.GetValue(SettingIds.AppApiBaseUrl);
                dealer.LogoRelativeUrl = $"{apiBaseUrl}{dealer.LogoRelativeUrl}";    
            }
            
            return dealer;
        }

        public DealerDto GetDefaultDealer()
        {
            var company = DbContext.Companies.FirstOrDefault().AsResult($"There is no company in system.");

            var dealer = new DealerDto {CompanyId = company.Value.Id, CompanyCode =  company.Value.CompanyCode};
            return dealer;
        }
        
        public async Task<DealerDto> CreateOrUpdateAsync(DealerDto dealerDto)
        {
            if (dealerDto.Id <= 0) // new
            {
                var entity = DealerDto.DtoToEntityMapper(dealerDto);
                DbContext.Dealers.Add(entity);
                DbContext.SaveChanges();
                dealerDto.Id = entity.Id;
            }
            else
            {
                //update
                var c = DbContext.Companies.Find(dealerDto.CompanyId);
                if (c == null) throw new EntityNotFoundException("I18N.Company.COMPANY_WITH_ID_IS_NOT_FOUND");

                var dealer = DbContext.Dealers.Find(dealerDto.Id);
                if (dealer == null)
                    throw new EntityNotFoundException("I18N.Company.DEALER_WITH_ID_IS_NOT_FOUND");
                
                dealer.Address = dealerDto.Address;
                dealer.City = dealerDto.City;
                dealer.Country = dealerDto.Country;
                dealer.Fax = dealerDto.Fax;
                dealer.LogoRelativeUrl = dealerDto.LogoRelativeUrl;
                dealer.Name = dealerDto.Name;
                dealer.Phone = dealerDto.Phone;
                dealer.PostalCode = dealerDto.PostalCode;
                dealer.State = dealerDto.State;
                dealer.WebSite = dealerDto.WebSite;
                dealer.LogoRelativeUrl = dealer.LogoRelativeUrl;
                DbContext.SaveChanges();
            }
            
            return GetDealerById(dealerDto.Id);
        }

        public PagingResult<DealerDto> SearchDealers(SearchDealerCommand command)
        {
            if (command == null)
                return PagingResult<DealerDto>.Empty();
            var predicate =
                Functions.Lambda<Model.Dealer, bool>(
                    c => (
                        (command.Name == null || c.Name.Contains(command.Name))
                        && (command.Address == null || c.Address.Contains(command.Address))
                        && (command.City == null || c.City.Contains(command.City))
                        && (command.State == null || c.State.Contains(command.State))
                        && (command.PostalCode == null || c.PostalCode.Contains(command.PostalCode))
                    ));

            var dealersQuery = DbContext.Dealers.Where(predicate).AsQueryable();

            if(!string.IsNullOrEmpty(command.OrderBy))
            {
                dealersQuery = dealersQuery.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = dealersQuery.Count();
            var dealers = dealersQuery.Paginate(command);

            return new PagingResult<DealerDto>(dealers.Select(DealerDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }
    }
}
