﻿using System.Collections.Generic;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Timing;

namespace DEA.CRM.Service.Core
{
    public class DefaultUserProfilePreferenceService : DefaultServiceBase, IUserProfilePreferenceService
    {
        private readonly IPickListService _pickListService;
        private readonly IUserInfoService _userInfoService;

        public DefaultUserProfilePreferenceService(
            CRMAppDbContext dbContext,
            IServiceFactory serviceFactory,
            IPickListService pickListService,
            IUserInfoService userInfoService) : base(dbContext, serviceFactory)
        {
            _pickListService = pickListService;
            _userInfoService = userInfoService;
        }

        public Result<UserProfilePreferenceFormDto> GetCurrentUserRequiredPreference(UserDetailDto currentUser)
        {
            if (currentUser == null)
                return Result<UserProfilePreferenceFormDto>.Fail("Expect a valid user info");

            var formDto = new UserProfilePreferenceFormDto
            {
                FormData = new UserProfilePreferenceDto
                {
                    UserKey = currentUser.UserKey,
                    TimeZone = currentUser.TimeZone,
                    Language = currentUser.Language,
                    DateFormat = currentUser.DateFormat,
                    DefaultCompanyId = currentUser.DefaultCompanyId,
                    DefaultDealerId = currentUser.DefaultDealerId,
                    DefaultShowroomId = currentUser.DefaultShowroomId
                },
                PickListValues = new Dictionary<string, IList<IPickListDto>>
                {
                    {"allCurrencyList", _pickListService.GetAllCurrenciesList() },
                    {"allTimeZoneList", _pickListService.GetAllTimeZoneList() },
                    {"allSupportedDateFormatList", _pickListService.GetAllSupportedDateFormatList() },
                    {"allLanguageList", _pickListService.GetAllLanguagesList() },
                    {"companies", _pickListService.GetCompaniesAsPickListForUser(currentUser) },
                    {"dealers", _pickListService.GetDealersAsPickListForUser(currentUser) },
                    {"showrooms", _pickListService.GetShowroomAsPickListForUser(currentUser) }
                }
            };

            return Result.Success(formDto);
        }

        public Result<UserDetailDto> SaveBasicProfilePreference(UserProfilePreferenceDto profilePreferenceDto, UserDetailDto currentUserDto)
        {
            if (profilePreferenceDto == null || currentUserDto == null || profilePreferenceDto.UserKey != currentUserDto.UserKey)
                return Result<UserDetailDto>.Fail("Invalid parameters");

            var userInfo = DbContext.UserInfoes.Find(profilePreferenceDto.UserKey);
            if (userInfo == null)
                return Result<UserDetailDto>.Fail($"User {profilePreferenceDto.UserKey} not found.");

            userInfo.TimeZone = profilePreferenceDto.TimeZone;
            userInfo.CurrencyCode = profilePreferenceDto.CurrencyCode;
            userInfo.Language = profilePreferenceDto.Language;
            userInfo.DateFormat = profilePreferenceDto.DateFormat;
            userInfo.DefaultCompanyId = profilePreferenceDto.DefaultCompanyId;
            userInfo.DefaultDealerId = profilePreferenceDto.DefaultDealerId;
            userInfo.DefaultShowroomId = profilePreferenceDto.DefaultShowroomId;
            userInfo.ModifiedDate = Clock.Now;
            userInfo.ModifiedByUserKey = currentUserDto.UserKey;
            userInfo.ModifiedByUserFullName = currentUserDto.FullName;

            DbContext.SaveChanges();

            var updatedUserDto = _userInfoService.GetFullUserDetailByUserKey(
                FindUserByKeyQuery.ActiveUserQuery(profilePreferenceDto.UserKey), refresh: true);

            return Result.Success(updatedUserDto);
        }
    }
}
