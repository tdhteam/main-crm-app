﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;

namespace DEA.CRM.Service.Core
{
    public class DefaultPermissionService : DefaultServiceBase, IPermissionService
    {
        public DefaultPermissionService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }
    }
}
