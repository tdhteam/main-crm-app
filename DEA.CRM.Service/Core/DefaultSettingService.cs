﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Memoization;
using DEA.CRM.Utils.Types;

namespace DEA.CRM.Service.Core
{
    public class DefaultSettingService : DefaultServiceBase, ISettingService
    {
        public static readonly string SettingGroupAll = "All";
        private static readonly string[] HiddenGroups =
        {
            "Debug",
            "System Scheduled Tasks"
        };

        public DefaultSettingService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        #region Caches

        private static readonly Func<CacheCommand<CRMAppDbContext>, Func<NonNullString, List<SettingDto>>>
            FindSettingsInGroupCache = Functions
                .Lambda((CRMAppDbContext ctx, NonNullString groupName) => ctx.Settings
                    .OrderBy(s => s.Name)
                    .Where(s => s.SettingGroup == groupName.Value || groupName.Value == SettingGroupAll)
                    .AsEnumerable()
                    .Select(SettingDto.EntityToDtoMapper)
                    .ToList())
                .MemoizeCtx();

        private static readonly Func<CacheCommand<DefaultSettingService>, Func<SettingIds, string>> GetValueCache =
            Functions
                .Lambda((DefaultSettingService ctx, SettingIds settingId) =>
                {
                    var setting = ctx.Get((int) settingId, false);
                    return setting?.SettingValue;
                })
                .MemoizeCtx();

        #endregion

        public List<string> Groups()
        {
            // Group setting key
            var groups = DbContext.Settings.Select(s => s.SettingGroup).Distinct().ToList();
            // Exclude special groups
            var result = new List<string> { SettingGroupAll };
            result.AddRange(groups.Where(groupName => !HiddenGroups.Contains(groupName)));

            return result;
        }

        public List<SettingDto> ByGroup(string groupName)
        {
            return FindSettingsInGroupCache(DbContext)(groupName).ToList();
        }

        public string GetValue(SettingIds setting)
        {
            return GetValueCache(this.AsCacheContext())(setting) ?? string.Empty;
        }

        public bool IsAffirmative(SettingIds setting)
        {
            var settingValue = GetValue(setting)?.ToUpper();
            var result = SettingHelper.IsAffirmative(settingValue);
            if (result == null) throw new ArgumentException("Unexpected setting value [" + settingValue + " for setting ID " + setting);
            return result == true;
        }

        public SettingDto UpdateSettingValue(UpdateSettingValueCommand command, UserDetailDto currentUser)
        {
            throw new NotImplementedException();
        }

        public void RefreshSettingCache(int settingId)
        {
            GetValueCache(this.AsCacheContext().DeleteKey())((SettingIds) settingId);
            FindSettingsInGroupCache(DbContext.AsCacheContext().Reset());
        }

        public SettingDto Get(int settingId, bool includeValueList)
        {
            var result = DbContext.Settings
                .Where(s => s.SettingId == settingId)
                .Select(SettingDto.EntityToDtoMapper)
                .FirstOrDefault();
            return result;
        }

        public List<SettingDto> Get(int[] settingIds)
        {
            var settingList = DbContext.Settings
                .Where(s => settingIds.Contains(s.SettingId))
                .Select(SettingDto.EntityToDtoMapper).ToList();

            return settingList;
        }

        public FileSystemAttachmentConfigurationDto GetLocalFileSystemAttachmentConfiguration()
        {
            var rootPath = GetValue(SettingIds.FileSystemAttachmentRootPath);
            return FileSystemAttachmentConfigurationDto.BasicConfiguration(rootPath);
        }

        public AzureStorageConfigurationDto GetAzureStorageConfiguration()
        {
            var azureSettings = Get(new[]
                {(int) SettingIds.AzureStorageConnectionString, (int) SettingIds.AzureBlobStorageRootContainerName});
            var connString = azureSettings.FirstOrDefault(x => x.SettingId == (int)SettingIds.AzureStorageConnectionString)?.SettingValue;
            var azureBlobRootContainerName = azureSettings.FirstOrDefault(x => x.SettingId == (int)SettingIds.AzureBlobStorageRootContainerName)?.SettingValue;
            return new AzureStorageConfigurationDto
            {
                AzureStorageConnectionString = connString,
                RootContainerName = !string.IsNullOrEmpty(azureBlobRootContainerName)
                    ? azureBlobRootContainerName
                    : "crm-attachment"
            };
        }

        public S3BucketConfigurationDto GetS3BucketConfiguration()
        {
            var s3Settings = Get(new[]{
                (int) SettingIds.S3RegionEndpointName
                , (int) SettingIds.S3RootBucketName
            });
            var regionEpName = s3Settings.FirstOrDefault(x => x.SettingId == (int)SettingIds.S3RegionEndpointName)?.SettingValue;
            var rootBucketName = s3Settings.FirstOrDefault(x => x.SettingId == (int)SettingIds.S3RootBucketName)?.SettingValue;
            return new S3BucketConfigurationDto
            {
                RegionEndpointName = regionEpName,
                RootBucketName = rootBucketName
            };
        }
    }
}
