﻿using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Core
{
    /// <summary>
    /// Business service to deal with Profile aka Permission Template for Role
    /// </summary>
    public class DefaultProfileService : DefaultServiceBase, IProfileService
    {
        public DefaultProfileService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        public List<ProfileDto> GetAll()
        {
            return DbContext.Profiles.Select(ProfileDto.EntityToDtoMapper).ToList();
        }

        public Result<ProfileDto> GetById(int id)
        {
            return DbContext.Profiles.Find(id)
                .AsResult($"Profile with id {id} not found.")
                .Bind(
                    entity => ProfileDto.EntityToDtoMapper(entity).AsResult("Cannot convert profile entity to dto")
                );
        }

        public Result<ProfileDto> CreateOrUpdate(CreateOrUpdateProfileCommand command)
        {
            if(command == null)
                return Result<ProfileDto>.Fail("Invalid profile creation request.");

            var profileId = 0;
            if (command.Id == 0)
            {
                // Add profile
                var profileToAdd = new Profile
                {
                    Name = command.Name,
                    Description = command.Description,
                    DirectlyRelatedToRole = command.DirectlyRelatedToRole
                };

                var profilePermissions = MakeStandardProfilePermissionListForProfile(command, profileToAdd);
                DbContext.ProfileToStandardPermissions.AddRange(profilePermissions);
                
                DbContext.SaveChanges();
                profileId = profileToAdd.Id;
            }
            else
            {
                // Edit profile
                var profile = DbContext.Profiles.Find(command.Id);
                if (profile != null)
                {
                    profile.Name = command.Name;
                    profile.Description = command.Description;
                    profile.DirectlyRelatedToRole = command.DirectlyRelatedToRole;

                    var existingProfilePermissions =
                        DbContext.ProfileToStandardPermissions.Where(x => x.ProfileId == command.Id);
                    DbContext.ProfileToStandardPermissions.RemoveRange(existingProfilePermissions);
                    DbContext.ProfileToStandardPermissions.AddRange(
                        MakeStandardProfilePermissionListForProfile(command, profile));
                    
                    DbContext.SaveChanges();
                    profileId = profile.Id;
                }
            }

            return GetById(profileId);
        }
        
        private static List<ProfileToStandardPermission> MakeStandardProfilePermissionListForProfile(CreateOrUpdateProfileCommand command, Profile profile)
        {
            return command.ModulePermissions
                .SelectMany(x => new List<ProfileToStandardPermission>
                {
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.View,
                        Permission = x.CanView
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Create,
                        Permission = x.CanCreate
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Edit,
                        Permission = x.CanEdit
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Delete,
                        Permission = x.CanDelete
                    }
                })
                .ToList();
        }

        public ResponseDto DeleteProfile(int id)
        {
            if(id < 1)
                return ResponseDto.OkNoContent();
            
            var profilePermissions = DbContext.ProfileToStandardPermissions.Where(x => x.ProfileId == id);
            DbContext.ProfileToStandardPermissions.RemoveRange(profilePermissions);

            var profileTabs = DbContext.ProfileToTabs.Where(x => x.ProfileId == id);
            DbContext.ProfileToTabs.RemoveRange(profileTabs);

            var profile = DbContext.Profiles.Find(id);
            if (profile != null)
                DbContext.Profiles.Remove(profile);

            DbContext.SaveChanges();
            
            return ResponseDto.OkNoContent();
        }
    }
}
