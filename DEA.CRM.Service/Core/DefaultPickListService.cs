﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.City;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Memoization;

namespace DEA.CRM.Service.Core
{
    public class DefaultPickListService : DefaultServiceBase, IPickListService
    {
        private readonly ICityService _cityService;
        private readonly ILeadSourceService _leadSourceService;
        
        public DefaultPickListService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            ICityService cityService, 
            ILeadSourceService leadSourceService) : base(dbContext, serviceFactory)
        {
            _cityService = cityService;
            _leadSourceService = leadSourceService;
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, Func<UserDetailDto, IList<IPickListDto>>>
            GetCompaniesAsPickListForUserCache =
                Functions.Lambda<DefaultPickListService, UserDetailDto, IList<IPickListDto>>(
                        (ctx, user) => ctx.GetCompaniesAsPickListForUserInternal(user)
                    )
                    .MemoizeCtx();

        public IList<IPickListDto> GetCompaniesAsPickListForUser(UserDetailDto user)
        {
            return GetCompaniesAsPickListForUserCache(this.AsCacheContext())(user);
        }
        
        private IList<IPickListDto> GetCompaniesAsPickListForUserInternal(UserDetailDto user)
        {
            if(user == null)
                return new List<IPickListDto>();
            
            var userCompanies = user.UserInfoToCompany.Select(x => x.CompanyId).ToHashSet();
            return DbContext.Companies
                .Where(x => userCompanies.Contains(x.Id) || user.IsAdmin)
                .OrderBy(x => x.Id)
                .Select(x => KeyValuePickListDto.Of(x.Id.ToString(), x.Name))
                .ToList();
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, Func<UserDetailDto, IList<IPickListDto>>>
            GetDealersAsPickListForUserCache =
                Functions.Lambda<DefaultPickListService, UserDetailDto, IList<IPickListDto>>(
                        (ctx, user) => ctx.GetDealersAsPickListForUserInternal(user)
                    )
                    .MemoizeCtx();
        
        private static readonly Func<CacheCommand<DefaultPickListService>,IList<IPickListDto>>
            GetDealersAsPickListCache =
                Functions.Lambda<DefaultPickListService, IList<IPickListDto>>(
                        (ctx) => ctx.GetDealersAsPickListInternal()
                    )
                    .MemoizeCtx();
        
        public IList<IPickListDto> GetDealersAsPickList()
        {
            return GetDealersAsPickListCache(this.AsCacheContext());
        }
        
        public IList<IPickListDto> GetDealersAsPickListForUser(UserDetailDto user)
        {
            return GetDealersAsPickListForUserCache(this.AsCacheContext())(user);
        }
        
        private IList<IPickListDto> GetDealersAsPickListForUserInternal(UserDetailDto user)
        {
            if(user == null)
                return new List<IPickListDto>();
            
            var userDealers = user.UserInfoToDealer.Select(x => x.DealerId).ToHashSet();
            return DbContext.Dealers
                .Where(x => userDealers.Contains(x.Id)|| user.IsAdmin)
                .Select(x => KeyValuePickListDto.Of(x.Id.ToString(), x.Name, x.CompanyId.ToString()))
                .ToList();
        }
        
        private IList<IPickListDto> GetDealersAsPickListInternal()
        {
            return DbContext.Dealers
                .Select(x => KeyValuePickListDto.Of(x.Id.ToString(), x.Name, x.CompanyId.ToString()))
                .ToList();
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, Func<UserDetailDto, IList<IPickListDto>>>
            GetShowroomAsPickListForUserCache =
                Functions.Lambda<DefaultPickListService, UserDetailDto, IList<IPickListDto>>(
                        (ctx, user) => ctx.GetShowroomAsPickListForUserInternal(user)
                    )
                    .MemoizeCtx();

        public IList<IPickListDto> GetShowroomAsPickListForUser(UserDetailDto user)
        {
            return GetShowroomAsPickListForUserCache(this.AsCacheContext())(user);
        }
        
        private IList<IPickListDto> GetShowroomAsPickListForUserInternal(UserDetailDto user)
        {
            if(user == null)
                return new List<IPickListDto>();
            
            var userShowrooms = user.UserInfoToShowroom.Select(x => x.ShowroomId).ToHashSet();
            return DbContext.Showrooms
                .Where(x => userShowrooms.Contains(x.Id)|| user.IsAdmin)
                .Select(x => KeyValuePickListDto.Of(x.Id.ToString(), x.Name, x.DealerId.ToString()))
                .ToList();
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, IList<ActivityPriorityDto>>
            GetActivityPriorityListCache =
                Functions.Lambda<DefaultPickListService, IList<ActivityPriorityDto>>(
                        ctx => ctx.GetActivityPriorityListInternal()
                    )
                    .MemoizeCtx();

        public IList<ActivityPriorityDto> GetActivityPriorityList()
        {
            return GetActivityPriorityListCache(this.AsCacheContext());
        }

        public IList<ActivityPriorityDto> GetActivityPriorityListInternal()
        {
            return DbContext.ActivityPriorities
                .Where(x => x.Presence)
                .OrderBy(x => x.SortOrderId)
                .Select(ActivityPriorityDto.EntityToDtoMapper)
                .ToList();
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, IList<ActivityStatusDto>>
            GetActivityStatusListCache =
                Functions.Lambda<DefaultPickListService, IList<ActivityStatusDto>>(
                        ctx => ctx.GetActivityStatusListInternal()
                    )
                    .MemoizeCtx();

        public IList<ActivityStatusDto> GetActivityStatusList()
        {
            return GetActivityStatusListCache(this.AsCacheContext());
        }
        
        private IList<ActivityStatusDto> GetActivityStatusListInternal()
        {
            return DbContext.ActivityStatuses
                .Where(x => x.Presence)
                .OrderBy(x => x.SortOrderId)
                .Select(ActivityStatusDto.EntityToDtoMapper)
                .ToList();
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, IList<EventStatusDto>>
            GetEventStatusListCache =
                Functions.Lambda<DefaultPickListService, IList<EventStatusDto>>(
                        ctx => ctx.GetEventStatusListInternal()
                    )
                    .MemoizeCtx();

        public IList<EventStatusDto> GetEventStatusList()
        {
            return GetEventStatusListCache(this.AsCacheContext());
        }
        
        private IList<EventStatusDto> GetEventStatusListInternal()
        {
            return DbContext.EventStatuses
                .Where(x => x.Presence)
                .OrderBy(x => x.SortOrderId)
                .Select(EventStatusDto.EntityToDtoMapper)
                .ToList();
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, IList<CalendarEventTypeDto>>
            GetEventTypeListCache =
                Functions.Lambda<DefaultPickListService, IList<CalendarEventTypeDto>>(
                        ctx => ctx.GetEventTypeListInternal()
                    )
                    .MemoizeCtx();

        public IList<CalendarEventTypeDto> GetEventTypeList()
        {
            return GetEventTypeListCache(this.AsCacheContext());
        }
        
        private IList<CalendarEventTypeDto> GetEventTypeListInternal()
        {
            return DbContext.CalendarEventTypes
                .Where(x => x.Presence)
                .OrderBy(x => x.SortOrderId)
                .Select(CalendarEventTypeDto.EntityToDtoMapper)
                .ToList();
        }

        private IList<IPickListDto> GetAllCurrenciesListInternal()
        {
            return DbContext.Currencies.Select(x => KeyValuePickListDto.Of(x.CurrencyCode, x.Description)).ToList();
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, IList<IPickListDto>>
            GetAllCurrenciesListCache =
                Functions.Lambda<DefaultPickListService, IList<IPickListDto>>(
                        ctx => ctx.GetAllCurrenciesListInternal()
                    )
                    .MemoizeCtx();

        public IList<IPickListDto> GetAllCurrenciesList()
        {
            return GetAllCurrenciesListCache(this.AsCacheContext());
        }

        public IList<IPickListDto> GetAllLanguagesList()
        {
            return new List<IPickListDto>
            {
                KeyValuePickListDto.Of("en", "English"),
                KeyValuePickListDto.Of("vi", "Vietnamese")
            };
        }
        
        public IList<IPickListDto> GetAllCities()
        {
            return GetAllCitiesCache(this.AsCacheContext());
        }

        private IList<IPickListDto> GetAllCitiesInternal()
        {
            var allCities = _cityService.GetAllCities().Select(x => KeyValuePickListDto.Of(x.Value, x.Name)).ToList();
            return allCities;
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, IList<IPickListDto>>
            GetAllCitiesCache =
                Functions.Lambda<DefaultPickListService, IList<IPickListDto>>(
                        ctx => ctx.GetAllCitiesInternal()
                    )
                    .MemoizeCtx();

        public IList<IPickListDto> GetAllDistricts(IList<IPickListDto> allCities)
        {
            return GetAllDistrictsCache(this.AsCacheContext());
        }
        
        private IList<IPickListDto> GetAllDistrictsInternal()
        {
            var allCities = GetAllCities();
            var allDistricts = new List<IPickListDto>();
            foreach (var citi in allCities)
            {
                var districts = _cityService.GetDistrictsByCity(citi.Key).Select(d => KeyValuePickListDto.Of(d.Value, d.Name, citi.Key));
                allDistricts.AddRange(districts);
            }

            return allDistricts;
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, IList<IPickListDto>>
            GetAllDistrictsCache =
                Functions.Lambda<DefaultPickListService, IList<IPickListDto>>(
                        ctx => ctx.GetAllDistrictsInternal()
                    )
                    .MemoizeCtx();

        private IList<IPickListDto> GetAllTimeZoneListInternal()
        {
            return TimeZoneInfo.GetSystemTimeZones()
                .Select(tz => KeyValuePickListDto.Of(tz.Id, tz.DisplayName))
                .ToList();
        }

        private static readonly Func<CacheCommand<DefaultPickListService>, IList<IPickListDto>> GetAllTimeZoneListCache
            = Functions.Lambda<DefaultPickListService, IList<IPickListDto>>(
                    ctx => ctx.GetAllTimeZoneListInternal()
                )
                .MemoizeCtx();

        public IList<IPickListDto> GetAllTimeZoneList()
        {
            return GetAllTimeZoneListCache(this.AsCacheContext());
        }

        public IList<IPickListDto> GetAllSupportedDateFormatList()
        {
            return new List<IPickListDto>
            {
                KeyValuePickListDto.Of("1", "dd-MM-yyyy"),
                KeyValuePickListDto.Of("2", "MM-dd-yyyy"),
                KeyValuePickListDto.Of("3", "yyyy-MM-dd")
            };
        }

        public IList<IPickListDto> GetSalutationList()
        {
            return new List<IPickListDto>
            {
                KeyValuePickListDto.Of("None", "None"),
                KeyValuePickListDto.Of("Mr.", "Mr."),
                KeyValuePickListDto.Of("Ms.", "Ms."),
                KeyValuePickListDto.Of("Mrs.", "Mrs."),
                KeyValuePickListDto.Of("Dr.", "Dr."),
                KeyValuePickListDto.Of("Prof.", "Prof."),
            };
        }
        
        private IList<LeadSourceDto> GetLeadSourcesInternal()
        {
            return _leadSourceService.GetAllLeadSources().ResultSet.ToList();
        }
        
        private static readonly Func<CacheCommand<DefaultPickListService>, IList<LeadSourceDto>> GetLeadSourcesCache
            = Functions.Lambda<DefaultPickListService, IList<LeadSourceDto>>(
                    ctx => ctx.GetLeadSourcesInternal()
                )
                .MemoizeCtx();

        public IList<LeadSourceDto> GetLeadSources()
        {
            return GetLeadSourcesCache(this.AsCacheContext());
        }

        public IList<IPickListDto> GetCarMakeList()
        {
            return new List<IPickListDto>
            {
                KeyValuePickListDto.Of("Alfa Romeo", "Alfa Romeo"),
                KeyValuePickListDto.Of("Audi", "Audi"),
                KeyValuePickListDto.Of("BMW", "BMW"),
                KeyValuePickListDto.Of("Chevrolet", "Chevrolet"),
                KeyValuePickListDto.Of("Chrysler", "Chrysler"),
                KeyValuePickListDto.Of("Citroën", "Citroën"),
                KeyValuePickListDto.Of("Dacia", "Dacia"),
                KeyValuePickListDto.Of("Daewoo", "Daewoo"),
                KeyValuePickListDto.Of("Dodge", "Dodge"),
                KeyValuePickListDto.Of("Fiat", "Fiat"),
                KeyValuePickListDto.Of("Ford", "Ford"),
                KeyValuePickListDto.Of("Honda", "Honda"),
                KeyValuePickListDto.Of("Hummer", "Hummer"),
                KeyValuePickListDto.Of("Hyundai", "Hyundai"),
                KeyValuePickListDto.Of("Infiniti", "Infiniti"),
                KeyValuePickListDto.Of("Jaguar", "Jaguar"),
                KeyValuePickListDto.Of("Jeep", "Jeep"),
                KeyValuePickListDto.Of("Kia", "Kia"),
                KeyValuePickListDto.Of("Land Rover", "Land Rover"),
                KeyValuePickListDto.Of("Lexus", "Lexus"),
                KeyValuePickListDto.Of("MINI", "MINI"),
                KeyValuePickListDto.Of("Mazda", "Mazda"),
                KeyValuePickListDto.Of("Mercedes-Benz", "Mercedes-Benz"),
                KeyValuePickListDto.Of("Mitsubishi", "Mitsubishi"),
                KeyValuePickListDto.Of("Nissan", "Nissan"),
                KeyValuePickListDto.Of("Opel", "Opel"),
                KeyValuePickListDto.Of("Peugeot", "Peugeot"),
                KeyValuePickListDto.Of("Porsche", "Porsche"),
                KeyValuePickListDto.Of("Renault", "Renault"),
                KeyValuePickListDto.Of("Rover", "Rover"),
                KeyValuePickListDto.Of("Saab", "Saab"),
                KeyValuePickListDto.Of("Seat", "Seat"),
                KeyValuePickListDto.Of("Smart", "Smart"),
                KeyValuePickListDto.Of("Subaru", "Subaru"),
                KeyValuePickListDto.Of("Suzuki", "Suzuki"),
                KeyValuePickListDto.Of("Toyota", "Toyota"),
                KeyValuePickListDto.Of("Volkswagen", "Volkswagen"),
                KeyValuePickListDto.Of("Volvo", "Volvo"),
                KeyValuePickListDto.Of("Škoda", "Škoda")
            };
        }
    }
}