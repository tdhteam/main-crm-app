﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DEA.CRM.Authentication.Models;
using DEA.CRM.EfModel;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Memoization;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using F = DEA.CRM.Utils.Functions;

namespace DEA.CRM.Service.Core
{
    public class DefaultUserInfoService : DefaultServiceBase, IUserInfoService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRoleService _roleService;
        private readonly ITabService _tabService;
        private readonly IPickListService _pickListService;
        private readonly ISettingService _settingService;

        public DefaultUserInfoService(
            CRMAppDbContext dbContext,
            IServiceFactory serviceFactory,
            UserManager<ApplicationUser> userManager,
            IRoleService roleService,
            ITabService tabService, IPickListService pickListService, 
            ISettingService settingService) : base(dbContext, serviceFactory)
        {
            _roleService = roleService;
            _userManager = userManager;
            _tabService = tabService;
            _pickListService = pickListService;
            _settingService = settingService;
        }

        public PagingResult<UserDetailSearchResultDto> SearchUsers(SearchUserQuery query)
        {
            if (query == null)
                return PagingResult<UserDetailSearchResultDto>.Empty();

            var filterFunc =
                F.Lambda<UserInfoSearch, bool>(
                    x => 
                        (string.IsNullOrEmpty(query.FirstName) || x.FirstName.Contains(query.FirstName))
                        && (string.IsNullOrEmpty(query.LastName) || x.LastName.Contains(query.LastName))
                        && (string.IsNullOrEmpty(query.Email) || x.Email.Contains(query.Email))
                        && (string.IsNullOrEmpty(query.RoleName) || x.RoleName.Contains(query.RoleName))
                        && (query.IsAdmin == null || x.IsAdmin == query.IsAdmin)
                        && (string.IsNullOrEmpty(query.Status) || x.Status.Contains(query.Status))
                    );

            var linqQuery = DbContext.UserInfoSearchs.Where(filterFunc).AsQueryable();

            if (!string.IsNullOrEmpty(query.OrderBy))
            {
                linqQuery = linqQuery.SetOrderByField(query.OrderAsc, query.OrderBy);
            }

            var totalCount = linqQuery.Count();
            var contacts = linqQuery.Paginate(query);

            return new PagingResult<UserDetailSearchResultDto>(contacts
                .Select(UserDetailSearchResultDto.SearchEntityToDtoMapper)
                .ToList())
            {
                PageSize = query.PageSize,
                PageNumber = query.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, query.PageSize),
                ResultCount = totalCount
            };
        }

        /// <summary>
        /// Get list of all users that current user can view
        /// </summary>
        /// <param name="currentUser">Current logged in user</param>
        /// <returns></returns>
        public Result<List<UserDetailSearchResultDto>> FindAllUsersForAssignedUser(UserDetailDto currentUser)
        {
            return FindAllUsersForAssignedUserCache(this.AsCacheContext())(currentUser);
        }

        private static readonly Func<CacheCommand<DefaultUserInfoService>,
                Func<UserDetailDto, Result<List<UserDetailSearchResultDto>>>>
            FindAllUsersForAssignedUserCache 
            = F.Lambda(
                    (DefaultUserInfoService ctx, UserDetailDto userDetailDto) 
                        => ctx.FindAllUsersForAssignedUserInternal(userDetailDto)
            )
            .MemoizeCtx();

        private Result<List<UserDetailSearchResultDto>> FindAllUsersForAssignedUserInternal(UserDetailDto currentUser)
        {
            if(currentUser == null)
                return Result.Success(new List<UserDetailSearchResultDto>());

            if (currentUser.IsAdmin)
                return Result.Success(DbContext.UserInfoSearchs
                    .Select(UserDetailSearchResultDto.SearchEntityToDtoMapper)
                    .ToList());

            var companyAccessList = currentUser.UserInfoToCompany.Select(x => x.CompanyId).ToList();
            var dealerAccessList = currentUser.UserInfoToDealer.Select(x => x.DealerId).ToList();
            var showroomAccessList = currentUser.UserInfoToShowroom.Select(x => x.ShowroomId).ToList();

            var userKeysOfUserHaveSameAccessLevelQuery =
                DbContext.UserInfoToCompanies.Where(x => companyAccessList.Contains(x.CompanyId))
                    .Select(x => x.UserKey)
                    .Union(
                        DbContext.UserInfoToDealers.Where(x => dealerAccessList.Contains(x.DealerId))
                            .Select(x => x.UserKey)
                    )
                    .Union(
                        DbContext.UserInfoToShowrooms.Where(x => showroomAccessList.Contains(x.ShowroomId))
                            .Select(x => x.UserKey)
                    );

            var userKeysOfUserHaveSameAccessLevel = userKeysOfUserHaveSameAccessLevelQuery.Distinct().ToList();

            return Result.Success(
                DbContext.UserInfoSearchs.Where(x => userKeysOfUserHaveSameAccessLevel.Contains(x.UserKey))
                    .Select(UserDetailSearchResultDto.SearchEntityToDtoMapper)
                    .ToList());
        }

        public UserDetailDto GetDefaultForNewUser()
        {
            var company = DbContext.Companies.OrderBy(x => x.Id).FirstOrDefault();
            return new UserDetailDto
            {
                CalendarColor = "#FF00EE",
                DateFormat = "dd/MM/yyyy",
                HourFotmat = "hh:mm",
                Language = "en",
                TimeZone = "UTC",
                AccessAllCompanies = false,
                AccessAllDealers = false,
                AccessAllShowrooms = false,
                CurrencyId = 0,
                CurrencyGroupingPattern = "123,456,789",
                CurrencyDecimalSeparator = ",",
                CurrencyGroupingSeparator = ",",
                CurrencySymbolPlacement = "1.0 $",
                NumberOfCurrencyDecimal = 2,
                DefaultCompanyId = company?.Id
            };
        }

        public Dictionary<string, IList<IPickListDto>> GetPickListsForUserDetailForm(UserDetailDto currentUserDto)
        {
            var allRoles = _roleService.GetAllRoles().ResultSet
                .Where(x => x.Depth > 0)
                .OrderBy(x => x.Depth)
                .Select(x => KeyValuePickListDto.Of(x.RoleId, x.Name))
                .ToList();
            var allCurrencies = _pickListService.GetAllCurrenciesList();
            var allLanguages = _pickListService.GetAllLanguagesList();
            var allCompanies = _pickListService.GetCompaniesAsPickListForUser(currentUserDto);
            var allDealers = _pickListService.GetDealersAsPickListForUser(currentUserDto);
            var allShowrooms = _pickListService.GetShowroomAsPickListForUser(currentUserDto);
            return new Dictionary<string, IList<IPickListDto>>
            {
                {"Roles", allRoles},
                {"Currencies", allCurrencies},
                {"Languages", allLanguages},
                {"Companies", allCompanies},
                {"Dealers", allDealers},
                {"Showrooms", allShowrooms}
            };
        }

        public async Task<UserDetailDto> CreateOrUpdateAsync(UpdateUserCommand command, UserDetailDto currentUserDto)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (currentUserDto == null)
                throw new ArgumentNullException(nameof(currentUserDto));

            if (string.IsNullOrEmpty(command.UserKey))
            {
                var appUser = new ApplicationUser
                {
                    UserName = command.UserName,
                    LockoutEnabled = false,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true
                };

                var identityUser = await _userManager.CreateAsync(appUser, command.Password);

                if (identityUser == null || !identityUser.Succeeded)
                {
                    throw new BusinessException($"Failed to create user {command.UserName}");
                }

                try
                {
                    await _userManager.SetLockoutEnabledAsync(appUser, false);
                    var userInfoEntityToAdd = UserDetailDto.DtoToEntityMapper(command);

                    command.UserKey = appUser.Id;
                    userInfoEntityToAdd.UserKey = appUser.Id;
                    userInfoEntityToAdd.UserName = appUser.UserName;
                    userInfoEntityToAdd.UserRole = new UserInfoToRole
                    {
                        UserKey = appUser.Id,
                        RoleId = command.RoleId
                    };

                    if (userInfoEntityToAdd.UserInfoToCompany?.Count > 0)
                    {
                        userInfoEntityToAdd.UserInfoToCompany.ToList().ForEach(
                            x => x.UserKey = appUser.Id
                        );
                    }

                    if (userInfoEntityToAdd.UserInfoToDealer?.Count > 0)
                    {
                        userInfoEntityToAdd.UserInfoToDealer.ToList().ForEach(
                            x => x.UserKey = appUser.Id
                        );
                    }

                    if (userInfoEntityToAdd.UserInfoToShowroom?.Count > 0)
                    {
                        userInfoEntityToAdd.UserInfoToShowroom.ToList().ForEach(
                            x => x.UserKey = appUser.Id
                        );
                    }

                    userInfoEntityToAdd.Status = GlobalConstants.StatusActive;
                    userInfoEntityToAdd.Deleted = false;
                    userInfoEntityToAdd.CreatedDate = DateTime.UtcNow;
                    userInfoEntityToAdd.ModifiedDate = DateTime.UtcNow;
                    userInfoEntityToAdd.ModifiedByUserKey = currentUserDto.UserKey;
                    userInfoEntityToAdd.ModifiedByUserFullName = currentUserDto.FullName;
                    DbContext.UserInfoes.Add(userInfoEntityToAdd);

                    DbContext.SaveChanges();
                }
                catch (Exception)
                {
                    await _userManager.DeleteAsync(appUser);
                    throw;
                }
            }
            else
            {
                var userInfoEntity = DbContext.UserInfoes.Find(command.UserKey);

                if (userInfoEntity != null)
                {
                    userInfoEntity = UserDetailDto.DtoToEntityMapper(command);
                    userInfoEntity.ModifiedDate = DateTime.UtcNow;
                    userInfoEntity.ModifiedByUserKey = currentUserDto.UserKey;
                    userInfoEntity.ModifiedByUserFullName = currentUserDto.FullName;

                    DbContext.SaveChanges();
                }
            }

            // Refresh user
            return GetFullUserDetailByUserKey(FindUserByKeyQuery.ActiveUserQuery(command.UserKey), true);
        }

        private static readonly Func<CacheCommand<DefaultUserInfoService>, Func<FindUserByNameQuery, UserDetailDto>> GetUserDetailByUserNameCache = F
            .Lambda((DefaultUserInfoService ctx, FindUserByNameQuery cmd) => ctx.GetFullUserDetailInternal(cmd))
            .MemoizeCtx();

        private static readonly Func<CacheCommand<DefaultUserInfoService>, Func<FindUserByKeyQuery, UserDetailDto>> GetUserDetailByUserKeyCache = F
            .Lambda((DefaultUserInfoService ctx, FindUserByKeyQuery cmd) => ctx.GetFullUserDetailInternal(cmd))
            .MemoizeCtx();

        private UserDetailDto GetFullUserDetailInternal(FindUserByNameQuery cmd)
        {
            var userStatus = cmd.IsActive ? GlobalConstants.StatusActive : GlobalConstants.StatusInActive;
            var userInfoEntity = DbContext.UserInfoes
                .Include(u => u.UserInfoToCompany).ThenInclude(c => c.Company)
                .Include(u => u.UserInfoToDealer).ThenInclude(c => c.Dealer)
                .Include(u => u.UserInfoToShowroom).ThenInclude(c => c.Showroom)
                .Include(u => u.UserRole)
                .Include(u => u.UserDashboard)
                .FirstOrDefault(u => u.UserName == cmd.UserName && !u.Deleted && u.Status == userStatus)
                .AsResult($"User with name {cmd.UserName} does not exist.");

            return TransformUserEntityToUserDetailDto(userInfoEntity.Value);
        }

        private UserDetailDto TransformUserEntityToUserDetailDto(UserInfo userInfo)
        {
            var userDto = UserDetailDto.EntityToDtoMapper(userInfo);
            if (!string.IsNullOrEmpty(userDto.AvatarRelativeUrl))
            {
                var apiBaseUrl = _settingService.GetValue(SettingIds.AppApiBaseUrl);
                userDto.AvatarRelativeUrl = $"{apiBaseUrl}{userDto.AvatarRelativeUrl}";
            }
            return userDto;
        }

        private UserDetailDto GetFullUserDetailInternal(FindUserByKeyQuery cmd)
        {
            var userStatus = cmd.IsActive ? GlobalConstants.StatusActive : GlobalConstants.StatusInActive;
            var userInfoEntity = DbContext.UserInfoes
                .Include(u => u.UserInfoToCompany).ThenInclude(c => c.Company)
                .Include(u => u.UserInfoToDealer).ThenInclude(c => c.Dealer)
                .Include(u => u.UserInfoToShowroom).ThenInclude(c => c.Showroom)
                .Include(u => u.UserRole)
                .Include(u => u.UserDashboard)
                .FirstOrDefault(u => u.UserKey == cmd.UserKey && !u.Deleted && u.Status == userStatus)
                .AsResult($"User with key {cmd.UserKey} does not exist.");

            return TransformUserEntityToUserDetailDto(userInfoEntity.Value);
        }

        public UserDetailDto GetFullUserDetailByUserName(FindUserByNameQuery query, bool refresh)
        {
            var getCommand = refresh ? this.AsCacheContext().RefreshKey() : this.AsCacheContext();
            var userDetailDto = GetUserDetailByUserNameCache(getCommand)(query);
            return PopulateUserPermission(userDetailDto);
        }

        public UserDetailDto GetFullUserDetailByUserKey(FindUserByKeyQuery query, bool refresh)
        {
            var getCommand = refresh ? this.AsCacheContext().RefreshKey() : this.AsCacheContext();
            var userDetailDto = GetUserDetailByUserKeyCache(getCommand)(query);
            return PopulateUserPermission(userDetailDto);
        }

        private UserDetailDto PopulateUserPermission(UserDetailDto userDetailDto)
        {
            // TODO add data caching for these queries

            // Transform from role - profile - permission to flat user permission
            var listProfileIds = (from ur in DbContext.UserInfoToRoles
                                  join r in DbContext.Roles on ur.RoleId equals r.RoleId
                                  join rp in DbContext.RoleToProfiles on r.RoleId equals rp.RoleId
                                  where ur.UserKey == userDetailDto.UserKey
                                  select rp.ProfileId).ToHashSet();

            var roleProfilePermissions = (from rp in DbContext.RoleToProfiles
                                          join p in DbContext.Profiles on rp.ProfileId equals p.Id
                                          join pp in DbContext.ProfileToStandardPermissions on p.Id equals pp.ProfileId
                                          join t in DbContext.Tabs on pp.TabId equals t.Id
                                          where listProfileIds.Contains(pp.ProfileId)
                                          orderby pp.TabId, pp.Operation
                                          select new InternalRoleProfilePermission
                                          {
                                              RoleId = rp.RoleId,
                                              ProfileId = p.Id,
                                              ModuleId = pp.TabId,
                                              ModuleName = t.Name,
                                              Operation = pp.Operation,
                                              Permission = pp.Permission
                                          })
                .ToList();

            var allModules = _tabService.GetAll().ToDictionary(t => t.Id, t => t);

            var extractStandardPermission = F.Lambda<List<InternalRoleProfilePermission>, PermissionOperation, bool>(
                (rolePermList, operation) =>
                {
                    var perm = rolePermList.FirstOrDefault(x => x.Operation == (int)operation);
                    return perm?.Permission ?? false;
                }
            );

            userDetailDto.StandardModulePermissions =
                allModules.Values.Select(m =>
                    {
                        var roleProfilePermissionsPerModule = roleProfilePermissions.Where(x => x.ModuleId == m.Id).ToList();
                        return new ModulePermissionDto
                        {
                            ModuleId = m.Id,
                            ModuleName = m.Name,
                            CanView = extractStandardPermission(roleProfilePermissionsPerModule, PermissionOperation.View),
                            CanCreate = extractStandardPermission(roleProfilePermissionsPerModule, PermissionOperation.Create),
                            CanEdit = extractStandardPermission(roleProfilePermissionsPerModule, PermissionOperation.Edit),
                            CanDelete = extractStandardPermission(roleProfilePermissionsPerModule, PermissionOperation.Delete)
                        };
                    })
                    .ToDictionary(k => k.ModuleName, item => item);

            userDetailDto.CustomPermissions =
                (from pp in DbContext.ProfileToCustomPermissions
                 join p in DbContext.Permissions
                 on pp.PermissionId equals p.Id
                 where listProfileIds.Contains(pp.ProfileId)
                 orderby p.DisplayOrder
                 select PermissionDto.EntityToDtoMapper(p)
                ).ToList();

            return userDetailDto;
        }

        internal class InternalRoleProfilePermission
        {
            public string RoleId { get; set; }
            public int ProfileId { get; set; }
            public int ModuleId { get; set; }
            public string ModuleName { get; set; }
            public int Operation { get; set; }
            public bool Permission { get; set; }
        }

        private static readonly Func<CacheCommand<DefaultUserInfoService>, Func<FindUserByNameQuery, BasicUserDetailDto>> GetBasicUserDetailByUserNameCache = F
            .Lambda((DefaultUserInfoService ctx, FindUserByNameQuery cmd) => ctx.GetBasicUserDetailInternal(cmd))
            .MemoizeCtx();

        private static readonly Func<CacheCommand<DefaultUserInfoService>, Func<FindUserByKeyQuery, BasicUserDetailDto>> GetBasicUserDetailByUserKeyCache = F
            .Lambda((DefaultUserInfoService ctx, FindUserByKeyQuery cmd) => ctx.GetBasicUserDetailInternal(cmd))
            .MemoizeCtx();

        private BasicUserDetailDto GetBasicUserDetailInternal(FindUserByNameQuery cmd)
        {
            var userStatus = cmd.IsActive ? GlobalConstants.StatusActive : GlobalConstants.StatusInActive;
            var userInfoEntity = DbContext.UserInfoes
                .FirstOrDefault(u => u.UserName == cmd.UserName && !u.Deleted && u.Status == userStatus)
                .AsResult($"User {cmd.UserName} does not exist.");

            return BasicUserDetailDto.BasicEntityToDtoMapper(userInfoEntity.Value);
        }

        private BasicUserDetailDto GetBasicUserDetailInternal(FindUserByKeyQuery cmd)
        {
            var userStatus = cmd.IsActive ? GlobalConstants.StatusActive : GlobalConstants.StatusInActive;
            var userInfoEntity = DbContext.UserInfoes
                .FirstOrDefault(u => u.UserName == cmd.UserKey && !u.Deleted && u.Status == userStatus)
                .AsResult($"User {cmd.UserKey} does not exist.");

            return BasicUserDetailDto.BasicEntityToDtoMapper(userInfoEntity.Value);
        }

        public BasicUserDetailDto GetBasicUserDetailByUserName(FindUserByNameQuery query, bool refresh)
        {
            var getCommand = refresh ? this.AsCacheContext().RefreshKey() : this.AsCacheContext();
            return GetBasicUserDetailByUserNameCache(getCommand)(query);
        }

        public BasicUserDetailDto GetBasicUserDetailByUserKey(FindUserByKeyQuery query, bool refresh)
        {
            var getCommand = refresh ? this.AsCacheContext().RefreshKey() : this.AsCacheContext();
            return GetBasicUserDetailByUserKeyCache(getCommand)(query);
        }

        public Result<ResponseDto> DeactivateUser(DeactivateUserCommand command)
        {
            throw new NotImplementedException();
        }

        public string GetUserFullName(string userKey)
        {
            var userLookup = GetAllUsersAsDictionaryCache(this.AsCacheContext());
            return userLookup[userKey].FirstOrDefault()?.FullName;
        }

        private static readonly Func<CacheCommand<DefaultUserInfoService>, ILookup<string, UserDetailSearchResultDto>>
            GetAllUsersAsDictionaryCache =
                F.Lambda((DefaultUserInfoService ctx) => ctx.GetAllUsersAsDictionary()).MemoizeCtx();

        private ILookup<string, UserDetailSearchResultDto> GetAllUsersAsDictionary()
        {
            return DbContext.UserInfoSearchs.Select(UserDetailSearchResultDto.SearchEntityToDtoMapper)
                .ToLookup(k => k.UserKey, user => user);
        }
    }
}
