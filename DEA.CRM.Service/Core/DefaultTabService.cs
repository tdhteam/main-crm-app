﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Memoization;

namespace DEA.CRM.Service.Core
{
    public class DefaultTabService : DefaultServiceBase, ITabService
    {
        public DefaultTabService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        private static readonly Func<CacheCommand<DefaultTabService>, List<TabInfoDto>> GetAllCache =
            Functions.Lambda((DefaultTabService ctx) => ctx.GetAllInternal()).MemoizeCtx();

        private List<TabInfoDto> GetAllInternal()
        {
            return DbContext.Tabs.Select(TabInfoDto.EntityToDtoMapper).ToList();
        }
        
        public List<TabInfoDto> GetAll()
        {
            return GetAllCache(this.AsCacheContext());
        }
    }
}
