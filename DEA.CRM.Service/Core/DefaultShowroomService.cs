﻿using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Core
{
    public class DefaultShowroomService : DefaultServiceBase, IShowroomService
    {
        private readonly IPickListService _pickListService;
        
        public DefaultShowroomService(
            CRMAppDbContext dbContext, 
            IPickListService pickListService,
            IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
            _pickListService = pickListService;
        }

        public ShowroomDetailFormDto GetDefaultShowroomForNew()
        {
            var dealerList = _pickListService.GetDealersAsPickList();
            var allCities = _pickListService.GetAllCities();
            var allDistricts = _pickListService.GetAllDistricts(allCities);
            return new ShowroomDetailFormDto
            {
                FormData = new ShowroomDto(),
                PickListValues = new Dictionary<string, IList<IPickListDto>>
                {
                    {"dealers", dealerList },
                    {"cities", allCities },
                    {"districts", allDistricts }
                }
            };
        }

        public ShowroomDetailFormDto GetShowroomById(int id)
        {
            var showroom = DbContext.Showrooms.Find(id).AsResult($"Cannot find showroom with id {id}");
            var showroomDto = ShowroomDto.EntityToDtoMapper(showroom.Value);

            var dealer = DbContext.Dealers.Find(showroom.Value.DealerId);
            if (dealer != null)
            {
                showroomDto.DealerName = dealer.Name;
            }

            var dealerList = _pickListService.GetDealersAsPickList();
            var allCities = _pickListService.GetAllCities();
            var allDistricts = _pickListService.GetAllDistricts(allCities);
            
            var result = new ShowroomDetailFormDto()
            {
                FormData = showroomDto,
                PickListValues = new Dictionary<string, IList<IPickListDto>>
                {
                    {"dealers", dealerList },
                    {"cities", allCities },
                    {"districts", allDistricts }
                },
                
            };
            
            return result;
        }

        public ShowroomDetailFormDto CreateOrUpdate(ShowroomDto showroomDto)
        {
            if (showroomDto.Id <= 0) // new
            {
                var entity = ShowroomDto.DtoToEntityMapper(showroomDto);
                DbContext.Showrooms.Add(entity);
                DbContext.SaveChanges();
                showroomDto.Id = entity.Id;
            }
            else
            {
                //update
                var showroom = DbContext.Showrooms.Find(showroomDto.Id);
                if (showroom == null)
                    throw new EntityNotFoundException("I18N.Company.SHOWROOM_WITH_ID_IS_NOT_FOUND");
                
                showroom.Address = showroomDto.Address;
                showroom.City = showroomDto.City;
                showroom.Country = showroomDto.Country;
                showroom.DealerId = showroomDto.DealerId;
                showroom.Fax = showroomDto.Fax;
                showroom.Name = showroomDto.Name;
                showroom.Phone = showroomDto.Phone;
                showroom.PostalCode = showroomDto.PostalCode;
                showroom.State = showroomDto.State;
                DbContext.SaveChanges();
            }
            
            return GetShowroomById(showroomDto.Id);
        }

        public PagingResult<ShowroomDto> SearchShowrooms(SearchShowroomCommand command)
        {
            if (command == null)
                return PagingResult<ShowroomDto>.Empty();
            var predicate =
                Functions.Lambda<Model.Showroom, bool>(
                    c => (
                        (command.Name == null || c.Name.Contains(command.Name))
                        && (command.Address == null || c.Address.Contains(command.Address))
                        && (command.City == null || c.City.Contains(command.City))
                        && (command.State == null || c.State.Contains(command.State))
                        && (command.PostalCode == null || c.PostalCode.Contains(command.PostalCode))
                    ));

            var query = DbContext.Showrooms.Where(predicate).AsQueryable();

            if(!string.IsNullOrEmpty(command.OrderBy))
            {
                query = query.SetOrderByField(command.OrderAsc, command.OrderBy);
            }

            var totalCount = query.Count();
            var showrooms = query.Paginate(command);

            return new PagingResult<ShowroomDto>(showrooms.AsEnumerable().Select(ShowroomDto.EntityToDtoMapper).ToList())
            {
                PageSize = command.PageSize,
                PageNumber = command.PageNumber,
                PageTotal = PagingHelper.GetPageTotal(totalCount, command.PageSize),
                ResultCount = totalCount
            };
        }
    }
}
