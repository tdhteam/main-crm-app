﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils;
using DEA.CRM.Utils.FunctionExtensions;

namespace DEA.CRM.Service.Core
{
    public class DefaultUserPermissionService : DefaultServiceBase, IUserPermissionService
    {
        public DefaultUserPermissionService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) :
            base(dbContext, serviceFactory)
        {
        }

        public bool HasBasicPermission(IUserDetailDto userDetailDto, string moduleName, PermissionOperation operation)
        {
            if (userDetailDto == null)
                return false;

            if (userDetailDto.IsAdmin)
                return true;

            userDetailDto.StandardModulePermissions.TryGetValue(moduleName, out var modPerm);
            switch (operation)
            {
                case PermissionOperation.View:
                    return modPerm?.CanView ?? false;

                case PermissionOperation.Create:
                    return modPerm?.CanCreate ?? false;

                case PermissionOperation.Edit:
                    return modPerm?.CanEdit ?? false;

                case PermissionOperation.Delete:
                    return modPerm?.CanDelete ?? false;
            }

            return false;
        }

        public bool HasCustomPermission(IUserDetailDto userDetailDto, PermissionCheckOpts opts)
        {
            if (userDetailDto == null)
                return false;

            if (opts.IncludeDelegation) return opts.Match(userDetailDto.HasPermission);

            var permissionIsUsersOwn = Functions
                .Lambda<IUserDetailDto, PermissionType, bool>(HasOwnPermission)
                .Partial(userDetailDto);

            return opts.Match(permissionIsUsersOwn);
        }

        private bool HasOwnPermission(IUserDetailDto userDetailDto, PermissionType permission)
            => userDetailDto.HasPermission(permission);
    }
}
