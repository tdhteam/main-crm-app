﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;

namespace DEA.CRM.Service.Core
{
    public abstract class DefaultServiceBase
    {
        public CRMAppDbContext DbContext { get; }

        public IServiceFactory ServiceFactory { get; }

        protected DefaultServiceBase(CRMAppDbContext dbContext, IServiceFactory serviceFactory)
        {
            DbContext = dbContext;
            ServiceFactory = serviceFactory;
        }

        protected T GetService<T>() => ServiceFactory.GetService<T>();
    }
}
