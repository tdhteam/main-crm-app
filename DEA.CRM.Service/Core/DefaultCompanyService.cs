﻿using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Exceptions;

namespace DEA.CRM.Service.Core
{
    public class DefaultCompanyService : DefaultServiceBase, ICompanyService
    {
        public DefaultCompanyService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }
        
        public Result<CompanyDto> GetCompanyById(int id)
        {
            var entity = DbContext.Companies.Find(id).AsResult($"Cannot find company with id {id}");
            return Result<CompanyDto>.Success(CompanyDto.EntityToDtoMapper(entity.Value));
        }
        
        public Result<CompanyDto> GetFirstCompany()
        {
            var entity = DbContext.Companies.OrderBy(x => x.Id).FirstOrDefault();
            return Result<CompanyDto>.Success(CompanyDto.EntityToDtoMapper(entity));
        }
        

        public Result<CompanyDto> CreateOrUpdate(CreateOrUpdateCompanyCommand companyDto)
        {
            if (companyDto.Id <= 0) // new
            {
                var entity = CompanyDto.DtoToEntityMapper(companyDto);
                DbContext.Companies.Add(entity);
                companyDto.Id = entity.Id;
            }
            else
            {
                //update
                var c = DbContext.Companies.Find(companyDto.Id);
                if (c == null) throw new EntityNotFoundException("I18N.Company.COMPANY_WITH_ID_IS_NOT_FOUND");
                c.Address = companyDto.Address;
                c.City = companyDto.City;
                c.CompanyCode = companyDto.CompanyCode;
                c.Country = companyDto.Country;
                c.Fax = companyDto.Fax;
                c.LogoRelativeUrl = companyDto.LogoRelativeUrl;
                c.Name = companyDto.Name;
                c.Phone = companyDto.Phone;
                c.PostalCode = companyDto.PostalCode;
                c.State = companyDto.State;
                c.WebSite = companyDto.WebSite;
            }
            DbContext.SaveChanges();
            return GetCompanyById(companyDto.Id);
        }
    }
}
