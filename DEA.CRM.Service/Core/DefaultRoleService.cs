﻿using System;
using System.Collections.Generic;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Exceptions;
using DEA.CRM.Utils.Extensions;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;
using Microsoft.EntityFrameworkCore;

namespace DEA.CRM.Service.Core
{
    public class DefaultRoleService : DefaultServiceBase, IRoleService
    {
        private readonly ITabService _tabService;

        public DefaultRoleService(CRMAppDbContext dbContext, IServiceFactory serviceFactory, ITabService tabService) : base(dbContext, serviceFactory)
        {
            _tabService = tabService;
        }

        public PagingResult<RoleDetailDto> GetAllRoles()
        {
            var allRoles = DbContext.Roles.Select(RoleDetailDto.EntityToDtoMapper).ToList();
            return PagingResult<RoleDetailDto>.AllResult(allRoles);
        }

        public RoleTreeDto GetRoleTree()
        {
            var allRoleList = DbContext.Roles.Select(RoleDetailDto.EntityToDtoMapper).OrderBy(x => x.Depth).ToList();

            var roleTree = new RoleTreeDto();

            allRoleList.Where(r => r.Depth == 0)
                .Select(RoleTreeNodeDto.RoleDtoToRoleNodeMapper)
                .ToList()
                .ForEach(r => roleTree.AddRoot(r));

            foreach (var roleDto in allRoleList.Where(r => r.Depth > 0))
            {
                roleTree.AddNode(RoleTreeNodeDto.RoleDtoToRoleNodeMapper(roleDto), roleDto.ParentRoleId);
            }

            return roleTree;
        }

        public Result<RoleDetailDto> GetDefaultNewRole(string parentRoleId)
        {
            var moduleList = _tabService.GetAll();
            // TODO extract to permission service
            var allPermissions = DbContext.Permissions.Select(RoleAssignPermissionDto.NonAssignedMapper).ToList();
            var parentRole = DbContext.Roles.Find(parentRoleId).AsResult($"Failed to load role with id {parentRoleId}");
            return RoleDetailDto.NewDefaultRoleDto(parentRoleId, parentRole.Value.Name, moduleList, allPermissions)
                .AsResult("Failed to create new default role data");
        }

        public Result<RoleDetailFormDto> CreateRoleDetailFormSchema(string parentRoleId)
        {
            var roleDto = GetDefaultNewRole(parentRoleId);
            var formDto = new RoleDetailFormDto
            {
                FormData = roleDto.Value
            };
            return Result<RoleDetailFormDto>.Success(formDto);
        }

        public Result<RoleDetailDto> GetRoleById(string roleId)
        {
            var roleEntity = FindRoleEntityWithChildren(roleId).AsResult($"Cannot find Role with id {roleId}");

            var roleDto = RoleDetailDto.EntityToDtoMapper(roleEntity.Value);
            var moduleList = _tabService.GetAll();
            PopulateRolePermissionFromProfile(roleDto, moduleList);            
            return Result<RoleDetailDto>.Success(roleDto);
        }

        private void PopulateRoleStandardPermissionFromProfile(RoleDetailDto roleDto, 
            IList<TabInfoDto> moduleList, 
            ICollection<int> allProfileIdsForThisRole,
            ICollection<int> moduleIdList)
        {
            var allProfilePermissions =
                DbContext.ProfileToStandardPermissions.Where(x =>
                        allProfileIdsForThisRole.Contains(x.ProfileId) && moduleIdList.Contains(x.TabId))
                    .OrderBy(x => x.ProfileId)
                    .ThenBy(x => x.TabId)
                    .ToList();

            roleDto.ModulePermissions = ModulePermissionDto.BuildNoAccessPermissionForModules(moduleList);
            allProfilePermissions.ForEach(perm =>
            {
                var modulePerm = roleDto.ModulePermissions.FirstOrDefault(x => x.ModuleId == perm.TabId);
                if (modulePerm != null)
                {
                    switch (perm.Operation)
                    {
                        case (int)PermissionOperation.View:
                            modulePerm.CanView = perm.Permission;
                            break;
                        
                        case (int)PermissionOperation.Create:
                            modulePerm.CanCreate = perm.Permission;
                            break;
                        
                        case (int)PermissionOperation.Edit:
                            modulePerm.CanEdit = perm.Permission;
                            break;
                        
                        case (int)PermissionOperation.Delete:
                            modulePerm.CanDelete = perm.Permission;
                            break;
                    }                    
                }                
            });
        }        

        private void PopulateRoleCustomPermissionFromProfile(RoleDetailDto roleDto, ICollection<int> allProfileIdsForThisRole)
        {
            var allProfileCustomPermissions = (from pp in DbContext.ProfileToCustomPermissions
                join p in DbContext.Profiles on pp.ProfileId equals p.Id
                join perm in DbContext.Permissions on pp.PermissionId equals perm.Id
                where allProfileIdsForThisRole.Contains(pp.ProfileId)
                orderby perm.DisplayOrder
                select new PermissionDto
                {
                    Id = perm.Id,
                    Name = perm.Name,
                    Description = perm.Description,
                    DisplayOrder = perm.DisplayOrder
                })
                .ToList()
                .Distinct(new PermissionDtoIEqualityComparer())
                .ToDictionary(k => k.Id, v => v);

            var allAvailablePermissions = DbContext.Permissions
                .OrderBy(p => p.DisplayOrder)
                .Select(PermissionDto.EntityToDtoMapper)
                .ToList();



            roleDto.CustomPermissions = allAvailablePermissions.Select(p =>
            {
                allProfileCustomPermissions.TryGetValue(p.Id, out var matchedP);
                if (matchedP != null)
                {
                    return new RoleAssignPermissionDto
                    {
                        IsAssigned = true,
                        Id = matchedP.Id,
                        Name = matchedP.Name,
                        Description = matchedP.Description,
                        DisplayOrder = matchedP.DisplayOrder
                    };
                }

                return new RoleAssignPermissionDto
                {
                    IsAssigned = false,
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    DisplayOrder = p.DisplayOrder
                };
            }).ToList();
        }
        
        private void PopulateRolePermissionFromProfile(RoleDetailDto roleDto, IList<TabInfoDto> moduleList)
        {
            var allProfileIdsForThisRole = roleDto.RoleToProfiles.Select(x => x.ProfileId).ToHashSet();
            var moduleIdList = moduleList.Select(x => x.Id).ToHashSet();

            PopulateRoleStandardPermissionFromProfile(roleDto, moduleList, allProfileIdsForThisRole, moduleIdList);
            PopulateRoleCustomPermissionFromProfile(roleDto, allProfileIdsForThisRole);

            if (roleDto.ModulePermissions?.Count == 0)
            {                
                roleDto.ModulePermissions = ModulePermissionDto.BuildNoAccessPermissionForModules(moduleList);
            }

            if (roleDto.CustomPermissions?.Count == 0)
            {
                //TODO use IPermissionService for this
                roleDto.CustomPermissions = DbContext.Permissions.Select(RoleAssignPermissionDto.NonAssignedMapper).ToList();
            }
        }

        public Result<RoleDetailDto> CreateOrUpdate(CreateOrUpdateRoleCommand command)
        {
            if(command == null)
                return Result<RoleDetailDto>.Fail("Invalid role creation request.");

            var roleId = string.Empty;
            var parentRoleName = string.Empty;
            var parentRoleTreePath = string.Empty;
            var parentTreeDepth = 0;

            if (string.IsNullOrEmpty(command.RoleId))
            {
                // Add new role with a new profile 
                
                var roleSeq = new RoleSeq();
                DbContext.RoleSeqs.Add(roleSeq);
                if (DbContext.SaveChanges() > 0)
                {
                    if (!string.IsNullOrEmpty(command.ParentRoleId))
                    {
                        var parentRoleEntity = DbContext.Roles.FirstOrDefault(r => r.RoleId == command.ParentRoleId);
                        if (parentRoleEntity != null)
                        {
                            parentRoleName = parentRoleEntity.Name;
                            parentRoleTreePath = parentRoleEntity.TreePath;
                            parentTreeDepth = parentRoleEntity.Depth;
                        }
                    }
                    var newRoleId = $"H{roleSeq.Id}";
                    var roleEntityToAdd = new Role
                    {
                        RoleId = newRoleId,
                        Name = command.Name,
                        ParentRoleId = command.ParentRoleId,
                        ParentRoleName = parentRoleName,
                        Depth = parentTreeDepth + 1,
                        TreePath = string.IsNullOrEmpty(parentRoleTreePath) ? string.Empty : $"{parentRoleTreePath}::{newRoleId}"
                    };

                    // Create new profile for each new role
                    var newProfile = new Profile
                    {
                        Name = $"{roleEntityToAdd.Name}+Profile",
                        Description = $"Default profile assigned direct to role {roleEntityToAdd.Name}",
                        DirectlyRelatedToRole = true
                    };

                    var newRoleToProfile = new RoleToProfile
                    {
                        Role = roleEntityToAdd,
                        Profile = newProfile
                    };
                    
                    DbContext.Profiles.Add(newProfile);
                    DbContext.Roles.Add(roleEntityToAdd);
                    DbContext.RoleToProfiles.Add(newRoleToProfile);
                    
                    if (command.CustomPermissions.Any(x => x.IsAssigned))
                    {
                        var newProfileToPermissions = command.CustomPermissions.Select(p => new ProfileToCustomPermission
                        {
                            Profile = newProfile,
                            PermissionId = p.Id
                        });                        
                        DbContext.ProfileToCustomPermissions.AddRange(newProfileToPermissions);
                    }

                    // Add permission to profile
                    if (command.ModulePermissions?.Count > 0)
                    {
                        var standardProfilePermissionList = MakeStandardProfilePermissionListForNewProfile(command, newProfile);

                        DbContext.ProfileToStandardPermissions.AddRange(standardProfilePermissionList);
                    }

                    roleId = newRoleId;
                }
            }
            else
            {
                // Handle role update 
                if(command.RoleId == command.ParentRoleId)
                    throw new ArgumentException("I18N.Role.INVALID_PARENT_ROLE_ID");

                var roleEntity = FindRoleEntityWithChildren(command.RoleId);

                if (roleEntity == null)
                    throw new EntityNotFoundException("I18N.Role.ROLE_WITH_ID_IS_NOT_FOUND");

                roleEntity.Name = command.Name;
                if (roleEntity.ParentRoleId != command.ParentRoleId)
                {
                    var parentRoleEntity = DbContext.Roles.FirstOrDefault(r => r.RoleId == command.ParentRoleId);
                    if (parentRoleEntity != null)
                    {
                        roleEntity.ParentRoleId = parentRoleEntity.RoleId;
                        roleEntity.ParentRoleName = parentRoleEntity.Name;
                        roleEntity.TreePath = $"{parentRoleEntity.TreePath}::{roleEntity.RoleId}";
                        roleEntity.Depth = parentRoleEntity.Depth + 1;
                    }
                }
                
                var profileIdsForUpdate = roleEntity.RoleToProfile.Select(x => x.ProfileId).ToHashSet();
                
                // Update permission on ProfileToStandardPermission
                // Role can have multi profiles
                if (command.ModulePermissions.Any())
                {
                    var existingProfileStandardPermissions =
                        DbContext.ProfileToStandardPermissions.Where(x => profileIdsForUpdate.Contains(x.ProfileId)).ToList();
                    command.ModulePermissions.ToList().ForEach(
                        modulePerm =>
                        {
                            var profileStdPerms =
                                existingProfileStandardPermissions.Where(x => x.TabId == modulePerm.ModuleId);
                            foreach (var stdPerm in profileStdPerms)
                            {
                                switch ((PermissionOperation)stdPerm.Operation)
                                {
                                    case PermissionOperation.View:
                                        stdPerm.Permission = modulePerm.CanView;
                                        break;
                                    case PermissionOperation.Create:
                                        stdPerm.Permission = modulePerm.CanCreate;
                                        break;
                                    case PermissionOperation.Edit:
                                        stdPerm.Permission = modulePerm.CanEdit;
                                        break;
                                    case PermissionOperation.Delete:
                                        stdPerm.Permission = modulePerm.CanDelete;
                                        break;
                                }
                            }
                        }
                    );
                }

                // Update permissions on profile to custom permission
                if (command.CustomPermissions.Any())
                {
                    var existingProfileCustomPermissions =
                        DbContext.ProfileToCustomPermissions
                            .Where(x => profileIdsForUpdate.Contains(x.ProfileId)).ToList();

                    DbContext.ProfileToCustomPermissions.RemoveRange(existingProfileCustomPermissions);

                    roleEntity.RoleToProfile
                        .Select(rf => rf.Profile)
                        .ToList()
                        .ForEach(profile =>
                        {
                            var newProfileToPermissions = command.CustomPermissions.Where(x => x.IsAssigned)
                                .Select(p => new ProfileToCustomPermission
                                {
                                    ProfileId = profile.Id,
                                    PermissionId = p.Id
                                });
                            DbContext.ProfileToCustomPermissions.AddRange(newProfileToPermissions);
                        });
                }

                roleId = roleEntity.RoleId;
            }
            
            DbContext.SaveChanges();
            
            return GetRoleById(roleId);
        }

        private Role FindRoleEntityWithChildren(string roleId)
        {
            return DbContext.Roles
                .Include(r => r.RoleToProfile).ThenInclude(rp => rp.Profile)
                .Include(r => r.UserInfoToRole)
                .FirstOrDefault(r => r.RoleId == roleId);
        }

        private static List<ProfileToStandardPermission> MakeStandardProfilePermissionListForNewProfile(CreateOrUpdateRoleCommand command, Profile profile)
        {
            return command.ModulePermissions
                .SelectMany(x => new List<ProfileToStandardPermission>
                {
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.View,
                        Permission = x.CanView
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Create,
                        Permission = x.CanCreate
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Edit,
                        Permission = x.CanEdit
                    },
                    new ProfileToStandardPermission
                    {
                        Profile = profile,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Delete,
                        Permission = x.CanDelete
                    }
                })
                .ToList();
        }

        private static List<ProfileToStandardPermission> MakeStandardProfilePermissionListForExistingProfile(CreateOrUpdateRoleCommand command, Profile profile)
        {
            return command.ModulePermissions
                .SelectMany(x => new List<ProfileToStandardPermission>
                {
                    new ProfileToStandardPermission
                    {
                        ProfileId = profile.Id,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.View,
                        Permission = x.CanView
                    },
                    new ProfileToStandardPermission
                    {
                        ProfileId = profile.Id,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Create,
                        Permission = x.CanCreate
                    },
                    new ProfileToStandardPermission
                    {
                        ProfileId = profile.Id,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Edit,
                        Permission = x.CanEdit
                    },
                    new ProfileToStandardPermission
                    {
                        ProfileId = profile.Id,
                        TabId = x.ModuleId,
                        Operation = (int)PermissionOperation.Delete,
                        Permission = x.CanDelete
                    }
                })
                .ToList();
        }

        public ResponseDto DeleteRole(DeleteRoleCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));
            
            if(command.RoleId.IsNullOrEmpty() || command.TransferOwnershipToRoleId.IsNullOrEmpty())
                return ResponseDto.OkNoContent();
            
            var roleEntity = DbContext.Roles.Find(command.RoleId);
            
            // Do not allow delete the root
            if(roleEntity == null || roleEntity.ParentRoleId.IsNullOrEmpty())
                return ResponseDto.OkNoContent();
            
            // Transfer ownership of this role to target role
            DbContext.UserInfoToRoles
                .Where(x => x.RoleId == command.RoleId)
                .ToList()
                .ForEach(ur => ur.RoleId = command.TransferOwnershipToRoleId);

            DbContext.SaveChanges();
            
            var roleToProfileEntity = DbContext.RoleToProfiles.FirstOrDefault(x => x.RoleId == command.RoleId);

            if (roleToProfileEntity != null)
                DbContext.RoleToProfiles.Remove(roleToProfileEntity);

            // Handle role tree removal
            // leaf node
            if (!DbContext.Roles.Any(n => n.ParentRoleId == command.RoleId))
            {
                DbContext.Roles.Remove(roleEntity);
            }
            //TODO: middle node

            DbContext.SaveChanges();

            return ResponseDto.OkNoContent();
        }
    }
    
    
    internal class PermissionDtoIEqualityComparer : IEqualityComparer<PermissionDto>
    {
        public bool Equals(PermissionDto x, PermissionDto y)
        {
            return x != null && y != null & x.Id == y.Id;
        }

        public int GetHashCode(PermissionDto obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
