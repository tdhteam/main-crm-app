using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;

namespace DEA.CRM.Service.Attachment
{
    public class S3BucketAttachmentMetadataService : BaseAttachmentMetadataService, IAttachmentMetadataService
    {
        public S3BucketAttachmentMetadataService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }
    }
}