﻿using System;
using System.Threading.Tasks;
using DEA.CRM.EfModel;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Attachment
{
    public class DefaultAttachmentManager : DefaultServiceBase, IAttachmentManager
    {
        private readonly IAttachmentStorageService _attachmentStorageService;
        private readonly IAttachmentMetadataService _attachmentMetadataService;
        private readonly IUserPermissionService _userPermissionService;

        public DefaultAttachmentManager(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            IAttachmentStorageService attachmentStorageService, 
            IAttachmentMetadataService attachmentMetadataService, IUserPermissionService userPermissionService) : base(dbContext, serviceFactory)
        {
            _attachmentStorageService = attachmentStorageService;
            _attachmentMetadataService = attachmentMetadataService;
            _userPermissionService = userPermissionService;
        }

        /// <summary>
        /// Upload to storage service and save attachment metadata
        /// </summary>
        /// <param name="attachmentDto"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<Result<AttachmentDto>> SaveAttachmentAsync(AttachmentDto attachmentDto, UserDetailDto currentUser)
        {
            if(attachmentDto == null)
                throw new ArgumentNullException(nameof(attachmentDto));

            if (currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Attachments, PermissionOperation.Create)
                && !_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Attachments, PermissionOperation.Edit))
            {
                return Result.Fail<AttachmentDto>($"User has no create or edit permission for attachment");
            }

            var newAttachmentDtoRes =
                await _attachmentStorageService.SaveAttachmentContentToStorageAsync(attachmentDto);
            if (newAttachmentDtoRes.IsSuccess)
            {
                var newAttachmentDto = _attachmentMetadataService.SaveAttachmentMetadata(newAttachmentDtoRes.Value);
                return Result.Success(newAttachmentDto);
            }

            return Result.Fail<AttachmentDto>("Failed to save attachment");
        }

        /// <summary>
        /// Load attachment metadata and include attachment content if specified
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <param name="includeFileContent"></param>
        /// <returns></returns>
        public async Task<Result<AttachmentDto>> LoadAttachmentAsync(string id, UserDetailDto currentUser, bool includeFileContent = false)
        {
            if (currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Attachments, PermissionOperation.View))
            {
                return Result.Fail<AttachmentDto>("User has no view permission for attachment");
            }

            return await LoadAttachmentAsync(id, includeFileContent);
        }

        public async Task<Result<AttachmentDto>> LoadAttachmentAsync(string id, bool includeFileContent = false)
        {
            var attachmentRes = _attachmentMetadataService.GetAttachmentMetadata(id);
            if (attachmentRes.IsSuccess && includeFileContent)
            {
                var fullAtt = await _attachmentStorageService.LoadAttachmentContentFromStorageAsync(attachmentRes.Value);
                return fullAtt;
            }

            return attachmentRes;
        }

        /// <summary>
        /// Remove attachment metadata and content from storage
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <param name="flushChanges"></param>
        /// <returns></returns>
        public async Task<Result<Unit>> RemoveAttachmentAsync(string id, UserDetailDto currentUser, bool flushChanges = true)
        {
            if (currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Attachments, PermissionOperation.Delete))
            {
                return Result.Fail<Unit>("User has no delete permission for attachment");
            }

            var attMetaRes = _attachmentMetadataService.GetAttachmentMetadata(id);
            if (attMetaRes.IsSuccess)
            {
                await _attachmentStorageService.RemoveAttachmentContentFromStorageAsync(attMetaRes.Value);
                _attachmentMetadataService.RemoveAttachmentMetadata(id);
            }

            return Result.Success();
        }

        /// <summary>
        /// Get list of attachment metadata
        /// </summary>
        /// <param name="command"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public Result<PagingResult<AttachmentDto>> FindAttachmentMetadataList(FindAttachmentCommand command, UserDetailDto currentUser)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            if (!_userPermissionService.HasBasicPermission(currentUser, GlobalConstants.ModuleNames.Attachments, PermissionOperation.View))
            {
                return Result.Fail<PagingResult<AttachmentDto>>("User has no view permission for attachment");
            }

            return Result.Success(
                _attachmentMetadataService.FindAttachmentMetadataList(command, currentUser)
            );
        }

        public string GetFileDownloadUrl(string attachmentId)
        {
            var attachmentMeta = _attachmentMetadataService.GetAttachmentMetadata(attachmentId).Unwrap(new AttachmentDto
            {
                DownloadFileUrl = string.Empty
            });
            return attachmentMeta.DownloadFileUrl;
        }
        
        public string GetFileDownloadThumbnailUrl(string attachmentId)
        {
            var attachmentMeta = _attachmentMetadataService.GetAttachmentMetadata(attachmentId).Unwrap(new AttachmentDto
            {
                DownloadThumbnailUrl = string.Empty
            });
            return attachmentMeta.DownloadThumbnailUrl;
        }
    }
}
