﻿using System;
using System.Linq;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Extensions;
using DEA.CRM.Utils.Monads;
using DEA.CRM.Utils.Pagination;

namespace DEA.CRM.Service.Attachment
{
    public abstract class BaseAttachmentMetadataService : DefaultServiceBase
    {
        protected BaseAttachmentMetadataService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        protected virtual string GetDownloadUrlFor(string relativeUrl)
        {
            return relativeUrl.ToUrlPath();
        }

        public virtual AttachmentDto SaveAttachmentMetadata(AttachmentDto attachmentDto)
        {
            var attachmentEntity = new Model.Attachment
            {
                Id = attachmentDto.Id,
                FileName = attachmentDto.FileName,
                FileSize = attachmentDto.FileSize,
                ContentType = attachmentDto.ContentType,
                DateAttached = attachmentDto.DateAttached,
                CreatedByUserKey = attachmentDto.CreatedByUserKey,
                Description = attachmentDto.Description,
                FileUrl = attachmentDto.FileUrl,
                ThumbnailUrl = attachmentDto.ThumbnailUrl,
                DownloadFileUrl = attachmentDto.DownloadFileUrl,
                DownloadThumbnailUrl = attachmentDto.DownloadThumbnailUrl
            };
            DbContext.Attachments.Add(attachmentEntity);
            DbContext.SaveChanges();
            attachmentDto.Id = attachmentEntity.Id;
            return attachmentDto;
        }

        public virtual Result<Unit> RemoveAttachmentMetadata(string id)
        {
            DbContext.Attachments.DeleteById(id);
            DbContext.SaveChanges();
            return Result.Success();
        }

        public virtual Result<AttachmentDto> GetAttachmentMetadata(string id)
        {
            var attachmentEntity = DbContext.Attachments.FirstOrDefault(x => x.Id == id);
            if (attachmentEntity == null)
                return Result.Fail<AttachmentDto>($"Failed to load attachment metadata with id {id}");
            return Result.Success(AttachmentDto.Mapper(attachmentEntity));
        }

        public virtual PagingResult<AttachmentDto> FindAttachmentMetadataList(FindAttachmentCommand command, UserDetailDto currentUser)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (currentUser == null)
                throw new ArgumentNullException(nameof(currentUser));

            // TODO more specific search
            var attList = DbContext.Attachments.Select(AttachmentDto.Mapper).ToList();
            return PagingResult<AttachmentDto>.AllResult(attList);
        }
    }
}
