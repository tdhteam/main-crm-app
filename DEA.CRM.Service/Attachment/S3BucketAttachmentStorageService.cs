﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using August.ImageProcessingService.Thumbnail;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Attachment
{
    internal class S3MultiPartUploadInfo
    {
        public S3MultiPartUploadInfo()
        {
            PartETags = new List<PartETag>();
        }
        
        public string ObjectKey { get; set; }
        public string UploadId { get; set; }
        public List<PartETag> PartETags { get; set; }
    }
    
    public class S3BucketAttachmentStorageService : BaseAttachmentStorageService, IAttachmentStorageService
    {
        private IAmazonS3 S3Client { get; }
        private AWSOptions S3Options { get; }
        private readonly ISettingService _settingService;
        private static readonly int ONE_MEG = (int)Math.Pow(2, 20);
        private readonly S3MultiPartUploadInfo _currentMultiPartUploadInfo;

        public S3BucketAttachmentStorageService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            IAmazonS3 s3Client, 
            AWSOptions s3Options, 
            ISettingService settingService) : base(dbContext, serviceFactory)
        {
            S3Client = s3Client;
            S3Options = s3Options;
            _settingService = settingService;
            _currentMultiPartUploadInfo = new S3MultiPartUploadInfo();
        }
        
        public async Task<Result<AttachmentDto>> PutStreamToStorageAsync(Stream dataStream, AttachmentDto attachmentMetaDto, int partNumber)
        {
            var s3AppConfig = _settingService.GetS3BucketConfiguration();
            var objectKey = $"{attachmentMetaDto.Id}/{attachmentMetaDto.FileName}";

            // tell S3 we're going to upload an object in multiple parts and receive an upload ID
            // in return
            var initializeUploadRequest = new InitiateMultipartUploadRequest
            {
                BucketName = s3AppConfig.RootBucketName,
                Key = objectKey
            };
            var initializeUploadResponse = await S3Client.InitiateMultipartUploadAsync(initializeUploadRequest);

            // this ID must accompany all parts and the final 'completed' call
            var uploadId = initializeUploadResponse.UploadId;

            // Send the file (synchronously) using 4*5MB parts - note we pass the upload id
            // with each call. For each part we need to log the returned etag value to pass
            // to the completion call            
            var partSize = 5 * ONE_MEG; // this is the minimum part size allowed
                        
            // part numbers must be between 1 and 1000
            var logicalPartNumber = partNumber + 1;
            var uploadPartRequest = new UploadPartRequest
            {
                BucketName = s3AppConfig.RootBucketName,
                Key = objectKey,
                UploadId = uploadId,
                PartNumber = logicalPartNumber,
                PartSize = partSize,
                InputStream = dataStream
            };

            var partUploadResponse = await S3Client.UploadPartAsync(uploadPartRequest);
            
            _currentMultiPartUploadInfo.ObjectKey = objectKey;
            _currentMultiPartUploadInfo.UploadId = uploadId;
            _currentMultiPartUploadInfo.PartETags.Add(new PartETag { PartNumber = logicalPartNumber, ETag = partUploadResponse.ETag });

            return Result.Success(attachmentMetaDto);
        }
        
        public override async Task<Result<AttachmentDto>> CommitStreamToStorageAsync(AttachmentDto attachmentMetaDto)
        {            
            var s3AppConfig = _settingService.GetS3BucketConfiguration();
            var completeUploadRequest = new CompleteMultipartUploadRequest
            {
                BucketName = s3AppConfig.RootBucketName,
                Key = _currentMultiPartUploadInfo.ObjectKey,
                UploadId = _currentMultiPartUploadInfo.UploadId,
                PartETags = _currentMultiPartUploadInfo.PartETags
            };
            var uploadResult = await S3Client.CompleteMultipartUploadAsync(completeUploadRequest);
            attachmentMetaDto.FileUrl = uploadResult.Location;
            attachmentMetaDto.DownloadFileUrl = uploadResult.Location;

            return Result.Success(attachmentMetaDto);
        }

        public async Task<Result<AttachmentDto>> SaveAttachmentContentToStorageAsync(AttachmentDto attachmentDto)
        {
            CheckParamsForSaveAttachment(attachmentDto);

            var s3AppConfig = _settingService.GetS3BucketConfiguration();
            var fileKeyName = $"{attachmentDto.Id}/{attachmentDto.FileName}";

            using (var ms = new MemoryStream())
            {
                using (var sw = new BinaryWriter(ms))
                {
                    sw.Write(attachmentDto.FileContentBytes);
                    sw.Flush();
                    var uploadRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = s3AppConfig.RootBucketName,
                        Key = fileKeyName,
                        CannedACL = S3CannedACL.PublicRead,
                        InputStream = ms
                    };
                    var transferUtility = new TransferUtility(S3Client);
                    await transferUtility.UploadAsync(uploadRequest);
                    attachmentDto.FileUrl = $"{s3AppConfig.PublicEndpoint}{fileKeyName}";
                }
            }
            var hasThumbnail = FileHelper.IsImageFileContentType(attachmentDto.ContentType);
            if (hasThumbnail)
            {
                var thumbnailMaker = GetService<IThumbnailMakerService>();
                var thumbnailBytes = thumbnailMaker.Resize(attachmentDto.FileContentBytes, s3AppConfig.PreferedThumbnailWidth);
                var thumbFileKeyName = $"{attachmentDto.Id}/{s3AppConfig.ThumbnailDirectoryName}/{attachmentDto.FileName}";
                
                using (var memStream = new MemoryStream(thumbnailBytes))
                {
                    var uploadRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = s3AppConfig.RootBucketName,
                        Key = thumbFileKeyName,
                        CannedACL = S3CannedACL.PublicRead,
                        InputStream = memStream
                    };
                    var transferUtility = new TransferUtility(S3Client);
                    await transferUtility.UploadAsync(uploadRequest);
                    attachmentDto.ThumbnailUrl = $"{s3AppConfig.PublicEndpoint}{thumbFileKeyName}";
                }
                attachmentDto.ThumbnailFileContentBytes = thumbnailBytes;
            }

            return Result.Success(attachmentDto);
        }                

        public async Task<Result<AttachmentDto>> LoadAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {            
            CheckParamsForLoadAttachment(attachmentMetadataDto);

            var s3AppConfig = _settingService.GetS3BucketConfiguration();
            var request = new GetObjectRequest
            {
                BucketName = s3AppConfig.RootBucketName,
                Key = $"{attachmentMetadataDto.Id}/{attachmentMetadataDto.FileName}"
            };

            using (var response = await S3Client.GetObjectAsync(request))
            {
                using (var ms = new MemoryStream())
                {
                    await response.ResponseStream.CopyToAsync(ms);
                    attachmentMetadataDto.FileContentBytes = ms.ToArray();
                }
            }
            return Result.Success(attachmentMetadataDto);
        }

        public async Task<Result<Unit>> RemoveAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {
            if (attachmentMetadataDto == null)
                throw new ArgumentNullException(nameof(attachmentMetadataDto));

            var s3AppConfig = _settingService.GetS3BucketConfiguration();
            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = s3AppConfig.RootBucketName,
                Key = $"{attachmentMetadataDto.Id}/{attachmentMetadataDto.FileName}"
            };

            await S3Client.DeleteObjectAsync(deleteObjectRequest);
            return Result.Success();
        }
    }
}
