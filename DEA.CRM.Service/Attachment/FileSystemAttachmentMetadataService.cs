﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;

namespace DEA.CRM.Service.Attachment
{
    public class FileSystemAttachmentMetadataService : BaseAttachmentMetadataService, IAttachmentMetadataService
    {
        public FileSystemAttachmentMetadataService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }
    }
}
