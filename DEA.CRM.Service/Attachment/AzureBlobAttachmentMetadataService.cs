﻿using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;

namespace DEA.CRM.Service.Attachment
{
    public class AzureBlobAttachmentMetadataService : BaseAttachmentMetadataService, IAttachmentMetadataService
    {
        public AzureBlobAttachmentMetadataService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) 
            : base(dbContext, serviceFactory)
        {
        }
    }
}
