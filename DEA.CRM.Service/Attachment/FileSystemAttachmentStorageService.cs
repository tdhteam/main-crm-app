﻿using System;
using System.IO;
using System.Threading.Tasks;
using August.ImageProcessingService.Thumbnail;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Attachment
{
    public class FileSystemAttachmentStorageService : BaseAttachmentStorageService, IAttachmentStorageService
    {
        private readonly ISettingService _settingService;
        private readonly IThumbnailMakerService _thumbnailMakerService;

        public FileSystemAttachmentStorageService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            ISettingService settingService, 
            IThumbnailMakerService thumbnailMakerService) : base(dbContext, serviceFactory)
        {
            _settingService = settingService;
            _thumbnailMakerService = thumbnailMakerService;
        }

        public async Task<Result<AttachmentDto>> PutStreamToStorageAsync(Stream dataStream, AttachmentDto attachmentMetaDto, int blockNumber)
        {
            if (dataStream == null)
                throw new ArgumentNullException(nameof(dataStream));

            if (attachmentMetaDto == null)
                throw new ArgumentNullException(nameof(attachmentMetaDto));

            var attachmentConfig = _settingService.GetLocalFileSystemAttachmentConfiguration();

            if (attachmentConfig == null)
                throw new ArgumentNullException(nameof(attachmentConfig), @"Attachment configuration is not configured properly.");

            var fileRelativePath = Path.Combine(attachmentMetaDto.Id, attachmentMetaDto.FileName);
            var fileFullPath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath, fileRelativePath);
            var fileDirPath = Path.GetDirectoryName(fileFullPath);

            if (!Directory.Exists(fileDirPath))
            {
                Directory.CreateDirectory(fileDirPath);
            }

            using (var targetStream = File.OpenWrite(fileFullPath))
            {
                await dataStream.CopyToAsync(targetStream);
            }

            attachmentMetaDto.FileUrl = fileRelativePath;
            attachmentMetaDto.DownloadFileUrl = $"attachment/{attachmentMetaDto.Id}";

            return Result.Success(attachmentMetaDto);
        }                

        public async Task<Result<AttachmentDto>> SaveAttachmentContentToStorageAsync(AttachmentDto attachmentDto)
        {
            CheckParamsForSaveAttachment(attachmentDto);

            var attachmentConfig = _settingService.GetLocalFileSystemAttachmentConfiguration();

            if (attachmentConfig == null) throw new ArgumentNullException(nameof(attachmentConfig), @"Attachment configuration is not configured properly.");

            var fileRelativePath = Path.Combine(attachmentDto.Id, attachmentDto.FileName);
            var fileFullPath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath, fileRelativePath);
            var fileDirPath = Path.GetDirectoryName(fileFullPath);

            if (!Directory.Exists(fileDirPath))
            {
                Directory.CreateDirectory(fileDirPath);
            }
            await File.WriteAllBytesAsync(fileFullPath, attachmentDto.FileContentBytes);
            attachmentDto.FileUrl = fileRelativePath;
            // Set download url as general attachment
            attachmentDto.DownloadFileUrl = $"attachment/{attachmentDto.Id}";

            var hasThumbnail = FileHelper.IsImageFileContentType(attachmentDto.ContentType);
            if (hasThumbnail)
            {
                var thumbnailBytes = _thumbnailMakerService.Resize(attachmentDto.FileContentBytes, attachmentConfig.PreferedThumbnailWidth);

                var thumbFileRelativePath = Path.Combine(attachmentDto.Id, attachmentConfig.ThumbnailDirectoryName, attachmentDto.FileName);
                var thumbnailFullFilePath = Path.Combine(attachmentConfig.ThumbnailRootDirectoryPath, thumbFileRelativePath);
                var thumbnailDirPath = Path.GetDirectoryName(thumbnailFullFilePath);

                if (!Directory.Exists(thumbnailDirPath))
                {
                    Directory.CreateDirectory(thumbnailDirPath);
                }
                await File.WriteAllBytesAsync(thumbnailFullFilePath, thumbnailBytes);

                attachmentDto.ThumbnailUrl = thumbFileRelativePath;
                // Set download url as image
                attachmentDto.DownloadFileUrl = $"attachment/image/{attachmentDto.Id}";
                attachmentDto.DownloadThumbnailUrl = $"attachment/thumb/{attachmentDto.Id}";
                attachmentDto.ThumbnailFileContentBytes = thumbnailBytes;
            }

            return Result.Success(attachmentDto);
        }

        public async Task<Result<AttachmentDto>> LoadAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {
            CheckParamsForLoadAttachment(attachmentMetadataDto);

            var attachmentConfig = _settingService.GetLocalFileSystemAttachmentConfiguration();
            var fullFilePath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath, attachmentMetadataDto.FileUrl);
            if (!File.Exists(fullFilePath))
            {
                var message = $"Attachment file {attachmentMetadataDto.FileUrl} does not exist.";
                Serilog.Log.Warning(message);
                return Result.Fail<AttachmentDto>(message);
            }

            attachmentMetadataDto.FileContentBytes = await File.ReadAllBytesAsync(fullFilePath);

            if (!string.IsNullOrEmpty(attachmentMetadataDto.ThumbnailUrl))
            {
                var thumbFilePath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath,
                    attachmentConfig.ThumbnailDirectoryName, attachmentMetadataDto.FileUrl);
                if (File.Exists(thumbFilePath))
                {
                    attachmentMetadataDto.ThumbnailFileContentBytes = await File.ReadAllBytesAsync(thumbFilePath);
                }
            }

            return Result.Success(attachmentMetadataDto);
        }

        public async Task<Result<Unit>> RemoveAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {
            if(attachmentMetadataDto == null)
                return Result.Success();

            var attachmentConfig = _settingService.GetLocalFileSystemAttachmentConfiguration();

            var fullFilePath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath, attachmentMetadataDto.FileUrl);

            try
            {
                if (File.Exists(fullFilePath))
                    await DeleteFileAsync(fullFilePath);

                if (!string.IsNullOrEmpty(attachmentMetadataDto.ThumbnailUrl))
                {
                    var thumbFilePath = Path.Combine(attachmentConfig.AttachmentRootDirectoryPath,
                        attachmentConfig.ThumbnailDirectoryName, attachmentMetadataDto.FileUrl);
                    if (File.Exists(thumbFilePath))
                        await DeleteFileAsync(thumbFilePath);
                }

                var attachmentDirPath = Path.GetDirectoryName(fullFilePath);
                Directory.Delete(attachmentDirPath, true);
            }
            catch (Exception ex)
            {
                return Result.Success(ex.Message);
            }

            return Result.Success();
        }

        private static async Task DeleteFileAsync(string file)
        {
            using (var stream = new FileStream(file, FileMode.Truncate, FileAccess.Write, FileShare.Delete, 4096, true))
            {
                await stream.FlushAsync();
                File.Delete(file);
            }
        }
    }
}
