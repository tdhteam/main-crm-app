﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using August.ImageProcessingService.Thumbnail;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils;
using DEA.CRM.Utils.Monads;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace DEA.CRM.Service.Attachment
{
    public class AzureBlobAttachmentStorageService : BaseAttachmentStorageService, IAttachmentStorageService
    {
        private readonly ISettingService _settingService;
        private readonly IThumbnailMakerService _thumbnailMakerService;
        private AzureStorageConfigurationDto _azureStorageConfig;
        private CloudBlobContainer _blobContainer;
        private bool _storageInitialized = false;
        private List<string> blockIds = new List<string>();

        public AzureBlobAttachmentStorageService(
            CRMAppDbContext dbContext, 
            IServiceFactory serviceFactory, 
            ISettingService settingService, 
            IThumbnailMakerService thumbnailMakerService) : base(dbContext, serviceFactory)
        {
            _settingService = settingService;
            _thumbnailMakerService = thumbnailMakerService;
        }

        private async Task InitStorage()
        {
            if (_storageInitialized)
                return;

            // Create a blob client for interacting with the blob service.
            _azureStorageConfig = _settingService.GetAzureStorageConfiguration();

            var storageAccount = CloudStorageAccount.Parse(_azureStorageConfig.AzureStorageConnectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();

            _blobContainer = blobClient.GetContainerReference(_azureStorageConfig.RootContainerName);
            await _blobContainer.CreateIfNotExistsAsync(
                BlobContainerPublicAccessType.Blob, 
                new BlobRequestOptions(), 
                new OperationContext()
            );

            _storageInitialized = true;
        }
        
        public async Task<Result<AttachmentDto>> PutStreamToStorageAsync(Stream dataStream, AttachmentDto attachmentMetaDto, int blockNumber)
        {
            if (dataStream == null)
                throw new ArgumentNullException(nameof(dataStream));

            if (attachmentMetaDto == null)
                throw new ArgumentNullException(nameof(attachmentMetaDto));

            await InitStorage();

            var blockId = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("BlockId{0}", blockNumber.ToString("0000000"))));
            blockIds.Add(blockId);

            // Calculate the MD5 hash of the buffer.
            var bytesToWrite = StreamHelper.ReadToEnd(dataStream);            
            var blockHash = CryptoHelper.Md5ComputeHashBytes(bytesToWrite);
            var md5Content = Convert.ToBase64String(blockHash, 0, 16);

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetaDto.Id}/{attachmentMetaDto.FileName}");
            await blockBlob.PutBlockAsync(blockId: blockId, blockData: dataStream, contentMD5: md5Content);
            
            return Result.Success(attachmentMetaDto);
        }

        public override async Task<Result<AttachmentDto>> CommitStreamToStorageAsync(AttachmentDto attachmentMetaDto)
        {
            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetaDto.Id}/{attachmentMetaDto.FileName}");
            await blockBlob.PutBlockListAsync(blockIds);
            
            attachmentMetaDto.FileUrl = blockBlob.Uri.ToString();
            attachmentMetaDto.DownloadFileUrl = blockBlob.Uri.ToString();
            blockIds = new List<string>();
            
            return Result.Success(attachmentMetaDto);
        }

        public async Task<Result<AttachmentDto>> SaveAttachmentContentToStorageAsync(AttachmentDto attachmentDto)
        {
            CheckParamsForSaveAttachment(attachmentDto);

            await InitStorage();

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentDto.Id}/{attachmentDto.FileName}");
            using (var memStream = new MemoryStream(attachmentDto.FileContentBytes))
            {
                await blockBlob.UploadFromStreamAsync(memStream);

                blockBlob.Properties.ContentType = attachmentDto.ContentType;
                await blockBlob.SetPropertiesAsync();

                attachmentDto.FileUrl = blockBlob.Uri.ToString();
                attachmentDto.DownloadFileUrl = blockBlob.Uri.ToString();
            }

            var hasThumbnail = FileHelper.IsImageFileContentType(attachmentDto.ContentType);
            if (hasThumbnail)
            {
                var thumbnailBytes = _thumbnailMakerService.Resize(attachmentDto.FileContentBytes, _azureStorageConfig.PreferedThumbnailWidth);

                var blockBlobFormThumbnail = _blobContainer.GetBlockBlobReference($"{attachmentDto.Id}/thumbnail/{attachmentDto.FileName}");
                using (var memStream = new MemoryStream(thumbnailBytes))
                {
                    await blockBlobFormThumbnail.UploadFromStreamAsync(memStream);
                    attachmentDto.ThumbnailUrl = blockBlobFormThumbnail.Uri.ToString();
                }
                attachmentDto.ThumbnailFileContentBytes = thumbnailBytes;
            }

            return Result<AttachmentDto>.Success(attachmentDto);
        }

        public async Task<Result<AttachmentDto>> LoadAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {
            CheckParamsForLoadAttachment(attachmentMetadataDto);

            await InitStorage();

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetadataDto.Id}/{attachmentMetadataDto.FileName}");
            await blockBlob.DownloadToByteArrayAsync(attachmentMetadataDto.FileContentBytes, 0);
            
            var hasThumbnail = FileHelper.IsImageFileContentType(attachmentMetadataDto.ContentType);
            if (hasThumbnail)
            {
                var thumbBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetadataDto.Id}/thumbnail/{attachmentMetadataDto.FileName}");
                await thumbBlob.DownloadToByteArrayAsync(attachmentMetadataDto.ThumbnailFileContentBytes, 0);
            }

            return Result<AttachmentDto>.Success(attachmentMetadataDto);
        }

        public async Task<Result<Unit>> RemoveAttachmentContentFromStorageAsync(AttachmentDto attachmentMetadataDto)
        {
            if (attachmentMetadataDto == null)
                throw new ArgumentNullException(nameof(attachmentMetadataDto));

            await InitStorage();

            var blockBlob = _blobContainer.GetBlockBlobReference($"{attachmentMetadataDto.Id}");
            await blockBlob.DeleteAsync();
            return Result.Success();
        }
    }
}
