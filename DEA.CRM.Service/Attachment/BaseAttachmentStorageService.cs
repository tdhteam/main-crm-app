﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Utils.Monads;

namespace DEA.CRM.Service.Attachment
{
    public abstract class BaseAttachmentStorageService : DefaultServiceBase
    {
        protected BaseAttachmentStorageService(CRMAppDbContext dbContext, IServiceFactory serviceFactory) : base(dbContext, serviceFactory)
        {
        }

        protected virtual void CheckParamsForLoadAttachment(AttachmentDto attachmentDto)
        {
            if (attachmentDto == null)
                throw new ArgumentNullException(nameof(attachmentDto));

            if (string.IsNullOrEmpty(attachmentDto.FileName))
                throw new ArgumentException("File name is empty.");
        }

        protected virtual void CheckParamsForSaveAttachment(AttachmentDto attachmentDto)
        {
            CheckParamsForLoadAttachment(attachmentDto);

            if (attachmentDto.FileContentBytes == null || attachmentDto.FileContentBytes.Length == 0)
                throw new ArgumentException("File content is empty.");
        }

        public virtual Task<Result<AttachmentDto>> CommitStreamToStorageAsync(AttachmentDto attachmentMetaDto)
        {
            Debug.WriteLine("This operation is only supported for Azure storage");
            return Task.FromResult(Result.Success(attachmentMetaDto));
        }
    }
}
