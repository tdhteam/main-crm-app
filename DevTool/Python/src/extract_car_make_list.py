import codecs
import json

datastore = json.loads(codecs.open('../../Data/car-list.json', encoding='utf-8').read())
makes = []
for item in datastore:
    makes.append(item["brand"])

makes.sort()
for name in makes:
    print(f'KeyValuePickListDto.Of("{name}", "{name}"),')
