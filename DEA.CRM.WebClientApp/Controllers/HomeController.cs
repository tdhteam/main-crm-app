﻿using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.WebClientApp.Controllers
{
  public class HomeController : Controller
  {
    // GET
    public IActionResult Index()
    {
      ViewData["apiBaseUrl"] = "http://api-host.io/api";
      ViewData["dataLocale"] = System.Globalization.CultureInfo.CurrentCulture.ToString();
      return View();
    }
  }
}
