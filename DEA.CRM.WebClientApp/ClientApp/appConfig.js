export const APP_CONFIG = {

  GeneralApiTimeOut: 100000, //ms
  UploadApiTimeOut: 20000,
  LongOperationApiTimeOut: 36000,

  API_URLS: {
    ApiRootUrl: 'http://localhost:5001',
    ApiBaseUrl: 'http://localhost:5001/api',
    //ApiRootUrl: 'http://14.161.34.237:5001',
    //ApiBaseUrl: 'http://14.161.34.237:5001/api'
  },

  APP_SESSION_STORAGE_KEYS: {
    userLoginPageInfo: 'userLoginPageInfo',
    userLoginToken: 'userLoginToken',
    appWorkContextInfo: 'appWorkContextInfo',
    cultureInfo: 'cultureInfo'
  },

  APP_LOCAL_STORAGE_KEYS: {
    userLoginPageInfo: 'userLoginPageInfo',
    userLoginToken: 'userLoginToken',
    appWorkContextInfo: 'appWorkContextInfo',
    cultureInfo: 'cultureInfo',
    activeLanguageCode: 'activeLanguageCode'
  }
};
