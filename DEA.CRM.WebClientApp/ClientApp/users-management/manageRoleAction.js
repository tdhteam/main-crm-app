import * as types from '../actions/actionTypes';
import { makeActionCreator } from '../actions/makeActionCreator';

export const fetchRoleTreeAction = makeActionCreator(types.FETCH_ROLE_TREE);
export const fetchRoleTreeSuccessAction = makeActionCreator(types.FETCH_ROLE_TREE_SUCCESS, "roleTreeData");
export const fetchRoleTreeErrorAction = makeActionCreator(types.FETCH_ROLE_TREE_ERROR, "errorStatus");

export const dispatchTreeDataChangeAction = makeActionCreator(types.ROLE_TREE_DATA_CHANGE, "treeData");

export const roleDeleteAction = makeActionCreator(types.ROLE_TREE_DELETE_NODE, "roleDeleteCommand");
export const roleDeleteSuccessAction = makeActionCreator(types.ROLE_TREE_DELETE_NODE_SUCCESS, "roleTreeData");
export const roleDeleteErrorAction = makeActionCreator(types.ROLE_TREE_DELETE_NODE_ERROR, "errorStatus");
