import * as types from '../actions/actionTypes';
import { makeActionCreator } from '../actions/makeActionCreator';

export const searchUserRequestAction = makeActionCreator(types.SEARCH_USERS_REQUESTED, 'searchRequest');
export const searchUserResultAction = makeActionCreator(types.SEARCH_USERS_RESULT_SUCCESS, 'searchUserPageInfo');
export const searchUserErrorAction = makeActionCreator(types.SEARCH_USERS_ERROR, 'errorStatus');
