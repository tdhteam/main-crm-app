import { call, put, takeLatest} from 'redux-saga/effects';
import { showLoading, hideLoading } from 'react-redux-loading-bar';

import * as types from '../actions/actionTypes';
import * as manageRoleAction from './manageRoleAction';
import UserRoleApi from '../api/userRoleApi';

function* fetchRoleTree() {
  try{

    yield put(showLoading());

    const { response, error } = yield call(UserRoleApi.getRoleTree);

    if (response && response.status === 200){
      yield put(manageRoleAction.fetchRoleTreeSuccessAction(response.data));
    }
    else if (error){
      yield put(manageRoleAction.fetchRoleTreeErrorAction(error.response.status));
    }
  }
  finally {
    yield put(hideLoading());
  }
}

/* fetch role for role search page */
export function* fetchRoleTreeSaga() {
  yield takeLatest(types.FETCH_ROLE_TREE, fetchRoleTree);
}

/* saga to handle delete role */
export function* roleDeleteSaga() {
  yield takeLatest(types.ROLE_TREE_DELETE_NODE, deleteRoleNode);
}

function* deleteRoleNode(actions) {

  try {
    yield put(showLoading());

    const { response, error } = yield call(UserRoleApi.deleteRole, actions.roleDeleteCommand);

    if (response && response.status === 200){
      yield put(manageRoleAction.fetchRoleTreeAction());
    }
    else if (error){
      yield put(manageRoleAction.roleDeleteErrorAction(error.response.status));
    }

  }
  finally {
    yield put(hideLoading());
  }
}
