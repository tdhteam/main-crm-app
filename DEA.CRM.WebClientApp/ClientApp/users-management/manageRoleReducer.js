import * as types from '../actions/actionTypes';
import initialState from '../reducers/initialState';

export default function manageRoleReducer(state = initialState.manageRolePageInfo, action) {

  switch(action.type) {

    case types.FETCH_ROLE_TREE_SUCCESS:
    case types.ROLE_TREE_DELETE_NODE_SUCCESS:
      return { ...state, isLoading: false, roleTreeData: action.roleTreeData };

    case types.FETCH_ROLE_TREE_ERROR:
    case types.ROLE_TREE_DELETE_NODE_ERROR:
      return {...state, isLoading : false, fetchDataErrorStatus: action.errorStatus};

    case types.ROLE_TREE_DATA_CHANGE:
      return { ...state, roleTreeData: {rootNodes: action.treeData} };

    case types.FETCH_ROLE_TREE:
    case types.ROLE_TREE_DELETE_NODE:
      return { ...state, isLoading: true };

    default:
      return state;
  }
}
