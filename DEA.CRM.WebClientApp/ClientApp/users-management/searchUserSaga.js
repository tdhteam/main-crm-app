import { call, put, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as actions from '../users-management/manageUserAction';
import UserRoleApi from '../api/userRoleApi';
import {hideLoading, showLoading} from "react-redux-loading-bar";

function* searchUsers(searchRequest) {

  try{

    yield put(showLoading());

    const { response, error } = yield call(UserRoleApi.searchUsers, searchRequest);

    if (response && response.status === 200){
      yield put(actions.searchUserResultAction(response.data));
    }
    else if (error){
      yield put(actions.searchUserErrorAction(error.response.status));
    }
  }
  finally {
    yield put(hideLoading());
  }

}

export function* searchUserSaga() {
  yield takeLatest(types.SEARCH_USERS_REQUESTED, searchUsers);
}
