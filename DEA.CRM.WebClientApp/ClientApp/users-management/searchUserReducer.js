import * as types from '../actions/actionTypes';
import initialState from '../reducers/initialState';

export default function searchUserPageInfoReducer(state = initialState.searchUserPageInfo, action) {
  switch(action.type) {
    case types.SEARCH_USERS_RESULT_SUCCESS:
      return {...action.searchUserPageInfo, isLoading: false};

    default:
      return state;
  }
}
