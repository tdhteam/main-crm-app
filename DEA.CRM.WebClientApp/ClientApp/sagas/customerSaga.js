
import { call, take, put, takeEvery, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as customerActions from '../actions/customerAction';
import CustomerApi from '../api/customerApi'; 
import axios from 'axios';

const  url = 'http://localhost:5001/api/customers';


function* searchCustomer(request){
    try {
        const response = yield call(CustomerApi.get,request);
        yield put(customerActions.searchCustomerResultAction(response.data));
     } catch (error) {
       // yield put({type: "FETCH_FAILED", error});
     }
    
}

export function* saveCustomer({customer}) {
    const savedCustomer =  yield call(CustomerApi.update,customer);  
    if(customer.id)
    {
      yield put(customerActions.updateCustomerSuccessAction(savedCustomer));
    } else{
      yield put(customerActions.createCustomerSuccessAction(savedCustomer));
    }
    
}

export function* searchCustomerSaga() {
  yield takeLatest(types.SEARCH_CUSTOMER_REQUESTED, searchCustomer);
}


export function* updateCustomerSaga() {
  yield takeLatest(types.CREATE_CUSTOMER, saveCustomer);
}







