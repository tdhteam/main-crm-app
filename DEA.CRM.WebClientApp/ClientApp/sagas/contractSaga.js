
import { call, take, put, takeEvery, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as contractActions from '../actions/contractAction';
import ContractApi from '../api/handOverContractApi'; 

function* searchContract(request){
    try {
        const response = yield call(ContractApi.get,request);
        yield put(contractActions.searchContractResultAction(response.data));
     } catch (error) {
       // yield put({type: "FETCH_FAILED", error});
     }
    
}

export function* saveContract({contract}) {
    const savedContract =  yield call(ContractApi.update,contract);  
    if(contract.id)
    {
      yield put(contractActions.updateContractSuccessAction(savedContract));
    } else{
      yield put(contractActions.createContractSuccessAction(savedContract));
    }
    
}

export function* searchContractSaga() {
  yield takeLatest(types.SEARCH_CONTRACT_REQUESTED, searchContract);
}


export function* updateContractSaga() {
  yield takeLatest(types.CREATE_CONTRACT, saveContract);
}








