import { call, take, put, takeEvery, takeLatest } from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as productActions from '../actions/productAction';
import ProductApi from '../api/productApi';
import axios from 'axios';
const url = 'http://localhost:5001/api/products';

function* searchProduct(request) {
  try {
    const response = yield call(ProductApi.get, request);
    yield put(productActions.searchProductResultAction(response.data));
  } catch (error) {
    // yield put({type: "FETCH_FAILED", error});
  }

}

function* saveProduct(request) {
  console.log("before calling update")
  console.log(request);
  const savedProduct = yield call(ProductApi.updateProduct, request.product);
  console.log("after calling update");
  console.log(savedProduct);
  if (savedProduct.id) {
    yield put(productActions.updateProductSuccessAction(savedProduct));
  } else {
    yield put(productActions.createProductSuccessAction(savedProduct));
  }

}

export function* saveProductSage() {
  console.log("run saveProductSage ");
  yield takeLatest(types.UPDATE_PRODUCT, saveProduct);
 
}

export function* searchProductSaga() {
  yield takeLatest(types.SEARCH_PRODUCT_REQUESTED, searchProduct);
}

export function* updateProductSaga() {
  yield takeLatest(types.CREATE_PRODUCT, saveProduct);
}

export function* deleteProductSaga(actions) {
  yield call(ProductApi.deleteRole, actions.roleDeleteCommand);
}








