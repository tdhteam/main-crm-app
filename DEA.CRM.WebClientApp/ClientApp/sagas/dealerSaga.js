
import { call, take, put, takeEvery, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as dealerActions from '../actions/dealerAction';
import DealerApi from '../api/dealerApi';

function* searchDealer(request){
  try {
    const response = yield call(DealerApi.get,request);
    yield put(dealerActions.searchDealerResultAction(response.data));
  } catch (error) {
    // yield put({type: "FETCH_FAILED", error});
  }

}

export function* searchDealerSaga() {
  yield takeLatest(types.SEARCH_DEALER_REQUESTED, searchDealer);
}








