import { call, put, takeLatest} from 'redux-saga/effects';
import { APP_CONFIG } from '../appConfig';
import { LOGIN_REQUEST } from '../actions/actionTypes';
import * as loginAction from '../actions/loginAction';
import { initDefaultHttpClient } from '../api/axiosInitHttpClient';
import AuthApi from '../api/authApi';
import StorageService from '../services/storageService';

function* loginRequest(actionPayload) {

  const { response, error } = yield call(AuthApi.login, actionPayload);
  if (response && response.status === 200){

    const userAuthToken = response.data.authToken && response.data.authToken.auth_token;
    StorageService.setObjToLocalStore(APP_CONFIG.APP_SESSION_STORAGE_KEYS.userLoginPageInfo, response.data);
    StorageService.setItemToLocalStore(APP_CONFIG.APP_SESSION_STORAGE_KEYS.userLoginToken, userAuthToken);

    initDefaultHttpClient(userAuthToken);

    yield put(loginAction.loginOkAction(response.data));
    yield put(loginAction.initDefaultWorkContextAction(response.data));

  }
  else if (error){
    if (error.response && error.response.status === 400){

      yield put(loginAction.loginErrorAction(error.response.data));

    }else if (error.response && error.response.status === 401){

      yield put(loginAction.loginErrorAction("Invalid user name or password."));

    }else{

      yield put(loginAction.loginErrorAction(error.message));

    }

  }
}

export function* loginRequestSaga() {
  yield takeLatest(LOGIN_REQUEST, loginRequest);
}
