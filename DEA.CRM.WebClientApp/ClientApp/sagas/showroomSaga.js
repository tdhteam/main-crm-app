import * as showroomActions from "../actions/showroomAction";
import ShowRoomApi from "../api/showroomApi";
import {call, put, takeLatest} from "redux-saga/effects";
import * as types from "../actions/actionTypes";

function* searchShowroom(request){
  try {
    const response = yield call(ShowRoomApi.get,request);
    yield put(showroomActions.searchShowroomResultAction(response.data));
  } catch (error) {
    //
  }

}

export function* searchShowroomSaga() {
  yield takeLatest(types.SEARCH_SHOWROOM_REQUESTED, searchShowroom);
}
