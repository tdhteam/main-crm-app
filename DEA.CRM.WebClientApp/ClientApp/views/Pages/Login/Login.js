import React, {Component} from 'react';
import { Route, Redirect } from 'react-router-dom';
import {Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, Alert} from 'reactstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as loginActions from "../../../actions/loginAction";

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: 'admin',
      password: 'password'
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  onInputChange(event){
    this.setState({[event.target.name]: event.target.value});
  }

  handleLogin(){
    this.props.actions.loginRequestAction(this.state.username, this.state.password);
  }

  render() {

    const { authenticatedUserInfo, authError } = this.props.loginPageInfo;
    return (
      <div className="app flex-row align-items-center">
        {authenticatedUserInfo && <Redirect to="/dashboard"/>}
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-user"/>
                        </span>
                      </div>
                      <Input type="text" name="username" placeholder="Username" onChange={this.onInputChange}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-lock"/>
                        </span>
                      </div>
                      <Input type="password" name="password" placeholder="Password" onChange={this.onInputChange}/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.handleLogin}>Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12">
                        {authError && <Alert color="danger">{authError}</Alert>
                        }
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginPageInfo: state.loginPageInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(loginActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
