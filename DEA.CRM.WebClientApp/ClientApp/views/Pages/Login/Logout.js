import React from 'react';

import StorageService from '../../../services/storageService';
import {APP_CONFIG} from '../../../appConfig';

class Logout extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StorageService.removeItemFromLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.userLoginPageInfo);
    StorageService.removeItemFromLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.userLoginToken);
    StorageService.removeItemFromLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.appWorkContextInfo);
    window.location.href = '/login';
  }

  render() {

    return (
      <React.Fragment>logging out</React.Fragment>
    );
  }
}

export default Logout;
