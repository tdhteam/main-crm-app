import React from 'react';
import {connect} from 'react-redux';
import {Formik} from 'formik';
import {
  Row,
  Col,
  CardGroup,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  FormGroup,
  Label
} from 'reactstrap';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';

import StaticKeyValueLookup from '../../components/AppLookup/StaticKeyValueLookup';

class UserReferenceFirstTimeLoginForm extends React.Component {

  constructor(props) {
    super(props);

    this._onFieldChange = this._onFieldChange.bind(this);
  }

  _onFieldChange(fieldName, selected, setFieldValue, valueProp, valueParser) {

    if (selected && selected.length > 0) {

      if (typeof (valueParser) === 'function') {
        setFieldValue(fieldName, valueParser(selected[0][valueProp]));
      } else {
        setFieldValue(fieldName, selected[0][valueProp]);
      }

    }
  }

  render() {
    const {
      formData,
      pickListValues,
      formActions,
      errorMessage
    } = this.props;

    return (
      <Formik
        initialValues={formData}
        validate={values => formActions.validateForm(values)}
        onSubmit={(values, {setSubmitting, setErrors, setValues}) => {
          formActions.handleFormSave(values).then(() => {
            setSubmitting(false);
          })
          .catch(
            ({error}) => {
              formActions.showFriendlyError(error);
            }
          );
        }}
        render={({
          values,
          errors,
          touched,
          dirty,
          setFieldValue,
          setValues,
          isSubmitting,
          handleChange,
          handleSubmit
        }) => (
          <form onSubmit={handleSubmit}>
            <Row>
              <Col xs={{ size: 4, offset: 4 }} sm={{ size: 4, offset: 4 }} md={{ size: 4, offset: 4 }} lg={{ size: 4, offset: 4 }}>
                <CardGroup>
                  <Card className="p-4">
                    <CardHeader>
                      <h3>Choose your required Preferences</h3>
                      <h6>Note: you can change preferences later.</h6>
                    </CardHeader>
                    <CardBody>
                      <FormGroup>
                        <Label for="timeZone">Time zone</Label>
                        <StaticKeyValueLookup
                          name="timeZone"
                          options={pickListValues.allTimeZoneList}
                          handleChange={(e) => this._onFieldChange('timeZone', e, setFieldValue, 'key')}
                        />
                        {touched.timeZone && errors.timeZone && <div className="redColor">{errors.timeZone}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="language">Default language</Label>
                        <StaticKeyValueLookup
                          name="language"
                          options={pickListValues.allLanguageList}
                          handleChange={(e) => this._onFieldChange('language', e, setFieldValue, 'key')}
                        />
                        {touched.language && errors.language && <div className="redColor">{errors.language}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="dateFormat">Date format</Label>
                        <StaticKeyValueLookup
                          name="dateFormat"
                          options={pickListValues.allSupportedDateFormatList}
                          handleChange={(e) => this._onFieldChange('dateFormat', e, setFieldValue, 'value')}
                        />
                        {touched.dateFormat && errors.dateFormat && <div className="redColor">{errors.dateFormat}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="currencyCode">Currency</Label>
                        <StaticKeyValueLookup
                          name="currencyCode"
                          options={pickListValues.allCurrencyList}
                          handleChange={(e) => this._onFieldChange('currencyCode', e, setFieldValue, 'key')}
                        />
                        {touched.currencyCode && errors.currencyCode && <div className="redColor">{errors.currencyCode}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="defaultCompanyId">Default Company Access</Label>
                        <StaticKeyValueLookup
                          name="defaultCompanyId"
                          options={pickListValues.companies}
                          handleChange={(e) => this._onFieldChange('defaultCompanyId', e, setFieldValue, 'key', parseInt)}
                        />
                        {touched.defaultCompanyId && errors.defaultCompanyId && <div className="redColor">{errors.defaultCompanyId}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="defaultDealerId">Default Dealer Access</Label>
                        <StaticKeyValueLookup
                          name="defaultDealerId"
                          options={pickListValues.dealers}
                          handleChange={(e) => this._onFieldChange('defaultDealerId', e, setFieldValue, 'key', parseInt)}
                        />
                        {touched.defaultDealerId && errors.defaultDealerId && <div className="redColor">{errors.defaultDealerId}</div>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="defaultShowroomId">Default Showroom Access</Label>
                        <StaticKeyValueLookup
                          name="defaultShowroomId"
                          options={pickListValues.showrooms}
                          parentFilterByValue={values.defaultDealerId}
                          handleChange={(e) => this._onFieldChange('defaultShowroomId', e, setFieldValue, 'key', parseInt)}
                        />
                        {touched.defaultShowroomId && errors.defaultShowroomId && <div className="redColor">{errors.defaultShowroomId}</div>}
                      </FormGroup>
                    </CardBody>
                    <CardFooter>
                      <LaddaButton
                        className="btn btn-success btn-ladda"
                        loading={isSubmitting}
                        onClick={handleSubmit}
                        data-color="green"
                        data-style={SLIDE_LEFT}
                        disabled={isSubmitting || !dirty}
                      >
                        Save
                      </LaddaButton>
                      {errorMessage && <div className="redColor">{errorMessage}</div>}
                    </CardFooter>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </form>
        )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUserInfo: state.loginPageInfo.authenticatedUserInfo || {},
    workspaceContextInfo: state.workspaceContextInfo
  };
}

export default connect(mapStateToProps)(UserReferenceFirstTimeLoginForm);
