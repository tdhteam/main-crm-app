import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Container} from 'reactstrap';

import * as actions from "../../actions/appGeneralAction";
import UserProfilePreferenceApi from "../../api/userProfilePreferenceApi";
import UserReferenceFirstTimeLoginForm from './UserReferenceFirstTimeLoginForm';
import StorageService from "../../services/storageService";
import {APP_CONFIG} from "../../appConfig";

class UserReferenceFirstTimeLoginPage extends React.Component {

  constructor(props) {
    super(props);
    const thisComp = this;

    this.state = {
      workspaceContextInfo: props.workspaceContextInfo,
      formData: null,
      isLoading: true,
      errorMessage: null,
      formActions: {
        handleFormSave: (values) => {
          return UserProfilePreferenceApi.updateUserRequiredPreference(values)
            .then(({response}) => {
              const {data} = response;
              if (data && data.success) {
                thisComp.props.actions.setUserPreferenceFirstTimeLoginSuccessAction(data.viewModel);
                StorageService.updateObjectPropInLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.userLoginPageInfo, "authenticatedUserInfo", data.viewModel);
                StorageService.setItemToLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.activeLanguageCode, data.viewModel.language);
                window.location.href = '/dashboard';
              }
              else if (data.errors) {
                thisComp.setState({
                  errorMessage: data.errors.detail
                });
              }
            });
        },
        showFriendlyError: (error) => {
          thisComp.setState({
            errorMessage: `User preference save error. ${error}`
          });
        },
        validateForm: (values) => {
          let errors = {};
          if (!values.timeZone) {
            errors.timeZone = 'Required';
          }

          if (!values.language) {
            errors.language = 'Required';
          }

          if (!values.dateFormat) {
            errors.dateFormat = 'Required';
          }

          if (!values.currencyCode) {
            errors.currencyCode = 'Required';
          }

          if (!values.defaultCompanyId || values.defaultCompanyId <= 0) {
            errors.defaultCompanyId = 'Required';
          }

          if (!values.defaultDealerId || values.defaultDealerId <= 0) {
            errors.defaultDealerId = 'Required';
          }

          if (!values.defaultShowroomId || values.defaultShowroomId <= 0) {
            errors.defaultShowroomId = 'Required';
          }

          return errors;
        }
      }
    };

  }

  componentDidMount() {

    UserProfilePreferenceApi.getUserRequiredPreference()
      .then(({response}) => {
        const {data} = response;
        if (data && data.success) {
          this.setState({
            isLoading: false,
            ...data.viewModel
          });
        }
        else if (data.errors) {
          this.setState({
            isLoading: false,
            errorMessage: `User preference load error. ${data.errors}`
          });
        }
      });
  }

  render() {

    if (this.state.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="app">
        <div className="app-body">
          <Container fluid>
            <div className="animated fadeIn">
              <UserReferenceFirstTimeLoginForm {...this.state}/>
            </div>
          </Container>
        </div>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    currentUserInfo: state.loginPageInfo.authenticatedUserInfo || {},
    currentWorkContextInfo: state.workspaceContextInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserReferenceFirstTimeLoginPage);
