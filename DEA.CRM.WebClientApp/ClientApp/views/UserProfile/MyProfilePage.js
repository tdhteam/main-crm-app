import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../../actions/appGeneralAction";

class MyProfilePage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>user profile</div>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUserInfo: state.loginPageInfo.authenticatedUserInfo || {},
    currentWorkContextInfo: state.workspaceContextInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyProfilePage);
