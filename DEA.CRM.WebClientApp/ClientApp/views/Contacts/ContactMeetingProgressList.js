import React, { PropTypes } from 'react';
import {
  Nav,
  NavLink,
  Button
} from 'reactstrap';
import ReactTable from "react-table";

class ContactMeetingProgressList extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ReactTable
          manual
          defaultPageSize={10}
          minRows={3}
          className="-striped -highlight"
          data={this.props.meetingProgressList}
          columns={[
            {
              Header: "#",
              accessor: "lineNumber",
              Cell: row => {
                return (
                  <div>
                    <Nav>
                      <NavLink href={`/contact/edit/${row.original.id}`}>{row.value}</NavLink>
                    </Nav>
                  </div>)
              }
            },
            {
              Header: "Interested Car",
              accessor: "carOfInterest"
            },
            {
              Header: "Color",
              accessor: "carColorOfInterest"
            },
            {
              Header: "Description",
              accessor: "description"
            },
            {
              Header: "Status",
              accessor: "meetingStatus"
            },
            {
              Header: "Date",
              accessor: "dateOfMeeting"
            },
            {
              Header: "Actions",
              accessor: "contactId",
              Cell: row => {
                return (
                  <div>
                    <Button outline color="info" className="btn-sm icon"><i className="fa fa-pencil-square-o"/></Button>{' '}
                    <Button outline color="danger" className="btn-sm icon"><i className="fa fa-trash-o"/></Button>{' '}
                  </div>
                )
              }
            }
          ]}
        />
      </div>
    );
  }
}

export default ContactMeetingProgressList;
