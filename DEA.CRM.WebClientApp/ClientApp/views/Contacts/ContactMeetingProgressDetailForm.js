import React, { PropTypes } from 'react';
import {Formik} from 'formik';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup,
  Button,
  Nav,
  NavLink
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import styled from 'styled-components';

import AppFormActionButton from '../../components/AppFormActionButton/AppFormActionButton';

const ContactMeetingProgressDetailForm =
  ({
    formData,
    formActions
  }) => (
    <Formik
      enableReinitialize={true}
      initialValues={formData}
      validate={values => formActions.validateForm(values)}
      onSubmit={(values, {setSubmitting, setErrors, setValues}) => {
        setSubmitting(false);
        formActions.saveMeetingProgress(values);
        setValues({
          interestedCar: '',
          color: '',
          description: '',
          status: ''
        });
      }}
      render={({
        values,
        errors,
        touched,
        dirty,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue
      }) => (
        <form onSubmit={handleSubmit}>
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Meeting Progress Detail</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="interestedCar">Interested Car</Label>
                        <Input type="text" name="interestedCar" value={values.interestedCar} onChange={handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="color">Color</Label>
                        <Input type="text" name="color" value={values.color} onChange={handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" name="description" value={values.description} onChange={handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="status">Status</Label>
                        <Input type="select" name="status" onChange={handleChange} value={values.status || ''}>
                          <option value="">--- select ---</option>
                          <option value="Hot">Hot</option>
                          <option value="Warm">Warm</option>
                          <option value="Cold">Cold</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="meetingDate">Meeting Date</Label>
                        <DatePicker className="form-control"
                                    name="meetingDate"
                                    selected={moment(values.meetingDate)}
                                    onChange={(date) => setFieldValue("meetingDate", date)}
                                    dateFormat="LLL"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Action Buttons*/}
          <Row>
            <Col lg="12" className="text-right crm-btn-group">
              <AppFormActionButton
                dirty={dirty}
                isSubmitting={isSubmitting}
                handleSubmit={handleSubmit}
                navigateBack={formActions.navigateBack}
              />
            </Col>
          </Row>
        </form>
      )}
    />
);

export default ContactMeetingProgressDetailForm;
