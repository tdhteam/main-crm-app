import React, { PropTypes } from 'react';
import {withRouter} from "react-router-dom";
import { toast } from 'react-toastify';
import {Sidebar} from 'primereact/sidebar';

import ContactApi from '../../api/contactApi';
import ContactDetailForm from './ContactDetailForm';
import ContactMeetingProgressDetailForm from './ContactMeetingProgressDetailForm';

class ContactDetailPage extends React.Component {

  constructor(props) {
    super(props);
    let thisComp = this;
    thisComp.state = {
      formData: null,
      contactList: {
        data: [],
        isSearching: false
      },
      isLoading: true,
      meetingProgressFormVisible: false,
      formActions: {
        handleFormSave: (values) => {
          console.log(values);
          return ContactApi.updateContact(values).then(
            ({response}) => {
              toast.success('Contact save successfully.');
              return response;
            }
          );
        },
        showMeetingProgressFormForAddNew: () => {
          console.log('showMeetingProgressFormForAddNew');
          thisComp.setState({
            meetingProgressFormVisible:true,
            meetingProgressFormData: ContactDetailPage.createDefaultMeetingProgressModel()
          });
        },
        searchContact: (query) => {
          this.setState({
            contactList: {
              isSearching: true
            }
          });
          const searchParams = {
            searchRequest: {
              keyword: query
            }
          };
          ContactApi.searchContacts(searchParams)
            .then(({data}) => {
              console.log(data);
              this.setState({
                contactList: {
                  data: data.resultSet,
                  isSearching: false
                }
              });
            });
        },
        navigateBack: () => {
          this.props.history.push('/contact');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Contact save error.');
        },
        validateForm: (values) => {
          let errors = {};
          if (!values.subject) {
            errors.subject = 'Required';
          }

          return errors;
        }
      },
      meetingProgressFormData: ContactDetailPage.createDefaultMeetingProgressModel(),
      meetingProgressFormActions: {
        validateForm: (values) => {
          let errors = {};
          if (!values.interestedCar) {
            errors.interestedCar = 'Required';
          }
          console.log(errors);

          return errors;
        },
        saveMeetingProgress: (values) => {
          console.log('save ContactMeetingProgressDetailForm ', values);
          const newFormData = thisComp.state.formData;
          newFormData.contactMeetingProgresses.push({
            isNew: true,
            contactId: 0,
            carOfInterest: values.interestedCar,
            carColorOfInterest: values.color,
            description: values.description,
            status: values.status,
            dateOfMeeting: values.meetingDate.toDateString()
          });
          thisComp.setState({
            formData: newFormData,
            meetingProgressFormVisible: false,
            meetingProgressFormData: ContactDetailPage.createDefaultMeetingProgressModel()
          });
        },
        navigateBack: () => {
          console.log('back');
          this.setState({
            meetingProgressFormVisible: false,
            meetingProgressFormData: ContactDetailPage.createDefaultMeetingProgressModel()
          });
          console.log(this.state);
        }
      }
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id || 0;
    ContactApi.getContactDetailForm(id)
      .then(({response}) => {
        this.setState({...response.data, isLoading: false});
        console.log('loaded contact ', this.state);
      })
      .catch(({error}) => {
        console.log(error);
      });
  }

  static createDefaultMeetingProgressModel() {
    return {
      interestedCar: '',
      color: '',
      description: '',
      status: '',
      meetingDate: new Date()
    };
  }

  render() {

    if (this.state.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <ContactDetailForm {...this.state}/>
        <Sidebar visible={this.state.meetingProgressFormVisible}
                 baseZIndex={1000000}
                 position="right"
                 className="ui-sidebar-lg"
                 onHide={(e) => this.setState({meetingProgressFormVisible: false})}>
          <ContactMeetingProgressDetailForm
            formData={this.state.meetingProgressFormData}
            formActions={this.state.meetingProgressFormActions}/>
        </Sidebar>
      </div>
    );
  }

}


export default withRouter(ContactDetailPage);
