import React from 'react';
import {withRouter} from "react-router-dom";
import {Formik} from 'formik';
import styled from 'styled-components';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup,
  Button,
  Nav,
  NavLink
} from 'reactstrap';
import { AsyncTypeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import AppFormActionButton from '../../components/AppFormActionButton/AppFormActionButton';
import UserLookup from '../../components/AppLookup/UserLookup';
import ContactMeetingProgressList from './ContactMeetingProgressList';

const StyledButton = styled(Button)`
  margin-top: 5px;
  margin-bottom: 5px;
`;

const ContactDetailForm =
  ({
     formData,
     salutationList,
     leadSources,
     assignedToUserList,
     carMakeList,
     formActions
   }) => (
    <Formik
      initialValues={formData}
      validate={values => formActions.validateForm(values)}
      onSubmit={(values, {setSubmitting, setErrors, setValues}) => {
        formActions.handleFormSave(values).then(() => {
          setSubmitting(false);
          formActions.navigateBack();
        })
          .catch(
            ({error}) => {
              formActions.showFriendlyError(error);
            }
          );
      }}
      render={({
                 values,
                 errors,
                 touched,
                 dirty,
                 setFieldValue,
                 setValues,
                 isSubmitting,
                 handleChange,
                 handleSubmit
               }) => (
        <form onSubmit={handleSubmit}>
          {/*Action Buttons*/}
          <Row>
            <Col lg="12" className="text-right crm-btn-group">
              <AppFormActionButton
                dirty={dirty}
                isSubmitting={isSubmitting}
                handleSubmit={handleSubmit}
                navigateBack={formActions.navigateBack}
              />
            </Col>
          </Row>
          {/*Basic Information*/}
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Basic Information</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="salutation">First Name</Label>
                        <InputGroup>
                          <Input type="select" name="salutation" onChange={handleChange} value={values.salutation || ''}>
                            <option value="">--- select ---</option>
                            {salutationList.map((item, index) => (
                              <option key={index} value={index.key}>{item.value}</option>
                            ))}
                          </Input>
                          <Input type="text" name="firstName" onChange={handleChange} value={values.firstName || ''}/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="lastName">Last Name</Label>
                        <Input type="text" name="lastName" onChange={handleChange} value={values.lastName || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="ownerUserKey">Assigned To</Label>
                        <UserLookup
                          name="ownerUserKey"
                          options={assignedToUserList}
                          selected={[values.handlerUserFullName || '']}
                          handleChange={(selected) => {
                            if (selected && selected.length > 0) {
                              setFieldValue("handlerUserKey", selected[0].userKey);
                              setFieldValue("handlerUserFullName", selected[0].fullName);
                            }
                          }}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="firstMeetingDate">Meeting Date</Label>
                        <DatePicker className="form-control"
                                    name="firstMeetingDate"
                                    selected={moment(values.firstMeetingDate)}
                                    onChange={(date) => {
                                      setFieldValue("firstMeetingDate", date);
                                    }}
                                    dateFormat="LLL"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="mobilePhone">Mobile Phone</Label>
                        <Input type="text" name="mobilePhone" onChange={handleChange} value={values.mobilePhone || ''}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" name="email" onChange={handleChange} value={values.email || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="officePhone">Office Phone</Label>
                        <Input type="text" name="officePhone" onChange={handleChange} value={values.officePhone || ''}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="homePhone">Home Phone</Label>
                        <Input type="text" name="homePhone" onChange={handleChange} value={values.homePhone || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="dateOfBirth">Date of birth</Label>
                        <DatePicker className="form-control"
                                    name="dateOfBirth"
                                    selected={moment(values.dateOfBirth)}
                                    onChange={(date) => {
                                      setFieldValue("dateOfBirth", date);
                                    }}
                                    dateFormat="LLL"
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="leadSource">Lead Source</Label>
                        <Input type="select" name="leadSource" onChange={handleChange} value={values.leadSource || ''}>
                          <option value="">--- select ---</option>
                          {leadSources.map((item, index) => (
                            <option key={item.id} value={index.id}>{item.name}</option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Address Detail*/}
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Address Detail</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="addressStreet">Address Street</Label>
                        <Input type="text" name="addressStreet" onChange={handleChange} value={values.addressStreet || ''}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="addressDistrict">District</Label>
                        <Input type="text" name="addressDistrict" onChange={handleChange} value={values.addressDistrict || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="addressCity">City</Label>
                        <Input type="text" name="addressCity" onChange={handleChange} value={values.addressCity || ''}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="addressPostalCode">Postal Code</Label>
                        <Input type="text" name="addressPostalCode" onChange={handleChange} value={values.addressPostalCode || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Customer vehicle information*/}
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Customer Current Vehicle Information</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="customerCurrentCarMake">Make</Label>
                        <Input type="select" name="customerCurrentCarMake" onChange={handleChange} value={values.customerCurrentCarMake || ''}>
                          <option value="">--- select ---</option>
                          {carMakeList.map((item, index) => (
                            <option key={index} value={index.key}>{item.value}</option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="customerCurrentCarModel">Model</Label>
                        <Input type="text" name="customerCurrentCarModel" onChange={handleChange} value={values.customerCurrentCarModel || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="customerCurrentCarYearOfProduction">Year of production</Label>
                        <Input type="number" name="customerCurrentCarYearOfProduction" onChange={handleChange} value={values.customerCurrentCarYearOfProduction || ''}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="customerCurrentCarTravelDistance">Travel distance</Label>
                        <Input type="number" name="customerCurrentCarTravelDistance" onChange={handleChange} value={values.customerCurrentCarTravelDistance || ''}/>
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Meeting progress*/}
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Meeting Progress</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <StyledButton color="primary" onClick={formActions.showMeetingProgressFormForAddNew}>Add</StyledButton>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <ContactMeetingProgressList meetingProgressList={values.contactMeetingProgresses}/>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Profile Picture*/}
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Profile Picture</strong>
                </CardHeader>
                <CardBody>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/*Action Buttons*/}
          <Row>
            <Col lg="12" className="text-right crm-btn-group">
              <AppFormActionButton
                dirty={dirty}
                isSubmitting={isSubmitting}
                handleSubmit={handleSubmit}
                navigateBack={formActions.navigateBack}
              />
            </Col>
          </Row>
        </form>
      )}
    />
  );

export default withRouter(ContactDetailForm);
