import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';

console.log("detail form");

const ProductDetailForm = ({ product, onSave, onChange }) =>

  <div className="animated fadeIn">
    <Row>
      <Col lg="12" className="text-right crm-btn-group">
        <Button type="submit" onClick={onSave} className="btn-success">Lưu</Button>
        <Button className="btn-cancel" >Hủy</Button>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Chi tiết sản phẩm</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="name" className="text-right label-in-form"><span className="asterisk-icon">* </span>Tên Xe</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="name" id="name" className="control-in-form" onChange={onChange} value={product.name}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="productCategory" className="text-right label-in-form">Dòng Xe</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="productCategory" id="productCategory" className="control-in-form" onChange={onChange} value={product.productCategory}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="vin" className="text-right label-in-form">Số khung</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="vin" id="vin" className="control-in-form" onChange={onChange} value={product.vin}>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="inActiveForSale" className="text-right label-in-form">Đang kinh doanh</Label>
                  <div className="col-lg-3 control-container ">
                    <Input type="checkbox" name="inActiveForSale" id="inActiveForSale" onChange={onChange} value={product.inActiveForSale} className="control-in-form checkbox-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="engineNumber" className="text-right label-in-form">Số máy</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="engineNumber" id="engineNumber" className="control-in-form" onChange={onChange} value={product.engineNumber}>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="warehouseName" className="text-right label-in-form">Tên kho</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="warehouseName" id="warehouseName" className="control-in-form" onChange={onChange} value={product.warehouseName}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="status" className="text-right label-in-form">Trạng thái</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="status" id="status" className="control-in-form" onChange={onChange} value={product.status}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="color" className="text-right label-in-form">Màu xe</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="color" id="color" className="control-in-form" onChange={onChange} value={product.color}>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="productType" className="text-right label-in-form">Loại</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="productType" id="productType" className="control-in-form" onChange={onChange} value={product.productType}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="dateEnteredStock" className="text-right label-in-form">Ngày nhập kho</Label>
                  <div className="col-lg-3 control-container input-group">
                    <Input type="text" name="dateEnteredStock" id="dateEnteredStock" className="control-in-form" onChange={onChange} value={product.dateEnteredStock}>
                    </Input>
                    <span className="input-group-prepend input-group-append">
                      <span className="input-group-text"><i className="fa fa-calendar"></i></span>
                    </span>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="supplierName" className="text-right label-in-form">Tên nhà CC</Label>
                  <div className="col-lg-3 control-container input-group">
                    <span className="input-group-prepend">
                      <span className="input-group-text"><i className="fa fa-times-circle"></i></span>
                    </span>
                    <Input type="text" name="supplierName" id="supplierName" className="control-in-form" onChange={onChange} value={product.supplierName}>
                    </Input>
                    <span className="input-group-prepend input-group-append">
                      <span className="input-group-text"><i className="fa fa-search"></i></span>
                    </span>
                    <span className="input-group-prepend">
                      <span className="input-group-text"><i className="fa fa-plus"></i></span>
                    </span>
                  </div>
                  <Label lg="3" htmlFor="dinhKhoan" className="text-right label-in-form">Định khoản</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="dinhKhoan" id="dinhKhoan" className="control-in-form" onChange={onChange} value={product.dinhKhoan}>
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="dateRelease" className="text-right label-in-form">Ngày xuất kho</Label>
                  <div className="col-lg-3 control-container input-group">
                    <Input type="text" name="dateRelease" id="dateRelease" className="control-in-form" onChange={onChange} value={product.dateRelease}>
                    </Input>
                    <span className="input-group-prepend input-group-append">
                      <span className="input-group-text"><i className="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <Label lg="3" htmlFor="transmissionType" className="text-right label-in-form">Hộp số</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="transmissionType" id="transmissionType" className="control-in-form" onChange={onChange} value={product.transmissionType}>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="productionDate" className="text-right label-in-form">Năm sản xuất</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="productionDate" id="productionDate" className="control-in-form" onChange={onChange} value={product.productionDate}>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="numberOfSeats" className="text-right label-in-form">Số chỗ ngồi</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="numberOfSeats" id="numberOfSeats" className="control-in-form" onChange={onChange} value={product.numberOfSeats}>
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="engine" className="text-right label-in-form">Động cơ</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="engine" id="engine" className="control-in-form" onChange={onChange} value={product.engine}>
                    </Input>
                  </div>
                  <Label lg="3" className="text-right label-in-form">&nbsp;</Label>
                  <div className="col-lg-3 control-container">
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Thông tin giá</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="unitPrice" className="text-right label-in-form">Đơn giá</Label>
                  <div className="col-lg-6 control-container ">
                    <Row>
                      <Col lg="6">
                        <div className="input-group">
                          <span className="input-group-prepend">
                            <span className="input-group-text currency-icon">đ</span>
                          </span>
                          <Input type="text" name="unitPrice" id="unitPrice" className="control-in-form">
                          </Input>
                          <span className="input-group-prepend input-group-append">
                            <span className="input-group-text"><i className="fa fa-search"></i></span>
                          </span>
                          <span className="input-group-prepend">
                            <span className="input-group-text"><i className="fa fa-plus"></i></span>
                          </span>
                        </div>
                      </Col>
                      <Col lg="6">
                        <a href="#" className="currency-link">thêm đơn vị tiền tệ>></a>
                      </Col>
                    </Row>
                  </div>
                  <div className="col-lg-3 control-container">
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Thông tin tồn kho</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="UOM" className="text-right label-in-form">Đơn vị tính</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="UOM" id="UOM" className="control-in-form">
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="quantityInStock" className="text-right label-in-form">Số lượng tồn kho</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="quantityInStock" id="quantityInStock" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="handlerUser" className="text-right label-in-form"><span className="asterisk-icon">* </span>Người xử lý</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="select" name="handlerUser" id="handlerUser" className="control-in-form">
                      <option value="0">Chọn một tùy chọn</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="inStockAge" className="text-right label-in-form">Tuổi kho</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="inStockAge" id="inStockAge" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Thông tin khác</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="contract" className="text-right label-in-form">Hợp đồng</Label>
                  <div className="col-lg-3 control-container input-group">
                    <span className="input-group-prepend">
                      <span className="input-group-text"><i className="fa fa-times-circle"></i></span>
                    </span>
                    <Input type="text" name="contract" id="contract" className="control-in-form">
                    </Input>
                    <span className="input-group-prepend input-group-append">
                      <span className="input-group-text"><i className="fa fa-search"></i></span>
                    </span>
                  </div>
                  <Label lg="3" htmlFor="purchaseOrderNo" className="text-right label-in-form">Số đơn hàng</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="purchaseOrderNo" id="purchaseOrderNo" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="bankOfGuarantee" className="text-right label-in-form">NH bảo lãnh</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="bankOfGuarantee" id="bankOfGuarantee" className="control-in-form">
                    </Input>
                  </div>
                  <Label lg="3" htmlFor="paymentStatus" className="text-right label-in-form">Tình trạng thanh toán</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="textarea" name="paymentStatus" id="paymentStatus" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Hình ảnh sản phẩm</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="picture" className="text-right label-in-form">Hình ảnh</Label>
                  <div className="col-lg-9 control-container">
                    <Input type="file" name="picture" id="picture" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col xs="12" sm="12" md="12" lg="12">
        <Card className="crm-card">
          <CardHeader className="crm-card-header">
            <strong>Mô tả chi tiết</strong>
          </CardHeader>
          <CardBody className="no-padding">
            <Row className="no-margin">
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">
                  <Label lg="3" htmlFor="description" className="text-right label-in-form">Mô tả</Label>
                  <div className="col-lg-9 control-container">
                    <Input type="textarea" name="description" id="description" className="control-in-form">
                    </Input>
                  </div>
                </FormGroup>
              </Col>
              <Col lg="12" className="no-padding">
                <FormGroup row className="no-margin">

                  <Label lg="3" htmlFor="plateNumber" className="text-right label-in-form">Biển số xe</Label>
                  <div className="col-lg-3 control-container">
                    <Input type="text" name="plateNumber" id="plateNumber" className="control-in-form">
                    </Input>
                  </div>
                  <Label lg="3" className="text-right label-in-form">&nbsp;</Label>
                  <div className="col-lg-3 control-container">
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>
      <Col lg="12" className="text-right crm-btn-group">
        <Button type="submit" className="btn-success">Lưu</Button>
        <Button className="btn-cancel">Hủy</Button>
      </Col>
    </Row>
  </div >


export default ProductDetailForm;
