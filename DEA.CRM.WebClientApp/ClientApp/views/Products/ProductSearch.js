import React, {Component,PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroup,
  Input,
  Button
} from 'reactstrap';
import ReactTable from "react-table";
import "react-table/react-table.css";
import {withRouter} from "react-router-dom";
import * as productActions from '../../actions/productAction';
import { Translate, getTranslate, getActiveLanguage, setActiveLanguage  } from 'react-localize-redux';

const AddProductButton = withRouter(({ history }) => (
  <Button
    type='button' color='primary'
    onClick={() => { history.push('/product/new') }}
  >
     <Translate id="add">Add</Translate>
  </Button>
));

const SearchProductButton = (props)=>{
 return  <Button type="button"  color='primary' onClick={props.searchProduct}><Translate id="search">Search</Translate></Button>;
};

class ProductSearch extends Component {
  constructor() {
    super();
    this.state = {
      searchRequest:{
        orderAsc:'',
        orderBy: [],
        pageNumber: 1,
        pageSize: 10,

      },
      sorted:[],
      filtered: []

    };

    this.searchProduct = this.searchProduct.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.props.actions.searchProductRequestAction(this.state.searchRequest);
  }

  searchProduct(){
    //build request for filter and sorted.
    if(this.state.sorted.length>0)
    {
      let sort = { orderAsc : !this.state.sorted[0].desc, orderBy : this.state.sorted[0].id } ;
      this.setState(Object.assign( this.state.searchRequest, sort));
    }else{
      let sort = { orderAsc : false, orderBy : '' } ;
      this.setState(Object.assign( this.state.searchRequest, sort));
    }

    let request = Object.assign({},this.state.searchRequest);

    if(this.state.filtered.length>0)
    {
        this.state.filtered.forEach(function(element) {
         request = Object.assign( request, {[element.id]:element.value});
      }, this);
    }

    this.props.actions.searchProductRequestAction(request);
  }

  onChange(name, data) {
        if(name === 'sorted' || name === 'filtered') {
          this.setState(Object.assign( this.state, data));
        }
        else{
          this.setState(Object.assign( this.state.searchRequest, data));
        }
        if(name != "filtered")
        {
          this.searchProduct();
        }
      }


  render() {

    const result  = this.props.productPageInfo;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <AddProductButton/>
                <SearchProductButton searchProduct = {this.searchProduct}/>
                {/* <Button  type='button' color='primary'  onClick = {this.searchProduct}>  Search</Button> */}
              </CardHeader>
              <CardBody>
                <ReactTable
                 manual
                 // Controlled props
                sorted={this.state.searchRequest.sorted}
                page={result.pageNumber-1}
                pages={result.pageTotal}
                pageSize={this.state.searchRequest.pageSize}
                filtered={this.state.searchRequest.filtered}
                 // Callbacks
                onSortedChange={sorted => this.onChange('sorted',{sorted:sorted})}
                onPageChange={page => this.onChange('page',{pageNumber:page+1})}
                onPageSizeChange={(pageSize, page) => this.onChange('pageSize',{pageNumber:page+1,pageSize:pageSize})}
                onFilteredChange={filtered => this.onChange('filtered',{filtered})}

                data={result.resultSet}
                filterable
                  columns={[
                    {
                      Header: "Tên xe",
                      accessor: "productName",
                      filterMethod: (filter, row) =>
                        row[filter.id].startsWith(filter.value) ||
                        row[filter.id].endsWith(filter.value)
                    },
                    {
                      Header: "Số khung",
                      id: "productSerial",
                      accessor: d => d.productSerial,
                      filterMethod: (filter, rows) =>
                        matchSorter(rows, filter.value, { keys: ["productSerial"] }),
                      filterAll: true
                    },
                    {
                      Header: "SL tồn kho",
                      accessor: "numberInStock"
                    },
                    {
                      Header: "Đơn giá",
                      accessor: "unitCost",
                      id: "unitCost",
                      filterMethod: (filter, row) => {
                        if (filter.value === "all") {
                          return true;
                        }
                        if (filter.value === "true") {
                          return row[filter.id] >= 21;
                        }
                        return row[filter.id] < 21;
                      }
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    productPageInfo: state.productPageInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(productActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductSearch);


