import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';



class ProductDetailForm extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: new Array(16).fill(false)
    };
  }

  toggle(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
    this.setState({
      dropdownOpen: newArray
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row className="header-line">
          <Col lg="6">
            SANTAFE 2.4 XĂNG FULL
          </Col>
          <Col lg="6" className="text-right crm-btn-group">
            <Button className="menu-button">
              Chỉnh sửa
            </Button>
            <ButtonDropdown isOpen={this.state.dropdownOpen[0]} toggle={() => { this.toggle(0); }}>
              <DropdownToggle caret className="menu-button">
                Chi tiết
                  </DropdownToggle>
              <DropdownMenu right>

                <DropdownItem>Xóa sản phẩm</DropdownItem>

                <DropdownItem>Sao chép sản phẩm</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
            <Button className="menu-button auto-width">
              <i className="fa fa-wrench"></i> &nbsp;
              <i className="fa fa-caret-down"></i>
            </Button>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Chi tiết sản phẩm</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="name" className="text-right label-in-form">Tên Xe</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        SANTAFE 2.4 XĂNG FULL
                      </div>
                      <Label lg="3" htmlFor="productCategory" className="text-right label-in-form">Dòng Xe</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        SANTAFE
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="vin" className="text-right label-in-form">Số khung</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        RLUSV81CDJN012040
                      </div>
                      <Label lg="3" htmlFor="vin" className="text-right label-in-form">Mã</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        PRO2497
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="inActiveForSale" className="text-right label-in-form">Đang kinh doanh</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        Có
                      </div>
                      <Label lg="3" htmlFor="engineNumber" className="text-right label-in-form">Số máy</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        G4KEJA112870
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="status" className="text-right label-in-form">Thời gian thay đổi</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        2018-04-06 3:45 PM
                      </div>
                      <Label lg="3" htmlFor="warehouseName" className="text-right label-in-form">Tên kho</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        NP
                      </div>



                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">

                      <Label lg="3" htmlFor="status" className="text-right label-in-form">Trạng thái</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        Đã giao xe
                      </div>
                      <Label lg="3" htmlFor="color" className="text-right label-in-form">Màu xe</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        Trắng
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">

                      <Label lg="3" htmlFor="color" className="text-right label-in-form">Thời gian tạo</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        2018-04-03 4:26 PM
                      </div>
                      <Label lg="3" htmlFor="productType" className="text-right label-in-form">Loại</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        SUV
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="dateEnteredStock" className="text-right label-in-form">Ngày nhập kho</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        2018-03-26
                      </div>
                      <Label lg="3" htmlFor="supplierName" className="text-right label-in-form">Tên nhà CC</Label>
                      <div className="col-lg-3 content-in-form control-container">

                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="dinhKhoan" className="text-right label-in-form">Định khoản</Label>
                      <div className="col-lg-3 content-in-form control-container">

                      </div>
                      <Label lg="3" htmlFor="dateRelease" className="text-right label-in-form">Ngày xuất kho</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        2018-04-04
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="transmissionType" className="text-right label-in-form">Hộp số</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        Tự động
                      </div>
                      <Label lg="3" htmlFor="productionDate" className="text-right label-in-form">Năm sản xuất</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        2016
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="numberOfSeats" className="text-right label-in-form">Số chỗ ngồi</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        7
                      </div>
                      <Label lg="3" htmlFor="engine" className="text-right label-in-form">Động cơ</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        XĂNG
                      </div>

                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Thông tin giá</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="unitPrice" className="text-right label-in-form">Đơn giá</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        1,125,000,000
                      </div>
                      <Label lg="3" htmlFor="unitPrice" className="text-right label-in-form">&nbsp;</Label>

                      <div className="col-lg-3 control-container">
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Thông tin tồn kho</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="UOM" className="text-right label-in-form">Đơn vị tính</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        Chiếc
                      </div>
                      <Label lg="3" htmlFor="quantityInStock" className="text-right label-in-form">Số lượng tồn kho</Label>
                      <div className="col-lg-3 control-container">
                        0
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="handlerUser" className="text-right label-in-form">Người xử lý</Label>
                      <div className="col-lg-3 content-in-form control-container highlight-text">
                        Lê Thị Huỳnh Dao
                      </div>
                      <Label lg="3" htmlFor="inStockAge" className="text-right label-in-form">Tuổi kho</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        0
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Thông tin khác</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="contract" className="text-right label-in-form">Hợp đồng</Label>
                      <div className="col-lg-3 content-in-form control-container highlight-text">
                        Santafe
                      </div>
                      <Label lg="3" htmlFor="purchaseOrderNo" className="text-right label-in-form">Số đơn hàng</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        1801SOp08523
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="bankOfGuarantee" className="text-right label-in-form">NH bảo lãnh</Label>
                      <div className="col-lg-3 content-in-form control-container">
                        MB
                      </div>
                      <Label lg="3" htmlFor="paymentStatus" className="text-right label-in-form">Tình trạng thanh toán</Label>
                      <div className="col-lg-3 content-in-form control-container">

                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Hình ảnh sản phẩm</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="picture" className="text-right label-in-form">Hình ảnh</Label>
                      <div className="col-lg-9 content-in-form control-container">

                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card className="crm-card">
              <CardHeader className="crm-card-header">
                <strong>Mô tả chi tiết</strong>
              </CardHeader>
              <CardBody className="no-padding">
                <Row className="no-margin">
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">
                      <Label lg="3" htmlFor="description" className="text-right label-in-form">Mô tả</Label>
                      <div className="col-lg-9 content-in-form control-container">

                      </div>
                    </FormGroup>
                  </Col>
                  <Col lg="12" className="no-padding">
                    <FormGroup row className="no-margin">

                      <Label lg="3" htmlFor="plateNumber" className="text-right label-in-form">Biển số xe</Label>
                      <div className="col-lg-3 content-in-form control-container">

                      </div>
                      <Label lg="3" className="text-right label-in-form">&nbsp;</Label>
                      <div className="col-lg-3 control-container">
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col lg="12" className="text-right crm-btn-group">

          </Col>
        </Row>
      </div >
    );
  }
}

export default ProductDetailForm;
