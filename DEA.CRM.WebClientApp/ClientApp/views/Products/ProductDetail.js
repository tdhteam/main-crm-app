import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as productActions from '../../actions/productAction';
import ProductDetailForm from './ProductDetailForm';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';

class ProductDetail extends React.Component {
  constructor(props, context) {
    console.log("product detail page");
    super(props, context);

    this.state = {
      product: Object.assign({}, this.props.product),
      errors: {},
      saving: false
    };

    this.saveProduct = this.saveProduct.bind(this);
    this.updateProductState = this.updateProductState.bind(this);
  }

  updateProductState(event) {
    const field = event.target.name;
    let product = Object.assign({
      "ProductNo": "ProductNo",
      "Name": "Name",
      "ProductActive": true,
      "ProductCategoryId": 1,
      "InActiveForSale": 1
    }, this.state.product);
    product[field] = event.target.value;
    return this.setState({ product: product });
  }

  productFormIsValid() {
    let formIsValid = true;
    let errors = {};

    //if (this.state.product.title.length < 5) {
    //  errors.title = 'Title must be at least 5 characters.';
    //  formIsValid = false;
    //}

    this.setState({ errors: errors });
    return formIsValid;
  }

  saveProduct() {
    event.preventDefault();

    if (!this.productFormIsValid()) {
      return;
    }

    //this.setState({ saving: true });
    console.log(this.state.product);
    this.props.actions.updateProductRequestAction(this.state.product);
      //.then(() => this.redirect())
      //.catch(error => {
      //  toastr.error(error);
      //  this.setState({ saving: false });
      //});
  }

  render() {
    return (
      <ProductDetailForm     
        onSave={this.saveProduct}
        product={this.state.product}
        onChange={this.updateProductState}
      />
    );
  } 
}

//ProductDetail.propTypes = {
//  product: PropTypes.object.isRequired 
//};

function mapStateToProps(state, ownProps) {
 
  if (ownProps.match.params.id) {
    const productId = ownProps.match.params.id;
  }

  //console.log(productId);
  return {
    //productPageInfo: state.productPageInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(productActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
