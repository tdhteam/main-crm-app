import React, {Component} from 'react';
import {Bar, Doughnut, Line, Pie, Polar, Radar} from 'react-chartjs-2';
import {CardColumns, Card, CardHeader, CardBody, ListGroup, ListGroupItem, Badge} from 'reactstrap';
import RGL, { WidthProvider, Responsive } from "react-grid-layout";
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import _ from "lodash";

import {line, bar, doughnut, radar, pie, polar} from "./DashboardTestData";

const ReactGridLayout = WidthProvider(RGL);
const ResponsiveReactGridLayout = WidthProvider(Responsive);

class Dashboard extends React.PureComponent {

  // static defaultProps = {
  //   isDraggable: true,
  //   isResizable: true,
  //   items: 5,
  //   rowHeight: 200,
  //   onLayoutChange: function() {},
  //   cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }
  // };

  static defaultProps = {
    className: "layout",
    items: 5,
    rowHeight: 30,
    onLayoutChange: function() {},
    cols: 12
  };

  constructor(props) {
    super(props);
    const layout = this.generateLayout();
    this.state = { layout };

    this.renderWidget = this.renderWidget.bind(this);
  }

  generateLayout() {
    const p = this.props;
    return _.map(new Array(p.items), function(item, i) {
      const y = _.result(p, "y") || Math.ceil(Math.random() * 4) + 1;
      return {
        x: (i * 2) % 12,
        y: Math.floor(i / 6) * y,
        w: 2,
        h: y,
        i: i.toString()
      };
    });
  }

  generateDOM() {
    const _thisComp = this;
    return _.map(_.range(this.props.items), function(i) {
      return (
        <div key={i}>
          {_thisComp.renderWidget()}
        </div>
      );
    });
  }

  onLayoutChange(layout) {
    this.props.onLayoutChange(layout);
  }

  renderWidget() {
    return (
      <Card>
        <CardHeader>Sale Funnels</CardHeader>
        <CardBody className="pb-0">
          No data matches this criteria
        </CardBody>
      </Card>
    );
  }

  renderReactGridLayout() {
    return (
      <ReactGridLayout
        layout={this.state.layout}
        onLayoutChange={this.onLayoutChange}
        {...this.props}
      >
        {this.generateDOM()}
      </ReactGridLayout>
    );
  }

  renderResponsiveReactGridLayout() {
    return (
      <ResponsiveReactGridLayout
        onLayoutChange={this.onLayoutChange}
        {...this.props}
      >
        <div
          data-grid={{
            w: { lg: 6, md: 5, sm: 3, xs: 4, xxs: 2 },
            h: { lg: 4, xxs: 3 }
          }}
        >
          xxx
        </div>
      </ResponsiveReactGridLayout>
    );
  }

  render() {

    return (
      <div className="animated fadeIn">
        {this.renderReactGridLayout()}
        <CardColumns className="cols-3">
          <Card>
            <CardHeader>Sale Funnels</CardHeader>
            <CardBody className="pb-0">
              No data matches this criteria
            </CardBody>
          </Card>
          <Card>
            <CardHeader>Key Metrics</CardHeader>
            <CardBody className="pb-0">
              <ListGroup>
                <ListGroupItem className="justify-content-between">Prospect Accounts <Badge className="float-right" pill>0</Badge></ListGroupItem>
                <ListGroupItem className="justify-content-between">Open Tickets <Badge className="float-right" pill>0</Badge></ListGroupItem>
                <ListGroupItem className="justify-content-between">Hot Leads <Badge className="float-right" pill>0</Badge></ListGroupItem>
                <ListGroupItem className="justify-content-between">Potentials Won <Badge className="float-right" pill>0</Badge></ListGroupItem>
                <ListGroupItem className="justify-content-between">Open Quotes <Badge className="float-right" pill>0</Badge></ListGroupItem>
              </ListGroup>
              <br/>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>Top Opportunities</CardHeader>
            <CardBody className="pb-0">
              No data matches this criteria
            </CardBody>
          </Card>
          <Card>
            <CardHeader>Upcoming Tasks</CardHeader>
            <CardBody className="pb-0">
              No data matches this criteria
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Line Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Line data={line}
                      options={{
                        maintainAspectRatio: false
                      }}
                />
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Line Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Line data={line}
                      options={{
                        maintainAspectRatio: false
                      }}
                />
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Key Metrics
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Bar data={bar}
                     options={{
                       maintainAspectRatio: false
                     }}
                />
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Doughnut Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Doughnut data={doughnut}/>
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Radar Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Radar data={radar}/>
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Pie Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Pie data={pie}/>
              </div>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              Polar Area Chart
            </CardHeader>
            <CardBody>
              <div className="chart-wrapper">
                <Polar data={polar}/>
              </div>
            </CardBody>
          </Card>
        </CardColumns>
      </div>
    )
  }
}

export default Dashboard;
