import React from 'react';
import {toast} from "react-toastify";

import DocumentDetailForm from './DocumentDetailForm';
import UserDocumentApi from '../../api/userDocumentApi';

class DocumentDetailPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      formTitle: 'Document Manager',
      allDocuments: [],
      formData: {
        description: '',
        folderName: 'Default'
      },
      formActions: {
        saveDocument: (values) => {
          console.log('save document ', values);
          return UserDocumentApi.createUserDocument(values)
            .then(
              () => {
                toast.success('save document successfully.');
                this.searchUserDocuments();
              }
            );
        },
        navigateBack: () => {
          //this.props.history.push('/setting/user');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Document save error.');
        }
      }
    };

    this.searchUserDocuments = this.searchUserDocuments.bind(this);
  }

  componentDidMount() {
    this.searchUserDocuments();
  }

  searchUserDocuments() {
    UserDocumentApi.searchUserDocuments({searchRequest: {}}).then(({response}) => {
      this.setState({
        allDocuments : response.data
      });
    });
  }

  render() {
    const {allDocuments} = this.state;
    return (
      <div>
        <DocumentDetailForm {...this.state} />
        <ul>
          {allDocuments && allDocuments.resultSet && allDocuments.resultSet.map((item, index) => (
            <li key={index}>{item.name} - {item.description} - {item.attachmentId} - {item.folderName}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default DocumentDetailPage;
