import React from 'react';
import {withRouter} from "react-router-dom";
import {Formik, Field, FieldArray} from 'formik';
import {
  Table,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  FormText,
  Label,
  Input
} from 'reactstrap';
import LaddaButton, { SLIDE_LEFT } from 'react-ladda';

import UploadSingleFile from '../../components/Attachment/UploadSingleFile';

const DocumentDetailForm = ({ formTitle, formData, formActions }) => (
  <Formik
    initialValues={formData}
    onSubmit={(values, {setSubmitting, setErrors, setValues }) => {
      formActions.saveDocument(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      }).catch(
        ({error}) => {
          formActions.showFriendlyError(error);
        }
      );
    }}
    render={({
               values,
               errors,
               touched,
               dirty,
               setFieldValue,
               setValues,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form>
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <LaddaButton
              className="btn btn-success btn-ladda"
              loading={isSubmitting}
              onClick={handleSubmit}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting || !dirty}
            >
              Save
            </LaddaButton>
            <LaddaButton
              type="button"
              className="btn btn-cancel btn-ladda"
              loading={isSubmitting}
              onClick={formActions.navigateBack}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting}
            >
              Cancel
            </LaddaButton>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="6">
            <Card>
              <CardHeader>
                <strong>{formTitle}</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Document title</Label>
                      <Input type="text" name="name" onChange={handleChange} value={values.name || ''}/>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="description">Document description</Label>
                      <Input type="text" name="description" onChange={handleChange} value={values.description || ''}/>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="folderName">Document folder</Label>
                      <Input type="select" name="folderName" onChange={handleChange} value={values.folderName || ''}>
                        <option key={0} value="">{'---Select---'}</option>
                        <option key={1} value="Default">{'Default'}</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <UploadSingleFile
                        onUploadSuccess={(uploadResultModel) => {
                          console.log('uploadResultModel: ', uploadResultModel);
                          setFieldValue("attachmentId", uploadResultModel.id);
                        }}
                        onUploadFailed={(error) => {
                          console.log('upload error', error);
                        }}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </form>
    )}
  />
);

export default withRouter(DocumentDetailForm);
