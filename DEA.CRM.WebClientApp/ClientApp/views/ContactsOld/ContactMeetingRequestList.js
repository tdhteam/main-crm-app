import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import ContactDetailForm from './ContactDetailForm';
import { toast } from 'react-toastify';
import 'spinkit/css/spinkit.css';
import ContactApi from '../../api/contactApi'
import axios from 'axios';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButton,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Nav,
    NavLink
} from 'reactstrap';
import ContactMeetingRequestForm from './ContactMeetingRequestForm';

class ContactMeetingRequestList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageError: '',
            contactMeeting: {
                id: 0,
                contactId: 0
            },
            modal: false,
            backdrop: true,
        };
        this.toggle = this.toggle.bind(this);
        // this.saveMeetingProgress = this.saveMeetingProgress.bind(this);
        //this.showEditMeeting = this.showEditMeeting.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    showEditMeeting(id, event) {
        const { contactMeetingProgresses } = this.props;
        const meeting = contactMeetingProgresses.filter(m => m.id == id)[0];
        this.setState({ contactMeeting: meeting });
        this.toggle();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.contactId != nextProps.contactId) {
            this.setState({
                contactMeeting: Object.assign({}, this.state.contactMeeting, { contactId: nextProps.contactId })
            });
           // this.forceUpdate();
        }
    }



    // saveMeetingProgress(values) {
    //     return ContactApi.updateMeetingProgress(values)
    //         .then(
    //             ({ response }) => {
    //                 toast.success('Meeting Progress save successfully.');
    //                 return response;
    //             }
    //         )
    //         .catch((error) => {
    //             this.setState({ messageError: error.stack });
    //             toast.error('Internal Server Error');
    //         });
    // }
    render() {
        const { contactMeetingProgresses, contactId } = this.props;
        return (
            <div>
                <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                        <Card>
                            <CardHeader>
                                <strong>Sales Progress Information</strong>
                                <div className="float-sm-right">
                                    <Button color="danger" onClick={this.toggle} disabled={this.state.contactMeeting.contactId === 0}>Add</Button>
                                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} backdrop={false} size="lg">
                                        <ContactMeetingRequestForm onSubmit={this.props.onSubmitMeetingRequest} initialValues={this.state.contactMeeting} onToggle={this.toggle} />
                                    </Modal>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <Table responsive>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Car interested</th>
                                            <th>Color</th>
                                            <th>Note</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {contactMeetingProgresses.map((contactMeeting, index) => <tr key={index}>
                                            <td>
                                                <Button color="danger" onClick={this.showEditMeeting.bind(this,contactMeeting.id)} >{index + 1}</Button>
                                            </td>
                                            <td>{contactMeeting.carOfInterest}</td>
                                            <td>{contactMeeting.carColorOfInterest}</td>
                                            <td>{contactMeeting.otherNote}</td>
                                            <td>{contactMeeting.meetingStatus}</td>
                                            <td>{contactMeeting.dateOfMeeting}</td>
                                            <td>{contactMeeting.description}</td>
                                        </tr>)}

                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

            </div >
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactMeetingRequestList);
