import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as validator from '../../utility/formUtility';

import renderDatePicker from '../../Components/renderDatePicker';
import renderField from '../../Components/renderField';
import renderSelectField from '../../Components/renderSelectField';
import CityDropdown from '../../Components/CityDropdownComponent';
import LeadResourceApi from '../../api/leadSourceApi';
import CityApi from '../../api/cityApi';
import LookupControlComponent from '../../Components/lookupControlComponent';
import ContactApi from '../../api/contactApi';
import { timingSafeEqual } from 'crypto';

const gridColumns = [
  {
    Header: "firstName",
    accessor: "firstName"
  },
  {
    Header: "lastName",
    accessor: "lastName"
  },
  {
    Header: "gender",
    id: "gender"
  }];

class ContactDetailForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageError: '',
      leadResource: [],
      city: [],
      district: []
    };

    this.loadLeadSource = this.loadLeadSource.bind(this);
    this.loadCity = this.loadCity.bind(this);
    // this.loadDistrict = this.loadDistrict.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleContactLookup = this.handleContactLookup.bind(this);
    this.handleContactGridSearch = this.handleContactGridSearch.bind(this);
    
  }
  componentDidMount() {
    //loadd list lead resource
    this.loadLeadSource();
    //load City
    this.loadCity();
    //load districts

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.initialValues.addressCity != undefined && nextProps.initialValues.addressCity != "") {
      if (this.props.initialValues.addressCity == undefined || nextProps.initialValues.addressCity != this.props.initialValues.addressCity) {
        CityApi.getDistrictsByCity(nextProps.initialValues.addressCity).then(
          (response) => {
            this.setState({ district: response.data });
          }
        )
          .catch((error) => {
            toast.error('Internal Server Error');
          });
      }
    }

  }

  handleCityChange(event, newValue, previousValue, name) {
    if (newValue != undefined && previousValue != newValue) {
      CityApi.getDistrictsByCity(newValue).then(
        (response) => {
          this.setState({ district: response.data });
        }
      )
        .catch((error) => {
          toast.error('Internal Server Error');
        });
    }

  }

  // loadDistrict() {
  //   debugger;
  //   var city = this.props.initialValues.addressCity;
  //   if (city != undefined || city != "") {
  //     CityApi.getDistrictsByCity(city).then(
  //       (response) => {
  //         this.setState({ district: response.data });
  //       }
  //     )
  //       .catch((error) => {
  //         toast.error('Internal Server Error');
  //       });
  //   }
  // }

  loadLeadSource() {
    LeadResourceApi.get().then(
      (response) => {
        this.setState({ leadResource: response.data.resultSet });
      }
    )
      .catch((error) => {
        toast.error('Internal Server Error');
      });
  }

  loadCity() {
    CityApi.getCities().then(
      (response) => {
        this.setState({ city: response.data });
      }
    )
      .catch((error) => {
        toast.error('Internal Server Error');
      });
  }

  handleContactLookup(query) {
    return ContactApi.getContatLookup(query);
  }

  handleContactGridSearch(request) {
    return ContactApi.get(request);
  }


  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Basic Information</strong>
                  <div className="float-sm-right">
                    <Button type='submit' color='primary' disabled={pristine || submitting}>Save</Button>{' '}
                    <Button type='button' color='info' disabled={pristine || submitting} onClick={reset}>Clear Values</Button>{' '}
                    <Button type='button' color='secondary' disabled={pristine || submitting}>Cancel</Button>
                  </div>
                </CardHeader>

                <CardBody>
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="dealerShowroom">Dealer</Label>
                        <Input type="select" name="dealerShowroom" id="dealerShowroom">
                          <option value="0">Please select</option>
                          <option value="1">Dealer #1</option>
                          <option value="2">Dealer #2</option>
                          <option value="3">Dealer #3</option>
                        </Input>
                        {/* <CityDropdown /> */}
                        <LookupControlComponent onLookupSearch={this.handleContactLookup} valueField="firstName"
                          displayField="firstName" label="firstName"  gridColumns={gridColumns} onGridSearch={this.handleContactGridSearch}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="dealerShowroom">Showroom</Label>
                        <Input type="select" name="dealerShowroom" id="dealerShowroom">
                          <option value="0">Please select</option>
                          <option value="1">Dealer #1</option>
                          <option value="2">Dealer #2</option>
                          <option value="3">Dealer #3</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="title"
                        type="text"
                        component={renderField}
                        label="Title"
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="contactNo"
                        type="text"
                        component={renderField}
                        label="Contact No"
                        validate={[validator.required, validator.maxLength15]}
                        warn={validator.alphaNumeric}
                      />

                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        {/* <Label htmlFor="createdDate">Meeting Date</Label>
                        <DatePicker
                          selected={this.state.startDate}
                          onChange={this.handleChange}
                          showTimeSelect
                          timeFormat="HH:mm"
                          timeIntervals={60}
                          dateFormat="LLL"
                          timeCaption="time"
                        /> */}
                        <Label htmlFor="createdDate">Meeting Date</Label>
                        <Field name="firstMeetingDate" showTime={false} component={renderDatePicker} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="firstName"
                        type="text"
                        component={renderField}
                        label="First Name"
                        validate={[validator.required, validator.maxLength15]}
                        warn={validator.alphaNumeric}
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="lastName"
                        type="text"
                        component={renderField}
                        label="Last Name"
                        validate={[validator.required, validator.maxLength15]}
                        warn={validator.alphaNumeric}
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Label htmlFor="dateOfBirth">dateOfBirth</Label>
                      <Field name="dateOfBirth" showTime={false} component={renderDatePicker} />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="gender">Gender</Label>
                        <Field name="gender" component="select" className="form-control">
                          <option></option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </Field>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="email"
                        type="text"
                        component={renderField}
                        label="Email"
                        validate={[validator.email]}
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="occupation"
                        type="text"
                        component={renderField}
                        label="Occupation"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="mobilePhone"
                        type="text"
                        component={renderField}
                        label="Mobile Phone"
                      // validate={[validator.required, validator.phoneNumber]}
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="homePhone"
                        type="text"
                        component={renderField}
                        label="Home Phone"
                      // validate={[validator.phoneNumber]}
                      />
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <Field
                        name="officePhone"
                        type="text"
                        component={renderField}
                        label="Office Phone"
                      // validate={[validator.phoneNumber]}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <Field
                        name="addressStreet"
                        type="text"
                        component={renderField}
                        label="Address"
                      />
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      {/* <FormGroup>
                        <Label htmlFor="city">Province/City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Please select</option>
                          <option value="1">HCM</option>
                          <option value="2">HN</option>
                        </Input>
                      </FormGroup> */}
                      <Field
                        name="addressCity"
                        component={renderSelectField}
                        label="City"
                        options={this.state.city}
                        valueField="value"
                        displayField="name"
                        onChange={this.handleCityChange}
                      />
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      {/* <FormGroup>
                        <Label htmlFor="district">AddressDistrict</Label>
                        <Input type="select" name="addressDistrict" id="district">
                          <option value="0">Please select</option>
                          <option value="1">Quan 1</option>
                          <option value="2">Quan 2</option>
                          <option value="2">Quan 3</option>
                        </Input>
                      </FormGroup> */}
                      <Field
                        name="addressDistrict"
                        component={renderSelectField}
                        label="Address District"
                        options={this.state.district}
                        valueField="value"
                        displayField="name"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <Field
                        name="contactSource"
                        component={renderSelectField}
                        label="Contact Source"
                        options={this.state.leadResource}
                        valueField="name"
                        displayField="name"
                      />
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="paymentOptions">Payment options</Label>
                        <Input type="select" name="paymentOptions" id="paymentOptions">
                          <option value="0">Please select</option>
                          <option value="1">Cash</option>
                          <option value="2">Installment</option>
                          <option value="2">Credit card</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <Field
                        name="occupation"
                        type="text"
                        component={renderField}
                        label="Occupation"
                      />
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="currentCar">Current car</Label>
                        <Input type="text" id="currentCar" placeholder="Current car" />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="planBuyDate">Plan buy date</Label>
                        <Input type="text" id="planBuyDate" placeholder="Plan buy date" />
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <Field
                        name="contactNote"
                        type="text"
                        component={renderField}
                        label="Note"
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>

        </div>
      </form>
    );
  }
}

ContactDetailForm = reduxForm({
  form: 'contact', // a unique identifier for this form
  enableReinitialize: true
})(ContactDetailForm)


// const selector = formValueSelector('ContactDetailForm') 
// ContactDetailForm = connect(state => {
//   debugger;
//   // can select values individually
//   const addressCity = selector(state, 'addressCity');
//   return
//   {
//     addressCity
//   };
// })(ContactDetailForm)

export default ContactDetailForm;