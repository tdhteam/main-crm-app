import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import ContactCommentForm from './ContactCommentForm';
import { toast } from 'react-toastify';
import 'spinkit/css/spinkit.css';
import ContactApi from '../../api/contactApi'
import axios from 'axios';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButton,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Nav,
    NavLink
} from 'reactstrap';
import UploadSingleFile from '../../components/Attachment/UploadSingleFile';



class ContactCommentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageError: '',
            contactComment: {
                id: 0,
                contactId: 0
            }
        };

        this.updateAttach = this.updateAttach.bind(this);

    }



    componentWillReceiveProps(nextProps) {
        if (this.props.contactId != nextProps.contactId) {
            this.setState({
                contactComment: Object.assign({}, this.state.contactComment, { contactId: nextProps.contactId })
            });
            // this.forceUpdate();
        }
    }


    updateAttach(commentId, info) {
        const attachInfo = {
            attachmentId: info.id,
            contactCommentId: commentId
        };
        // this.props.onUpdateCommentAttachment.bind(this,attachInfo);
        this.props.onUpdateCommentAttachment(attachInfo);
        // ContactApi.updateCommentAttach(attachInfo)
        //     .then(
        //         ({ response }) => {

        //             toast.success('Attach save successfully.');

        //             return response;
        //         }
        //     )
        //     .catch((error) => {
        //         toast.error('Internal Server Error');
        //     });
    }

    render() {
        const { contactId, contactComments } = this.props;
        return (

            <div>
                <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                        {/* <div className="float-sm-right">
                            <Button color="danger" onClick={this.toggle} disabled={this.state.contactComment.contactId === 0}>Add</Button>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} backdrop={false} size="lg">
                                <ContactCommentForm onSubmit={this.props.onSubmitComment} initialValues={this.state.contactComment} onToggle={this.toggle} />
                            </Modal>
                        </div> */}
                        <ContactCommentForm onSubmit={this.props.onSubmitComment} initialValues={this.state.contactComment} disabled={contactId === 0} />
                    </Col>
                </Row>
                {contactComments.map((comment, index) =>
                    <Row key={index}>
                        <Col xs="12" sm="12" md="9" lg="9">
                            <Card>
                                <CardHeader>
                                    {comment.modifiedBy} -  {comment.modifiedDate}
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col xs="12" sm="12" md="12" lg="12">
                                            <Input type="textarea" value={comment.commentContent} rows="2" readOnly />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" sm="12" md="3" lg="3">
                                            <FormGroup>
                                                <UploadSingleFile

                                                    onUploadSuccess={this.updateAttach.bind(this, comment.id)}
                                                    // onUploadSuccess={(uploadResultModel) => {
                                                    //     console.log('uploadResultModel: ', uploadResultModel);
                                                    //     debugger;
                                                    //     this.updateAttach.bind({
                                                    //         attachmentId: uploadResultModel.id,
                                                    //         contactCommentId: comment.id
                                                    //     });
                                                    //     //this.props.onUpdateCommentAttachment;
                                                    //     // this.props.onUpdateCommentAttachment
                                                    //     //     .bind({attachmentId:uploadResultModel.id,
                                                    //     //         contactCommentId:comment.id});
                                                    //     // const attachInfo = {
                                                    //     //     attachmentId: uploadResultModel.id,
                                                    //     //     contactCommentId: comment.id
                                                    //     // };
                                                    //     // ContactApi.updateCommentAttach(attachInfo)
                                                    //     //     .then(
                                                    //     //         ({ response }) => {

                                                    //     //             toast.success('Attach save successfully.');
                                                    //     //             return response;
                                                    //     //         }
                                                    //     //     )
                                                    //     //     .catch((error) => {
                                                    //     //         toast.error('Internal Server Error');
                                                    //     //     });
                                                    // }}
                                                    onUploadFailed={(error) => {
                                                        console.log('upload error', error);
                                                    }}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="12" md="6" lg="6">
                                            <ul>
                                                {comment.contactCommentAttachments.map((item, index) => (
                                                    <li key={index}>
                                                        <a href={item.attachment.downloadFileUrl}>{item.attachment.fileName}</a>
                                                    </li>
                                                    //       <Nav key={index}>
                                                    //       <NavLink href={item.attachment.downloadFileUrl}>{item.attachment.fileName}</NavLink>
                                                    //   </Nav>
                                                ))}
                                            </ul>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="12" sm="12" md="3" lg="3">
                        </Col>
                    </Row>
                )}

            </div >
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactCommentList);
