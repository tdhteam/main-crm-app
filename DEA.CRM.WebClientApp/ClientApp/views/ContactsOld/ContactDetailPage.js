import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { ModalWrapper, show } from 'react-redux-bootstrap-modal';
import ContactDetailForm from './ContactDetailForm';
import { toast } from 'react-toastify';
import 'spinkit/css/spinkit.css';
import ContactApi from '../../api/contactApi'
import LeadResourceApi from '../../api/leadSourceApi'
import axios from 'axios';
import ContactMeetingRequestList from './ContactMeetingRequestList';
import ContactCommentList from './ContactCommentList';
import ContactAttachmentList from './ContactAttachmentList';
import classnames from 'classnames';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  CardTitle,
  CardText
} from 'reactstrap';
import { debug } from 'util';

class ContactDetailPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      activeTab: '1',
      messageError: '',
      leadResource: [],
      contact: {
        id: 0,
        contactMeetingProgresses: [],
        contactComments: []
      }
    };
    this.saveContact = this.saveContact.bind(this);
    this.saveMeetingProgress = this.saveMeetingProgress.bind(this);
    this.saveComment = this.saveComment.bind(this);
    this.setTab = this.setTab.bind(this);
    this.saveAttachmentInfo = this.saveAttachmentInfo.bind(this);
  }


  setTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.id != nextProps.match.params.id) {
      // Necessary to populate form when existing course is loaded directly.
      this.setState({ contact: Object.assign({}, nextProps.contact) });
    }
  }

  componentDidMount() {

    //contact detail 
    if (this.props.match.params.id !== undefined) {
      this.getContactDetail(this.props.match.params.id);
    }
  }


  getContactDetail(id) {
    ContactApi.getContactDetail(id).then(
      ({ response }) => {
        this.setState({ contact: response.data.value });
      }
    )
      .catch((error) => {
        this.setState({ messageError: error.stack });
        toast.error('Internal Server Error');
      });
  }

  saveContact(values) {
    return ContactApi.updateContact(values)
      .then(
        ({ response }) => {
          toast.success('Contact save successfully.');
          return response;
        }
      )
      .catch((error) => {
        this.setState({ messageError: error.stack });
        toast.error('Internal Server Error');
      });
  }

  saveMeetingProgress(values) {
    return ContactApi.updateMeetingProgress(values)
      .then(
        (response) => {
          toast.success('Meeting Progress save successfully.');
          this.setState({ contact: response.data.value });
        }
      )
      .catch((error) => {
        this.setState({ messageError: error.stack });
        toast.error('Internal Server Error');
      });
  }

  saveComment(values) {
    return ContactApi.updateComment(values)
      .then(
        (response) => {
          toast.success('Meeting Progress save successfully.');
          const comments = [Object.assign({}, response.data.value), ...this.state.contact.contactComments];
          this.setState(Object.assign(this.state.contact, { contactComments: comments }));
        }
      )
      .catch((error) => {
        this.setState({ messageError: error.stack });
        toast.error('Internal Server Error');
      });
  }

  saveAttachmentInfo(attachInfo) {
    debugger;
    ContactApi.updateCommentAttach(attachInfo)
      .then(
        (response) => {
          debugger;
          toast.success('Attach save successfully.');

          return response;
        }
      )
      .catch((error) => {
        toast.error('Internal Server Error');
      });
  }

  render() {
    const { messageError, contact } = this.state;
    return (
      <div>
        {messageError && <h3 className='error'>{messageError}</h3>}
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.setTab('1'); }}
            >
              Basic Information
             </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.setTab('2'); }}
            >
              Comment
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <ContactDetailForm onSubmit={this.saveContact} initialValues={contact} />
            <ContactMeetingRequestList contactMeetingProgresses={contact.contactMeetingProgresses} contactId={contact.id} onSubmitMeetingRequest={this.saveMeetingProgress} />
          </TabPane>
          <TabPane tabId="2">
            <ContactCommentList contactComments={contact.contactComments} contactId={contact.id} onSubmitComment={this.saveComment} onUpdateCommentAttachment={this.saveAttachmentInfo} />
          </TabPane>
        </TabContent>

      </div>
      // <div>
      //   {messageError && <h3 className='error'>{messageError}</h3>}
      //   <ContactDetailForm onSubmit={this.saveContact} initialValues={contact} />
      //   <ContactMeetingRequestList contactMeetingProgresses={contact.contactMeetingProgresses} contactId={contact.id} onSubmitMeetingRequest={this.saveMeetingProgress} />
      // </div>

    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetailPage);
