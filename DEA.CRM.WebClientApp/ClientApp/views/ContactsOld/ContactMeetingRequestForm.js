import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { Field, reduxForm } from 'redux-form'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as validator from '../../utility/formUtility';
import renderDatePicker from '../../Components/renderDatePicker';
import renderField from '../../Components/renderField';


class ContactMeetingRequestForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      backdrop: true,
      messageError: '',
      contactId: 0
    };
    // this.toggle = this.toggle.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }

  // toggle() {
  //   this.setState({
  //     modal: !this.state.modal
  //   });
  // }


  getContractMeetingDetail(id) {
    ContactApi.getContactMeetingDetail(id).then(
      ({ response }) => {
        this.setState({ contactMeeting: response.data.value });
      }
    )
      .catch((error) => {
        this.setState({ messageError: error.stack });
        toast.error('Internal Server Error');
      });
  }


  componentWillReceiveProps(nextProps) {
    // if (this.props.initialValues.contactId != nextProps.initialValues.contactId) {
    //   this.setState({
    //     contactId: nextProps.initialValues.contactId
    //   })
    //   this.forceUpdate();
    // };
  }



  render() {
    const { handleSubmit, pristine, reset, submitting, initialValues, disabledAdd } = this.props;
    return (
      <div>
        {/* <Button color="danger" onClick={this.toggle} disabled={disabledAdd}>Add</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} backdrop={false} size="lg"> */}
        <form onSubmit={handleSubmit}>
          <ModalHeader toggle={this.toggle}>Meeting Progresss</ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12" sm="12" md="6" lg="6">
                <Field
                  name="carOfInterest"
                  type="text"
                  component={renderField}
                  label="CarOfInterest"
                  validate={[validator.required, validator.maxLength15]}
                  warn={validator.alphaNumeric}
                />
              </Col>
              <Col xs="12" sm="12" md="6" lg="6">
                <Field
                  name="carColorOfInterest"
                  type="text"
                  component={renderField}
                  label="CarColorOfInterest"
                  validate={[validator.required, validator.maxLength15]}
                  warn={validator.alphaNumeric}
                />
              </Col>
            </Row>
            <Row>
              <Col xs="12" sm="12" md="6" lg="6">
                <FormGroup>
                  <Label htmlFor="DateOfMeeting">Meeting Date</Label>
                  <Field name="dateOfMeeting" showTime={false} component={renderDatePicker} />
                </FormGroup>
              </Col>
              <Col xs="12" sm="12" md="6" lg="6">
                <Field
                  name="description"
                  type="text"
                  component={renderField}
                  label="Description"
                />
              </Col>
            </Row>
            <Row>
              <Col xs="12" sm="12" md="6" lg="6">
                <FormGroup>
                  <Field
                    name="meetingStatus"
                    type="text"
                    component={renderField}
                    label="MeetingStatus"
                  />

                </FormGroup>
              </Col>
              <Col xs="12" sm="12" md="6" lg="6">
                <Field
                  name="otherNote"
                  type="text"
                  component={renderField}
                  label="OtherNote"
                />
              </Col>
            </Row>

          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="primary">OK</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Cancel</Button>
          </ModalFooter>
        </form>
        {/* </Modal> */}

      </div>

    );
  }
}

export default reduxForm({
  form: 'contactMeetingRequest', // a unique identifier for this form
  enableReinitialize: true
})(ContactMeetingRequestForm)


