import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';

class ContactForm extends React.Component {
    

    render() {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Basic Information</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="dealerShowroom">Dealer/Showroom</Label>
                        <Input type="select" name="dealerShowroom" id="dealerShowroom">
                          <option value="0">Please select</option>
                          <option value="1">Dealer #1</option>
                          <option value="2">Dealer #2</option>
                          <option value="3">Dealer #3</option>
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="createdDate">Meeting Date</Label>
                        <Input type="text" id="createdDate" placeholder="Meeting date" required/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="salesPerson">Sales Person</Label>
                        <Input type="text" id="salesPerson" placeholder="Sales person" required/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="gender">Gender</Label>
                        <Input type="select" name="gender" id="gender">
                          <option value="0">Please select</option>
                          <option value="1">Male</option>
                          <option value="2">Female</option>
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="name">First Name</Label>
                        <Input type="text" id="firstName" placeholder="First name" required/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="name">Last Name</Label>
                        <Input type="text" id="lastName" placeholder="Last name" required/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="homePhone">Home Phone</Label>
                        <Input type="text" id="homePhone" placeholder="Home phone" required/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="mobile">Mobile</Label>
                        <Input type="text" id="mobile" placeholder="Mobile" required/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="6" md="4" lg="4">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" id="email" placeholder="Email" required/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="12" lg="12">
                      <FormGroup>
                        <Label htmlFor="address">Address</Label>
                        <Input type="text" id="address" placeholder="Address" required/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="city">Province/City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Please select</option>
                          <option value="1">HCM</option>
                          <option value="2">HN</option>
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="district">District</Label>
                        <Input type="select" name="district" id="district">
                          <option value="0">Please select</option>
                          <option value="1">Quan 1</option>
                          <option value="2">Quan 2</option>
                          <option value="2">Quan 3</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="leadSource">Lead source</Label>
                        <Input type="select" name="leadSource" id="leadSource">
                          <option value="0">Please select</option>
                          <option value="1">Den showroom</option>
                          <option value="2">Tu internet</option>
                          <option value="2">Test drive</option>
                          <option value="2">VIP customer</option>
                          <option value="2">Referal</option>
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="paymentOptions">Payment options</Label>
                        <Input type="select" name="paymentOptions" id="paymentOptions">
                          <option value="0">Please select</option>
                          <option value="1">Cash</option>
                          {/* <option value="2">Installment</option> */}
                          <option value="2">Credit card</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="occupation">Occupation</Label>
                        <Input type="text" id="occupation" placeholder="Occupation"/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="currentCar">Current car</Label>
                        <Input type="text" id="currentCar" placeholder="Current car"/>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="planBuyDate">Plan buy date</Label>
                        <Input type="text" id="planBuyDate" placeholder="Plan buy date"/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" sm="12" md="6" lg="6">
                      <FormGroup>
                        <Label htmlFor="note">Note</Label>
                        <Input type="text" id="note" placeholder="Note"/>
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {/* <Row>
            <Col xs="12" sm="12" md="12" lg="12">
              <Card>
                <CardHeader>
                  <strong>Sales Progress Information</strong>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Car interested</th>
                        <th>Color</th>
                        <th>Note</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Friderik Dávid</td>
                        <td>Mitsubishi Pajero</td>
                        <td>Black</td>
                        <td>het mau den</td>
                        <td>Hot</td>
                        <td>1/1/2018</td>
                        <td/>
                      </tr>
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row> */}
        </div>
      );
    }
  }
  
  export default ContactForm;