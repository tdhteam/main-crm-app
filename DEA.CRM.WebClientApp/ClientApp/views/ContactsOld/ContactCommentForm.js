import React from 'react';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButton,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';

import { Field, reduxForm } from 'redux-form'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as validator from '../../utility/formUtility';
import renderDatePicker from '../../Components/renderDatePicker';
import renderField from '../../Components/renderField';


class ContactCommentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contactId: 0
        };
    }
    componentWillReceiveProps(nextProps) {
    }

    render() {
        const { handleSubmit, pristine, reset, submitting, initialValues, disabledAdd } = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit}>

                    <Row>
                        <Col xs="12" sm="12" md="12" lg="12">
                            <Field
                                name="commentContent"
                                type="textarea"
                                component={renderField}
                                label="Comment"
                                validate={[validator.required]}
                                rows="9"
                            />
                        </Col>
                    </Row>
                    <Row>

                        <Col xs="12" sm="12" md="12" lg="12">
                            <div className="float-sm-right">
                                <Button type="submit" color="primary" disabled={this.props.disabled}>Post</Button>
                                
                            </div>
                        </Col>

                    </Row>

                </form>
            </div>

        );
    }
}

export default reduxForm({
    form: 'ContactCommentForm', // a unique identifier for this form
    enableReinitialize: true
})(ContactCommentForm)


