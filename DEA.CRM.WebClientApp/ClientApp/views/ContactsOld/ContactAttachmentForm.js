import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { Field, reduxForm } from 'redux-form'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as validator from '../../utility/formUtility';
import renderDatePicker from '../../Components/renderDatePicker';
import renderField from '../../Components/renderField';

export const ContactAttachmentForm = (props) => {
  const { handleSubmit } = props;
  const onFormSubmit = (data) => {
      let formData = new FormData();
      formData.append('name', data.name)
      formData.append('profile_pic', data.profile_pic[0])
      const config = {
          headers: { 'content-type': 'multipart/form-data' }
      }
      const url = 'http://example.com/fileupload/';
      post(url, formData, config)
          .then(function(response) {
              console.log(response);
          })
          .catch(function(error) {
              console.log(error);
          });
  }
  return (
        <form onSubmit={handleSubmit(onFormSubmit)}>
          <div>
            <label>Profile Picture</label>
            <Field name="profile_pic" component="input" type="file"/>
          </div>
          <button type="submit">Submit</button>
        </form>
  )
}

export default reduxForm({
  form: 'ContactAttachmentForm'
})(ContactAttachmentForm)