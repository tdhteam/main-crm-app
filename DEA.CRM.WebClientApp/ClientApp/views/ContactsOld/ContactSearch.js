import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroup,
  Input,
  Button,
  Nav,
  NavLink
} from 'reactstrap';
import { withRouter } from 'react-router-dom';
import ReactTable from "react-table";
import "react-table/react-table.css";


import * as contactActions from '../../contact/contactAction';
import buildQuery from '../../utility/queryString';
import { Translate, getTranslate, getActiveLanguage, setActiveLanguage } from 'react-localize-redux';

const AddContactButton = withRouter(({ history }) => (
  <Button
    type='button' color='primary'
    onClick={() => { history.push('/contact/new') }}
  >
    <Translate id="add">Add</Translate>
  </Button>
));



const SearchButton = (searchContact) => {
}

class ContactSearch extends React.Component {

  constructor() {
    super();
    this.state = {
      searchRequest: {
        orderAsc: '',
        orderBy: [],
        pageNumber: 1,
        pageSize: 10,

      },
      sorted: [],
      filtered: []

    };
    this.addContact = this.addContact.bind(this);
    this.searchContact = this.searchContact.bind(this);
    this.onChange = this.onChange.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
  }

  componentDidMount() {
    this.props.actions.searchContactRequestAction(this.state.searchRequest);
  }

  addContact() {

  }
  changeLanguage() {
    if (this.props.currentLanguage == 'en') {
      this.props.setActiveLanguage('vn');
    } else {
      this.props.setActiveLanguage('en');
    }
  }

  searchContact() {
    //build request for filter and sorted.
    if (this.state.sorted.length > 0) {
      let sort = { orderAsc: !this.state.sorted[0].desc, orderBy: this.state.sorted[0].id };
      this.setState(Object.assign(this.state.searchRequest, sort));
    } else {
      let sort = { orderAsc: false, orderBy: '' };
      this.setState(Object.assign(this.state.searchRequest, sort));
    }

    let request = Object.assign({}, this.state.searchRequest);

    if (this.state.filtered.length > 0) {
      this.state.filtered.forEach(function (element) {
        request = Object.assign(request, { [element.id]: element.value });
      }, this);
    }

    this.props.actions.searchContactRequestAction(request);
  }

  onChange(name, data) {
    // //    this.setState(Object.assign( this.state.searchRequest, data));
    //     if(name === 'sorted'){
    //       this.setState(Object.assign( this.state, data));
    //       // let sort = { orderAsc : !data.sorted.desc, orderBy : data.sorted.id } ;
    //       // this.setState(Object.assign( this.state.searchRequest, sort));

    //     }else if(name === 'filtered'){
    //        this.setState(Object.assign( this.state, data));
    //         data.filtered.forEach(function(element) {
    //          this.setState(Object.assign( this.state.searchRequest, {[element.id]:element.value}));
    //       }, this);
    //     }
    //     else{
    //        this.setState(Object.assign( this.state.searchRequest, data));
    //     }

    if (name === 'sorted' || name === 'filtered') {
      this.setState(Object.assign(this.state, data));
    }
    else {
      this.setState(Object.assign(this.state.searchRequest, data));
    }
    if (name != "filtered") {
      this.searchContact();
    }
  }

  render() {
    const result = this.props.contactPageInfo;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <AddContactButton />{" "}
                <Button type='button' color='primary' onClick={this.searchContact}>Search</Button>
              </CardHeader>
              <CardBody>
                <ReactTable
                  manual
                  // Controlled props
                  sorted={this.state.searchRequest.sorted}
                  page={result.pageNumber - 1}
                  pages={result.pageTotal}
                  pageSize={this.state.searchRequest.pageSize}
                  filtered={this.state.searchRequest.filtered}
                  // Callbacks
                  onSortedChange={sorted => this.onChange('sorted', { sorted: sorted })}
                  onPageChange={page => this.onChange('page', { pageNumber: page + 1 })}
                  onPageSizeChange={(pageSize, page) => this.onChange('pageSize', { pageNumber: page + 1, pageSize: pageSize })}
                  onFilteredChange={filtered => this.onChange('filtered', { filtered })}

                  data={result.resultSet}
                  filterable

                  columns={[
                    {
                      Header: "Họ",
                      accessor: "firstName",
                      Cell: row => {
                        return (
                          <div>
                            <Nav>
                              <NavLink href={`/contact/edit/${row.original.id}`}>{row.value}</NavLink>
                            </Nav>
                            {/* <Link to="{`/contacts/edit/${row.id}`}">{row.value}</Link> */}

                          </div>)
                      }
                    },

                    {
                      Header: "Tên",
                      id: "lastName",
                      accessor: "lastName",
                      Cell: row => {
                        return (
                          <Nav>
                            <NavLink href={`/contact/edit/${row.original.id}`}>{row.value}</NavLink>
                          </Nav>
                        )
                      }
                    },
                    {
                      Header: "Sex",
                      accessor: "gender"
                    },
                    {
                      Header: "Email",
                      accessor: "email"
                    },
                    {
                      Header: "Phone",
                      accessor: "mobilePhone"
                    },
                    {
                      Header: "DateOfBirth",
                      accessor: "dateOfBirth"
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    )
  }
}

// ContactSearch.propTypes = {

//   contactSearchResult: PropTypes.object.isRequired,
//   actions: PropTypes.object.isRequired
// };

function mapStateToProps(state, ownProps) {
  return {
    contactPageInfo: state.contactPageInfo,
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale).code
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(contactActions, dispatch),
    setActiveLanguage: (l) => dispatch(setActiveLanguage(l))
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactSearch);
