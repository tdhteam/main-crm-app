import React from 'react';
import {withRouter} from 'react-router-dom';
import ShowroomApi from '../../../api/ShowroomApi';
import {toast} from 'react-toastify';
import ShowroomDetailForm from './ShowroomDetailForm';

class ManageShowroomDetailPage extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      formData: null,
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          return ShowroomApi.updateShowroomDetail(values).then(
            ({response}) => {
              toast.success('Showroom saved successfully.');
              return response;
            }
          );
        },
        navigateBack: () => {
          this.props.history.push('/setting/showrooms');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Showroom saved error.');
        },
        showValues: (data) => {
          console.log(data);
        }
      }
    };
  }

  componentDidMount() {
    let {showroomId} = this.props.match.params;
    if (showroomId === undefined)
      showroomId = 0;
    ShowroomApi.getById({
      showroomId
    })
      .then(({response}) => {
        this.setState({...response.data, isLoading: false});
      })
      .catch(({error}) => {
        console.log(error);
      });
  }

  render() {

    if (this.state.isLoading){
      return <div className="animated fadeIn">Loading...</div>;
    }

    return (
      <div className="animated fadeIn">
        <ShowroomDetailForm {...this.state}/>
      </div>
    );
  }
}

export default withRouter(ManageShowroomDetailPage);
