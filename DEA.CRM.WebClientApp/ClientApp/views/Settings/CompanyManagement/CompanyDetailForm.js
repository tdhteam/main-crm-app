import React from 'react';
import { Formik } from 'formik';
import {
  Table,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  ButtonGroup,
  Button
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import LaddaButton, { SLIDE_LEFT } from 'react-ladda';
import '../../../../scss/app/_app_company.scss'

const CompanyDetailForm = ({ formData, formActions }) => (
  <Formik
    initialValues={formData}
    validate={values => {
      // same as above, but feel free to move this into a class method now.
      let errors = {};
      if (!values.name) {
        errors.name = 'Required';
      }
      return errors;
    }}
    onSubmit={(values, {setSubmitting, setErrors, setValues }) => {
      formActions.handleFormSave(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      }).catch(
        ({error}) => {
          formActions.showFriendlyError(error);
        }
      );
    }}
    render={({
               values,
               errors,
               touched,
               dirty,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form>
        <Row className="justify-content-center">
          <Col md="6">
            <Card className="mx-4">
              <CardHeader className="action-bar">
                <ButtonGroup>
                  <LaddaButton
                    className="btn btn-success btn-ladda"
                    loading={isSubmitting}
                    onClick={handleSubmit}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                    disabled={isSubmitting || !dirty}
                  >
                    Save
                  </LaddaButton>
                  <LaddaButton
                    type="button"
                    className="btn btn-cancel btn-ladda"
                    loading={isSubmitting}
                    onClick={formActions.navigateBack}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                    disabled={isSubmitting}
                  >
                    Cancel
                  </LaddaButton>
                </ButtonGroup>
              </CardHeader>
              <CardHeader>
                <strong>Company Details</strong>
              </CardHeader>
              <CardBody className="p-4">
                <FormGroup className="mb-3">
                  <Label htmlFor="companyCode">companyCode</Label>
                  <Input type="text" id='companyCode' disabled='disabled' placeholder='companyCode' defaultValue={values.companyCode || ''}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="name">companyName</Label>
                  <Input type="text" id="name" placeholder="companyName" required value={values.name} onChange={handleChange} />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="address">address</Label>
                  <Input type="text" id="address" placeholder="address" required value={values.address} onChange={handleChange}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="city">city</Label>
                  <Input type="select" name="city" id="city" required value={values.city} onChange={handleChange}>
                    <option value>--- select ---</option>
                    <option value="0">city #1</option>
                    <option value="1">city #2</option>
                  </Input>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="state">state</Label>
                  <Input type="select" name="state" id="state" required value={values.state} onChange={handleChange}>
                    <option value>--- select ---</option>
                    <option value="0">state #1</option>
                    <option value="1">state #2</option>
                  </Input>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="country">country</Label>
                  <Input type="select" name="country" id="country" required value={values.country} onChange={handleChange}>
                    <option value>--- select ---</option>
                    <option value="0">country #1</option>
                    <option value="1">country #2</option>
                  </Input>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="postalCode">postalCode</Label>
                  <Input type="text" id="postalCode" placeholder="postalCode" required value={values.postalCode} onChange={handleChange}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="phone">phone</Label>
                  <Input type="text" id="phone" placeholder="phone" required value={values.phone} onChange={handleChange}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="fax">fax</Label>
                  <Input type="text" id="fax" placeholder="fax" required value={values.fax} onChange={handleChange}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="webSite">webSite</Label>
                  <Input type="text" id="webSite" placeholder="webSite" required value={values.webSite} onChange={handleChange}/>
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="logoRelativeUrl">logoRelativeUrl</Label>
                  <Input type="file" name="logoRelativeUrl" id="logoRelativeUrl" required value={values.logoRelativeUrl} onChange={handleChange}/>
                </FormGroup>

              </CardBody>
              <CardHeader className="action-bar">
                <ButtonGroup>
                  <LaddaButton
                    className="btn btn-success btn-ladda"
                    loading={isSubmitting}
                    onClick={handleSubmit}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                    disabled={isSubmitting || !dirty}
                  >
                    Save
                  </LaddaButton>
                  <LaddaButton
                    type="button"
                    className="btn btn-cancel btn-ladda"
                    loading={isSubmitting}
                    onClick={formActions.navigateBack}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                    disabled={isSubmitting}
                  >
                    Cancel
                  </LaddaButton>
                </ButtonGroup>
              </CardHeader>
            </Card>
          </Col>
        </Row>
      </form>
    )}
  />
);

export default withRouter(CompanyDetailForm);
