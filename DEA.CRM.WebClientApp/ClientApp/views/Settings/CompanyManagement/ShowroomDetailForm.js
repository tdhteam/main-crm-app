import React from 'react';
import {Formik, Field, FieldArray} from 'formik';
import {
  Table,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  ButtonGroup,
  Button,
  FormText
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import LaddaButton, { SLIDE_LEFT } from 'react-ladda';
import '../../../../scss/app/_app_company.scss'

import AppFormActionButton from '../../../components/AppFormActionButton/AppFormActionButton';

import { Translate } from 'react-localize-redux';
import {connect} from "react-redux";
import StaticKeyValueLookup from '../../../components/AppLookup/StaticKeyValueLookup';

const ShowroomDetailForm = ({ formData, pickListValues, formActions }) => (
  <Formik
    initialValues={formData}
    validate={values => {
      // same as above, but feel free to move this into a class method now.
      let errors = {};
      if (!values.name) {
        errors.name = 'Required';
      }
      return errors;
    }}
    onSubmit={(values, {setSubmitting, setErrors, setValues }) => {
      formActions.handleFormSave(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      }).catch(
        ({error}) => {
          formActions.showFriendlyError(error);
        }
      );
    }}
    render={({
               values,
               errors,
               touched,
               setFieldValue,
               setValues,
               dirty,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form>
        <Row className="justify-content-center">

          <Col md="6">
            <Card className="mx-4">
              <CardHeader className="action-bar text-right">
                <AppFormActionButton
                  dirty={dirty}
                  isSubmitting={isSubmitting}
                  handleSubmit={handleSubmit}
                  navigateBack={formActions.navigateBack}
                />
              </CardHeader>
              <CardHeader>
                <strong>Showroom Details</strong>
              </CardHeader>
              <CardBody className="p-4">
                <FormGroup className="mb-3">
                  <Label for="dealerId"><Translate id="Shared.dealer">Dealer</Translate></Label>
                  <StaticKeyValueLookup
                    name="dealerId"
                    className="form-control"
                    selected={[values.dealerName || '']}
                    options={pickListValues.dealers}
                    handleChange={(e) => {
                      console.log(e);
                      if (e && e.length > 0) {
                        setFieldValue("dealerId", e[0].key);
                        setFieldValue("dealerName", e[0].value);
                      }
                    }}
                  />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="name"><Translate id="Shared.name">Name</Translate></Label>
                  <Input type="text" id="name" placeholder="name" value={values.name || ''}  required onChange={handleChange} />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="address"><Translate id="Shared.address">Address</Translate></Label>
                  <Input type="text" id="address" placeholder="address" value={values.address || ''} onChange={handleChange} />
                </FormGroup>



                <FormGroup className="mb-3">
                  <Label htmlFor="city"><Translate id="Shared.city">City</Translate></Label>
                  <StaticKeyValueLookup
                    name="city"
                    className="form-control"
                    selected={[values.city || '']}
                    options={pickListValues.cities}
                    handleChange={(e) => {
                      if (e && e.length > 0) {
                        setFieldValue("city", e[0].value);
                        setFieldValue("cityKey", e[0].key);
                      }
                    }}
                  />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="state"><Translate id="Shared.district">District</Translate></Label>
                  <StaticKeyValueLookup
                    name="state"
                    className="form-control"
                    selected={[values.state || '']}
                    parentFilterByValue={values.cityKey}
                    options={pickListValues.districts}
                    handleChange={(e) => {
                      if (e && e.length > 0) {
                        setFieldValue("state", e[0].value);
                      }
                    }}
                  />
                </FormGroup>



                <FormGroup className="mb-3">
                  <Label htmlFor="postalCode"><Translate id="Shared.postalCode">Postal Code</Translate></Label>
                  <Input type="text" id="postalCode" placeholder="postalCode" value={values.postalCode || ''} onChange={handleChange} />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="phone"><Translate id="Shared.phone">Phone</Translate></Label>
                  <Input type="text" id="phone" placeholder="phone" value={values.phone || ''} onChange={handleChange} />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="fax"><Translate id="Shared.fax">Fax</Translate></Label>
                  <Input type="text" id="fax" placeholder="fax" value={values.fax || ''} onChange={handleChange} />
                </FormGroup>

                <FormGroup className="mb-3">
                  <Label htmlFor="webSite"><Translate id="Shared.website">Website</Translate></Label>
                  <Input type="text" id="webSite" placeholder="webSite" value={values.webSite || ''} onChange={handleChange} />
                </FormGroup>

              </CardBody>
              <CardHeader>
                <AppFormActionButton
                  dirty={dirty}
                  isSubmitting={isSubmitting}
                  handleSubmit={handleSubmit}
                  navigateBack={formActions.navigateBack}
                />
              </CardHeader>
            </Card>
          </Col>
        </Row>
      </form>
    )}
  />
);

function mapStateToProps(state) {
  return {

  };
}
export default connect(mapStateToProps)(ShowroomDetailForm);
