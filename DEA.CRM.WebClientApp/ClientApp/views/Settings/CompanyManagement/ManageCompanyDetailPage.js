import React from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {toast} from 'react-toastify';

//import * as companyActions from "../../../company/manageCompanyAction";
import CompanyDetailForm from './CompanyDetailForm';

import CompanyApi from '../../../api/companyApi';
import * as manageRoleActions from "../../../users-management/manageRoleAction";

class ManageCompanyDetailPage extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      formData: null,
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          return CompanyApi.updateCompanyDetail(values).then(
            ({response}) => {
              toast.success('Company save successfully.');
              return response;
            }
          );
        },
        navigateBack: () => {
          this.props.history.push('/setting');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Company save error.');
        },
        showValues: (data) => {
          console.log(data);
        }
      }
    };
  }

  componentDidMount() {
      // Edit
    CompanyApi.getFirstCompany({
      })
        .then(({response}) => {
          this.setState({
            formData: response.data,
            isLoading: false
          });
      })
        .catch(({error}) => {
          console.log(error);
        });
  }

  render() {

    if (this.state.isLoading){
      return <div className="animated fadeIn">Loading...</div>;
    }

    return (
      <div className="animated fadeIn">
        <CompanyDetailForm {...this.state}/>
      </div>
    );
  }
}

export default withRouter(ManageCompanyDetailPage);
