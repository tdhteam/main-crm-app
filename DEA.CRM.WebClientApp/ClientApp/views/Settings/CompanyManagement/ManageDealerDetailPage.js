import React from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {toast} from 'react-toastify';

import DealerDetailForm from './DealerDetailForm';

import DealerApi from '../../../api/dealerApi';

class ManageDealerDetailPage extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      formData: null,
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          return DealerApi.updateDealerDetail(values).then(
            ({response}) => {
              toast.success('Dealer save successfully.');
              return response;
            }
          ).catch(error => {
            console.log(error);
          });
        },
        navigateBack: () => {
          this.props.history.push('/setting/dealer');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Dealer save error.');
        }
      }
    };
  }

  componentDidMount() {
    let {dealerId} = this.props.match.params;
    if (dealerId === undefined)
      dealerId = 0;
    DealerApi.getById({
      dealerId
    })
      .then(({response}) => {
        this.setState({
          formData: response.data,
          isLoading: false
        });
      })
      .catch(({error}) => {
        console.log(error);
      });
  }

  render() {

    if (this.state.isLoading){
      return <div className="animated fadeIn">Loading...</div>;
    }

    return (
      <div className="animated fadeIn">
        <DealerDetailForm {...this.state}/>
      </div>
    );
  }
}

export default withRouter(ManageDealerDetailPage);
