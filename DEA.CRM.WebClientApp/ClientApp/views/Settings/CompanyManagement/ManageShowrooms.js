import React, {Component,PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroup,
  Input,
  Button
} from 'reactstrap';
import { withRouter } from 'react-router-dom';
import ReactTable from "react-table";
import "react-table/react-table.css";

import * as showroomActions from '../../../actions/showroomAction';
import { Translate, getTranslate, getActiveLanguage, setActiveLanguage  } from 'react-localize-redux';


const AddShowroomButton = withRouter(({ history }) => (
  <Button
    type='button' color='primary'
    onClick={() => { history.push('showroom/new') }}
  >
    <Translate id="add">Add</Translate>
  </Button>
));


class ManageShowrooms extends React.Component{
  constructor() {
    super();
    this.state = {
      searchRequest:{
        orderAsc:'',
        orderBy: [],
        pageNumber: 1,
        pageSize: 10,
      },
      sorted:[],
      filtered: []

    };
    this.searchShowroom = this.searchShowroom.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.actions.searchShowroomRequestAction(this.state.searchRequest);
  }

  searchDealer(){
    //build request for filter and sorted.
    if(this.state.sorted.length>0)
    {
      let sort = { orderAsc : !this.state.sorted[0].desc, orderBy : this.state.sorted[0].id } ;
      this.setState(Object.assign( this.state.searchRequest, sort));
    }else{
      let sort = { orderAsc : false, orderBy : '' } ;
      this.setState(Object.assign( this.state.searchRequest, sort));
    }

    let request = Object.assign({},this.state.searchRequest);

    if(this.state.filtered.length>0)
    {
      this.state.filtered.forEach(function(element) {
        request = Object.assign( request, {[element.id]:element.value});
      }, this);
    }

    this.props.actions.searchShowroomRequestAction(request);
  }

  onChange(name, data) {
    if(name === 'sorted' || name === 'filtered') {
      this.setState(Object.assign( this.state, data));
    }
    else{
      this.setState(Object.assign( this.state.searchRequest, data));
    }
    if(name != "filtered")
    {
      this.searchShowroom();
    }
  }



  render() {
    const result  = this.props.showroomPageInfo;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <AddShowroomButton />
                <Button  type='button' color='primary'  onClick = {this.searchShowroom}>{this.props.translate('search')}</Button>
              </CardHeader>
              <CardBody>
                <ReactTable
                  manual
                  // Controlled props
                  sorted={this.state.searchRequest.sorted}
                  page={result.pageNumber-1}
                  pages={result.pageTotal}
                  pageSize={this.state.searchRequest.pageSize}
                  filtered={this.state.searchRequest.filtered}
                  // Callbacks
                  onSortedChange={sorted => this.onChange('sorted',{sorted:sorted})}
                  onPageChange={page => this.onChange('page',{pageNumber:page+1})}
                  onPageSizeChange={(pageSize, page) => this.onChange('pageSize',{pageNumber:page+1,pageSize:pageSize})}
                  onFilteredChange={filtered => this.onChange('filtered',{filtered})}
                  data={result.resultSet}
                  filterable
                  columns={[
                    {
                      Header: "Name",
                      accessor: "name"
                    },

                    {
                      Header: "Address",
                      accessor: "address"

                    },
                    {
                      Header: "City",
                      accessor: "city"
                    },
                    {
                      Header: "State",
                      accessor: "state"
                    },
                    {
                      Header: "Postal Code",
                      accessor: "postalCode"
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }

}


function mapStateToProps(state, ownProps) {
  return {
    showroomPageInfo: state.showroomPageInfo,
    translate: getTranslate(state.locale)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(showroomActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageShowrooms);



