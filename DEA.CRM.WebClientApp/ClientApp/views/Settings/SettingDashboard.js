import React from 'react';
import { Link } from 'react-router-dom';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  ListGroup, ListGroupItem
} from 'reactstrap';

class SettingDashboard extends React.Component{

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/><strong>Users and Roles</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  <ListGroupItem><Link to="/setting/role">Manage Roles</Link></ListGroupItem>
                  <ListGroupItem><Link to="/setting/user">Manage Users</Link></ListGroupItem>
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/><strong>Company Information</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  <ListGroupItem><Link to="/setting/company/">Company Detail</Link></ListGroupItem>
                  <ListGroupItem><Link to="/setting/dealer">Manage Dealers</Link></ListGroupItem>
                  <ListGroupItem><Link to="/setting/showroom">Manage Showrooms</Link></ListGroupItem>
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/><strong>Global Configuration</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  <ListGroupItem><Link to="/setting/role">Global Settings</Link></ListGroupItem>
                  <ListGroupItem><Link to="/setting/user">Application Logs</Link></ListGroupItem>
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/><strong>CRM Configurations</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  <ListGroupItem><Link to="/setting/company">Picklist Editor</Link></ListGroupItem>
                  <ListGroupItem><Link to="/document">Documents</Link></ListGroupItem>
                  <ListGroupItem><Link to="/large-document">Large Document Uploads</Link></ListGroupItem>
                  <ListGroupItem><Link to="/document-manager">Document Manager</Link></ListGroupItem>
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SettingDashboard;
