import React from 'react';
import {withRouter} from "react-router-dom";
import {Formik, Field, FieldArray} from 'formik';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  FormText,
  Table,
  Button,
  InputGroup,
  InputGroupAddon
} from 'reactstrap';

import LaddaButton, {SLIDE_LEFT} from 'react-ladda';

const UserDetailForm = ({formData, pickListValues, formActions}) => (
  <Formik
    initialValues={formData}
    validate={values => formActions.validateForm(values)}
    onSubmit={(values, {setSubmitting, setErrors, setValues}) => {
      formActions.handleFormSave(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      })
      .catch(
        ({error}) => {
          formActions.showFriendlyError(error);
        }
      );
    }}
    render={({
               values,
               errors,
               touched,
               dirty,
               setFieldValue,
               setValues,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form onSubmit={handleSubmit}>
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <LaddaButton
              className="btn btn-success btn-ladda"
              loading={isSubmitting}
              onClick={handleSubmit}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting || !dirty}
            >
              Save
            </LaddaButton>{' '}
            <LaddaButton
              type="button"
              className="btn btn-cancel btn-ladda"
              loading={isSubmitting}
              onClick={formActions.navigateBack}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting}
            >
              Cancel
            </LaddaButton>
          </Col>
        </Row>
        {/*Users Login and Role*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Users Login and Role</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="userName">User Name <span className="redColor">*</span></Label>
                      <Input type="text" name="userName" onChange={handleChange} value={values.userName || ''}
                             placeholder="User name"/>
                      {touched.userName && errors.userName && <div className="redColor">{errors.userName}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="email">Email <span className="redColor">*</span></Label>
                      <Input type="email" name="email" onChange={handleChange} value={values.email || ''}
                             placeholder="Email"/>
                      {touched.email && errors.email && <div className="redColor">{errors.email}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="firstName">First Name <span className="redColor">*</span></Label>
                      <Input type="text" name="firstName" onChange={handleChange} value={values.firstName || ''}
                             placeholder="First name"/>
                      {touched.firstName && errors.firstName && <div className="redColor">{errors.firstName}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="lastName">Last Name <span className="redColor">*</span></Label>
                      <Input type="text" name="lastName" onChange={handleChange} value={values.lastName || ''}
                             placeholder="Last Name"/>
                      {touched.lastName && errors.lastName && <div className="redColor">{errors.lastName}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="password">Password <span className="redColor">*</span></Label>
                      <Input type="password" name="password" onChange={handleChange} value={values.password || ''}
                             placeholder="Password"/>
                      {touched.password && errors.password && <div className="redColor">{errors.password}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="passwordConfirmation">Confirm Password <span className="redColor">*</span></Label>
                      <Input type="password" name="passwordConfirmation" onChange={handleChange}
                             value={values.passwordConfirmation || ''}
                             placeholder="Confirm Password"/>
                      {touched.passwordConfirmation && errors.passwordConfirmation && <div className="redColor">{errors.passwordConfirmation}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup check>
                      <Label htmlFor="isAdmin" check>
                        <Input type="checkbox" name="isAdmin" onChange={handleChange} value={values.isAdmin || ''}
                               placeholder="Is Admin?"/>
                        {' Admin'}
                      </Label>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="roleId">Role <span className="redColor">*</span></Label>
                      <Input type="select" name="roleId" onChange={handleChange} value={values.roleId || ''}
                             placeholder="Role">
                        <option>Select a role</option>
                        {pickListValues.Roles.map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.roleId && errors.roleId && <div className="redColor">{errors.roleId}</div>}
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Company Access*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Company Access</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup check>
                      <Label htmlFor="accessAllCompanies" check>
                        <Input type="checkbox" name="accessAllCompanies" onChange={handleChange}
                               value={values.accessAllCompanies || ''}/>
                        {' Access All Companies?'}
                      </Label>
                      {touched.accessAllCompanies && errors.accessAllCompanies &&
                      <div>{errors.accessAllCompanies}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="defaultCompanyId">Default Company</Label>
                      <Input type="select" name="defaultCompanyId" onChange={handleChange}
                             value={values.defaultCompanyId || ''}>
                        {pickListValues.Companies.map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.defaultCompanyId && errors.defaultCompanyId && <div>{errors.defaultCompanyId}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <FieldArray
                  name="userInfoToCompany"
                  render={arrayHelpers => (
                    <Row>
                      <Col xs="12" sm="12" md="12" lg="12">
                        <Row>
                          <Col xs="8" sm="4" md="4" lg="4">
                            <FormGroup>
                              <Input type="select" name="selectCompanyId" onChange={handleChange}
                                     value={values.selectCompanyId || ''}>
                                <option>Select a company</option>
                                {pickListValues.Companies.map((item, index) => (
                                  <option key={index} value={index}>{item.value}</option>
                                ))}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col xs="4" sm="2" md="2" lg="2">
                            <Button color="info" onClick={() => {
                              const company = pickListValues.Companies[values.selectCompanyId];
                              arrayHelpers.push({CompanyId: company.key, companyName: company.value});
                            }}
                            disabled={!values.selectCompanyId}>
                              Add
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs="12" sm="6" md="6" lg="6">
                            <Table striped responsive>
                              <thead>
                              <tr>
                                <th colSpan="2">Company</th>
                              </tr>
                              </thead>
                              <tbody>
                              {values.userInfoToCompany && values.userInfoToCompany.map((item, index )=> (
                                <tr key={index}>
                                  <td>
                                    <Button className="btn-sm" color="danger" onClick={() => arrayHelpers.remove(index)}>Remove</Button>
                                  </td>
                                  <td>{item.companyName}</td>
                                </tr>
                              ))}
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  )}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Dealer Access*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Dealer Access</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup check>
                      <Label htmlFor="accessAllDealers" check>
                        <Input type="checkbox" name="accessAllDealers" onChange={handleChange}
                               value={values.accessAllDealers || ''}/>
                        {' Access All Dealers?'}
                      </Label>
                      {touched.accessAllDealers && errors.accessAllDealers && <div>{errors.accessAllDealers}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="defaultDealerId">Default Dealer</Label>
                      <Input type="select" name="defaultDealerId" onChange={handleChange}
                             value={values.defaultDealerId || ''}>
                        <option>Select a dealer</option>
                        {pickListValues.Dealers.filter(d => parseInt(d.parentKey) === values.defaultCompanyId).map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.defaultDealerId && errors.defaultDealerId && <div>{errors.defaultDealerId}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <FieldArray
                  name="userInfoToDealer"
                  render={arrayHelpers => (
                    <Row>
                      <Col xs="12" sm="12" md="12" lg="12">
                        <Row>
                          <Col xs="8" sm="4" md="4" lg="4">
                            <FormGroup>
                              <Input type="select" name="selectDealerId" onChange={handleChange}
                                     value={values.selectDealerId || ''}>
                                <option>Select a dealer</option>
                                {pickListValues.Dealers.map((item, index) => (
                                  <option key={index} value={index}>{item.value}</option>
                                ))}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col xs="4" sm="2" md="2" lg="2">
                            <Button color="info" onClick={() => {
                              const dealer = pickListValues.Dealers[values.selectDealerId];
                              arrayHelpers.push({dealerId: dealer.key, dealerName: dealer.value});
                            }}
                            disabled={!values.selectDealerId}>
                              Add
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs="12" sm="6" md="6" lg="6">
                            <Table striped responsive>
                              <thead>
                              <tr>
                                <th colSpan="2">Dealer</th>
                              </tr>
                              </thead>
                              <tbody>
                              {values.userInfoToDealer && values.userInfoToDealer.map((item, index )=> (
                                <tr key={index}>
                                  <td>
                                    <Button className="btn-sm" color="danger" onClick={() => arrayHelpers.remove(index)}>Remove</Button>
                                  </td>
                                  <td>{item.dealerName}</td>
                                </tr>
                              ))}
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  )}
                >
                </FieldArray>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Showroom Access*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Showroom Access</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup check>
                      <Label htmlFor="accessAllShowrooms" check>
                        <Input type="checkbox" name="accessAllShowrooms" onChange={handleChange}
                               value={values.accessAllShowrooms || ''}/>
                        {' Access All Showrooms?'}
                      </Label>
                      {touched.accessAllShowrooms && errors.accessAllShowrooms && <div>{errors.accessAllShowrooms}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="defaultShowroomId">Default Showroom</Label>
                      <Input type="select" name="defaultShowroomId" onChange={handleChange}
                             value={values.defaultShowroomId || ''}>
                        <option>Select a showroom</option>
                        {pickListValues.Showrooms.filter(sr => sr.parentKey === values.defaultDealerId).map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.defaultShowroomId && errors.defaultShowroomId && <div>{errors.defaultShowroomId}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <FieldArray
                  name="userInfoToShowroom"
                  render={arrayHelpers => (
                    <Row>
                      <Col xs="12" sm="12" md="12" lg="12">
                        <Row>
                          <Col xs="8" sm="4" md="4" lg="4">
                            <FormGroup>
                              <Input type="select" name="selectShowroomId" onChange={handleChange}
                                     value={values.selectShowroomId || ''}>
                                <option>Select a showroom</option>
                                {pickListValues.Showrooms
                                  .filter(sr => values.selectDealerId ? sr.parentKey === pickListValues.Dealers[values.selectDealerId].key : false)
                                  .map((item, index) => (
                                  <option key={index} value={index}>{item.value}</option>
                                ))}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col xs="4" sm="2" md="2" lg="2">
                            <Button color="info" onClick={() => {
                              const showroom = pickListValues.Showrooms[values.selectShowroomId];
                              arrayHelpers.push({showroomId: showroom.key, showroomName: showroom.value});
                            }}
                            disabled={!values.selectShowroomId}>
                              Add
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs="12" sm="6" md="6" lg="6">
                            <Table striped responsive>
                              <thead>
                              <tr>
                                <th colSpan="2">Showroom</th>
                              </tr>
                              </thead>
                              <tbody>
                              {values.userInfoToShowroom && values.userInfoToShowroom.map((item, index )=> (
                                <tr key={index}>
                                  <td>
                                    <Button className="btn-sm" color="danger" onClick={() => arrayHelpers.remove(index)}>Remove</Button>
                                  </td>
                                  <td>{item.showroomName}</td>
                                </tr>
                              ))}
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  )}
                >
                </FieldArray>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Currency and Number Field Configuration</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="currency">Currency</Label>
                      <Input type="select" name="currency" onChange={handleChange} value={values.currency || ''}>
                        {pickListValues.Currencies.map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.currency && errors.currency && <div>{errors.currency}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="currencyGroupingPattern">Digit Grouping Pattern</Label>
                      <Input type="select" name="currencyGroupingPattern" onChange={handleChange}
                             value={values.currencyGroupingPattern || ''}>
                        <option value="123,456,189">123,456,189</option>
                      </Input>
                      {touched.currencyGroupingPattern && errors.currencyGroupingPattern &&
                      <div>{errors.currencyGroupingPattern}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="currencyDecimalSeparator">Decimal Separator</Label>
                      <Input type="select" name="currencyDecimalSeparator" onChange={handleChange}
                             value={values.currencyDecimalSeparator || ''}>
                        <option value=".">.</option>
                        <option value=",">,</option>
                      </Input>
                      {touched.currencyDecimalSeparator && errors.currencyDecimalSeparator &&
                      <div>{errors.currencyDecimalSeparator}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="currencyGroupingSeparator">Digit Grouping Separator</Label>
                      <Input type="select" name="currencyGroupingSeparator" onChange={handleChange}
                             value={values.currencyGroupingSeparator || ''}>
                        <option value=".">.</option>
                        <option value=",">,</option>
                      </Input>
                      {touched.currencyGroupingSeparator && errors.currencyGroupingSeparator &&
                      <div>{errors.currencyGroupingSeparator}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="currencySymbolPlacement">Symbol Placement</Label>
                      <Input type="select" name="currencySymbolPlacement" onChange={handleChange}
                             value={values.currencySymbolPlacement || ''}>
                        <option value="$ 1.0">$ 1.0</option>
                        <option value="1.0 $">1.0 $</option>
                      </Input>
                      {touched.currencySymbolPlacement && errors.currencySymbolPlacement &&
                      <div>{errors.currencySymbolPlacement}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="numberOfCurrencyDecimal">Number Of Currency Decimals </Label>
                      <Input type="select" name="numberOfCurrencyDecimal" onChange={handleChange}
                             value={values.numberOfCurrencyDecimal || ''}>
                        {[0, 1, 2, 3, 4, 5].map(item => (
                          <option key={item} value={item}>{item}</option>
                        ))}
                      </Input>
                      {touched.numberOfCurrencyDecimal && errors.numberOfCurrencyDecimal &&
                      <div>{errors.numberOfCurrencyDecimal}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup check>
                      <Label htmlFor="truncateTrailingZero" check>
                        <Input type="checkbox" name="truncateTrailingZero" onChange={handleChange}
                               value={values.truncateTrailingZero || ''}/>
                        {' Truncate Trailing Zeros'}
                      </Label>
                      {touched.truncateTrailingZero && errors.truncateTrailingZero &&
                      <div>{errors.truncateTrailingZero}</div>}
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*More Information*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>More Information</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="title">Title</Label>
                      <Input type="text" name="title" onChange={handleChange} value={values.title || ''}
                             placeholder="Title"/>
                      {touched.title && errors.title && <div>{errors.title}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="fax">Fax</Label>
                      <Input type="text" name="fax" onChange={handleChange} value={values.fax || ''}
                             placeholder="Fax"/>
                      {touched.fax && errors.fax && <div>{errors.fax}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="department">Department</Label>
                      <Input type="text" name="department" onChange={handleChange} value={values.department || ''}
                             placeholder="Department"/>
                      {touched.department && errors.department && <div>{errors.department}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="homePhone">Home Phone</Label>
                      <Input type="text" name="homePhone" onChange={handleChange} value={values.homePhone || ''}
                             placeholder="Home Phone"/>
                      {touched.homePhone && errors.homePhone && <div>{errors.homePhone}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="workPhone">Work Phone</Label>
                      <Input type="text" name="workPhone" onChange={handleChange} value={values.workPhone || ''}
                             placeholder="Work Phone"/>
                      {touched.workPhone && errors.workPhone && <div>{errors.workPhone}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="mobilePhone">Mobile Phone</Label>
                      <Input type="text" name="mobilePhone" onChange={handleChange} value={values.mobilePhone || ''}
                             placeholder="Mobile Phone"/>
                      {touched.mobilePhone && errors.mobilePhone && <div>{errors.mobilePhone}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="otherPhone">Other Phone</Label>
                      <Input type="text" name="otherPhone" onChange={handleChange} value={values.otherPhone || ''}
                             placeholder="Other Phone"/>
                      {touched.otherPhone && errors.otherPhone && <div>{errors.otherPhone}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="preferLanguage">Prefer Language</Label>
                      <Input type="select" name="preferLanguage" onChange={handleChange}
                             value={values.preferLanguage || ''}>
                        {pickListValues.Languages.map(item => (
                          <option key={item.key} value={item.key}>{item.value}</option>
                        ))}
                      </Input>
                      {touched.preferLanguage && errors.preferLanguage && <div>{errors.preferLanguage}</div>}
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*User Address*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>User Address</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressStreet">Street Address</Label>
                      <Input type="text" name="addressStreet" onChange={handleChange} value={values.addressStreet || ''}
                             placeholder="Street Address"/>
                      {touched.addressStreet && errors.addressStreet && <div>{errors.addressStreet}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressDistrict">District</Label>
                      <Input type="text" name="addressDistrict" onChange={handleChange}
                             value={values.addressDistrict || ''}
                             placeholder="District"/>
                      {touched.addressDistrict && errors.addressDistrict && <div>{errors.addressDistrict}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressCity">City</Label>
                      <Input type="text" name="addressCity" onChange={handleChange} value={values.addressCity || ''}
                             placeholder="City"/>
                      {touched.addressCity && errors.addressCity && <div>{errors.addressCity}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressState">State</Label>
                      <Input type="text" name="addressState" onChange={handleChange} value={values.addressState || ''}
                             placeholder="State"/>
                      {touched.addressState && errors.addressState && <div>{errors.addressState}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressCountry">Country</Label>
                      <Input type="text" name="addressCountry" onChange={handleChange}
                             value={values.addressCountry || ''}
                             placeholder="Country"/>
                      {touched.addressCountry && errors.addressCountry && <div>{errors.addressCountry}</div>}
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="addressPostalCode">Postal Code</Label>
                      <Input type="text" name="addressPostalCode" onChange={handleChange}
                             value={values.addressPostalCode || ''}
                             placeholder="Postal Code"/>
                      {touched.addressPostalCode && errors.addressPostalCode && <div>{errors.addressPostalCode}</div>}
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*User Avatar*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>User Avatar</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="12" md="12" lg="12">
                    <FormGroup>
                      <Input type="file" id="userAvatar" name="userAvatar" onChange={(event) => {
                        let reader = new FileReader();
                        const file = event.currentTarget.files[0];
                        reader.onloadend = () => {
                          setFieldValue("userAvatarDataUrl", reader.result);
                          setFieldValue("userAvatarDataFileInfo", {
                              "fileName": file.name,
                              "fileSize": file.size,
                              "contentType": file.type
                          });
                        };
                        reader.readAsDataURL(file);
                      }}/>
                      <FormText color="muted">
                        Existing attachments(images/files) will be replaced
                      </FormText>
                      {values.userAvatarDataUrl && <img src={values.userAvatarDataUrl} className="img-thumbnail mt-2" height={200} width={200}/>}
                      <br/>
                      {values.avatarRelativeUrl && <img src={values.avatarRelativeUrl} className="img-thumbnail mt-2" height={200} width={200}/>}
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <LaddaButton
              className="btn btn-success btn-ladda"
              loading={isSubmitting}
              onClick={handleSubmit}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting || !dirty}
            >
              Save
            </LaddaButton>{' '}
            <LaddaButton
              type="button"
              className="btn btn-cancel btn-ladda"
              loading={isSubmitting}
              onClick={formActions.navigateBack}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting}
            >
              Cancel
            </LaddaButton>
          </Col>
        </Row>
      </form>
    )}
  />
);

export default withRouter(UserDetailForm);
