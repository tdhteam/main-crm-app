import React from 'react';
import {withRouter} from "react-router-dom";
import {Formik, Field, FieldArray} from 'formik';
import {
  Table,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input
} from 'reactstrap';

import LaddaButton, { SLIDE_LEFT } from 'react-ladda';

const RoleDetailForm = ({ formData, formActions }) => (
  <Formik
    initialValues={formData}
    validate={values => {
      // same as above, but feel free to move this into a class method now.
      let errors = {};
      if (!values.name) {
        errors.name = 'Required';
      }
      return errors;
    }}
    onSubmit={(values, {setSubmitting, setErrors, setValues }) => {
      formActions.handleFormSave(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      }).catch(
        ({error}) => {
          formActions.showFriendlyError(error);
        }
      );
    }}
    render={({
               values,
               errors,
               touched,
               dirty,
               setFieldValue,
               setValues,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form>
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <LaddaButton
              className="btn btn-success btn-ladda"
              loading={isSubmitting}
              onClick={handleSubmit}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting || !dirty}
            >
              Save
            </LaddaButton>
            <LaddaButton
              type="button"
              className="btn btn-cancel btn-ladda"
              loading={isSubmitting}
              onClick={formActions.navigateBack}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting}
            >
              Cancel
            </LaddaButton>
          </Col>
        </Row>
        {/*Role Information*/}
        <Row>
          <Col xs="12" sm="6">
            <Card>
              <CardHeader>
                <strong>Role Information</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Name <span className="redColor">*</span></Label>
                      <Input type="text" name="name" onChange={handleChange} value={values.name || ''} placeholder="Role name"/>
                      {touched.name && errors.name && <div className="redColor">{errors.name}</div>}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="parentRoleName">Reporting to</Label>
                      <Input type="text" name="parentRoleName" disabled value={values.parentRoleName}/>
                      <Input type="hidden" name="parentRoleId" disabled value={values.parentRoleId}/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Standard Role Module Permissions */}
        <FieldArray
          name="modulePermissions"
          render={arrayHelpers => (
            <Row>
              <Col xs="12" sm="12" lg="12">
                <Card>
                  <CardHeader>
                    <strong>Standard Role Module Permissions</strong>
                  </CardHeader>
                  <CardBody>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Table hover responsive>
                          <thead>
                          <tr>
                            <th>Modules</th>
                            <th>View</th>
                            <th>Create</th>
                            <th>Edit</th>
                            <th>Delete</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                            values.modulePermissions.map(
                              (perm, index) => (
                                <tr key={index}>
                                  <td>
                                    <Field type="checkbox" name={perm.moduleName} onChange={() => {
                                      const newVal = !values[perm.moduleName];
                                      values.modulePermissions[index].canAccessModule = newVal;
                                      values.modulePermissions[index].canView = newVal;
                                      values.modulePermissions[index].canCreate = newVal;
                                      values.modulePermissions[index].canEdit = newVal;
                                      values.modulePermissions[index].canDelete = newVal;
                                      setValues({
                                        ...values,
                                        [perm.moduleName]: newVal,
                                        [`canView_${perm.moduleName}_${perm.moduleId}`]: newVal,
                                        [`canCreate_${perm.moduleName}_${perm.moduleId}`]: newVal,
                                        [`canEdit_${perm.moduleName}_${perm.moduleId}`]: newVal,
                                        [`canDelete_${perm.moduleName}_${perm.moduleId}`]: newVal
                                      });
                                    }} checked={values[perm.moduleName]} />
                                    {' '} {perm.moduleName}
                                  </td>
                                  <td>
                                    <Field type="checkbox"
                                             name={`canView_${perm.moduleName}_${perm.moduleId}`}
                                             checked={values[`canView_${perm.moduleName}_${perm.moduleId}`]}
                                             onChange={() => {
                                               const fCanView = `canView_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanCreate = `canCreate_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanEdit = `canEdit_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanDelete = `canDelete_${perm.moduleName}_${perm.moduleId}`;
                                               const newVal = !values[fCanView];
                                               setFieldValue(fCanView, newVal);
                                               if((!newVal && !values[fCanCreate] && !values[fCanEdit] && !values[fCanDelete])){
                                                 setFieldValue(perm.moduleName, false);
                                                 values.modulePermissions[index].canAccessModule = false;
                                               }else{
                                                 setFieldValue(perm.moduleName, true);
                                                 values.modulePermissions[index].canAccessModule = true;
                                               }
                                               values.modulePermissions[index].canView = newVal;
                                             }} />
                                  </td>
                                  <td>
                                    <Field type="checkbox"
                                             name={`canCreate_${perm.moduleName}_${perm.moduleId}`}
                                             checked={values[`canCreate_${perm.moduleName}_${perm.moduleId}`]}
                                             onChange={() => {
                                               const fCanView = `canView_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanCreate = `canCreate_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanEdit = `canEdit_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanDelete = `canDelete_${perm.moduleName}_${perm.moduleId}`;
                                               const newVal = !values[fCanCreate];
                                               setFieldValue(fCanCreate, newVal);
                                               if((!newVal && !values[fCanView] && !values[fCanEdit] && !values[fCanDelete])){
                                                 setFieldValue(perm.moduleName, false);
                                                 values.modulePermissions[index].canAccessModule = false;
                                               }else{
                                                 setFieldValue(perm.moduleName, true);
                                                 values.modulePermissions[index].canAccessModule = true;
                                               }
                                               values.modulePermissions[index].canCreate = newVal;
                                             }} />
                                  </td>
                                  <td>
                                    <Field type="checkbox"
                                             name={`canEdit_${perm.moduleName}_${perm.moduleId}`}
                                             checked={values[`canEdit_${perm.moduleName}_${perm.moduleId}`]}
                                             onChange={() => {
                                               const fCanView = `canView_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanCreate = `canCreate_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanEdit = `canEdit_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanDelete = `canDelete_${perm.moduleName}_${perm.moduleId}`;
                                               const newVal = !values[fCanEdit];
                                               setFieldValue(fCanEdit, newVal);
                                               if((!newVal && !values[fCanView] && !values[fCanCreate] && !values[fCanDelete])){
                                                 setFieldValue(perm.moduleName, false);
                                                 values.modulePermissions[index].canAccessModule = false;
                                               }else{
                                                 setFieldValue(perm.moduleName, true);
                                                 values.modulePermissions[index].canAccessModule = true;
                                               }
                                               values.modulePermissions[index].canEdit = newVal;
                                             }} />
                                  </td>
                                  <td>
                                    <Field type="checkbox"
                                             name={`canDelete_${perm.moduleName}_${perm.moduleId}`}
                                             checked={values[`canDelete_${perm.moduleName}_${perm.moduleId}`]}
                                             onChange={() => {
                                               const fCanView = `canView_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanCreate = `canCreate_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanEdit = `canEdit_${perm.moduleName}_${perm.moduleId}`;
                                               const fCanDelete = `canDelete_${perm.moduleName}_${perm.moduleId}`;
                                               const newVal = !values[fCanDelete];
                                               setFieldValue(fCanDelete, newVal);
                                               if((!newVal && !values[fCanView] && !values[fCanCreate] && !values[fCanEdit])){
                                                 setFieldValue(perm.moduleName, false);
                                                 values.modulePermissions[index].canAccessModule = false;
                                               }else{
                                                 setFieldValue(perm.moduleName, true);
                                                 values.modulePermissions[index].canAccessModule = true;
                                               }
                                               values.modulePermissions[index].canDelete = newVal;
                                             }} />
                                  </td>
                                </tr>
                              )
                            )
                          }
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        />
        {/*Role Custom Permissions */}
        <FieldArray
          name="customPermissions"
          render={arrayHelpers => (
            <Row>
              <Col xs="12" sm="12" lg="12">
                <Card>
                  <CardHeader>
                    <strong>Role Custom Permissions</strong>
                  </CardHeader>
                  <CardBody>
                    <Row>
                      <Col xs="12" sm="12" lg="12">
                        <Table hover responsive>
                          <thead>
                          <tr>
                            <th>Assigned {' '}</th>
                            <th>Permission</th>
                            <th>Description</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                            values.customPermissions.map(
                              (item, index) => (
                                <tr key={item.id}>
                                  <td>
                                    <Input type="checkbox" name={`customPerm_${item.id}`} onChange={() => {
                                      item.isAssigned = !item.isAssigned;
                                    }}
                                     defaultChecked={item.isAssigned} />
                                  </td>
                                  <td>{item.name}</td>
                                  <td>{item.description}</td>
                                </tr>
                              )
                            )
                          }
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        />
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <LaddaButton
              className="btn btn-success btn-ladda"
              loading={isSubmitting}
              onClick={handleSubmit}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting || !dirty}
            >
              Save
            </LaddaButton>
            <LaddaButton
              type="button"
              className="btn btn-cancel btn-ladda"
              loading={isSubmitting}
              onClick={formActions.navigateBack}
              data-color="green"
              data-style={SLIDE_LEFT}
              disabled={isSubmitting}
            >
              Cancel
            </LaddaButton>
          </Col>
        </Row>
      </form>
    )}
  />
);

export default withRouter(RoleDetailForm);
