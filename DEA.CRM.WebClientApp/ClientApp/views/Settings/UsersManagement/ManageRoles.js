import React from 'react';
import {withRouter} from 'react-router-dom';
import SortableTree from 'react-sortable-tree';
import {toast} from 'react-toastify';
import {connect} from "react-redux";
import * as manageRoleActions from "../../../users-management/manageRoleAction";
import {bindActionCreators} from 'redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import 'spinkit/css/spinkit.css';

class ManageRoles extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      modalConfirmDeleteNode: false
    };

    this.handleTreeDataChange = this.handleTreeDataChange.bind(this);
    this.handleAddChildNode = this.handleAddChildNode.bind(this);
    this.handleEditNode = this.handleEditNode.bind(this);
    this.toggleConfirmDeleteNodeModal = this.toggleConfirmDeleteNodeModal.bind(this);
    this.handleSelectNodeFromModal = this.handleSelectNodeFromModal.bind(this);
  }

  componentDidMount() {
    this.props.actions.fetchRoleTreeAction();
  }

  handleTreeDataChange(treeData) {
    this.props.actions.dispatchTreeDataChangeAction(treeData);
  }

  handleTreeDataChangeFromModal(treeData) {
    this.props.actions.dispatchTreeDataChangeAction(treeData);
  }

  handleAddChildNode(node) {
    const { history } = this.props;
    history.push(`/setting/role/new/${node.key}/true`);
  }

  handleEditNode(node) {
    const { history } = this.props;
    history.push(`/setting/role/edit/${node.key}/false`);
  }

  toggleConfirmDeleteNodeModal(node) {
    this.setState({
      modalConfirmDeleteNode: !this.state.modalConfirmDeleteNode,
      roleToDelete: node
    });
  }

  handleSelectNodeFromModal(node) {
    if (node){
      this.setState({
        modalConfirmDeleteNode: !this.state.modalConfirmDeleteNode,
        roleToTakeOwnership: node
      });
      this.props.actions.roleDeleteAction({
        roleToDelete: this.state.roleToDelete,
        roleToTakeOwnership: node
      });
    }
    else{
      this.toggleConfirmDeleteNodeModal(null);
    }
  }

  render() {

    const { isLoading } = this.props.manageRolePageInfo;

    if(isLoading){
      return <div className="sk-rotating-plane"/>;
    }

    const {rootNodes} = (this.props.manageRolePageInfo && this.props.manageRolePageInfo.roleTreeData) ?
      this.props.manageRolePageInfo.roleTreeData : {rootNodes: []};

    return (
      <div className="animated fadeIn" style={{height: 800}}>
        <SortableTree
          treeData={rootNodes}
          getNodeKey={({ node }) => node.key}
          onChange={treeData => this.handleTreeDataChange(treeData)}
          canDrag={false}
          generateNodeProps={({node, path}) => ({
            buttons: [
              <Button color="primary" size="sm" onClick={()=> this.handleEditNode(node)}><i className="fa fa-pencil"/></Button>,
              <Button color="info" size="sm" onClick={() => this.handleAddChildNode(node)}><i className="fa fa-plus"/></Button>,
              (node.children && node.children.length === 0) && <Button color={(node.children && node.children.length > 0) ? 'secondary' : 'danger'} size="sm" onClick={() => this.toggleConfirmDeleteNodeModal(node)}><i className="fa fa-minus"/></Button>
            ]
          })}
        />
        {this.state.roleToDelete && <Modal isOpen={this.state.modalConfirmDeleteNode} toggle={this.toggleConfirmDeleteNodeModal} className={'modal-primary ' + this.props.className}>
          <ModalHeader toggle={this.toggleConfirmDeleteNodeModal}>{`Delete role ${this.state.roleToDelete.title}? Select a new role to transfer ownership to`}</ModalHeader>
          <ModalBody>
            <div style={{height: 400}}>
              <SortableTree
                treeData={rootNodes}
                getNodeKey={({ node }) => node.key}
                onChange={treeData => this.handleTreeDataChangeFromModal(treeData)}
                canDrag={false}
                generateNodeProps={({node, path}) => ({
                  buttons: [
                    <Button color="primary" size="sm" onClick={()=> this.handleSelectNodeFromModal(node)}><i className="fa fa-check"/></Button>
                  ]
                })}
              />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleConfirmDeleteNodeModal}>Proceed</Button>{' '}
            <Button color="secondary" onClick={this.toggleConfirmDeleteNodeModal}>Cancel</Button>
          </ModalFooter>
        </Modal>}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    manageRolePageInfo: state.manageRolePageInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(manageRoleActions, dispatch)
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ManageRoles));
