import React from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {toast} from 'react-toastify';
import 'spinkit/css/spinkit.css';
import * as R from 'ramda';

import * as manageRoleActions from "../../../users-management/manageRoleAction";
import UserRoleApi from '../../../api/userRoleApi';
import RoleDetailForm from './RoleDetailForm';

class ManageRoleDetailPage extends React.Component{

  constructor(props) {
    super(props);
    const thisComp = this;
    this.state = {
      formData: null,
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          return UserRoleApi.addNewRole(values).then(
            ({response}) => {
              toast.success('Role save successfully.');
              return response;
            }
          );
        },
        navigateBack: () => {
          this.props.history.push('/setting/role');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Role save error.');
        }
      }
    };
  }

  componentDidMount() {
    const { roleKey, isNew } = this.props.match.params;

    if (isNew === 'true') {
      UserRoleApi.getDefaultNewRole({
        parentRoleId: roleKey
      })
        .then(({response}) => {
          const formData = this.normalizeFormDataFromResponse(response.data);
          this.setState({formData: formData, isLoading: false});
        })
        .catch(({error}) => {
          console.log(error);
        });
    }
    else {
      // Edit
      UserRoleApi.getRoleDetail({
        roleId: roleKey
      })
        .then(({response}) => {
          const formData = this.normalizeFormDataFromResponse(response.data);
          console.log('edit - normalizeFormDataFromResponse ', formData);
          this.setState({formData: formData, isLoading: false});
      })
        .catch(({error}) => {
          console.log(error);
        });
    }

  }

  normalizeFormDataFromResponse(responseData) {
    if (!responseData || !responseData.formData)
      return {};
    const {modulePermissions} = responseData.formData;
    let newFormData = {...responseData.formData};

    R.forEach(perm => {
      newFormData[perm.moduleName] = perm.canAccessModule;
      newFormData[`canView_${perm.moduleName}_${perm.moduleId}`] = perm.canView;
      newFormData[`canCreate_${perm.moduleName}_${perm.moduleId}`] = perm.canCreate;
      newFormData[`canEdit_${perm.moduleName}_${perm.moduleId}`] = perm.canEdit;
      newFormData[`canDelete_${perm.moduleName}_${perm.moduleId}`] = perm.canDelete;
    }, modulePermissions);

    return newFormData;
  }

  render() {

    if (this.state.isLoading){
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <RoleDetailForm {...this.state}/>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(manageRoleActions, dispatch)
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ManageRoleDetailPage));
