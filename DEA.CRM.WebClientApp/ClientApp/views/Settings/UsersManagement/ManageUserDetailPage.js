import React from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {toast} from 'react-toastify';
import 'spinkit/css/spinkit.css';

import * as actions from "../../../users-management/manageUserAction";
import UserRoleApi from '../../../api/userRoleApi';
import UserDetailForm from './UserDetailForm';

class ManageUserDetailPage extends React.Component {

  constructor(props) {
    super(props);
    const thisComp = this;
    this.state = {
      formData: null,
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          // upload using data url
          return UserRoleApi.addUser(values).then(
            ({response}) => {
              toast.success('User save successfully.');
              return response;
            }
          );
        },
        navigateBack: () => {
          this.props.history.push('/setting/user');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('User save error.');
        },
        validateForm: (values) => {
          let errors = {};
          if (!values.userName) {
            errors.userName = 'Required';
          }
          if (!values.email) {
            errors.email = 'Required';
          }
          if (!values.firstName) {
            errors.firstName = 'Required';
          }
          if (!values.lastName) {
            errors.lastName = 'Required';
          }
          if (!values.password) {
            errors.password = 'Required';
          }
          if (!values.passwordConfirmation) {
            errors.passwordConfirmation = 'Required';
          }
          if (values.password !== values.passwordConfirmation) {
            errors.passwordConfirmation = 'Password confirmation does not match';
          }
          if (values.roleId !== values.roleId) {
            errors.roleId = 'Required';
          }
          return errors;
        }
      }
    };
  }

  componentDidMount() {
    const {userKey} = this.props.match.params;
    UserRoleApi.getUserDetail({
      userKey: userKey || 'new'
    })
    .then(({response}) => {
      this.setState({...response.data, isLoading: false});
    })
    .catch(({error}) => {
      console.log(error);
    });
  }

  render() {

    if (this.state.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <UserDetailForm {...this.state}/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ManageUserDetailPage));
