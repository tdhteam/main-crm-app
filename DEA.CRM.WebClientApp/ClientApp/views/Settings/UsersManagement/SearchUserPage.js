import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Badge
} from 'reactstrap';
import ReactTable from "react-table";
import "react-table/react-table.css";
import 'spinkit/css/spinkit.css';
import {Translate, getTranslate, getActiveLanguage} from 'react-localize-redux';

import * as actions from '../../../users-management/manageUserAction';

class SearchUserPage extends React.Component {

  constructor() {
    super();
    this.state = {
      searchRequest: {
        orderAsc: '',
        orderBy: [],
        pageNumber: 1,
        pageSize: 10
      },
      sorted: [],
      filtered: []

    };
    this.search = this.search.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.actions.searchUserRequestAction(this.state.searchRequest);
  }

  search() {
    //build request for filter and sorted.
    if (this.state.sorted.length > 0) {
      let sort = {orderAsc: !this.state.sorted[0].desc, orderBy: this.state.sorted[0].id};
      this.setState(Object.assign(this.state.searchRequest, sort));
    } else {
      let sort = {orderAsc: false, orderBy: ''};
      this.setState(Object.assign(this.state.searchRequest, sort));
    }

    let request = Object.assign({}, this.state.searchRequest);

    if (this.state.filtered.length > 0) {
      this.state.filtered.forEach(function (element) {
        request = Object.assign(request, {[element.id]: element.value});
      }, this);
    }

    this.props.actions.searchUserRequestAction(request);
  }

  onChange(name, data) {

    if (name === 'sorted' || name === 'filtered') {
      this.setState(Object.assign(this.state, data));
    }
    else {
      this.setState(Object.assign(this.state.searchRequest, data));
    }
    if (name !== "filtered") {
      this.search();
    }
  }

  render() {
    const result = this.props.searchUserPageInfo;

    if (result.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <Link to="/setting/user/new">
                  <Button type='button' color='info'>Add User</Button>
                </Link>{' '}
                <Button type='button' color='primary' onClick={this.search}>Search</Button>
              </CardHeader>
              <CardBody>
                <ReactTable
                  manual
                  // Controlled props
                  sorted={this.state.searchRequest.sorted}
                  page={result.pageNumber - 1}
                  pages={result.pageTotal}
                  pageSize={this.state.searchRequest.pageSize}
                  filtered={this.state.searchRequest.filtered}
                  // Callbacks
                  onSortedChange={sorted => this.onChange('sorted', {sorted: sorted})}
                  onPageChange={page => this.onChange('page', {pageNumber: page + 1})}
                  onPageSizeChange={(pageSize, page) => this.onChange('pageSize', {
                    pageNumber: page + 1,
                    pageSize: pageSize
                  })}
                  onFilteredChange={filtered => this.onChange('filtered', {filtered})}

                  data={result.resultSet}
                  filterable

                  columns={[
                    {
                      Header: "Tên",
                      id: "fullName",
                      accessor: d => ({userKey: d.userKey, fullName: d.fullName}),
                      Cell: row => (
                        <Link to={`/setting/user/edit/${row.value.userKey}`}>{row.value.fullName}</Link>
                      )
                    },
                    {
                      Header: "Email",
                      accessor: "email"
                    },
                    {
                      Header: "Role",
                      accessor: "role"
                    },
                    {
                      Header: "User Name",
                      accessor: "userName"
                    },
                    {
                      Header: "Admin",
                      accessor: "isAdmin",
                      Cell: row => (
                        row.value && <Badge color="primary">Yes</Badge>
                      )
                    },
                    {
                      Header: "Status",
                      accessor: "status"
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    searchUserPageInfo: state.searchUserPageInfo,
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale).code
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchUserPage);
