import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import CalendarEventApi from '../../api/calendarEventApi';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

class MyCalendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      actionDropdownOpen: false,
      eventList: []
    };

    this._toggleActionButtons = this._toggleActionButtons.bind(this);
  }

  _toggleActionButtons() {
    this.setState({
      actionDropdownOpen: !this.state.actionDropdownOpen
    });
  }

  componentDidMount() {

    const currDate = new Date();
    const currMonth = currDate.getMonth();

    CalendarEventApi.getCalendarItemsByMonth(currMonth + 1)
      .then(({response}) => {
        this.setState({
          isLoading: false,
          eventList: response.data
        });
      });
  }

  render() {

    const {isLoading, eventList} = this.state;
    const currDate = new Date();
    const currYear = currDate.getFullYear();
    const currMonth = currDate.getMonth();

    if (isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated">
        <Card>
          <CardHeader>
            <ButtonDropdown className="mr-1" isOpen={this.state.actionDropdownOpen} toggle={this._toggleActionButtons}>
              <DropdownToggle caret color="primary">
                Create
              </DropdownToggle>
              <DropdownMenu>
                <NavLink to="/calendar/event/new">
                  <DropdownItem>Create Event</DropdownItem>
                </NavLink>
                <DropdownItem divider/>
                <NavLink to="/calendar/activity/new">
                  <DropdownItem>Create Task</DropdownItem>
                </NavLink>
              </DropdownMenu>
            </ButtonDropdown>
          </CardHeader>
          <CardBody style={{height: '40em'}}>
            <BigCalendar
              className="d-sm-down-none"
              {...this.props}
              events={eventList}
              views={['month', 'week', 'day']}
              step={30}
              defaultDate={new Date(currYear, currMonth, 1)}
              defaultView='month'
              toolbar={true}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default MyCalendar;
