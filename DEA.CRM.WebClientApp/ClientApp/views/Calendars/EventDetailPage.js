import React from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {toast} from 'react-toastify';
import 'spinkit/css/spinkit.css';

import ContactApi from '../../api/contactApi';
import CalendarEventApi from '../../api/calendarEventApi';
import EventDetailForm from './EventDetailForm';

class EventDetailPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      workspaceContextInfo: props.workspaceContextInfo,
      formData: null,
      contactList: {
        data: [],
        isSearching: false
      },
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          return CalendarEventApi.addOrUpdateEvent(values).then(
            ({response}) => {
              toast.success('Event save successfully.');
              return response;
            }
          );
        },
        searchContact: (query) => {
          this.setState({
            contactList: {
              isSearching: true
            }
          });
          const searchParams = {
            searchRequest: {
              keyword: query
            }
          };
          ContactApi.searchContacts(searchParams)
            .then(({data}) => {
              this.setState({
                contactList: {
                  data: data.resultSet,
                  isSearching: false
                }
              });
            });
        },
        navigateBack: () => {
          this.props.history.push('/calendar/calendar-list-view');
        },
        showFriendlyError: (error) => {
          toast.error(`Event save error. ${error}`);
        },
        validateForm: (values) => {
          let errors = {};
          if (!values.subject) {
            errors.subject = 'Required';
          }

          return errors;
        }
      }
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id || 0;
    CalendarEventApi.getEventDetail(id)
      .then(({response}) => {
        this.setState({...response.data, isLoading: false});
      })
      .catch(({error}) => {
        console.log(error);
        toast.error(`Event load error. ${error}`);
      });
  }

  render() {

    if (this.state.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <EventDetailForm {...this.state}/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUserInfo: state.loginPageInfo.authenticatedUserInfo || {},
    workspaceContextInfo: state.workspaceContextInfo
  };
}

export default withRouter(connect(mapStateToProps)(EventDetailPage));
