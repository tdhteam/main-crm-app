import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import ReactTable from "react-table";
import "react-table/react-table.css";
import 'spinkit/css/spinkit.css';
import {Translate, getTranslate, getActiveLanguage} from 'react-localize-redux';

import * as actions from '../../calendar/calendarAction';

class CalendarListViewPage extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      dropdownOpen: new Array(1).fill(false),
      searchRequest: {
        orderAsc: '',
        orderBy: [],
        pageNumber: 1,
        pageSize: 10
      },
      sorted: [],
      filtered: []

    };

    this.search = this.search.bind(this);
    this.onChange = this.onChange.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this.props.actions.searchCalendarItemsListViewRequestAction(this.state.searchRequest);
  }

  search() {
    //build request for filter and sorted.
    if (this.state.sorted.length > 0) {
      let sort = {orderAsc: !this.state.sorted[0].desc, orderBy: this.state.sorted[0].id};
      this.setState(Object.assign(this.state.searchRequest, sort));
    } else {
      let sort = {orderAsc: false, orderBy: ''};
      this.setState(Object.assign(this.state.searchRequest, sort));
    }

    let request = Object.assign({}, this.state.searchRequest);

    if (this.state.filtered.length > 0) {
      this.state.filtered.forEach(function (element) {
        request = Object.assign(request, {[element.id]: element.value});
      }, this);
    }

    this.props.actions.searchCalendarItemsListViewRequestAction(request);
  }

  onChange(name, data) {

    if (name === 'sorted' || name === 'filtered') {
      this.setState(Object.assign(this.state, data));
    }
    else {
      this.setState(Object.assign(this.state.searchRequest, data));
    }
    if (name !== "filtered") {
      this.search();
    }
  }

  toggle(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
    this.setState({
      dropdownOpen: newArray
    });
  }

  render() {

    const result = this.props.searchCalendarListViewPageInfo;

    if (result.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <ButtonDropdown className="mr-1" isOpen={this.state.dropdownOpen[0]} toggle={() => { this.toggle(0); }}>
                  <DropdownToggle caret color="primary">
                    Create
                  </DropdownToggle>
                  <DropdownMenu>
                    <NavLink to="/calendar/event/new">
                      <DropdownItem>Create Event</DropdownItem>
                    </NavLink>
                    <DropdownItem divider/>
                    <NavLink to="/calendar/activity/new">
                      <DropdownItem>Create Task</DropdownItem>
                    </NavLink>
                  </DropdownMenu>
                </ButtonDropdown>
              </CardHeader>
              <CardBody>
                <ReactTable
                  manual
                  // Controlled props
                  sorted={this.state.searchRequest.sorted}
                  page={result.pageNumber - 1}
                  pages={result.pageTotal}
                  pageSize={this.state.searchRequest.pageSize}
                  filtered={this.state.searchRequest.filtered}
                  // Callbacks
                  onSortedChange={sorted => this.onChange('sorted', {sorted: sorted})}
                  onPageChange={page => this.onChange('page', {pageNumber: page + 1})}
                  onPageSizeChange={(pageSize, page) => this.onChange('pageSize', {
                    pageNumber: page + 1,
                    pageSize: pageSize
                  })}
                  onFilteredChange={filtered => this.onChange('filtered', {filtered})}
                  data={result.resultSet}
                  filterable
                  columns={[
                    {
                      Header: "Status",
                      accessor: "eventStatus"
                    },
                    {
                      Header: "ActivityType",
                      accessor: "activityType"
                    },
                    {
                      Header: "Subject",
                      id: "subject",
                      accessor: d => ({activityType: d.activityType, id: d.id, uniqueKey: d.uniqueKey, subject: d.subject}),
                      Cell: row => (
                        'Task' === row.value.activityType ?
                          <Link to={`/calendar/activity/edit/${row.value.id}`}>{row.value.subject}</Link>
                          : <Link to={`/calendar/event/edit/${row.value.id}`}>{row.value.subject}</Link>
                      )
                    },
                    {
                      Header: "Contact Name",
                      accessor: "relatedToContactFullName",
                      Cell: row => (
                        row.value
                      )
                    },
                    {
                      Header: "Related To",
                      accessor: "relatedToHandoverContractId"
                    },
                    {
                      Header: "Start Date Time",
                      accessor: "startDateTime"
                    },
                    {
                      Header: "Due Date",
                      accessor: "dueDateTime"
                    },
                    {
                      Header: "Assigned To",
                      accessor: "ownerUserFullName"
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    searchCalendarListViewPageInfo: state.searchCalendarListViewPageInfo,
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale).code
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarListViewPage);
