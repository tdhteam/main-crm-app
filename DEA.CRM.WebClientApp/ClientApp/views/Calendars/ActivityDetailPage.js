import React from 'react';
import {withRouter} from "react-router-dom";
import {toast} from 'react-toastify';
import 'spinkit/css/spinkit.css';

import ContactApi from '../../api/contactApi';
import CalendarEventApi from '../../api/calendarEventApi';
import ActivityDetailForm from './ActivityDetailForm';

class ActivityDetailPage extends React.Component {

  constructor(props) {
    super(props);
    const thisComp = this;
    this.state = {
      formData: null,
      contactList: {
        data: [],
        isSearching: false
      },
      isLoading: true,
      formActions: {
        handleFormSave: (values) => {
          console.log(values);
          return CalendarEventApi.addOrUpdateActivity(values).then(
            ({response}) => {
              toast.success('Activity save successfully.');
              return response;
            }
          );
        },
        searchContact: (query) => {
          this.setState({
            contactList: {
              isSearching: true
            }
          });
          const searchParams = {
            searchRequest: {
              keyword: query
            }
          };
          ContactApi.searchContacts(searchParams)
            .then(({data}) => {
              console.log(data);
              this.setState({
                contactList: {
                  data: data.resultSet,
                  isSearching: false
                }
              });
            });
        },
        navigateBack: () => {
          this.props.history.push('/calendar/calendar-list-view');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Activity save error.');
        },
        validateForm: (values) => {
          let errors = {};
          if (!values.subject) {
            errors.subject = 'Required';
          }

          return errors;
        }
      }
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id || 0;
    CalendarEventApi.getActivityDetail(id)
      .then(({response}) => {
        this.setState({...response.data, isLoading: false});
      })
      .catch(({error}) => {
        console.log(error);
      });
  }

  render() {

    if (this.state.isLoading) {
      return <div className="sk-rotating-plane"/>;
    }

    return (
      <div className="animated fadeIn">
        <ActivityDetailForm {...this.state}/>
      </div>
    );
  }
}

export default withRouter(ActivityDetailPage);
