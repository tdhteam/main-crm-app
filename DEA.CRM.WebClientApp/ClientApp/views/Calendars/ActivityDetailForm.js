import React from 'react';
import {withRouter} from "react-router-dom";
import {Formik} from 'formik';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { AsyncTypeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import AppFormActionButton from '../../components/AppFormActionButton/AppFormActionButton';
import HandoverContractLookup from '../../components/AppLookup/HandoverContractLookup';
import UserLookup from '../../components/AppLookup/UserLookup';
import DurationSelect from '../../components/DateTime/DurationSelect';

const ActivityDetailForm =
  ({
    formData,
    activityStatusList,
    assignedToUserList,
    contactList,
    pickListValues,
    priorityList,
    formActions
  }) => (
  <Formik
    initialValues={formData}
    validate={values => formActions.validateForm(values)}
    onSubmit={(values, {setSubmitting, setErrors, setValues}) => {
      formActions.handleFormSave(values).then(() => {
        setSubmitting(false);
        formActions.navigateBack();
      })
        .catch(
          ({error}) => {
            formActions.showFriendlyError(error);
          }
        );
    }}
    render={({
               values,
               errors,
               touched,
               dirty,
               setFieldValue,
               setValues,
               isSubmitting,
               handleChange,
               handleSubmit
             }) => (
      <form onSubmit={handleSubmit}>
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <AppFormActionButton
              dirty={dirty}
              isSubmitting={isSubmitting}
              handleSubmit={handleSubmit}
              navigateBack={formActions.navigateBack}
            />
          </Col>
        </Row>
        {/*Task Details*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Task Details</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="subject">Subject <span className="redColor">*</span></Label>
                      <Input type="text" name="subject" onChange={handleChange} value={values.subject || ''}/>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="ownerUserKey">Assigned To</Label>
                      <UserLookup
                        name="ownerUserKey"
                        options={assignedToUserList}
                        selected={[values.ownerUserFullName || '']}
                        handleChange={(selected) => {
                          if (selected && selected.length > 0) {
                            setFieldValue("ownerUserKey", selected[0].userKey);
                            setFieldValue("ownerUserFullName", selected[0].fullName);
                          }
                        }}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="startDateTime">Start Date and Time</Label>
                      <DatePicker className="form-control"
                        name="startDateTime"
                        selected={moment(values.startDateTime)}
                        onChange={(date) => {
                          setFieldValue("startDateTime", date);
                        }}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        dateFormat="LLL"
                        timeCaption="time"
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="dueDateTime">Due Date</Label>
                      <DatePicker className="form-control"
                        name="dueDateTime"
                        selected={moment(values.dueDateTime)}
                        onChange={(date) => {
                          setFieldValue("dueDateTime", date);
                        }}
                        dateFormat="LLL"
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <Label htmlFor="relatedToContactId">Contact Name</Label>
                    <FormGroup>
                      <AsyncTypeahead
                        name="relatedToContactId"
                        labelKey="fullName"
                        minLength={3}
                        filterBy={['fullName']}
                        onSearch={formActions.searchContact}
                        onChange={(selected) => {
                          if (selected && selected.length > 0) {
                            setFieldValue("relatedToContactId", selected[0].id);
                            setFieldValue("relatedToContactFullName", selected[0].fullName);
                          }
                        }}
                        onInputChange={(keyword) => {
                          setFieldValue("relatedToContactFullName", keyword);
                        }}
                        isLoading={contactList.isSearching}
                        options={contactList.data}
                        selected={[values.relatedToContactFullName || '']}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="2" md="2" lg="2">
                    <FormGroup>
                      <Label htmlFor="otherModule">Others</Label>
                      <Input type="select" name="otherModule" onChange={handleChange} value={values.otherModule || ''}>
                        <option value="">--- select ---</option>
                        <option value="Handover Contracts">Handover Contracts</option>
                        <option value="Customer Services">Customer Services</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  {values.otherModule === 'Handover Contracts' && <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label htmlFor="otherModule">{values.otherModule}</Label>
                      <HandoverContractLookup
                        handleChange={(selected) => {
                          if (selected && selected.length > 0) {
                            setFieldValue("relatedToHandoverContractId", selected[0].userKey);
                            setFieldValue("relatedToHandoverContractFullName", selected[0].fullName);
                          }
                        }}
                        selected={[values.relatedToHandoverContractFullName || '']}
                      />
                    </FormGroup>
                  </Col>}
                  {values.otherModule === 'Customer Services' && <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label htmlFor="otherModule">{values.otherModule}</Label>
                      <HandoverContractLookup
                        handleChange={(selected) => {
                          if (selected && selected.length > 0) {
                            setFieldValue("relatedToCustomerServiceId", selected[0].userKey);
                            setFieldValue("relatedToCustomerServiceFullName", selected[0].fullName);
                          }
                        }}
                        selected={[values.relatedToCustomerServiceFullName || '']}
                      />
                    </FormGroup>
                  </Col>}
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="status">Status</Label>
                      <Input type="select" name="eventStatus" id="eventStatus" onChange={handleChange} value={values.eventStatus || ''}>
                        <option value>--- select ---</option>
                        {activityStatusList.map((item, index) => (
                          <option key={index} value={item.name}>{item.name}</option>
                        ))}
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="priority">Priority</Label>
                      <Input type="select" name="priority" onChange={handleChange} value={values.priority || ''}>
                        <option value>--- select ---</option>
                        {priorityList.map((item, index) => (
                          <option key={index} value={item.name}>{item.name}</option>
                        ))}
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Row>
                        <Col xs="12" sm="6" md="4" lg="4">
                          <Label htmlFor="sendNotification">Send Notification</Label>
                        </Col>
                        <Col xs="12" sm="6" md="4" lg="4">
                          <Label className="switch switch-default switch-primary">
                            <Input type="checkbox" className="switch-input" name="sendNotification" onChange={handleChange} defaultChecked={values.sendNotification}/>
                            <span className="switch-label" data-on="On" data-off="Off"/>
                            <span className="switch-handle"/>
                          </Label>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label htmlFor="location">Location</Label>
                      <Input type="text" name="location" onChange={handleChange} value={values.location || ''}/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Reminder Details*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Reminder Details</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Row>
                        <Col xs="12" sm="6" md="4" lg="4">
                          <Label htmlFor="sendReminder">Send Reminder Before</Label>
                        </Col>
                        <Col xs="12" sm="6" md="4" lg="4">
                          <Label className="switch switch-default switch-primary">
                            <Input type="checkbox" className="switch-input" name="sendReminder" onChange={handleChange} defaultChecked={values.sendReminder}/>
                            <span className="switch-label" data-on="On" data-off="Off"/>
                            <span className="switch-handle"/>
                          </Label>
                        </Col>
                      </Row>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    {values.sendReminder && <DurationSelect
                        day={values.reminderBeforeDay}
                        hour={values.reminderBeforeHour}
                        minute={values.reminderBeforeMinute}
                        onDurationChange={(e) => {
                        setFieldValue("reminderBeforeDay", e.day);
                        setFieldValue("reminderBeforeHour", e.hour);
                        setFieldValue("reminderBeforeMinute", e.minute);
                      }}/>
                    }
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Description Details*/}
        <Row>
          <Col xs="12" sm="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Description Details</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="12" md="12" lg="12">
                    <FormGroup>
                      <Label htmlFor="description">Description</Label>
                      <Input type="textarea" rows={5} name="description" onChange={handleChange} value={values.description || ''}/>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/*Action Buttons*/}
        <Row>
          <Col lg="12" className="text-right crm-btn-group">
            <AppFormActionButton
              dirty={dirty}
              isSubmitting={isSubmitting}
              handleSubmit={handleSubmit}
              navigateBack={formActions.navigateBack}
            />
          </Col>
        </Row>
      </form>
    )}
  />
);

export default withRouter(ActivityDetailForm);
