import React from 'react';
import {toast} from "react-toastify";

import BasicDocumentUploadForm from './BasicDocumentUploadForm';
import AttachmentApi from '../../api/attachmentApi';

class DocumentUploadPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      formTitle: 'Upload small file using form multipart - ideal for file is less than 2MB ',
      allAttachments: [],
      formData: {
        name: ''
      },
      formActions: {
        handleFormSave: (values) => {
          console.log('upload file ', values);
          return AttachmentApi.uploadFile(values)
            .then(
              () => {
                toast.success('file uploaded successfully.');
                this.forceUpdate();
              }
            );
        },
        navigateBack: () => {
          //this.props.history.push('/setting/user');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Document upload error.');
        }
      }
    }
  }

  componentDidMount() {
    AttachmentApi.getAllAttachments().then(({response}) => {
      this.setState({
        allAttachments : response.data
      });
    })
  }

  render() {
    const {allAttachments} = this.state;
    return (
      <div>
        <BasicDocumentUploadForm {...this.state} />
        <ul>
          {allAttachments && allAttachments.resultSet && allAttachments.resultSet.map((item, index) => (
            <li key={index}>{item.fileName} - {item.contentType} - {item.fileSize} - {item.description}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default DocumentUploadPage;
