import React from 'react';
import {toast} from "react-toastify";

import LargeDocumentUploadForm from './LargeDocumentUploadForm';
import AttachmentApi from '../../api/attachmentApi';

class LargeDocumentUploadPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      formTitle: 'Large documents upload',
      allAttachments: [],
      formData: {
        description: ''
      },
      formActions: {
        uploadLargeFile: (values) => {
          console.log('upload large file ', values);
          return AttachmentApi.uploadLargeFile(values)
            .then(
              () => {
                toast.success('file uploaded successfully.');
                this.forceUpdate();
              }
            );
        },
        navigateBack: () => {
          //this.props.history.push('/setting/user');
        },
        showFriendlyError: (error) => {
          console.error(error);
          toast.error('Document upload error.');
        }
      }
    }
  }

  componentDidMount() {
    AttachmentApi.getAllAttachments().then(({response}) => {
      this.setState({
        allAttachments : response.data
      });
    })
  }

  render() {
    const {allAttachments} = this.state;
    return (
      <div>
        <LargeDocumentUploadForm {...this.state} />
        <ul>
          {allAttachments && allAttachments.resultSet && allAttachments.resultSet.map((item, index) => (
            <li key={index}>{item.fileName} - {item.contentType} - {item.fileSize} - {item.description}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default LargeDocumentUploadPage;
