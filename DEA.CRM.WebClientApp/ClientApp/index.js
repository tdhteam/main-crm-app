import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Provider} from 'react-redux';

import { APP_CONFIG } from './appConfig';
import initialState from './reducers/initialState';
import StorageService from './services/storageService';
import {configureHttpClientInterceptors, initDefaultHttpClient} from './api/axiosInitHttpClient';

// Styles
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss';
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss';

import 'spinkit/css/spinkit.css';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-sortable-tree/style.css';
import "react-table/react-table.css";
// primereact
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

// Containers
import Full from './containers/Full/Full';


// Views
import Login from './views/Pages/Login/Login';
import Logout from './views/Pages/Login/Logout';
import Page404 from './views/Pages/Page404/Page404';
import Page500 from './views/Pages/Page500/Page500';
import UserReferenceFirstTimeLoginPage from './views/UserProfile/UserReferenceFirstTimeLoginPage';

import  getStore  from './store/getStore';
import {initialize, addTranslationForLanguage, setActiveLanguage} from 'react-localize-redux';
import english from './languages/english';
import vietnamese from './languages/vietnamese';


// Restore redux state from local storage
const persistedUserLoginInfo =
  StorageService.getObjFromLocalStore(APP_CONFIG.APP_SESSION_STORAGE_KEYS.userLoginPageInfo) || initialState.loginPageInfo;
const persistedAppWorkContextInfo =
  StorageService.getObjFromLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.appWorkContextInfo) || initialState.workspaceContextInfo;

if ((persistedAppWorkContextInfo.company.length === 0
  || persistedAppWorkContextInfo.dealer.length === 0
  || persistedAppWorkContextInfo.showroom.length === 0)
      && persistedUserLoginInfo.authenticatedUserInfo) {

  if (persistedUserLoginInfo.authenticatedUserInfo.userInfoToCompany.length > 0){
    persistedAppWorkContextInfo.company.push(persistedUserLoginInfo.authenticatedUserInfo.userInfoToCompany[0]);
  }

  if (persistedUserLoginInfo.authenticatedUserInfo.userInfoToDealer.length > 0) {
    persistedAppWorkContextInfo.dealer.push(persistedUserLoginInfo.authenticatedUserInfo.userInfoToDealer[0]);
  }

  if (persistedUserLoginInfo.authenticatedUserInfo.userInfoToShowroom.length > 0) {
    persistedAppWorkContextInfo.showroom.push(persistedUserLoginInfo.authenticatedUserInfo.userInfoToShowroom[0]);
  }

}

const defaultStoreState = {...initialState,
  loginPageInfo: persistedUserLoginInfo,
  workspaceContextInfo: persistedAppWorkContextInfo
};

const store = getStore(defaultStoreState);

const languages = [
  { name: 'english', code: 'en' },
  { name: 'vietnamese', code: 'vi' }
];

const activeLanguageCode =
  StorageService.getItemFromLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.activeLanguageCode) || 'en';

store.dispatch(initialize(languages, { defaultLanguage: 'en' }));
store.dispatch(addTranslationForLanguage(english, 'en'));
store.dispatch(addTranslationForLanguage(vietnamese, 'vi'));
store.dispatch(setActiveLanguage(activeLanguageCode));

const authToken = StorageService.getItemFromLocalStore(APP_CONFIG.APP_SESSION_STORAGE_KEYS.userLoginToken);
if (authToken){
  initDefaultHttpClient(authToken);
}
configureHttpClientInterceptors();

ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/login" name="Login Page" component={Login}/>
        <Route exact path="/logout" name="Logout" component={Logout}/>
        <Route exact path="/404" name="Page 404" component={Page404}/>
        <Route exact path="/500" name="Page 500" component={Page500}/>
        <Route exact path="/first-time-login" name="My Profile" component={UserReferenceFirstTimeLoginPage}/>
        <Route path="/" name="Home" component={Full}/>
      </Switch>
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));
