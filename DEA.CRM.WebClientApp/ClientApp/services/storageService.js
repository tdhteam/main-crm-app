export default class StorageService {

  /// Deal with session storage
  static setItemToSessionStore(key, value) {
    sessionStorage.setItem(key, value);
  }

  static setObjToSessionStore(key, obj) {
    return sessionStorage.setItem(key, JSON.stringify(obj));
  }

  static getItemFromSessionStore(key) {
    return sessionStorage.getItem(key);
  }

  static getObjFromSessionStore(key) {
    try {
      return JSON.parse(sessionStorage.getItem(key));
    }
    catch (e) {
      return {};
    }
  }

  static removeItemFromSessionStore(key) {
    sessionStorage.removeItem(key);
  }

  static clearSessionStore() {
    sessionStorage.clear();
  }

  /// Deal with local storage
  static setItemToLocalStore(key, value) {
    localStorage.setItem(key, value);
  }

  static setObjToLocalStore(key, obj) {
    return localStorage.setItem(key, JSON.stringify(obj));
  }

  static updateObjectPropInLocalStore(key, propNameToUpdate, newObjForProp) {
    const objToUpdate = StorageService.getObjFromLocalStore(key);
    if (objToUpdate) {
      objToUpdate[propNameToUpdate] = newObjForProp;
      StorageService.setObjToLocalStore(key, objToUpdate);
    }
  }

  static getItemFromLocalStore(key) {
    return localStorage.getItem(key);
  }

  static getObjFromLocalStore(key) {
    try {
      return JSON.parse(localStorage.getItem(key));
    }
    catch (e) {
      return {};
    }
  }

  static removeItemFromLocalStore(key) {
    localStorage.removeItem(key);
  }

  static clearLocalStore() {
    localStorage.clear();
  }
}
