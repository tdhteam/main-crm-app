import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function dealerReducer(state = initialState.dealerPageInfo, action) {
  switch(action.type) {
    case types.SEARCH_DEALER_RESULT:
      return Object.assign({}, ...state, action.dealerPageInfo);
    default:
      return state;
  }
}
