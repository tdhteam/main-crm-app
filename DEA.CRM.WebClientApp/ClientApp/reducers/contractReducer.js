import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function contractReducer(state = initialState.contractPageInfo, action) {
    switch(action.type) {
    case types.SEARCH_CONTRACT_RESULT:
       return Object.assign({}, ...state, action.contractPageInfo);
    default:
        return state;
  }
}