import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function productReducer(state = initialState.productPageInfo, action) {
    switch(action.type) {
    case types.SEARCH_PRODUCT_RESULT:
       return Object.assign({}, ...state, action.productPageInfo);
 
    default:
        return state;
  }
}