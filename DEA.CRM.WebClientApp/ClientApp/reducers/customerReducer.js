import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function customerReducer(state = initialState.customerPageInfo, action) {
    switch(action.type) {
    case types.SEARCH_CUSTOMER_RESULT:
       return Object.assign({}, ...state, action.customerPageInfo);
 
    default:
        return state;
  }
}