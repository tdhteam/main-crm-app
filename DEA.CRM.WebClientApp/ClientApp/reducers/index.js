import {combineReducers} from 'redux';

import loginReducer from './loginReducer';
import contactPageInfo from '../contact/contactReducer';
import customerPageInfo from './customerReducer';
import productPageInfo from './productReducer';
import contractPageInfo from './contractReducer';
import dealerPageInfo from './dealerReducer';
import manageRolePageInfoReducer from '../users-management/manageRoleReducer';
import searchUserPageInfoReducer from '../users-management/searchUserReducer';
import searchCalendarListViewPageInfoReducer from '../calendar/searchCalendarListViewPageInfoReducer';
import {localeReducer} from 'react-localize-redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import workspaceContextInfoReducer from './workspaceContextInfoReducer';
import { reducer as reduxFormReducer } from 'redux-form'
import showroomReducer from "./showroomReducer";

const rootReducer = combineReducers({
  workspaceContextInfo: workspaceContextInfoReducer,
  loginPageInfo: loginReducer,
  manageRolePageInfo: manageRolePageInfoReducer,
  searchUserPageInfo: searchUserPageInfoReducer,
  contactPageInfo: contactPageInfo,
  customerPageInfo: customerPageInfo,
  productPageInfo: productPageInfo,
  contractPageInfo: contractPageInfo,
  locale: localeReducer,
  loadingBar: loadingBarReducer,
  form: reduxFormReducer,
  dealerPageInfo: dealerPageInfo,
  showroomPageInfo: showroomReducer,
  searchCalendarListViewPageInfo: searchCalendarListViewPageInfoReducer
});

export default rootReducer;
