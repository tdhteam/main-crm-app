import { fromJS } from 'immutable';

// TODO... finalize value list
export  default   {
  workspaceContextInfo: {
    company: [],
    dealer: [],
    showroom: []
  },
  loginPageInfo: {},
  manageRolePageInfo: {
    isLoading: true
  },
  searchUserPageInfo: {
    isLoading: true,
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  contactPageInfo:{
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  productPageInfo:{
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  customerPageInfo:{
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  contractPageInfo:{
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  searchCalendarListViewPageInfo: {
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  dealerPageInfo: {
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
  showroomPageInfo:{
    resultSet: [],
    current_page: 1,
    total_count: 0,
    pageTotal: 1,
    hasMore: false,
    resultCount:0,
    pageNumber:1
  },
};
