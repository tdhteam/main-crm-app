import {
  CHANGE_WORKSPACE_REQUEST,
  INIT_DEFAULT_WORK_CONTEXT_AFTER_LOGIN_IN_OK
} from '../actions/actionTypes';
import initialState from './initialState';

export default function workspaceContextInfoReducer(state = initialState.workspaceContextInfo, action) {

  switch(action.type) {

    case CHANGE_WORKSPACE_REQUEST:
      return Object.assign({}, ...state, action.workspaceContextInfo);

    case INIT_DEFAULT_WORK_CONTEXT_AFTER_LOGIN_IN_OK:
      {
        // compute the default work context info from user login info
        const workspaceContextInfo = initialState.workspaceContextInfo;
        if (action.authenticatedUserInfo) {
          const {userInfoToCompany, userInfoToDealer, userInfoToShowroom} = action.authenticatedUserInfo.authenticatedUserInfo;

          if (userInfoToCompany && userInfoToCompany.length > 0) {
            workspaceContextInfo.company.push(userInfoToCompany[0]);
          }

          if (userInfoToDealer && userInfoToDealer.length > 0) {
            workspaceContextInfo.dealer.push(userInfoToDealer[0]);
          }

          if (userInfoToShowroom && userInfoToShowroom.length > 0) {
            workspaceContextInfo.showroom.push(userInfoToShowroom[0]);
          }
        }

        return Object.assign({}, ...state, workspaceContextInfo);
      }


    default:
      return state;
  }
}
