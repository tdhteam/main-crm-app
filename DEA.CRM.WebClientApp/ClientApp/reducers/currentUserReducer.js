import * as actionTypes from '../actions/actionTypes';

export default function currentUserReducer(state, action) {

  switch(action.type) {

    case actionTypes.SET_CURRENT_USER:
      return { ...state, currentUserInfo: action.currentUserInfo };

    case actionTypes.FETCH_CURRENT_USER_REQUEST:
    default:
      return state;
  }
}
