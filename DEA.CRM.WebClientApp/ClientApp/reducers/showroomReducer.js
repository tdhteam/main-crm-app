import initialState from "./initialState";
import * as types from "../actions/actionTypes";

export default function showroomReducer(state = initialState.showroomPageInfo, action) {
  switch(action.type) {
    case types.SEARCH_SHOWROOM_RESULT:
      return Object.assign({}, ...state, action.showroomPageInfo);
    default:
      return state;
  }
}
