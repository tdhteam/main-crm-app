import {
  LOGIN_REQUEST,
  LOGIN_OK,
  LOGIN_ERROR,
  SET_USER_PREFERENCE_FIRST_TIME_LOGIN_SUCCESS } from '../actions/actionTypes';
import initialState from './initialState';

export default function loginReducer(state = initialState.loginPageInfo, action) {

  switch(action.type) {

    case LOGIN_OK:
      return Object.assign({}, ...state, action.authenticatedUserInfo);

    case LOGIN_ERROR:
      return Object.assign({}, ...state, {authError: action.authError});

    case SET_USER_PREFERENCE_FIRST_TIME_LOGIN_SUCCESS:
      return Object.assign({}, ...state, {authenticatedUserInfo: action.authenticatedUserInfo});

    case LOGIN_REQUEST:
    default:
      return state;
  }
}
