import React from 'react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Table
} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';

const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning },
    ...rest
}) => (
        <FormGroup>
            <Label >{label}</Label>
            <Input {...input} type={type} placeholder={label}  {...rest}/>
            {touched && ((error && <span className='error'>{error}</span>) || (warning && <span className='error'>{warning}</span>))}
        </FormGroup>
    );

export default renderField;