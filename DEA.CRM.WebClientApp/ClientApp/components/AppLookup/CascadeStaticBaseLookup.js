import React from 'react';
import PropTypes from 'prop-types';
import {Typeahead} from 'react-bootstrap-typeahead';
import StaticBaseLookup from './StaticBaseLookup';

/*
* Base class for static lookup use Typeahead
* */
export default class CascadeStaticBaseLookup extends StaticBaseLookup {
  constructor(props) {
    super(props);

    this.state = {
      options: props.options || [],
      selected: props.selected || [],
      labelKey: props.labelKey || 'fullName',
      parentFilterByValue: props.parentFilterByValue
    };

    this._handleChange = this._handleChange.bind(this);
    this.filterBy = this.filterBy.bind(this);
  }

  _handleChange(selected) {
    this.props.handleChange(selected);
    this.setState({selected});
  }

  /*
  * Child component will override this method to have different filter logic
  * */
  filterBy(option, props) {
    throw new Error('child component must override this method');
  }

  render() {
    return (
      <React.Fragment>
        <Typeahead
          {...this.state}
          filterBy={this.filterBy}
          onChange={this._handleChange}
        />
      </React.Fragment>
    );
  }
}

CascadeStaticBaseLookup.propTypes = {
  handleChange: PropTypes.func.isRequired
};
