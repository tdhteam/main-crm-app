import React from 'react';
import PropTypes from 'prop-types';
import {Typeahead} from 'react-bootstrap-typeahead';

/*
* Base class for static lookup use Typeahead
* */
export default class StaticBaseLookup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      options: props.options || [],
      selected: props.selected || [],
      labelKey: props.labelKey || 'fullName',
      filterBy: props.filterBy || ['fullName']
    };

    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(selected) {
    this.props.handleChange(selected);
    this.setState({selected});
  }

  render() {
    return (
      <React.Fragment>
        <Typeahead
          {...this.state}
          onChange={this._handleChange}
        />
      </React.Fragment>
    );
  }
}

StaticBaseLookup.propTypes = {
  handleChange: PropTypes.func.isRequired
};
