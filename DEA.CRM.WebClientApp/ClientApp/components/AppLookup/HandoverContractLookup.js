import React from 'react';
import throttle from 'lodash/throttle';

import HandOverContractApi from '../../api/handOverContractApi';
import AsyncBaseAppLookup from './AsyncBaseAppLookup';

class HandoverContractLookup extends AsyncBaseAppLookup {

  constructor(props) {
    super(props);
  }

  performSearch(searchParams) {
    return throttle(HandOverContractApi.get, 3000)(searchParams);
  }

  handleSearch(query) {
    this.setState({isLoading: true});
    const searchParams = {
      searchRequest: {
        keyword: query
      }
    };
    return this.performSearch(searchParams)
      .then(({data}) => {
        this.setState({
          options: data.resultSet,
          isLoading: false
        });
      });
  }

}

export default HandoverContractLookup;
