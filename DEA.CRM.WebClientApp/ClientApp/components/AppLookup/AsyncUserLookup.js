import React from 'react';

import UserRoleApi from '../../api/userRoleApi';
import AsyncBaseAppLookup from './AsyncBaseAppLookup';

class AsyncUserLookup extends AsyncBaseAppLookup {

  constructor(props) {
    super(props);

    this.state.labelKey = props.labelKey || 'fullName';
    this.state.filterBy = props.filterBy || ['fullName'];
  }

  performSearch(searchParams) {
    return UserRoleApi.searchUsers(searchParams);
  }

}

export default AsyncUserLookup;
