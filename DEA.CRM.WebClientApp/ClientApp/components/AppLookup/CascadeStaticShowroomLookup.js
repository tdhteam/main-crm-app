import React from 'react';

import CascadeStaticBaseLookup from './CascadeStaticBaseLookup';

class CascadeStaticShowroomLookup extends CascadeStaticBaseLookup {

  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.parentFilterByValue !== prevState.parentFilterByValue) {
      return {
        parentFilterByValue : nextProps.parentFilterByValue,
        selected: []
      };
    }
    return null;
  }

  filterBy(option, props) {
    return option.showroomName.toLowerCase().indexOf(props.text.toLowerCase()) !== -1
      && option.showroomDealerId === parseInt(props.parentFilterByValue);
  }
}

export default CascadeStaticShowroomLookup;
