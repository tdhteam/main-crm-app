import React from 'react';
import StaticBaseLookup from './StaticBaseLookup';

/**
 * Component perform user lookup search from static data list using typeahead style
 */
class UserLookup extends StaticBaseLookup {

  constructor(props) {
    super(props);
  }
}

export default UserLookup;
