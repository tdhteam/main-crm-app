import React from 'react';
import CascadeStaticBaseLookup from './CascadeStaticBaseLookup';

class StaticKeyValueLookup extends CascadeStaticBaseLookup {

  constructor(props) {
    super(props);

    this.state = {
      options: props.options || [],
      selected: props.selected || [],
      labelKey: props.labelKey || 'value',
      parentFilterByValue: props.parentFilterByValue
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.parentFilterByValue !== prevState.parentFilterByValue) {
      return {
        parentFilterByValue : nextProps.parentFilterByValue,
        selected: []
      };
    }
    return null;
  }

  filterBy(option, props) {
    return (option.key.toLowerCase().indexOf(props.text.toLowerCase()) !== -1
        || option.value.toLowerCase().indexOf(props.text.toLowerCase()) !== -1)
        && (!props.parentFilterByValue
            || (option.parentKey === props.parentFilterByValue || parseInt(option.parentKey) === parseInt(props.parentFilterByValue)));
  }

}

export default StaticKeyValueLookup;
