import React from 'react';
import throttle from 'lodash/throttle';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';

/*
* Base class for async lookup component that use type AsyncTypeahead as underlying component
* The child component must override performSearch to provide concrete search api
* Optionally override handleSearch method to parse search result
* If you handle handleSearch, you need to handle api call throttle in your code
* */
export default class AsyncBaseAppLookup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      options: [],
      selected: props.selected,
      labelKey: props.labelKey || 'title',
      minLength: 3,
      filterBy: props.filterBy || ['title']
    };

    this.performSearch = this.performSearch.bind(this);
    this.handleSearch = this.handleSearch.bind(this);

    this._handleChange = this._handleChange.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }

  /**
   * make call to api this method must be override in child component
   * @param {object} searchParams contains search properties and values
   */
  performSearch(searchParams) {
    throw new Error('must override this method at child component');
  }

  /**
   * handle onSearch for typeahead base class provide standard implementation
   * which is suitable for most case. Some cases child component can override for its purpose
   * @param {string} query the input keyword to search
   */
  handleSearch(query) {
    this.setState({isLoading: true});
    const searchParams = {
      searchRequest: {
        keyword: query
      }
    };
    const performSearchThrottle = throttle(this.performSearch, 3000);
    return performSearchThrottle(searchParams)
      .then(({response}) => {
        this.setState({
          options: response.data.resultSet,
          isLoading: false
        });
      });
  }

  _handleChange(selected) {
    this.props.handleChange(selected);
    this.setState({selected});
  }

  _handleInputChange(text) {
    this.setState({selected: [text]});
  }

  render() {
    return (
      <React.Fragment>
        <AsyncTypeahead
          {...this.state}
          onSearch={this.handleSearch}
          onChange={this._handleChange}
          onInputChange={this._handleInputChange}
        />
      </React.Fragment>
    );
  }
}
