import React from 'react';

import CascadeStaticBaseLookup from './CascadeStaticBaseLookup';

class CascadeStaticDealerLookup extends CascadeStaticBaseLookup {

  constructor(props) {
    super(props);
  }

  filterBy(option, props) {
    return option.dealerName.toLowerCase().indexOf(props.text.toLowerCase()) !== -1;
  }
}

export default CascadeStaticDealerLookup;
