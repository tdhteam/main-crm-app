export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Calendars',
      icon: 'icon-event',
      children: [
        {
          name: 'My Calendar',
          url: '/calendar/my-calendar',
          icon: 'icon-event',
        },
        {
          name: 'Shared Calendar',
          url: '/calendar/shared-calendar',
          icon: 'icon-event',
        },
        {
          name: 'List View',
          url: '/calendar/calendar-list-view',
          icon: 'icon-event',
        }
      ]
    },
    {
      name: 'Products',
      url: '/product',
      icon: 'icon-tag'
    },
    {
      name: 'Contacts',
      url: '/contact',
      icon: 'icon-people'
    },
    {
      name: 'Contracts',
      url: '/contract',
      icon: 'icon-docs'
    },
    {
      name: 'Services',
      url: '/customer-service',
      icon: 'icon-phone'
    },
    {
      name: 'Reports',
      url: '/report',
      icon: 'icon-chart',
      children: [
        {
          name: 'Contacts By Status',
          url: 'contact-by-status'
        },
        {
          name: 'Contacts By Source',
          url: 'contact-by-source'
        },
        {
          name: 'Test Drives',
          url: 'test-drive'
        },
        {
          name: 'Contracts Status',
          url: 'contract-by-status'
        },
        {
          name: 'Services Status',
          url: 'contract-by-status'
        },
        {
          name: 'Sales Funnel',
          url: 'report-sale-funnel'
        },
        {
          name: 'Revenue By Salesperson',
          url: 'report-revenue-by-salesperson'
        }
      ]
    },
    {
      name: 'Settings',
      icon: 'icon-settings',
      url: '/setting'
    }
  ]
};
