export default {
  items: [
    {
      name: 'Users Management',
      icon: 'icon-user',
      children: [
        {
          name: 'Roles',
          url: '/setting/role'
        },
        {
          name: 'Users',
          url: '/setting/user'
        },
        {
          name: 'Permissions',
          url: '/setting/permission'
        }
      ]
    },
    {
      name: 'CRM Settings',
      url: '/setting/crm-setting',
      children: [
        {
          name: 'Company',
          url: '/setting/company'
        },
        {
          name: 'Dealers',
          url: '/setting/dealer'
        },
        {
          name: 'Showrooms',
          url: '/setting/showroom'
        },
        {
          name: 'System Configurations',
          url: '/setting/system-configuration'
        },
        {
          name: 'Picklist Editor',
          url: '/setting/picklist-editor'
        }
      ]
    }
  ]
};
