import React, { Fragment } from 'react';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButton,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Nav,
    NavLink
} from 'reactstrap';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
export default class LookupControlComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            selected: [],
            allowNew: false,
            isLoading: false,
            multiple: false,
            options: [],
            //for modal
            modal: false,
            backdrop: true,
            //for grid
            searchRequest: {
                orderAsc: '',
                orderBy: [],
                pageNumber: 1,
                pageSize: 10,

            },
            sorted: [],
            filtered: [],
            pageInfo: {
                resultSet: [],
                current_page: 1,
                total_count: 0,
                pageTotal: 1,
                hasMore: false,
                resultCount: 0,
                pageNumber: 1
            }
        };



        this.handleLookupSearch = this.handleLookupSearch.bind(this);
        this.handleGridSearch = this.handleGridSearch.bind(this);
        this.onRowClick = this.onRowClick.bind(this);

        this.toggle = this.toggle.bind(this);
    }


    componentDidMountMount() {

    }

    handleLookupSearch(query) {
        this.setState({ isLoading: true });
        this.props.onLookupSearch(query)
            .then(({ options }) => {
                this.setState({
                    isLoading: false,
                    options
                });
            });
    }

    handleGridSearch() {
        if (this.state.sorted.length > 0) {
            let sort = { orderAsc: !this.state.sorted[0].desc, orderBy: this.state.sorted[0].id };
            this.setState(Object.assign(this.state.searchRequest, sort));
        } else {
            let sort = { orderAsc: false, orderBy: '' };
            this.setState(Object.assign(this.state.searchRequest, sort));
        }

        let request = Object.assign({}, this.state.searchRequest);

        if (this.state.filtered.length > 0) {
            this.state.filtered.forEach(function (element) {
                request = Object.assign(request, { [element.id]: element.value });
            }, this);
        }

        this.props.onGridSearch({ searchRequest: this.state.searchRequest })
            .then((response) => {
                this.setState({ pageInfo: response.data });
            });
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    onRowClick(e, t, rowInfo) {
        if (rowInfo === undefined) return;
        debugger;
        const selected = [{ id: rowInfo.original.id, name: rowInfo.original.firstName }];
        this.setState({selected})
        this.toggle();

    }

    onChange(name, data) {
        if (name === 'sorted' || name === 'filtered') {
            this.setState(Object.assign(this.state, data));
        }
        else {
            this.setState(Object.assign(this.state.searchRequest, data));
        }
        if (name != "filtered") {
            this.handleGridSearch();
        }
    }

    render() {
        const {
            gridColumns,
            valueField,
            displayField,
            input,
            label,
            type,
            // meta: { touched, error, warning },
            labelKey
        } = this.props;
        const result = this.state.pageInfo;
        return (
            <div>
                <Row >
                    <Col xs="12" sm="9" md="9" lg="9">
                        <Fragment>
                            <AsyncTypeahead
                                selected={this.state.selected}
                                allowNew={this.state.allowNew}
                                isLoading={this.state.isLoading}
                                multiple={this.state.multiple}
                                options={this.state.options}
                                useCache={true}
                                labelKey="name"
                                minLength={1}
                                onSearch={this.handleLookupSearch}
                                placeholder={label}
                                onChange={(selected) => {
                                    console.log(selected);
                                    this.setState({ selected });
                                }}
                                renderMenuItemChildren={(option, props, index) => (
                                    <div>
                                        <span>{option.name}</span>
                                    </div>
                                )}
                            />
                        </Fragment>
                    </Col>
                    <Col xs="12" sm="3" md="3" lg="3" className="float-sm-left">
                        {/* <Button color="info" size="sm" className="btn-sm" type="button">Search</Button> */}
                        <i className="icon-magnifier-add icons font-2xl d-block mt-2" onClick={this.toggle}></i>
                        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} backdrop={false} size="lg">
                            <ModalHeader toggle={this.toggle}>Search Form</ModalHeader>
                            <ModalBody>

                                <div className="animated fadeIn">
                                    <Row>
                                        <Col xs="12" lg="12">
                                            <Card>
                                                <CardHeader>
                                                    <Button type='button' color='primary' onClick={this.handleGridSearch}>Search</Button>
                                                </CardHeader>
                                                <CardBody>
                                                    <ReactTable
                                                        manual
                                                        // Controlled props
                                                        sorted={this.state.searchRequest.sorted}
                                                        page={result.pageNumber - 1}
                                                        pages={result.pageTotal}
                                                        pageSize={this.state.searchRequest.pageSize}
                                                        filtered={this.state.searchRequest.filtered}
                                                        // Callbacks
                                                        onSortedChange={sorted => this.onChange('sorted', { sorted: sorted })}
                                                        onPageChange={page => this.onChange('page', { pageNumber: page + 1 })}
                                                        onPageSizeChange={(pageSize, page) => this.onChange('pageSize', { pageNumber: page + 1, pageSize: pageSize })}
                                                        onFilteredChange={filtered => this.onChange('filtered', { filtered })}
                                                        data={result.resultSet}
                                                        filterable
                                                        columns={this.props.gridColumns}
                                                        defaultPageSize={10}
                                                        className="-striped -highlight"
                                                        getTdProps={(state, rowInfo, column, instance) => {
                                                            return {
                                                                onClick: (e, t) => { this.onRowClick(e, t, rowInfo) }
                                                            }
                                                        }}
                                                    />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                {/* <Button color="secondary" onClick={this.props.onToggle}>Search</Button> */}
                            </ModalFooter>
                        </Modal>
                    </Col>
                </Row>
            </div>
        );
    }

}