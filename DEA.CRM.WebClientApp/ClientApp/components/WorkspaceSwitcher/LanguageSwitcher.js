import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { Translate, getTranslate, getActiveLanguage, setActiveLanguage } from 'react-localize-redux';

import * as actions from "../../actions/appGeneralAction";
import StorageService from '../../services/storageService';
import { APP_CONFIG } from '../../appConfig';

class LanguageSwitcher extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedLanguageCode: this.props.activeLanguageCode || 'en',
      languageList: [
        {name:'English', code: 'en'},
        {name:'Vietnamese', code: 'vi'}
      ]
    };

    this._onOk = this._onOk.bind(this);
    this._handleChange = this._handleChange.bind(this);
  }

  _onOk() {
    this.props.toggle();
    const {selectedLanguageCode} = this.state;
    if (selectedLanguageCode === '') {
      return;
    }
    StorageService.setItemToLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.activeLanguageCode, selectedLanguageCode);
    this.props.setActiveLanguage(selectedLanguageCode);
  }

  _handleChange(e) {
    if (e.target.value === '') {
      return;
    }
    this.setState({
      selectedLanguageCode: e.target.value
    });
  }

  render() {
    const {isOpen, className} = this.props;
    const {languageList, selectedLanguageCode} = this.state;

    return (
      <React.Fragment>
        <Modal isOpen={isOpen} className={className} toggle={this.props.toggle}>
          <ModalHeader toggle={this.props.toggle}><Translate id="ChangeLanguage">Change Language</Translate></ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="language">Select Language</Label>
              <Input type="select" name="language" onChange={this._handleChange} value={selectedLanguageCode}>
                <option value="">Select</option>
                {
                  languageList.map((item, index) => {
                    return <option key={index} value={item.code}>{item.name}</option>;
                  })
                }
              </Input>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this._onOk}>Ok</Button>{' '}
            <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    translate: getTranslate(state.locale),
    activeLanguageCode: getActiveLanguage(state.locale).code
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    setActiveLanguage: (l) => dispatch(setActiveLanguage(l))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSwitcher);
