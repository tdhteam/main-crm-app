import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  FormGroup,
  Label
} from 'reactstrap';

import CascadeStaticDealerLookup from '../AppLookup/CascadeStaticDealerLookup';
import CascadeStaticShowroomLookup from '../AppLookup/CascadeStaticShowroomLookup';
import * as actions from "../../actions/appGeneralAction";
import StorageService from '../../services/storageService';
import { APP_CONFIG } from '../../appConfig';

class WorkspaceSwitcher extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedDealer: props.currentWorkContextInfo.dealer || [],
      selectedShowroom: props.currentWorkContextInfo.showroom || [],
      dealerList: props.currentUserInfo.userInfoToDealer || [],
      showRoomList: props.currentUserInfo.userInfoToShowroom || []
    };

    this._onOk = this._onOk.bind(this);
    this._dealerChange = this._dealerChange.bind(this);
    this._showroomChange = this._showroomChange.bind(this);
  }

  _onOk() {
    this.props.toggle();

    const {selectedDealer, selectedShowroom} = this.state;
    const workCtxInfo = {
      company: this.props.currentUserInfo.userInfoToCompany,
      dealer: selectedDealer,
      showroom: selectedShowroom
    };

    StorageService.setObjToLocalStore(APP_CONFIG.APP_LOCAL_STORAGE_KEYS.appWorkContextInfo, workCtxInfo);

    this.props.actions.dispatchWorkspaceContextRequest(workCtxInfo);
  }

  _dealerChange(selected) {
    this.setState({
      selectedDealer: selected,
      selectedShowroom: []
    });
  }

  _showroomChange(selected) {
    this.setState({
      selectedShowroom: selected
    });
  }

  render() {

    const {isOpen, className} = this.props;
    const {selectedDealer, selectedShowroom, dealerList, showRoomList} = this.state;
    const selectedDealerId = (selectedDealer && selectedDealer.length > 0 && selectedDealer[0].dealerId) || 0;

    return (
      <React.Fragment>
        <Modal isOpen={isOpen} className={className} toggle={this.props.toggle}>
          <ModalHeader toggle={this.props.toggle}>Change Workspace</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="dealer">Dealer</Label>
              <CascadeStaticDealerLookup
                className="form-control"
                name="dealer"
                labelKey="dealerName"
                selected={selectedDealer}
                options={dealerList}
                handleChange={this._dealerChange}/>
            </FormGroup>
            <FormGroup>
              <Label for="showroom">Showroom</Label>
              <CascadeStaticShowroomLookup
                className="form-control"
                name="showroom"
                labelKey="showroomName"
                selected={selectedShowroom}
                parentFilterByValue={selectedDealerId}
                options={showRoomList}
                handleChange={this._showroomChange}/>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this._onOk}>Ok</Button>{' '}
            <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUserInfo: state.loginPageInfo.authenticatedUserInfo || {},
    currentWorkContextInfo: state.workspaceContextInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkspaceSwitcher);
