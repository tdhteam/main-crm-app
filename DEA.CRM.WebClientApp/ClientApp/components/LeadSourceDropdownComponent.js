import React from 'react';
import PropTypes from 'prop-types';


export default class LeadSourceDropdown extends React.Component {

  constructor(props, context) {
    super(props, context);

  }

  
  componentDidMountMount() {
   
  }

  
  render() {
    const {
      input,
      meta: { touched, error },
      ...rest
    } = this.props;

    return (
      <div>
       
        {touched &&
          error &&
          <span className="datepicker__error">
            {error}
          </span>}
      </div>

    );
  }

}