import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import {
  Progress
} from 'reactstrap';
import LaddaButton, { SLIDE_LEFT } from 'react-ladda';
import 'ladda/dist/ladda-themeless.min.css';

import AttachmentApi from '../../api/attachmentApi';

class UploadSingleFile extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      isUploading: false,
      isUploadedSuccess: false,
      uploadProgress: 10,
      uploadedFileName: ''
    };

    this.updateUploadProgress = this.updateUploadProgress.bind(this);
    this.onDropAccepted = this.onDropAccepted.bind(this);
  }

  updateUploadProgress(newUploadProgress) {
    this.setState({
      uploadProgress: newUploadProgress
    });
  }

  onDropAccepted(accepted) {
    console.log('accepted : ',accepted);
    if (!accepted || accepted.length === 0)
      return;
    this.setState({
      isUploading: true
    });
    const uploadPayload = {
      uploadedFile: accepted[0]
    };
    AttachmentApi.uploadLargeFile(uploadPayload, this.updateUploadProgress)
      .then(({response}) => {
        console.log('upload done : ', response);
        let uploadedSuccess = response.status === 200 && response.data.success && response.data.viewModel;
        let uploadedFileName = '';
        if (uploadedSuccess){
          this.props.onUploadSuccess(response.data.viewModel);
          uploadedSuccess = true;
          uploadedFileName = response.data.viewModel.fileName;
        }
        else {
          this.props.onUploadFailed(response);
        }
        this.setState({
          isUploading: false,
          isUploadedSuccess: uploadedSuccess,
          uploadedFileName: uploadedFileName
        });
      });
  }

  render() {
    const {isUploading, uploadProgress, isUploadedSuccess, uploadedFileName} = this.state;
    let dropzoneRef;

    return (
      <div className="attachment-container">
        <Dropzone
          multiple={false}
          ref={(node) => { dropzoneRef = node; }}
          onDropAccepted={this.onDropAccepted}
        >
          <p>Drop files here.</p>
        </Dropzone>
        <LaddaButton
          type="button"
          className="btn btn-success btn-ladda"
          loading={isUploading}
          onClick={() => {dropzoneRef.open()}}
          data-color="green"
          data-style={SLIDE_LEFT}
          disabled={isUploading}
        >
          <i className="fa fa-paperclip"/>
          {' Add file'}
        </LaddaButton>
        {isUploading && <Progress animated value={uploadProgress} className="mb-3"/>}
        {isUploadedSuccess && <span>{uploadedFileName}</span>}
      </div>
    );
  }
}

UploadSingleFile.propTypes = {
  onUploadSuccess: PropTypes.func,
  onUploadFailed: PropTypes.func
};

export default UploadSingleFile;
