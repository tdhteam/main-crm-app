import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default class renderDatePicker extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      selectedDate: moment()
    };

    this.handleChange = this.handleChange.bind(this);
  }

  
  componentDidMountMount() {
    if (this.props.input.value) {
      this.setState({
        selectedDate: moment(this.props.input.value, this.props.inputValueFormat),
      });
    }
  }

  handleChange(date) {
    this.setState({
      selectedDate: date,
    });
    this.props.input.onChange(date);
  }

  render() {
    const {
      input,
      meta: { touched, error },
      ...rest
    } = this.props;

    return (
      <div>
        <DatePicker
          {...rest}
          // selected={this.state.selectedDate}
          selected={input.value ? moment(input.value) : this.state.selectedDate} 
          onChange={this.handleChange}
          className="form-control"
          peekNextMonth
          showMonthDropdown
          showYearDropdown
          dropdownMode="select"
        />
        {touched &&
          error &&
          <span className="datepicker__error">
            {error}
          </span>}
      </div>

    );
  }

}