import React, {Component} from 'react';
import { NavLink as RouteNavLink, Link  } from 'react-router-dom';
import {
  Nav,
  NavbarToggler,
  NavItem,
} from 'reactstrap';
import HeaderDropdown from './HeaderDropdown';

class Header extends Component {

  constructor(props) {
    super(props);
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"/>
        </NavbarToggler>
        <Link to="/" className="navbar-brand"/>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
          <span className="navbar-toggler-icon"/>
        </NavbarToggler>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <RouteNavLink to="/dashboard" className="nav-link">Dashboard</RouteNavLink>
          </NavItem>
          <NavItem className="px-3">
            <RouteNavLink to="/calendar" className="nav-link">Calendar</RouteNavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <HeaderDropdown accnt/>
        </Nav>
        <NavbarToggler className="d-md-down-none" onClick={this.asideToggle}>
          <span className="navbar-toggler-icon"/>
        </NavbarToggler>
      </header>
    );
  }
}

export default Header;
