import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span>CRM Version 0.1 prototype build</span>
        <span className="ml-auto">Powered by <a href="http://www.dealersedge.asia">DealersEdgeAsia</a></span>
      </footer>
    )
  }
}

export default Footer;
