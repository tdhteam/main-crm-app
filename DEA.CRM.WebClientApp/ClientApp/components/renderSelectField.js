import React from 'react';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButton,
    Table
} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';

const renderSelectField = ({
    input,
    label,
    options,
    valueField,
    displayField,
    meta: { touched, error, warning }
}) => (
        <FormGroup>
            <Label >{label}</Label>
            <Input {...input} type="select" placeholder={label} >
                <option />
                {
                    options.map(function (value, index) {
                        return <option key={index} value={value[valueField]}>{value[displayField]}</option>;
                    })
                }
            </Input>
            {/* <Select
                {...input} type={type} placeholder={label} type={type}
            /> */}
            {touched && ((error && <span className='error'>{error}</span>) || (warning && <span className='error'>{warning}</span>))}
        </FormGroup>
    );

export default renderSelectField;