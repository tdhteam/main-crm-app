import React from 'react';
import {ButtonGroup, Input} from 'reactstrap';
import range from 'lodash/range';

class DurationSelect extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      day: props.day || 0,
      hour: props.hour || 0,
      minute: props.minute || 0
    };

    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(e) {
    const {day, hour, minute} = this.state;
    this.setState({
      [e.target.name] : parseInt(e.target.value)
    });

    const newDuration = {
      day,
      hour,
      minute
    };
    newDuration[e.target.name] = parseInt(e.target.value);
    this.props.onDurationChange(newDuration);
  }

  render() {
    const {day, hour, minute} = this.state;
    const dayList = range(31);
    const hourList = range(24);
    const minuteList = range(60);
    return (
      <ButtonGroup>
        <Input type="select" name="day" onChange={this._handleChange} value={day}>
          {dayList.map((item, index) => (
            <option key={index} value={item}>{item}</option>
          ))}
        </Input>
        <span className="text-padding">days</span>
        <Input type="select" name="hour" onChange={this._handleChange} value={hour}>
          {hourList.map((item, index) => (
            <option key={index} value={item}>{item}</option>
          ))}
        </Input>
        <span className="text-padding">hours</span>
        <Input type="select" name="minute" onChange={this._handleChange} value={minute}>
          {minuteList.map((item, index) => (
            <option key={index} value={item}>{item}</option>
          ))}
        </Input>
        <span className="text-padding">minutes</span>
      </ButtonGroup>
    );
  }

}

export default DurationSelect;
