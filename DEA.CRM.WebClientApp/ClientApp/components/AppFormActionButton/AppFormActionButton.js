import React from 'react';
import { Translate } from 'react-localize-redux';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';

const AppFormActionButton = ({isSubmitting, dirty, handleSubmit, navigateBack}) => {
  return (
    <React.Fragment>
      <LaddaButton
        className="btn btn-success btn-ladda"
        loading={isSubmitting}
        onClick={handleSubmit}
        data-color="green"
        data-style={SLIDE_LEFT}
        disabled={isSubmitting || !dirty}
      >
        <Translate id="Shared.save">Save</Translate>
      </LaddaButton>{' '}
      <LaddaButton
        type="button"
        className="btn btn-cancel btn-ladda"
        loading={isSubmitting}
        onClick={navigateBack}
        data-color="green"
        data-style={SLIDE_LEFT}
        disabled={isSubmitting}
      >
        <Translate id="Shared.cancel">Cancel</Translate>
      </LaddaButton>
    </React.Fragment>
  );
};

export default AppFormActionButton;
