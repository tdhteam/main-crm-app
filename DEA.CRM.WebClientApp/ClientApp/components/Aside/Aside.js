import React, {Component} from 'react';
import {TabContent, TabPane, Nav, NavItem, NavLink, Progress, Label, Input} from 'reactstrap';
import classnames from 'classnames';

class Aside extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <aside className="aside-menu">
        <Nav tabs>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                     onClick={() => { this.toggle('1'); }}>
              <i className="icon-list"/>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <div className="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
              <small><b>Today</b></small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout callout-warning m-0 py-3">
              <div className="avatar float-right">
                <img src={'img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              </div>
              <div>Call <strong>Tuan Tran</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"/>&nbsp; 1 - 2pm</small>
              <small className="text-muted"><i className="icon-location-pin"/>&nbsp; Call</small>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-info m-0 py-3">
              <div className="avatar float-right">
                <img src={'img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              </div>
              <div>Call <strong>Nam Le</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"/>&nbsp; 4 - 5pm</small>
              <small className="text-muted"><i className="icon-social-skype"/>&nbsp; Call</small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
              <small><b>Tomorrow</b></small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout callout-danger m-0 py-3">
              <div>Car service - <strong>deadline</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"/>&nbsp; 10 - 11pm</small>
              <small className="text-muted"><i className="icon-home"/>&nbsp; CSKH Q1</small>
              <div className="avatars-stack mt-2">
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/3.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/5.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
              </div>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-success m-0 py-3">
              <div><strong>Car service</strong> Yen Tran</div>
              <small className="text-muted mr-3"><i className="icon-calendar"/>&nbsp; 1 - 3pm</small>
              <small className="text-muted"><i className="icon-location-pin"/>&nbsp; CSKH Q3</small>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-primary m-0 py-3">
              <div>Call <strong>Huyen Le</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"/>&nbsp; 4 - 6pm</small>
              <small className="text-muted"><i className="icon-home"/>&nbsp; SR Q5</small>
              <div className="avatars-stack mt-2">
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
              </div>
            </div>
            <hr className="mx-3 my-0"/>
          </TabPane>
        </TabContent>
      </aside>
    )
  }
}

export default Aside;
