import * as types from '../actions/actionTypes';
import initialState from '../reducers/initialState';

export default function searchCalendarListViewPageInfoReducer(state = initialState.searchCalendarListViewPageInfo, action) {
  switch(action.type) {
    case types.SEARCH_CALENDAR_ITEMS_LIST_VIEW_RESULT_SUCCESS:
      return {...action.searchCalendarListViewPageInfo, isLoading: false};

    default:
      return state;
  }
}
