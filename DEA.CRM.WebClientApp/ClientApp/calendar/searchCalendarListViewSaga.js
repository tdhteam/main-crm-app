import { call, put, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as actions from './calendarAction';
import CalendarEventApi from '../api/calendarEventApi';
import {hideLoading, showLoading} from 'react-redux-loading-bar';

function* searchCalendarItemsListView(searchRequest) {

  try{

    yield put(showLoading());

    const { response, error } = yield call(CalendarEventApi.searchCalendarItemsListView, searchRequest);

    if (response && response.status === 200){
      yield put(actions.searchCalendarItemsListViewResultAction(response.data));
    }
    else if (error){
      yield put(actions.searchCalendarItemsListViewErrorAction(error.response.status));
    }
  }
  finally {
    yield put(hideLoading());
  }

}

export function* searchCalendarItemsListViewSaga() {
  yield takeLatest(types.SEARCH_CALENDAR_ITEMS_LIST_VIEW_REQUESTED, searchCalendarItemsListView);
}
