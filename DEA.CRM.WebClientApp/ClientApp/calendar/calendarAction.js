import * as types from '../actions/actionTypes';
import { makeActionCreator } from '../actions/makeActionCreator';

export const searchCalendarItemsListViewRequestAction = makeActionCreator(types.SEARCH_CALENDAR_ITEMS_LIST_VIEW_REQUESTED, 'searchRequest');
export const searchCalendarItemsListViewResultAction = makeActionCreator(types.SEARCH_CALENDAR_ITEMS_LIST_VIEW_RESULT_SUCCESS, 'searchCalendarListViewPageInfo');
export const searchCalendarItemsListViewErrorAction = makeActionCreator(types.SEARCH_CALENDAR_ITEMS_LIST_VIEW_ERROR, 'errorStatus');

