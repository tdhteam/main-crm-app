import axios from "axios/index";
import buildQuery from "../utility/queryString";

export default class ShowroomApi {
  static get(params) {
    return axios.get('showrooms' + '?' + buildQuery(params.searchRequest));
  }

  static updateShowroomDetail(payload) {
    if (payload.id === 0){
      return axios.post("showrooms", payload)
        .then(response => ({response}));
    }
    return axios.put("showrooms", payload)
      .then(response => ({response}));
  }

  static getById(payload){
    const {showroomId} = payload;
    return axios.get(`showrooms/${showroomId}`)
      .then (response => ({response}))
      .catch(error => ({error}));
  }


}
