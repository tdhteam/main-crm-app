import axios from 'axios';

export default class UserProfilePreferenceApi {

  static get apiPathForRequiredPreference() {
    return 'user-profiles/required-preference';
  }

  static getUserRequiredPreference() {
    return axios.get(`${UserProfilePreferenceApi.apiPathForRequiredPreference}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static updateUserRequiredPreference(payload) {
    return axios.post(`${UserProfilePreferenceApi.apiPathForRequiredPreference}`, payload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

}
