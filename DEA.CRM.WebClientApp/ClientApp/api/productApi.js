import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class ProductApi{
  static  get(params){
    return axios.get('products'+ '?'+ buildQuery(params.searchRequest));
  }

  static getProductDetail(payload) {
    const { productId } = payload;
    return axios.get('products/${productId}')
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }

  static addNewProduct(productPayload) {
    return axios.post("products", productPayload)
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }

  static updateProduct(productPayload) {
    return axios.put("products", productPayload)
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }

  static deleteProduct(payload) {
    const { productToDelete } = payload;
    const productId = productToDelete && productToDelete.key;  
    return axios.delete('products/${productId}')
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }
}
