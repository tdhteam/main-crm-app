import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class DealerApi{
  static  get(params){
    return axios.get('dealers'+ '?'+ buildQuery(params.searchRequest));
  }

  static getById(payload){
    const {dealerId} = payload;
    return axios.get(`dealers/${dealerId}`)
      .then (response => ({response}))
      .catch(error => ({error}));
  }

  static getDefaultDealer(){
    return axios.get(`dealers/getDefaultDealer`)
      .then (response => ({response}))
      .catch(error => ({error}));
  }

  static updateDealerDetail(dealerDetail) {
    if (dealerDetail.id === 0){
      return axios.post("dealers", dealerDetail)
        .then(response => ({response}));
    }
    return axios.put("dealers", dealerDetail)
      .then(response => ({response}));
  }
}
