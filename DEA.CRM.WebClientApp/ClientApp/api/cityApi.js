import axios from 'axios';
import buildQuery from '../utility/queryString';


export default class CityApi {
  static getCities() {
    return axios.get('cities');
  }

  static getDistrictsByCity(city) {
    return axios.get(`cities/districts/${city}`);
  }

}
