import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class UserRoleApi {

  /**
   * search users from api/accounts
   * @param {object} params contains search properties and values
   */
  static searchUsers(params) {
    return axios.get(`accounts?${buildQuery(params.searchRequest)}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * get a user detail from api/accounts
   * @param {object} payload contains props userKey
   */
  static getUserDetail(payload) {
    const { userKey } = payload;
    return axios.get(`accounts/${userKey}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Add user by posting to api/accounts
   * @param {object} userPayload contains props of user detail
   */
  static addUser(userPayload) {
    return axios.post("accounts", userPayload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Update user by putting to api/accounts
   * @param {object} userPayload contains props of user detail
   */
  static updateUser(userPayload) {
    return axios.put("accounts", userPayload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Deactivate a user
   * @param {object} payload contains props userKey, transferOwnershipToUserKey
   */
  static deactivateUser(payload) {
    const { userKey, transferOwnershipToUserKey } = payload;
    return axios.delete(`accounts/${userKey}/${transferOwnershipToUserKey}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Get list of roles from api/roles
   */
  static getAllRoles() {
    return axios.get("roles")
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Get list of roles in the form of tree
   */
  static getRoleTree() {
    return axios.get("roles/tree")
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static getDefaultNewRole(payload) {
    const { parentRoleId } = payload;
    return axios.get(`roles/new/${parentRoleId}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static getRoleDetail(payload) {
    const { roleId } = payload;
    return axios.get(`roles/${roleId}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static addNewRole(rolePayload) {
    return axios.post("roles", rolePayload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static updateRole(rolePayload) {
    return axios.put("roles", rolePayload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static deleteRole(payload) {
    const { roleToDelete, roleToTakeOwnership } = payload;
    const roleId = roleToDelete && roleToDelete.key;
    const transferOwnershipToRoleId = roleToTakeOwnership && roleToTakeOwnership.key;
    return axios.delete(`roles/${roleId}/${transferOwnershipToRoleId}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

}
