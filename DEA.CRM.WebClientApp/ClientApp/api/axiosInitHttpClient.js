import axios from 'axios';
import Swal from 'sweetalert2';

import StorageService from '../services/storageService';
import {APP_CONFIG} from '../appConfig';

export function configureHttpClientInterceptors() {
  // Add a response interceptor
  axios.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    // Redirect to login if we have 401, 403 status code
    if (error && error.response
      && (error.response.status === 401 || error.response.status === 403)) {

      Swal({
        title: "Unauthorized access",
        text: "You need to login to the application",
        type: "error",
        showCancelButton: false,
        confirmButtonText: "Login"
      })
        .then(() => {
          StorageService.clearSessionStore();
          window.location.href = '/logout';
        });
    }
    else if (error && error.response && error.response.status === 400) {

      Swal({
        title: "Application request error",
        text: error.response.data,
        type: "error",
        showCancelButton: false,
        confirmButtonText: "Reload"
      })
        .then(() => {
          StorageService.clearSessionStore();
          window.location.href = '/logout';
        });
    }
    else if (error && error.response && error.response.status === 500) {

      Swal({
        title: "Server error",
        text: error.response.data,
        type: "error",
        showCancelButton: false,
        confirmButtonText: "Reload"
      })
        .then(() => {
          StorageService.clearSessionStore();
          window.location.href = '/logout';
        });
    }
    // Else just pass back response error for now
    return Promise.reject(error);
  });
}

export function initDefaultHttpClient(authToken) {
  axios.defaults.baseURL = APP_CONFIG.API_URLS.ApiBaseUrl;
  axios.defaults.timeout = APP_CONFIG.GeneralApiTimeOut;
  axios.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
  axios.defaults.headers.post['Content-Type'] = 'application/json';
}
