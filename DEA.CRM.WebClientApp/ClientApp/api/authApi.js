import axios from 'axios';
import { APP_CONFIG } from '../appConfig';

export default class AuthApi {

  static login(payload) {
    const instance = axios.create({
      baseURL : APP_CONFIG.API_URLS.ApiBaseUrl,
      timeout : APP_CONFIG.GeneralApiTimeOut
    });
    instance.defaults.headers.common['Authorization'] = '';
    instance.defaults.headers.post['Content-Type'] = 'application/json';

    return instance.post("auth/login", {
      UserName: payload.userName,
      Password: payload.password
    })
    .then(response => ({response}))
    .catch(error => ({error}));
  }

}
