import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class CompanyApi{
  static getCompanyDetail(payload) {
    const { companyId } = payload;
    return axios.get(`companies/${companyId}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static updateCompanyDetail(companyDetail) {
    return axios.put("companies", companyDetail)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static getFirstCompany() {
    return axios.get(`companies/getFirstCompany`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }



}
