import axios from 'axios';

export default class DashboardApi {

  static getDashboard() {
    const instance = axios.create();

    return instance.get("dashboard/home")
      .then(response => ({response}))
      .catch(error => ({error}));
  }

}
