import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class AttachmentApi {

  /**
   * Get all attachments available in system
   *
   */
  static getAllAttachments() {
    return axios.get('attachment/all')
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static uploadFile(payload) {
    // upload as multipart form
    let formData = new FormData();
    Object.keys(payload).forEach( objKey => formData.append(objKey, payload[objKey]));

    const config = {
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log('upload percentCompleted : ', percentCompleted);
      }
    };

    let reqConfig = {...axios.defaults};
    reqConfig.headers.post['Content-Type'] = 'multipart/form-data';
    const instance = axios.create(reqConfig);

    return axios.post('attachment/small-upload', formData, config)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Upload file using streaming method
   * @param {object} payload is a json object contains file information. Payload format as {uploadedFile: fileLikeObject, ...others object prop}
   * @param {function} onUploadProgressUpdate a callback function to execute while upload is inprogress
   */
  static uploadLargeFile(payload, onUploadProgressUpdate) {
    // upload as multipart form
    let formData = new FormData();
    Object.keys(payload).forEach( objKey => formData.append(objKey, payload[objKey]));
    const {size, type} = payload.uploadedFile;
    formData.append("size", size);
    formData.append("type", type);

    const config = {
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log('upload percentCompleted : ', percentCompleted);
        if (onUploadProgressUpdate && typeof(onUploadProgressUpdate) === 'function'){
          onUploadProgressUpdate(percentCompleted);
        }
      }
    };

    let reqConfig = {...axios.defaults};
    reqConfig.headers.post['Content-Type'] = 'multipart/form-data';
    const instance = axios.create(reqConfig);

    return axios.post('attachment/large-upload', formData, config)
      .then(response => ({response}))
      .catch(error => ({error}));
  }
}
