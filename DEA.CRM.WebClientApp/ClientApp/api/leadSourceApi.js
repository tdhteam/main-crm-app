import axios from 'axios';
import buildQuery from '../utility/queryString';


export default class LeadSourceApi {
  static get() {
    return axios.get('leadsources');
  }

}
