import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class CalendarEventApi {

  static get calendarEventsApiPath() {
    return 'calendar-events';
  }

  static get eventsApiPath() {
    return 'calendar-events/events';
  }

  static get activityApiPath() {
    return 'calendar-events/activities';
  }

  /**
   * search activities and events from api/calendar-events/list
   * @param {object} params contains search properties and values
   */
  static searchCalendarItemsListView(params) {
    return axios.get(`${CalendarEventApi.calendarEventsApiPath}/list?${buildQuery(params.searchRequest)}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Get calendar events by month
   * @param {int} month
   */
  static getCalendarItemsByMonth(month) {
    return axios.get(`${CalendarEventApi.calendarEventsApiPath}/month/${month}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * get a activity detail from api/calendar-events/activities
   * @param {number} id is key of activity
   */
  static getActivityDetail(id) {
    return axios.get(`${CalendarEventApi.activityApiPath}/${id}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Add activity by posting to api/calendar-events/activities
   * @param {object} payload contains props of activity detail
   */
  static addActivity(payload) {
    return axios.post(`${CalendarEventApi.activityApiPath}`, payload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Update activity by putting to api/calendar-events/activities
   * @param {object} payload contains props of activity detail
   */
  static updateActivity(payload) {
    return axios.put(`${CalendarEventApi.activityApiPath}`, payload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Add or Update activity
   * @param {object} payload contains props of activity detail
   */
  static addOrUpdateActivity(payload) {
    const {id} = payload;

    if (id <= 0)
      return CalendarEventApi.addActivity(payload);

    return CalendarEventApi.updateActivity(payload);
  }

  /**
   * Deactivate a activity
   * @param {number} id is key of activity
   */
  static deleteActivity(id) {
    return axios.delete(`${CalendarEventApi.activityApiPath}/${id}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * get a event detail from api/calendar-events/activities
   * @param {number} id is key of activity
   */
  static getEventDetail(id) {
    return axios.get(`${CalendarEventApi.eventsApiPath}/${id}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Add event
   * @param {object} payload contains props of event detail
   */
  static addEvent(payload) {
    return axios.post(`${CalendarEventApi.eventsApiPath}`, payload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Update event
   * @param {object} payload contains props of event detail
   */
  static updateEvent(payload) {
    return axios.put(`${CalendarEventApi.eventsApiPath}`, payload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  /**
   * Add or Update event
   * @param {object} payload contains props of event detail
   */
  static addOrUpdateEvent(payload) {
    const {id} = payload;

    if (id <= 0)
      return CalendarEventApi.addEvent(payload);

    return CalendarEventApi.updateEvent(payload);
  }

  /**
   * Deactivate a event
   * @param {number} id is key of event
   */
  static deleteEvent(id) {
    return axios.delete(`${CalendarEventApi.eventsApiPath}/${id}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

}
