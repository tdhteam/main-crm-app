import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class ContactApi {

  static get contactsApiPath() {
    return 'contacts';
  }

  static get contactMeetingsApiPath() {
    return 'contactMeetings';
  }

  static get contactCommentsApiPath() {
    return 'contactComments';
  }

  static searchContacts(params) {
    return axios.get(`${ContactApi.contactsApiPath}?${buildQuery(params.searchRequest)}`);
  }

  static getContactDetail(id) {
    return axios.get(`${ContactApi.contactsApiPath}/${id}`)
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }

  static getContactDetailForm(id) {
    return axios.get(`${ContactApi.contactsApiPath}/form/${id}`)
      .then(response => ({ response }))
      .catch(error => ({ error }));
  }

  static updateContact(contact) {
    return axios.post(`${ContactApi.contactsApiPath}`, contact);
    // .then(response => ({response}))
    // .catch(error => ({error}));
  }

  static updateMeetingProgress(meeting) {
    return axios.post(`${ContactApi.contactMeetingsApiPath}`, meeting);
    // .then(response => ({response}))
    // .catch(error => ({error}));
  }

  static updateComment(comment) {
    return axios.post(`${ContactApi.contactCommentsApiPath}`, comment);
  }

  static updateCommentAttach(attachInfo) {
    return axios.post(`${ContactApi.contactCommentsApiPath}/attachment`, attachInfo);
  }

  static getContatLookup(query) {
    let filter = "firstName = query & pageNumber=1 & pageSize=100";
    return axios.get(`${ContactApi.contactsApiPath}?${filter}`)
    .then(response => (response.data))
    .then(({resultSet, resultCount}) => {
        const options = resultSet.map((i) => ({
          id: i.id,
          name: `${i.firstName} - ${i.lastName}`
        }));
        return { options, resultCount };
      });
  }

}
