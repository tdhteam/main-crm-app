import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class UserDocumentApi {

  static get apiPath() {
    return 'user-documents';
  }

  /**
   * search user documents from api/user-documents
   * @param {object} params contains search properties and values
   */
  static searchUserDocuments(params) {
    return axios.get(`${UserDocumentApi.apiPath}?${buildQuery(params.searchRequest)}`)
      .then(response => ({response}))
      .catch(error => ({error}));
  }

  static createUserDocument(documentPayload) {
    return axios.post(`${UserDocumentApi.apiPath}`, documentPayload)
      .then(response => ({response}))
      .catch(error => ({error}));
  }
}
