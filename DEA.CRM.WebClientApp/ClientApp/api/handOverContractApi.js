import axios from 'axios';
import buildQuery from '../utility/queryString';

export default class HandOverContractApi{
  static  get(params){
    return axios.get('handovercontracts'+ '?'+ buildQuery(params.searchRequest));
  }
}
