
const buildPageRequestQueryString = (searchRequest) => {
  return Object.keys(searchRequest).map(key => key + '=' + searchRequest[key]).join('&');
};

export default buildPageRequestQueryString;
