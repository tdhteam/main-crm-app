export default  {
  hello:"xin chào",
  add: "Thêm",
  search:"Tìm kiếm",
  ChangeLanguage: "Thay Đổi Ngôn Ngữ",
  Shared: {
    company: "Công ty",
    dealer: "Đại lý",
    showroom: "Showroom",
    name: "Tên",
    address: "Địa chỉ",
    city: "Thành phố",
    district: "Quận",
    postalCode: "Mã bưu điện",
    phone: "Điện thoại",
    fax: "Fax",
    website: "Website",
    save: "Lưu",
    cancel: "Hủy"
  }
};
