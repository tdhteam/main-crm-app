
export default  {
  hello:"hello",
  add: "Add",
  search:"Search",
  ChangeLanguage: "Change Language",
  Shared: {
    company: "Company",
    dealer: "Dealer",
    showroom: "Showroom",
    name: "Name",
    address: "Address",
    city: "City",
    district: "District",
    postalCode: "Postal Code",
    phone: "Phone",
    fax: "Fax",
    website: "Website",
    save: "Save",
    cancel: "Cancel"
  }
};
