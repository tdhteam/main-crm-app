import * as types from '../actions/actionTypes';
import { makeActionCreator } from '../actions/makeActionCreator';

// export function createCourseSuccess(course) {
//   return {type: types.CREATE_COURSE_SUCCESS, course};
// }

export const searchContactRequestAction = makeActionCreator(types.SEARCH_CONTACT_REQUESTED, 'searchRequest');
export const searchContactResultAction = makeActionCreator(types.SEARCH_CONTACT_RESULT, 'contactPageInfo');
export const updateContactSuccessAction = makeActionCreator(types.UPDATE_CONTACT_SUCCESS, 'contact');
export const createContactSuccessAction = makeActionCreator(types.CREATE_CONTACT_SUCCESS, 'contact');
/*example if implement by thunk

export function loadCourses() {
  return function(dispatch) {
     dispatch(beginAjaxCall());
    return courseApi.getAllCourses().then(courses => {
      dispatch(loadCoursesAction(courses));
    }).catch(error => {
      throw(error);
    });
  };
}

export function saveCourse(course) {
  return function(dispatch, getState) {
    dispatch(beginAjaxCall());
    return courseApi.saveCourse(course).then(savedCourse => {
      course.id ? dispatch(updateCourseSuccess(savedCourse)) : dispatch(createCourseSuccess(savedCourse));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
*/