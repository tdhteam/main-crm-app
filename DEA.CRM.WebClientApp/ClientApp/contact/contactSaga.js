
import { call, take, put, takeEvery, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import * as contactActions from './contactAction';
import ContactApi from '../api/contactApi';
import axios from 'axios';


function* searchContact(request){
    try {
        const response = yield call(ContactApi.searchContacts, request);
        yield put(contactActions.searchContactResultAction(response.data));
     } catch (error) {
       // yield put({type: "FETCH_FAILED", error});
     }

}

export function* saveContact({contact}) {
    const savedContact =  yield call(ContactApi.update,contact);
    if(contact.id)
    {
      yield put(contactActions.updateContactSuccessAction(savedContact));
    } else{
      yield put(contactActions.createContactSuccessAction(savedContact));
    }

}

export function* searchContactSaga() {
  yield takeLatest(types.SEARCH_CONTACT_REQUESTED, searchContact);
}


export function* updateContactSaga() {
  yield takeLatest(types.CREATE_CONTACT, saveContact);
}







