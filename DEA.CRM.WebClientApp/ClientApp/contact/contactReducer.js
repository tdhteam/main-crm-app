import * as types from '../actions/actionTypes';
import initialState from '../reducers/initialState';

export default function contactReducer(state = initialState.contactPageInfo, action) {
    switch(action.type) {
    case types.SEARCH_CONTACT_RESULT:
       return Object.assign({}, ...state, action.contactPageInfo);

    // case types.UPDATE_CONTACT_SUCCESS:
    //   return [
    //     ...state.filter(contact => contact.id !== action.contact.id),
    //     Object.assign({}, action.contact)
    //   ];
    // case types.CREATE_CONTACT_SUCCESS:
    //   return  [
    //     ...state,
    //     Object.assign({}, action.contact)
    //   ];
 
    default:
        return state;
  }
}