import * as types from './actionTypes';
import { makeActionCreator } from './makeActionCreator';


export const searchCustomerRequestAction = makeActionCreator(types.SEARCH_CUSTOMER_REQUESTED, 'searchRequest');
export const searchCustomerResultAction = makeActionCreator(types.SEARCH_CUSTOMER_RESULT, 'customerPageInfo');
export const updateCustomerSuccessAction = makeActionCreator(types.UPDATE_CUSTOMER_SUCCESS, 'customer');
export const createCustomerSuccessAction = makeActionCreator(types.CREATE_CUSTOMER_SUCCESS, 'customer');
