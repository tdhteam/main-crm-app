import {
  LOGIN_REQUEST,
  LOGIN_OK,
  LOGIN_ERROR,
  INIT_DEFAULT_WORK_CONTEXT_AFTER_LOGIN_IN_OK,
  SET_USER_PREFERENCE_FIRST_TIME_LOGIN_SUCCESS } from './actionTypes';
import { makeActionCreator } from './makeActionCreator';

export const loginRequestAction = makeActionCreator(LOGIN_REQUEST, "userName", "password");
export const loginOkAction = makeActionCreator(LOGIN_OK, "authenticatedUserInfo");
export const loginErrorAction = makeActionCreator(LOGIN_ERROR, "authError");
export const initDefaultWorkContextAction = makeActionCreator(INIT_DEFAULT_WORK_CONTEXT_AFTER_LOGIN_IN_OK, "authenticatedUserInfo");
