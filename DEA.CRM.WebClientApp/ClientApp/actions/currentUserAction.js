import * as actionTypes from './actionTypes';
import { makeActionCreator } from './makeActionCreator';

export const fetchCurrentUserRequestAction = makeActionCreator(actionTypes.FETCH_CURRENT_USER_REQUEST);
export const setCurrentUserAction = makeActionCreator(actionTypes.SET_CURRENT_USER, "currentUserInfo");
