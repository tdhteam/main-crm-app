import * as types from './actionTypes';
import { makeActionCreator } from './makeActionCreator';


export const searchContractRequestAction = makeActionCreator(types.SEARCH_CONTRACT_REQUESTED, 'searchRequest');
export const searchContractResultAction = makeActionCreator(types.SEARCH_CONTRACT_RESULT, 'contractPageInfo');
export const updateContractSuccessAction = makeActionCreator(types.UPDATE_CONTRACT_SUCCESS, 'contract');
export const createContractSuccessAction = makeActionCreator(types.CREATE_CONTRACT_SUCCESS, 'contract');
