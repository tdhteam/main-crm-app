import * as types from "./actionTypes";
import {makeActionCreator} from "./makeActionCreator";

export const dispatchGeneralError = makeActionCreator(types.APP_GENERAL_ERROR_ACTION, "error");
export const dispatchWorkspaceContextRequest = makeActionCreator(types.CHANGE_WORKSPACE_REQUEST, "workspaceContextInfo");
export const dispatchChangeLanguageRequest = makeActionCreator(types.CHANGE_LANGUAGE_REQUEST, "cultureInfo");
export const setUserPreferenceFirstTimeLoginSuccessAction = makeActionCreator(types.SET_USER_PREFERENCE_FIRST_TIME_LOGIN_SUCCESS, "authenticatedUserInfo");
