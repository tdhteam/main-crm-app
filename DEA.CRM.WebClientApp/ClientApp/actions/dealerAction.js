import * as types from './actionTypes';
import { makeActionCreator } from './makeActionCreator';

export const searchDealerRequestAction = makeActionCreator(types.SEARCH_DEALER_REQUESTED, 'searchRequest');
export const searchDealerResultAction = makeActionCreator(types.SEARCH_DEALER_RESULT, 'dealerPageInfo');

