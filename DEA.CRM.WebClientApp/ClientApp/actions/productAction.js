import * as types from './actionTypes';
import { makeActionCreator } from './makeActionCreator';


export const searchProductRequestAction = makeActionCreator(types.SEARCH_PRODUCT_REQUESTED, 'searchRequest');
export const searchProductResultAction = makeActionCreator(types.SEARCH_PRODUCT_RESULT, 'productPageInfo');
export const updateProductRequestAction = makeActionCreator(types.UPDATE_PRODUCT, 'product');
export const updateProductSuccessAction = makeActionCreator(types.UPDATE_PRODUCT_SUCCESS, 'product');
export const updateProductFailureAction = makeActionCreator(types.UPDATE_PRODUCT_FAILURE, 'product');
export const createProductSuccessAction = makeActionCreator(types.CREATE_PRODUCT_SUCCESS, 'product');
