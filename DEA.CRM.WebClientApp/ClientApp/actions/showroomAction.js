import * as types from "./actionTypes";
import {makeActionCreator} from "./makeActionCreator";

export const searchShowroomRequestAction = makeActionCreator(types.SEARCH_SHOWROOM_REQUESTED, 'searchRequest');
export const searchShowroomResultAction = makeActionCreator(types.SEARCH_SHOWROOM_RESULT, 'showroomPageInfo');
