import 'regenerator-runtime/runtime';
import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../reducers';
import createSagaMiddleware from 'redux-saga';
//import { loadingBarMiddleware } from 'react-redux-loading-bar';
import thunk from 'redux-thunk';
import { initSagas } from '../initSaga';

export default function getStore(initialState) {

  const sagaMiddleware = createSagaMiddleware();
  const middleWares = [sagaMiddleware, thunk];
  const composables = [applyMiddleware(...middleWares)];
  const enhancer = compose(
    ... composables
    );
    const store = createStore(
        rootReducer,
        initialState,
        enhancer
    );
    initSagas(sagaMiddleware);
    return store;
}
