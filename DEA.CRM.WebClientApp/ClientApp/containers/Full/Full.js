import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Container} from 'reactstrap';
import {ToastContainer} from 'react-toastify';
import LoadingBar from 'react-redux-loading-bar';

import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/Sidebar';
import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import Aside from '../../components/Aside/Aside';
import Footer from '../../components/Footer/Footer';

import Dashboard from '../../views/Dashboard/Dashboard';

// Settings
import SettingDashboard from '../../views/Settings/SettingDashboard';
import ManageRoles from '../../views/Settings/UsersManagement/ManageRoles';
import ManageRoleDetailPage from '../../views/Settings/UsersManagement/ManageRoleDetailPage';
import SearchUserPage from '../../views/Settings/UsersManagement/SearchUserPage';
import ManageUserDetailPage from '../../views/Settings/UsersManagement/ManageUserDetailPage';

// Products
import ProductSearch from '../../views/Products/ProductSearch';
import ProductDetail from '../../views/Products/ProductDetail';
import ProductViewForm from '../../views/Products/ProductViewForm';

// Contacts
import ContactSearch from '../../views/Contacts/ContactSearch';
import ContactDetailPage from '../../views/Contacts/ContactDetailPage';

// HandOverContracts
import ContractSearch from '../../views/HandOverContracts/HandOverContractSearch';

// Customer Services
import CustomerServiceSearch from '../../views/CustomerServices/CustomerServiceSearch';

// company
import ManageCompanyDetailPage from '../../views/Settings/CompanyManagement/ManageCompanyDetailPage';

// dealers
import ManageDealers from '../../views/Settings/CompanyManagement/ManageDealers';
import ManageDealerDetailPage from '../../views/Settings/CompanyManagement/ManageDealerDetailPage';

// showrooms
import ManageShowrooms from '../../views/Settings/CompanyManagement/ManageShowrooms';
import ManageShowroomDetailPage from '../../views/Settings/CompanyManagement/ManageShowroomDetailPage';

// Calendars
import MyCalendar from '../../views/Calendars/MyCalendar';
import SharedCalendar from '../../views/Calendars/SharedCalendar';
import CalendarListViewPage from '../../views/Calendars/CalendarListViewPage';
import ActivityDetailPage from '../../views/Calendars/ActivityDetailPage';
import EventDetailPage from '../../views/Calendars/EventDetailPage';

// DocumentManager
import DocumentDetailPage from '../../views/DocumentManager/DocumentDetailPage';
import DocumentUploadPage from '../../views/Documents/DocumentUploadPage';
import LargeDocumentUploadPage from '../../views/Documents/LargeDocumentUploadPage';

// User Profile
import MyProfilePage from '../../views/UserProfile/MyProfilePage';

// Routing order is important from most specific to least specific route match
class Full extends Component {
  render() {
    const { authenticatedUserInfo } = this.props;

    return (
      <div className="app">
        {!authenticatedUserInfo && <Redirect to="/login" />}
        {authenticatedUserInfo && authenticatedUserInfo.needFirstTimeLoginSetup && <Redirect to="/first-time-login" />}
        <LoadingBar style={{ backgroundColor: 'blue', height: '5px', zIndex: 2000 }} />
        <ToastContainer position="top-right" autoClose={3000} hideProgressBar={true} style={{zIndex: 1999}}/>
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>

                <Route exact path="/my-profile" name="My Profile" component={MyProfilePage}/>

                <Route exact path="/calendar/event/new" name="New Event Details" component={EventDetailPage}/>
                <Route exact path="/calendar/event/edit/:id" name="New Event Details" component={EventDetailPage}/>
                <Route exact path="/calendar/activity/new" name="New Activity Details" component={ActivityDetailPage}/>
                <Route exact path="/calendar/activity/edit/:id" name="New Activity Details" component={ActivityDetailPage}/>
                <Route exact path="/calendar/my-calendar" name="Calendar" component={MyCalendar}/>
                <Route exact path="/calendar/shared-calendar" name="Calendar" component={SharedCalendar}/>
                <Route exact path="/calendar/calendar-list-view" name="Calendar" component={CalendarListViewPage}/>

                <Route exact path="/setting/role/new/:roleKey/:isNew" name="Add Role" component={ManageRoleDetailPage}/>
                <Route exact path="/setting/role/edit/:roleKey/:isNew" name="Edit Role" component={ManageRoleDetailPage}/>
                <Route exact path="/setting/role" name="Search Roles" component={ManageRoles}/>

                <Route exact path="/setting/user/new" name="Add User" component={ManageUserDetailPage}/>
                <Route exact path="/setting/user/edit/:userKey" name="Edit User" component={ManageUserDetailPage}/>
                <Route exact path="/setting/user" name="Search Users" component={SearchUserPage}/>

                <Route exact path="/setting/company/" name="Manage Company" component={ManageCompanyDetailPage}/>

                <Route exact path="/setting/showroom/edit/:showroomId" name="Edit Showroom" component={ManageShowroomDetailPage}/>
                <Route exact path="/setting/showroom/new" name="Add Showroom" component={ManageShowroomDetailPage}/>
                <Route exact path="/setting/showrooms/" name="Manage Showrooms" component={ManageDealers}/>

                <Route exact path="/setting/dealer/edit/:dealerId" name="Edit Dealer" component={ManageDealerDetailPage}/>
                <Route exact path="/setting/dealer/new" name="Add Dealer" component={ManageDealerDetailPage}/>
                <Route exact path="/setting/dealers/" name="Manage Dealers" component={ManageDealers}/>

                <Route exact path="/setting" name="Administration" component={SettingDashboard}/>

                <Route path="/product/new" name="Add Product" component={ProductDetail}/>
                <Route path="/product/edit/:id" name="Edit Product" component={ProductDetail}/>
                <Route path="/product/view/:id" name="View Product" component={ProductViewForm}/>
                <Route path="/product" name="Product Searchs" component={ProductSearch}/>

                <Route path="/contact/new" name="New Contact Details" component={ContactDetailPage}/>
                <Route path="/contact/edit/:id" name="Edit Contact Details" component={ContactDetailPage}/>
                <Route path="/contact" name="Contact Searchs" component={ContactSearch}/>

                <Route path="/contract" name="Contract Searchs" component={ContractSearch}/>

                <Route path="/customer-service" name="Customer Service Searchs" component={CustomerServiceSearch}/>

                <Route path="/document" name="Documents" component={DocumentUploadPage}/>
                <Route path="/large-document" name="Documents" component={LargeDocumentUploadPage}/>
                <Route path="/document-manager" name="Document Manager" component={DocumentDetailPage}/>

                <Route path="/" name="Dashboard" component={Dashboard}/>

              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticatedUserInfo: state.loginPageInfo.authenticatedUserInfo
  };
}

export default connect(mapStateToProps)(Full);
