﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DEA.CRM.Api.Infrastructure.Attributes;
using DEA.CRM.Api.ViewModels;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Timing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{        
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/attachment")]
    public class AttachmentApiController : FileUploadApiControllerBase
    {
        private readonly IAttachmentStorageService _attachmentStorageService;
        private readonly IAttachmentMetadataService _attachmentMetadataService;
        private readonly IAttachmentManager _attachmentManager;
        
        public AttachmentApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            IAttachmentManager attachmentManager, 
            IAttachmentStorageService attachmentStorageService, 
            IAttachmentMetadataService attachmentMetadataService) : base(userInfoService, httpContextAccessor, identityParser, attachmentStorageService)
        {
            _attachmentManager = attachmentManager;
            _attachmentStorageService = attachmentStorageService;
            _attachmentMetadataService = attachmentMetadataService;
        }

        [HttpPost]
        [Route("small-upload")]
        [ProducesResponseType(typeof(ResponseDto<AttachmentDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponseDto), 200)]
        public async Task<IActionResult> SmallFileUpload(DocumentFileUploadViewModel model)
        {
            // Read the file stream into bytes array
            var uploadRes = await HandleSmallFileUpload(model);

            if (uploadRes.IsFailure)
            {
                return new JsonResult(new ErrorResponseDto
                {
                    Detail = "Failed to upload file"
                });
            }

            var uploadedFileRes = uploadRes.Value;
                
            // Use attachment manager to store uploaded file and save metadata
            var attachmentDto = new AttachmentDto
            {
                FileName = uploadedFileRes.FileName,
                Description = !string.IsNullOrEmpty(model.Name) ? model.Name : uploadedFileRes.Description,
                ContentType = uploadedFileRes.ContentType,
                FileContentBytes = uploadedFileRes.FileContentBytes,
                FileSize = uploadedFileRes.Length,
                DateAttached = Clock.Now,
                CreatedByUserKey = CurrentProfile.UserKey
            };

            var newAttRes = await _attachmentManager.SaveAttachmentAsync(attachmentDto, CurrentProfile);            
            
            return newAttRes.IsSuccess
                ? new JsonResult(ResponseDto<AttachmentDto>.OkWithData(AttachmentDto.ToFileMetaResponse(newAttRes.Value)))
                : new JsonResult(ResponseDto.ErrorContent(newAttRes.Error));
        }

        [HttpPost]
        [Route("large-upload")]
        [DisableFormValueModelBinding]
        //[ValidateAntiForgeryToken]
        [ProducesResponseType(typeof(AttachmentDto), 200)]
        [ProducesResponseType(typeof(ErrorResponseDto), 200)]
        public async Task<IActionResult> HandleLargeFileUpload()
        {
            var attachmentMeta = new AttachmentDto
            {
                FileName = Guid.NewGuid().ToString(),
                DateAttached = Clock.Now,
                CreatedByUserKey = CurrentProfile.UserKey
            };
            
            var uploadRes = await HandleLargeFileUploadAsync(attachmentMeta);
            if (uploadRes.IsSuccess)
            {
                var uploadedFile = uploadRes.Value;
                attachmentMeta.FileName = uploadedFile.FileName;
                attachmentMeta.Description = uploadedFile.Description;
                attachmentMeta.ContentType = uploadedFile.ContentType;
                attachmentMeta.FileSize = uploadedFile.Length;
                attachmentMeta.FileUrl = uploadedFile.FileUrl;
                attachmentMeta.DownloadFileUrl = uploadedFile.DownloadFileUrl;
                _attachmentMetadataService.SaveAttachmentMetadata(attachmentMeta);
                return new JsonResult(
                    ResponseDto<AttachmentDto>.OkWithData(
                        AttachmentDto.ToFileMetaResponse(attachmentMeta)
                    )
                );
            }
            
            return new JsonResult(ResponseDto.ErrorContent(uploadRes.Error));
        }

        [HttpGet]
        [Route("image/{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(FileStreamResult), 200)]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DownloadAttachmentAsImage([FromRoute] string id)
        {
            var attachmentWithContent = await _attachmentManager.LoadAttachmentAsync(id, includeFileContent: true);
            if (attachmentWithContent.IsSuccess)
            {
                var ms = new MemoryStream(attachmentWithContent.Value.FileContentBytes);
                return new FileStreamResult(ms, attachmentWithContent.Value.ContentType);
            }
            return NoContent();
        }

        [HttpGet]
        [Route("thumb/{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(FileStreamResult), 200)]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DownloadImageThumb([FromRoute] string id)
        {
            var attachmentWithContent = await _attachmentManager.LoadAttachmentAsync(id, includeFileContent: true);
            if (attachmentWithContent.IsSuccess && attachmentWithContent.Value.ThumbnailFileContentBytes != null)
            {
                var ms = new MemoryStream(attachmentWithContent.Value.ThumbnailFileContentBytes);
                return new FileStreamResult(ms, attachmentWithContent.Value.ContentType);
            }
            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(FileStreamResult), 200)]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DownloadAttachment([FromRoute] string id)
        {
            var attachmentWithContent = await _attachmentManager.LoadAttachmentAsync(id, includeFileContent: true);
            if (attachmentWithContent.IsSuccess)
            {
                var ms = new MemoryStream(attachmentWithContent.Value.FileContentBytes);
                return new FileStreamResult(ms, attachmentWithContent.Value.ContentType);
            }
            return NoContent();
        }

        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(List<AttachmentDto>), 200)]
        public IActionResult DownloadAllAttachmentMetadataList([FromRoute] string id)
        {
            var allAttachments =
                _attachmentManager.FindAttachmentMetadataList(new FindAttachmentCommand(), CurrentProfile);

            return allAttachments.IsSuccess
                ? new JsonResult(allAttachments.Value)
                : new JsonResult(new List<AttachmentDto>());
        }
    }
}
