﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Customer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/Customers")]
    public class CustomerApiController : ApiControllerBase
    {
        
        private readonly ICustomerService _customerService;

        public CustomerApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            ICustomerService customerService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _customerService = customerService;
        }

        // GET api/Customer
        public IActionResult GetCustomers([ModelBinder]SearchCustomerCommand command)
        {
            var result = _customerService.SearchCustomers(command);
            return new JsonResult(result);
        }
    }
}
