﻿using System.Collections.Generic;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Calendar;
using DEA.CRM.Service.Contract.Calendar.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/calendar-events")]
    public class CalendarEventApiController : ApiControllerBase
    {
        private readonly ICalendarEventService _calendarEventService;

        public CalendarEventApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            ICalendarEventService calendarEventService) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _calendarEventService = calendarEventService;
        }

        #region Search activities and calendar events

        // GET api/calendar-events/list
        [HttpGet]
        [Route("list")]
        [ProducesResponseType(typeof(PagingResult<CalendarEventListViewSearchResultDto>), 200)]
        public IActionResult SearchCalendarItems(SearchCalendarItemQuery query)
        {
            var result = _calendarEventService.SearchCalendarItems(query, CurrentProfile);
            return new JsonResult(result);
        }

        // GET api/calendar-events/month/{m}
        [HttpGet]
        [Route("month/{month}")]
        [ProducesResponseType(typeof(List<EventItemDto>), 200)]
        public IActionResult GetCalendarItemsByMonth([FromRoute]GetCalendarQuery query)
        {
            var result = _calendarEventService.GetCalendarItemsByMonth(query, CurrentProfile);
            return new JsonResult(result);
        }

        #endregion

        #region Activities

        // GET api/calendar-events/activities/{id}
        [HttpGet]
        [Route("activities/{id}", Name = "activity_detail")]
        [ProducesResponseType(typeof(ActivityDetailFormDto), 200)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult GetActivityDetail([FromRoute]int id)
        {
            var activityFormRes = _calendarEventService.BuildActivityDetailFormSchema(id, CurrentProfile);

            if (activityFormRes.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(activityFormRes.Error));

            return new JsonResult(activityFormRes.Value);
        }

        // POST api/calendar-events/activities
        [HttpPost]
        [Route("activities")]
        [ProducesResponseType(typeof(ActivityDto), 201)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult CreateActivity([FromBody] UpdateActivityCommand command)
        {
            var createResult = _calendarEventService.CreateOrUpdateActivity(command, CurrentProfile);
            if (createResult.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(createResult.Error));

            return new CreatedAtRouteResult("activity_detail", new {createResult.Value.Id}, createResult.Value);
        }

        // PUT api/calendar-events/activities
        [HttpPut]
        [Route("activities")]
        [ProducesResponseType(typeof(ActivityDto), 200)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult UpdateActivity([FromBody] UpdateActivityCommand command)
        {
            var createResult = _calendarEventService.CreateOrUpdateActivity(command, CurrentProfile);
            if (createResult.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(createResult.Error));

            return new JsonResult(createResult.Value);
        }

        // DELETE api/calendar-events/activities/{id}
        [HttpDelete]
        [Route("activities/{id}")]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult DeleteActivity([FromRoute]int id)
        {
            var result = _calendarEventService.DeleteActivityById(id, CurrentProfile);
            return new JsonResult(result);
        }

        #endregion

        #region Calendar Events

        // GET api/calendar-events/events/{id}
        [HttpGet]
        [Route("events/{id}", Name = "calendar_event_detail")]
        [ProducesResponseType(typeof(CalendarEventDetailFormDto), 200)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult GetCalendarEventDetail([FromRoute]int id)
        {
            var eventFormRes = _calendarEventService.BuildCalendarEventDetailFormSchema(id, CurrentProfile);

            if (eventFormRes.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(eventFormRes.Error));

            return new JsonResult(eventFormRes.Value);
        }

        // POST api/calendar-events/events
        [HttpPost]
        [Route("events")]
        [ProducesResponseType(typeof(CalendarEventDto), 201)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult CreateCalendarEvent([FromBody] UpdateCalendarEventCommand command)
        {
            var createResult = _calendarEventService.CreateOrUpdateCalendarEvent(command, CurrentProfile);
            if (createResult.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(createResult.Error));

            return new CreatedAtRouteResult("calendar_event_detail", new {createResult.Value.Id}, createResult.Value);
        }

        // PUT api/calendar-events/events
        [HttpPut]
        [Route("events")]
        [ProducesResponseType(typeof(CalendarEventDto), 200)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult UpdateCalendarEvent([FromBody] UpdateCalendarEventCommand command)
        {
            var createResult = _calendarEventService.CreateOrUpdateCalendarEvent(command, CurrentProfile);
            if (createResult.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(createResult.Error));

            return new JsonResult(createResult.Value);
        }

        // DELETE api/calendar-events/events/{id}
        [HttpDelete]
        [Route("events/{id}")]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult DeleteCalendarEvent([FromRoute]int id)
        {
            var result = _calendarEventService.DeleteCalendarEventById(id, CurrentProfile);
            return new JsonResult(result);
        }

        #endregion

    }
}
