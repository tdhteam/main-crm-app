﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/roles")]
    public class RoleApiController : ApiControllerBase 
    {
        private readonly IRoleService _roleService;

        public RoleApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IRoleService roleService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _roleService = roleService;
        }
        
        // GET api/roles
        [HttpGet]
        [ProducesResponseType(typeof(PagingResult<RoleDetailDto>), 200)]
        public IActionResult GetAllRoles()
        {
            var allRoles = _roleService.GetAllRoles();
            return new JsonResult(allRoles);
        }

        // GET api/roles/tree
        [HttpGet]
        [Route("tree", Name = "role_tree")]
        [ProducesResponseType(typeof(RoleTreeDto), 200)]
        public IActionResult GetRoleTree()
        {
            var roleTree = _roleService.GetRoleTree();
            return new JsonResult(roleTree);
        }

        // GET api/roles/{roleId}
        [HttpGet]
        [ProducesResponseType(typeof(RoleDetailFormDto), 200)]
        [Route("{roleId}", Name = "detail")]
        public IActionResult GetRoleDetail(string roleId)
        {
            var roleDto = _roleService.GetRoleById(roleId);

            return new JsonResult(new RoleDetailFormDto
            {
                FormData = roleDto.Value
            });
        }

        // GET api/roles/new/{parentRoleId}
        [HttpGet]        
        [Route("new/{parentRoleId}")]
        [ProducesResponseType(typeof(RoleDetailFormDto), 200)]
        public IActionResult GetDefaultNewRole(string parentRoleId)
        {
            var roleDto = _roleService.CreateRoleDetailFormSchema(parentRoleId);
            return new JsonResult(roleDto.Value);
        }

        // POST api/roles
        [HttpPost]
        [ProducesResponseType(typeof(RoleDetailDto), 201)]
        [ProducesResponseType(typeof(RoleDetailDto), 400)]
        public IActionResult CreateRole([FromBody]CreateOrUpdateRoleCommand roleDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var roleInfoResult = _roleService.CreateOrUpdate(roleDetailDto);
            
            if(roleInfoResult.IsFailure)
                return new JsonResult(ResponseDto.ErrorContent(roleInfoResult.Error));

            var roleInfo = roleInfoResult.Value;

            return new CreatedAtRouteResult("detail", new {roleId = roleInfo.RoleId}, roleInfo);
        }

        // PUT api/roles
        [HttpPut]
        [ProducesResponseType(typeof(RoleDetailDto), 200)]
        [ProducesResponseType(typeof(RoleDetailDto), 400)]
        public IActionResult UpdateRole([FromBody]CreateOrUpdateRoleCommand roleDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var roleInfo = _roleService.CreateOrUpdate(roleDetailDto);

            return new OkObjectResult(roleInfo);
        }

        // DELETE api/roles
        [HttpDelete]
        [Route("{roleId}/{transferOwnershipToRoleId}", Name = "delete")]
        [ProducesResponseType(typeof(ResponseDto), 200)]        
        public IActionResult DeleteRole([FromRoute]DeleteRoleCommand command)
        {
            var result = _roleService.DeleteRole(command);
            return new OkObjectResult(result);
        }
    }
}
