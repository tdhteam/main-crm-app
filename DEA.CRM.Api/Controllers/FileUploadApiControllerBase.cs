﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DEA.CRM.Api.Helpers;
using DEA.CRM.Api.ViewModels;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Utils.Extensions;
using DEA.CRM.Utils.Monads;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

namespace DEA.CRM.Api.Controllers
{        
    public abstract class FileUploadApiControllerBase : ApiControllerBase
    {
        private readonly IAttachmentStorageService _attachmentStorageService;
        
        protected FileUploadApiControllerBase(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser, 
            IAttachmentStorageService attachmentStorageService) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _attachmentStorageService = attachmentStorageService;
        }
        
        private static readonly FormOptions DefaultFormOptions = new FormOptions();

        /// <summary>
        /// Handle small and simple file upload for client post as application/json
        /// </summary>
        /// <param name="fileContentAsBase64"></param>
        /// <returns></returns>
        protected Result<IFileUploadResult> HandleSmallFileUpload(string fileContentAsBase64)
        {
            if(string.IsNullOrEmpty(fileContentAsBase64))
                return Result<IFileUploadResult>.Fail("Invalid file uploaded");

            var fileContent = fileContentAsBase64.SplitBy(",");

            if (fileContent == null || fileContent.Length != 2)
                return Result<IFileUploadResult>.Fail("Invalid file uploaded");

            var bytes = Convert.FromBase64String(fileContent[1]);

            return Result.Success<IFileUploadResult>(new BasicFileUploadResult
            {
                FileContentBytes = bytes
            });
        }

        /// <summary>
        /// Handle small file upload with size is less than 2MB
        /// This method buffer file content in memory handle large file will cause performance goes down
        /// </summary>
        /// <param name="fileUploadViewModel"></param>
        /// <returns></returns>
        protected async Task<Result<IFileUploadResult>> HandleSmallFileUpload(BasicFileUploadViewModel fileUploadViewModel)
        {
            using (var memoryStream = new MemoryStream())
            {
                await fileUploadViewModel.UploadedFile.CopyToAsync(memoryStream);
                return Result.Success<IFileUploadResult>(new BasicFileUploadResult
                {
                    FileName = fileUploadViewModel.UploadedFile.FileName,
                    Description = fileUploadViewModel.UploadedFile.Name,
                    Length = fileUploadViewModel.UploadedFile.Length,
                    ContentType =  fileUploadViewModel.UploadedFile.ContentType,
                    FileContentBytes = memoryStream.ToArray()
                });
            }
        }

        /// <summary>
        /// Handle large file upload with streaming method, this method will stream the content of uploaded file
        /// to target location provided by the param attachmentMetadataDto, the attachment storage service
        /// will handle where to store file content
        /// </summary>
        /// <param name="attachmentMetadataDto"></param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException"></exception>
        protected async Task<Result<IFileUploadResult>> HandleLargeFileUploadAsync(AttachmentDto attachmentMetadataDto)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                return Result<IFileUploadResult>.Fail($"Expected a multipart request, but got {Request.ContentType}");
            }

            // Used to accumulate all the form url encoded key value pairs in the 
            // request.
            var formFields = new Dictionary<string, string>();

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType), 
                DefaultFormOptions.MultipartBoundaryLengthLimit
            );

            var reader = new MultipartReader(boundary, HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();
            var iSectionCounter = 0;
            while (section != null)
            {
                var hasContentDispositionHeader =
                    ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out var contentDisposition);

                if (hasContentDispositionHeader)
                {
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        if (!formFields.ContainsKey("filename") && contentDisposition.FileName.HasValue)
                        {
                            formFields.TryAdd("filename", contentDisposition.FileName.Value);
                            attachmentMetadataDto.FileName = contentDisposition.FileName.Value.Trim('\"');
                        }

                        await _attachmentStorageService.PutStreamToStorageAsync(section.Body, attachmentMetadataDto, iSectionCounter);
                        
                        Serilog.Log.Information($"Copied the uploaded file '{iSectionCounter++}'");
                    }
                    else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition))
                    {
                        // Content-Disposition: form-data; name="key"
                        //
                        // value

                        // Do not limit the key name length here because the 
                        // multipart headers length limit is already in effect.
                        var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name).Value;
                        var encoding = GetEncoding(section);
                        using (var streamReader = new StreamReader(
                            section.Body,
                            encoding,
                            detectEncodingFromByteOrderMarks: true,
                            bufferSize: 1024,
                            leaveOpen: true))
                        {
                            // The value length limit is enforced by MultipartBodyLengthLimit
                            var value = await streamReader.ReadToEndAsync();
                            if (string.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
                            {
                                value = string.Empty;
                            }

                            formFields.Add(key, value);

                            if (formFields.Values.Count > DefaultFormOptions.ValueCountLimit)
                            {
                                throw new InvalidDataException(
                                    $"Form key count limit {DefaultFormOptions.ValueCountLimit} exceeded.");
                            }
                        }
                    }
                }

                // Drains any remaining section body that has not been consumed and
                // reads the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }
            
            // Commit all blocks change to blob storage
            var attachmentMetaRes = await _attachmentStorageService.CommitStreamToStorageAsync(attachmentMetadataDto);
            if (attachmentMetaRes.IsSuccess)
            {
                formFields.TryGetValue("description", out var description);
                formFields.TryGetValue("filename", out var filename);
                formFields.TryGetValue("type", out var contentType);
                formFields.TryGetValue("size", out var size);
                
                return Result.Success<IFileUploadResult>(new BasicFileUploadResult
                {
                    Description = description,
                    FileName = filename,
                    ContentType = contentType,
                    Length = Convert.ToInt64(size),
                    FileUrl = attachmentMetaRes.Value.FileUrl,
                    DownloadFileUrl = attachmentMetaRes.Value.DownloadFileUrl 
                });
            }

            return Result<IFileUploadResult>.Fail(attachmentMetaRes.Error);
        }
        
        private static Encoding GetEncoding(MultipartSection section)
        {
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out var mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }
    }
}
