﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Contract.UserDocument;
using DEA.CRM.Service.Contract.UserDocument.Dto;
using DEA.CRM.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/user-documents")]    
    public class UserDocumentApiController : FileUploadApiControllerBase
    {
        private readonly IUserDocumentService _userDocumentService;
        private readonly IAttachmentManager _attachmentManager;
        
        public UserDocumentApiController(IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            IAttachmentStorageService attachmentStorageService, 
            IUserDocumentService userDocumentService, 
            IAttachmentManager attachmentManager) : base(userInfoService, httpContextAccessor, identityParser, attachmentStorageService)
        {
            _userDocumentService = userDocumentService;
            _attachmentManager = attachmentManager;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagingResult<UserDocumentDto>), 200)]
        public IActionResult SearchUserDocuments(SearchUserDocumentQuery query)
        {
            return new JsonResult(
                _userDocumentService.SearchUserDocuments(query).Value    
            );
        }

        [HttpPost]
        [ProducesResponseType(typeof(UserDocumentDto), 201)]
        [ProducesResponseType(typeof(ErrorResponseDto), 200)]
        public IActionResult CreateUserDocument([FromBody] UserDocumentDto userDocumentDto)
        {
            var userDocCreatedRes = _userDocumentService.CreateOrUpdate(userDocumentDto, CurrentProfile);
            
            return userDocCreatedRes.IsFailure
                ? new JsonResult(ErrorResponseDto.ToError("UserDocument", "Create user document",
                    userDocCreatedRes.Error))
                : new JsonResult(userDocCreatedRes.Value);
        }
    }
}
