﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/dashboard")]
    public class DashboardApiController : ApiControllerBase
    {
        protected DashboardApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser) : base(userInfoService, httpContextAccessor, identityParser)
        {
        }

        // GET api/dashboard/home
        [HttpGet]
        [Route("home")]
        public IActionResult Home()
        {
            return new JsonResult(new
            {
                Message = "This is secure API and user data!"
            });
        }
    }
}
