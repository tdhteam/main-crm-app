﻿using System;
using System.Threading.Tasks;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/initialisation")]
    public class InitialisationApiController : Controller
    {
        private readonly IUserInfoService _userInfoService;

        public InitialisationApiController(IUserInfoService userInfoService)
        {
            _userInfoService = userInfoService;
        }

        // POST api/initialisation
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UpdateUserCommand userDetailDto)
        {
            Log.Information("Start init default admin");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userInfo = await _userInfoService.CreateOrUpdateAsync(userDetailDto, new UserDetailDto
            {
                UserKey = Guid.NewGuid().ToString(),
                UserName = "system",
                LastName = "system"
            });

            return new JsonResult(userInfo);
        }
    }
}
