﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    /// <summary>
    /// Api for dealing with personal user profile preference
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/user-profiles")]
    public class UserProfileApiController : ApiControllerBase
    {
        private readonly IUserProfilePreferenceService _profilePreferenceService;

        public UserProfileApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            IUserProfilePreferenceService profilePreferenceService) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _profilePreferenceService = profilePreferenceService;
        }

        /// <summary>
        /// Get required preferences for user that need to be set for first time login
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("required-preference")]
        [ProducesResponseType(typeof(ResponseDto<UserProfilePreferenceFormDto>), 200)]
        public IActionResult GetCurrentUserRequiredPreference()
        {
            var result = _profilePreferenceService.GetCurrentUserRequiredPreference(CurrentProfile);

            return result.IsSuccess
                ? new JsonResult(ResponseDto<UserProfilePreferenceFormDto>.OkWithData(result.Value))
                : new JsonResult(ResponseDto.ErrorContent(result.Error));
        }

        /// <summary>
        /// Update user required preferences such as timezone, language, date format,...
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("required-preference")]
        [ProducesResponseType(typeof(ResponseDto<UserDetailDto>), 200)]
        public IActionResult UpdateCurrentUserRequiredPreference([FromBody] UserProfilePreferenceDto dto)
        {
            var modelCheckRes = CheckModelState(ModelState);

            if (modelCheckRes.IsFailure)
            {
                return new JsonResult(ResponseDto.ErrorContent(modelCheckRes.Error));
            }

            var result = _profilePreferenceService.SaveBasicProfilePreference(dto, CurrentProfile);

            return result.IsSuccess
                ? new JsonResult(ResponseDto<UserDetailDto>.OkWithData(result.Value))
                : new JsonResult(ResponseDto.ErrorContent(result.Error));
        }
    }
}
