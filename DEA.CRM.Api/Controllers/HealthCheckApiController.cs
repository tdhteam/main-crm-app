﻿using DEA.CRM.Utils.Timing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [AllowAnonymous]
    [Route("api/healthcheck")]
    public class HealthCheckApiController : Controller
    {
        [HttpGet]
        [Route("ping")]
        public IActionResult Ping()
        {
            return Ok(Clock.Now);
        }
    }
}
