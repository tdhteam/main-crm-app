﻿using System.Threading.Tasks;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Core;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils.Timing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/dealers")]
    public class DealerApiController : FileUploadApiControllerBase
    {
        private readonly IDealerService _dealerService;
        private readonly IAttachmentManager _attachmentManager;
        private readonly IAttachmentStorageService _attachmentStorageService;

        public DealerApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IDealerService dealerService,
            IAttachmentManager attachmentManager, 
            IAttachmentStorageService attachmentStorageService
        ) : base(userInfoService, httpContextAccessor, identityParser, attachmentStorageService)
        {
            _dealerService = dealerService;
            _attachmentManager = attachmentManager;
            _attachmentStorageService = attachmentStorageService;
        }

        [HttpGet(Name="dealer_detail")]
        [Route("{dealerId}")]
        public IActionResult GetDealerById([FromRoute] int dealerId)
        {
            var dealer = dealerId == 0 ? _dealerService.GetDefaultDealer() : _dealerService.GetDealerById(dealerId);
            return new JsonResult(dealer);
        }

        // GET api/contact
        public IActionResult GetDealers([ModelBinder]SearchDealerCommand command)
        {
            var result = _dealerService.SearchDealers(command);
            return new JsonResult(result);
        }
        
        // POST api/dealers
        [HttpPost]
        [ProducesResponseType(typeof(DealerDto), 201)]
        [ProducesResponseType(typeof(DealerDto), 400)]
        public async Task<IActionResult> CreateDealerAsync([FromBody]UpdateDealerCommand dealerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            await SaveAttachment(dealerDto);

            var dealerInfo = await _dealerService.CreateOrUpdateAsync(dealerDto);
            return new CreatedAtRouteResult("dealer_detail", new {dealerId = dealerDto.Id}, dealerDto);
        }

        private async Task SaveAttachment(UpdateDealerCommand dealerDto)
        {
            if (!string.IsNullOrEmpty(dealerDto.LogoDataUrl))
            {
                var fileUploadRes = HandleSmallFileUpload(dealerDto.LogoDataUrl);
                if (fileUploadRes.IsSuccess)
                {
                    var avatarFileExt = "png";
                    if (dealerDto.LogoDataFileInfo?.ContentType != null)
                    {
                        avatarFileExt = MimeTypeMap.GetExtension(dealerDto.LogoDataFileInfo.ContentType);
                    }

                    var uploadedFile = fileUploadRes.Value;
                    var saveAvatarRes = await _attachmentManager.SaveAttachmentAsync(new AttachmentDto
                    {
                        FileName = $"{dealerDto.Name}{avatarFileExt}",
                        FileSize = dealerDto.LogoDataFileInfo?.FileSize ?? uploadedFile.Length,
                        ContentType = dealerDto.LogoDataFileInfo?.ContentType ?? "image/png",
                        CreatedByUserKey = CurrentProfile.UserKey,
                        DateAttached = Clock.Now,
                        FileContentBytes = uploadedFile.FileContentBytes,
                        Description = $"avatar for user {dealerDto.Name}"
                    }, CurrentProfile);

                    if (saveAvatarRes.IsSuccess)
                    {
                        dealerDto.LogoRelativeUrl = saveAvatarRes.Value.DownloadFileUrl;
                    }
                }
            }
        }

        // PUT api/dealers
        [HttpPut]
        [ProducesResponseType(typeof(DealerDto), 200)]
        [ProducesResponseType(typeof(DealerDto), 400)]
        public async Task<IActionResult> UpdateDealer([FromBody]UpdateDealerCommand dealerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            await SaveAttachment(dealerDto);

            var dealerInfo = _dealerService.CreateOrUpdateAsync(dealerDto);
            return new OkObjectResult(dealerInfo);
        }
    }
}
