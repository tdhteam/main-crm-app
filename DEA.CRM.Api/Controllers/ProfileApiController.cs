﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/profiles")]
    public class ProfileApiController : ApiControllerBase
    {
        private readonly IProfileService _profileService;

        public ProfileApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            IProfileService profileService) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _profileService = profileService;
        }
    }
}
