﻿using System.Threading.Tasks;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Model;
using DEA.CRM.Service.Contract.Attachment;
using DEA.CRM.Service.Contract.Attachment.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils.Extensions;
using DEA.CRM.Utils.Timing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    /// <summary>
    /// Api for dealing with managing user info, searching users
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/accounts")]
    public class AccountsController : FileUploadApiControllerBase
    {
        private readonly IUserInfoService _userInfoService;
        private readonly IAttachmentManager _attachmentManager;
        private readonly IAttachmentStorageService _attachmentStorageService;

        public AccountsController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser, 
            IAttachmentManager attachmentManager, 
            IAttachmentStorageService attachmentStorageService) : base(userInfoService, httpContextAccessor, identityParser, attachmentStorageService)
        {
            _userInfoService = userInfoService;
            _attachmentManager = attachmentManager;
            _attachmentStorageService = attachmentStorageService;
        }

        // GET api/accounts
        [HttpGet]
        [ProducesResponseType(typeof(UserDetailSearchResultDto), 200)]
        public IActionResult SearchUsers(SearchUserQuery query)
        {
            var result = _userInfoService.SearchUsers(query);
            return new JsonResult(result);
        }
        
        // GET api/accounts/{userKey}
        [HttpGet("{userKey}")]
        [ProducesResponseType(typeof(UserDetailFormDto), 200)]
        public IActionResult GetUserDetail([FromRoute]string userKey)
        {
            var userDetailDto =
                GlobalConstants.NewStringId.EqualsIgnoreCase(userKey)
                    ? _userInfoService.GetDefaultForNewUser()
                    : _userInfoService.GetFullUserDetailByUserKey(FindUserByKeyQuery.ActiveUserQuery(userKey), true);
            
            var result = new UserDetailFormDto
            {
                FormData = userDetailDto,
                PickListValues = _userInfoService.GetPickListsForUserDetailForm(CurrentProfile)
            };
            return new JsonResult(result);
        }
        
        // POST api/accounts
        [HttpPost]
        [ProducesResponseType(typeof(UserDetailDto), 200)]
        [ProducesResponseType(typeof(UserDetailDto), 400)]
        public async Task<IActionResult> CreateUserAsync([FromBody] UpdateUserCommand userDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!string.IsNullOrEmpty(userDetailDto.UserAvatarDataUrl))
            {
                var fileUploadRes = HandleSmallFileUpload(userDetailDto.UserAvatarDataUrl);
                if (fileUploadRes.IsSuccess)
                {
                    var avatarFileExt = GlobalConstants.DefaultImageExt;//png
                    if (userDetailDto.UserAvatarDataFileInfo?.ContentType != null)
                    {
                        avatarFileExt = MimeTypeMap.GetExtension(userDetailDto.UserAvatarDataFileInfo.ContentType);
                    }
                    var uploadedFile = fileUploadRes.Value;
                    var saveAvatarRes = await _attachmentManager.SaveAttachmentAsync(new AttachmentDto
                    {
                        FileName = $"{userDetailDto.UserName}{avatarFileExt}",
                        FileSize = userDetailDto.UserAvatarDataFileInfo?.FileSize ?? uploadedFile.Length,
                        ContentType = userDetailDto.UserAvatarDataFileInfo?.ContentType ?? GlobalConstants.DefaultImageContentType,//image/png
                        CreatedByUserKey = CurrentProfile.UserKey,
                        DateAttached = Clock.Now,
                        FileContentBytes = uploadedFile.FileContentBytes,
                        Description = $"avatar for user {userDetailDto.UserName}"
                    }, CurrentProfile);

                    if (saveAvatarRes.IsSuccess)
                    {
                        userDetailDto.AvatarRelativeUrl = saveAvatarRes.Value.DownloadFileUrl;
                    }
                }
            }

            var userInfo = await _userInfoService.CreateOrUpdateAsync(userDetailDto, CurrentProfile);

            return new JsonResult(userInfo);
        }
        
        // PUT api/accounts
        [HttpPut]
        [ProducesResponseType(typeof(UserDetailDto), 200)]
        [ProducesResponseType(typeof(UserDetailDto), 400)]
        public async Task<IActionResult> UpdateUserAsync([FromBody]UpdateUserCommand userDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userInfo = await _userInfoService.CreateOrUpdateAsync(userDetailDto, CurrentProfile);

            return new JsonResult(userInfo);
        }

        // DELETE api/accounts
        [HttpDelete]
        [Route("{userKey}/{transferOwnershipToUserKey}")]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult DeactivateUser([FromRoute]DeactivateUserCommand command)
        {
            var result = _userInfoService.DeactivateUser(command);
            return new JsonResult(result);
        }
    }
}
