﻿using System.Security.Claims;
using System.Threading.Tasks;
using DEA.CRM.Api.ViewModels;
using DEA.CRM.Authentication.Helpers;
using DEA.CRM.Authentication.JwtAuth;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IUserInfoService _userInfoService;

        public AuthController(
            UserManager<ApplicationUser> userManager, 
            IJwtFactory jwtFactory, 
            IOptions<JwtIssuerOptions> jwtOptions, 
            IUserInfoService userInfoService)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _userInfoService = userInfoService;
            _jwtOptions = jwtOptions.Value;
        }

        // POST api/auth/login
        [HttpPost("login")]
        [ProducesResponseType(typeof(AuthResultViewModel), 200)]
        [ProducesResponseType(typeof(object), 400)]
        [ProducesResponseType(typeof(object), 401)]
        public async Task<IActionResult> Post([FromBody]CredentialsViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await GetClaimsIdentity(credentials.UserName, credentials.Password);

            if (identity == null)
            {
                return Unauthorized();
            }

            var jwtObj = await Tokens.GenerateJwtAsObject(identity, _jwtFactory, credentials.UserName, _jwtOptions);

            var userInfo =
                _userInfoService.GetFullUserDetailByUserName(FindUserByNameQuery.ActiveUserQuery(credentials.UserName), refresh: true);

            return new JsonResult(new AuthResultViewModel
            {
                AuthToken = jwtObj,
                AuthenticatedUserInfo = userInfo
            });
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
