﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Contact.Dto;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Dto;
using DEA.CRM.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/contacts")]
    public class ContactApiController : ApiControllerBase
    {
        private readonly IContactService _contactService;

        public ContactApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IContactService contactService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _contactService = contactService;
        }

        // GET api/contact
        [HttpGet]
        [ProducesResponseType(typeof(PagingResult<ContactDto>), 200)]
        public IActionResult SearchContacts([ModelBinder]SearchContactCommand command)
        {
            command.UserKey = CurrentProfile.UserKey;
            var result = _contactService.SearchContacts(command);
            return new JsonResult(result);
        }

        // Create or Update contact
        [HttpPost]
        [ProducesResponseType(typeof(PagingResult<ContactDetailDto>), 200)]
        public IActionResult Update([FromBody]CreateOrUpdateContactCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var rs = _contactService.Update(command);
            return new OkObjectResult(rs.Value);
        }

        // GET api/contacts/{contactId}
        [HttpGet]
        [Route("{contactId}")]
        [ProducesResponseType(typeof(PagingResult<ContactDetailDto>), 200)]
        public IActionResult GetContactDetail(int contactId)
        {
            var rs = _contactService.GetById(contactId);
            return new OkObjectResult(rs.Value);
        }

        // GET api/contacts/form/{contactId}
        [HttpGet]
        [Route("form/{contactId}")]
        [ProducesResponseType(typeof(PagingResult<ContactDetailFormDto>), 200)]
        [ProducesResponseType(typeof(ResponseDto), 200)]
        public IActionResult GetContactDetailForm(int contactId)
        {
            var contactFormRes = _contactService.BuildContactDetailFormSchema(contactId, CurrentProfile);

            if (contactFormRes.IsFailure)
            {
                return new JsonResult(ResponseDto.ErrorContent(contactFormRes.Error));
            }

            return new OkObjectResult(contactFormRes.Value);
        }
    }
}
