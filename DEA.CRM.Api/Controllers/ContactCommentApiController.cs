﻿using System;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/contactComments")]
    public class ContactCommentApiController : ApiControllerBase
    {
        private readonly IContactService _contactService;
        public ContactCommentApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IContactService contactService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _contactService = contactService;
        }

        // Create or Update contact meeting
        [HttpPost]
        public IActionResult Update([FromBody]CreateOrUpdateContactCommentCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            command.ModifiedBy = CurrentProfile.UserName;
            command.ModifiedDate = DateTime.Now;
            var rs = _contactService.UpdateContactComment(command);
            return new OkObjectResult(rs);
        }

        
        [HttpPost]
        [Route("attachment")]
        public IActionResult UpdateCommentAttachment([FromBody]CreateOrUpdateContactCommentAttachmentCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            command.ModifiedBy = CurrentProfile.UserName;
            command.ModifiedDate = DateTime.Now;
            var rs = _contactService.UpdateContactCommentAttachment(command);
            return new OkObjectResult(rs);
        }
    }
}
