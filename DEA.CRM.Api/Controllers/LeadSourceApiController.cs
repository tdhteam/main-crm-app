﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/leadsources")]
    public class LeadSourceApiController : ApiControllerBase
    {
        private readonly ILeadSourceService _leadSourceService;
        public LeadSourceApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            ILeadSourceService leadSourceService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _leadSourceService = leadSourceService;
        }

        // GET api/contact
        public IActionResult GetAllLeadSources()
        {
            return new JsonResult(_leadSourceService.GetAllLeadSources());
        }

    }
}
