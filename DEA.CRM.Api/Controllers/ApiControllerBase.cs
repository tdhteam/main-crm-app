﻿using System.Collections.Generic;
using System.Globalization;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using DEA.CRM.Service.Localization;
using DEA.CRM.Utils.Monads;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DEA.CRM.Api.Controllers
{
    public abstract class ApiControllerBase : Controller
    {
        private readonly IUserInfoService _userInfoService;

        private readonly string _currentUserName;

        private UserDetailDto _currentProfile;

        protected UserDetailDto CurrentProfile
        {
            private set => _currentProfile = value;
            get => _currentProfile ?? LoadCurrentUserInfo();
        }

        protected ApiControllerBase(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor, 
            IIdentityParser<ApplicationUser> identityParser)
        {
            _userInfoService = userInfoService;
            var user = httpContextAccessor.HttpContext.User;
            _currentUserName = identityParser.Parse(user).UserName;
        }

        private UserDetailDto LoadCurrentUserInfo()
        {
            CurrentProfile =
                _userInfoService.GetFullUserDetailByUserName(FindUserByNameQuery.ActiveUserQuery(_currentUserName),
                    false).AsResult("No active user is found for current session").Value;
            return CurrentProfile;
        }

        protected Result<string> CheckModelState(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                var modelErrors = new List<string>();
                foreach (var stateEntry in modelState.Values)
                {
                    foreach (var modelError in stateEntry.Errors)
                    {
                        if(!string.IsNullOrEmpty(modelError.ErrorMessage))
                            modelErrors.Add(modelError.ErrorMessage);

                        if (!string.IsNullOrEmpty(modelError.Exception?.Message))
                            modelErrors.Add(modelError.Exception.Message);
                    }
                }
                return Result.Fail<string>(string.Join(",", modelErrors));
            }
            return Result.Success("");
        }

        #region Localization supported methods
        /// <summary>
        /// Reference to the localization manager.
        /// </summary>
        public ILocalizationManager LocalizationManager { protected get; set; }

        /// <summary>
        /// Gets/sets name of the localization source that is used in this application service.
        /// It must be set in order to use <see cref="L(string)"/> and <see cref="L(string,CultureInfo)"/> methods.
        /// </summary>
        protected string LocalizationSourceName { get; set; }

        /// <summary>
        /// Gets localization source.
        /// It's valid if <see cref="LocalizationSourceName"/> is set.
        /// </summary>
        protected ILocalizationSource LocalizationSource
        {
            get
            {
                if (LocalizationSourceName == null)
                {
                    throw new LocalizationSourceRequireException("Must set LocalizationSourceName before, in order to get LocalizationSource");
                }

                if (_localizationSource == null || _localizationSource.Name != LocalizationSourceName)
                {
                    _localizationSource = LocalizationManager.GetSource(LocalizationSourceName);
                }

                return _localizationSource;
            }
        }
        private ILocalizationSource _localizationSource;

        /// <summary>
        /// Gets localized string for given key name and current language.
        /// </summary>
        /// <param name="name">Key name</param>
        /// <returns>Localized string</returns>
        protected virtual string L(string name)
        {
            return LocalizationSource.GetString(name);
        }

        /// <summary>
        /// Gets localized string for given key name and current language with formatting strings.
        /// </summary>
        /// <param name="name">Key name</param>
        /// <param name="args">Format arguments</param>
        /// <returns>Localized string</returns>
        protected string L(string name, params object[] args)
        {
            return LocalizationSource.GetString(name, args);
        }

        /// <summary>
        /// Gets localized string for given key name and specified culture information.
        /// </summary>
        /// <param name="name">Key name</param>
        /// <param name="culture">culture information</param>
        /// <returns>Localized string</returns>
        protected virtual string L(string name, CultureInfo culture)
        {
            return LocalizationSource.GetString(name, culture);
        }

        /// <summary>
        /// Gets localized string for given key name and current language with formatting strings.
        /// </summary>
        /// <param name="name">Key name</param>
        /// <param name="culture">culture information</param>
        /// <param name="args">Format arguments</param>
        /// <returns>Localized string</returns>
        protected string L(string name, CultureInfo culture, params object[] args)
        {
            return LocalizationSource.GetString(name, culture, args);
        }
        #endregion
    }
}
