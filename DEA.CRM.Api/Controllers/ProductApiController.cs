﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    //[Authorize(Policy = "ApiUser")] /* Uncomment this line to enable JWT token check to access this api - uncomment when login implement fully*/
    [Route("api/products")]
    public class ProductApiController : ApiControllerBase
    {
        private IProductService _productService;

        public ProductApiController(
            IProductService productService,
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser
            ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _productService = productService;
        }   

        // GET api/products
        [HttpGet]
        public IActionResult GetProducts([ModelBinder]SearchProductCommand command)
        {
            var result = _productService.SearchProducts(command);
            return new JsonResult(result);
        }

      


        // GET api/products/{productId}
        [HttpGet]
        [Route("{productId}", Name = "productDetail")]
        public IActionResult GetProductDetail(int productId)
        {
            var productDto = _productService.GetProductById(productId);
            return new JsonResult(productDto.Value);
        }

        // POST api/products
        [HttpPost]
        public IActionResult CreateProduct([FromBody]CreateOrUpdateProductCommand productDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productInfo = _productService.CreateOrUpdate(productDetailDto);

            return new CreatedAtRouteResult("productDetail", new { productId = productInfo.Id }, productInfo);
        }

        // PUT api/products
        [HttpPut]
        public IActionResult UpdateProduct([FromBody]CreateOrUpdateProductCommand productDetailDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productInfo = _productService.CreateOrUpdate(productDetailDto);

            return new OkObjectResult(productInfo);
        }

        // DELETE api/products
        [HttpDelete]
        [Route("{productId}")]
        public IActionResult DeleteProduct(int productId)
        {
            var result = _productService.DeleteProduct(productId);
            return new OkObjectResult(result);
        }
    }
}
