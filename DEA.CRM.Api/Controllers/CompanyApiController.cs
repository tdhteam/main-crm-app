﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/companies")]
    public class CompanyApiController : ApiControllerBase 
    {
        private readonly ICompanyService _companyService;

        public CompanyApiController(IUserInfoService userInfoService, IHttpContextAccessor httpContextAccessor, IIdentityParser<ApplicationUser> identityParser, ICompanyService companyService) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _companyService = companyService;
        }

        // GET api/companies/{companyId}
        [HttpGet]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(CompanyDto), 400)]
        [Route("{companyId}")]
        public IActionResult GetCompanyDetail([FromRoute]int companyId)
        {
            var companyDto = _companyService.GetCompanyById(companyId);
            return new JsonResult(companyDto.Value);
        }
        
        // GET api/companies/getFirstCompany
        [HttpGet]
        [Route("getFirstCompany")]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(CompanyDto), 400)]
        public IActionResult GetFirstCompany()
        {
            var companyDto = _companyService.GetFirstCompany();
            return new JsonResult(companyDto.Value);
        }

        // PUT api/companies
        [HttpPut]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(CompanyDto), 400)]
        public IActionResult UpdateCompany([FromBody]CreateOrUpdateCompanyCommand companyDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyInfo = _companyService.CreateOrUpdate(companyDto);

            return new OkObjectResult(companyInfo);
        }
    }
}
