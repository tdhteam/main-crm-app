﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "ApiUser")]
    [Route("api/contactMeetings")]
    public class ContactMeetingProgressApiController : ApiControllerBase
    {
        private readonly IContactService _contactService;
        public ContactMeetingProgressApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IContactService contactService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _contactService = contactService;
        }

        // Create or Update contact meeting
        [HttpPost]
        public IActionResult Update([FromBody]CreateOrUpdateContactMeetingCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var rs = _contactService.UpdateContactMeetingProgress(command);
            return new OkObjectResult(rs);
        }

    }
}
