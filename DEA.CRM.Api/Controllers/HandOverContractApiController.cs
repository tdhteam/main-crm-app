﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.HandOverContract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/handovercontracts")]
    public class HandOverContractApiController : ApiControllerBase
    {

        private readonly IHandOverContractService _handOverContractService;

        public HandOverContractApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IHandOverContractService handOverContractService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _handOverContractService = handOverContractService;
        }

        // GET api/handovercontracts
        public IActionResult GetHandOverContracts([ModelBinder]SearchHandOverContractCommand command)
        {
            var result = _handOverContractService.SearchHandOverContracts(command);
            return new JsonResult(result);
        }

        // GET api/handovercontracts/{handovercontractId}
        [HttpGet]
        [Route("{handovercontractId}")]
        public IActionResult GetHandovercontractDetail(int handovercontractId)
        {
            var rs = _handOverContractService.GetById(handovercontractId);
            return new OkObjectResult(rs);
        }
    }
}
