﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.Core.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/showrooms")]
    public class ShowroomApiController : ApiControllerBase
    {
        private readonly IShowroomService _showroomService;

        public ShowroomApiController(
            IUserInfoService userInfoService, 
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            IShowroomService showroomService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _showroomService = showroomService;
        }

        [HttpGet(Name="showroom_detail")]
        [Route("{showroomId}")]
        [ProducesResponseType(typeof(ShowroomDetailFormDto), 201)]
        public IActionResult GetShowroomById([FromRoute] int showroomId)
        {
            var showroom = showroomId == 0
                ? _showroomService.GetDefaultShowroomForNew()
                : _showroomService.GetShowroomById(showroomId);
            return new JsonResult(showroom);
        }

        // GET api/contact
        public IActionResult GetShowrooms([ModelBinder]SearchShowroomCommand command)
        {
            var result = _showroomService.SearchShowrooms(command);
            return new JsonResult(result);
        }
        
        // POST api/showrooms
        [HttpPost]
        [ProducesResponseType(typeof(ShowroomDto), 201)]
        [ProducesResponseType(typeof(ShowroomDto), 400)]
        public IActionResult CreateShowroom([FromBody]ShowroomDto showroomDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var newShowroomDto = _showroomService.CreateOrUpdate(showroomDto);
            return new CreatedAtRouteResult("showroom_detail", new {showroomId = newShowroomDto.FormData.Id}, newShowroomDto);
        }

        // PUT api/showrooms
        [HttpPut]
        [ProducesResponseType(typeof(ShowroomDto), 200)]
        [ProducesResponseType(typeof(ShowroomDto), 400)]
        public IActionResult UpdateShowroom([FromBody]ShowroomDto showroomDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var dealerInfo = _showroomService.CreateOrUpdate(showroomDto);
            return new OkObjectResult(dealerInfo);
        }
    }
}
