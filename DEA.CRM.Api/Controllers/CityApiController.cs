﻿using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Models;
using DEA.CRM.Service.Contract.City;
using DEA.CRM.Service.Contract.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DEA.CRM.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/cities")]
    public class CityApiController : ApiControllerBase
    {

        private readonly ICityService _cityService;
        public CityApiController(
            IUserInfoService userInfoService,
            IHttpContextAccessor httpContextAccessor,
            IIdentityParser<ApplicationUser> identityParser,
            ICityService cityService
        ) : base(userInfoService, httpContextAccessor, identityParser)
        {
            _cityService = cityService;
        }

        // GET api/contact
        public IActionResult GetAllCities()
        {
            return new JsonResult(_cityService.GetAllCities());
        }

        // GET api/contact
        [HttpGet]
        [Route("districts/{city}")]
        public IActionResult GetDistrictsByCity(string city)
        {
            return new JsonResult(_cityService.GetDistrictsByCity(city));
        }
    }
}