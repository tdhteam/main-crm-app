﻿using System;
using System.Net;
using System.Reflection;
using System.Text;
using Amazon.S3;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DEA.CRM.Api.Extensions;
using DEA.CRM.Api.Infrastructure.Autofac;
using DEA.CRM.Authentication;
using DEA.CRM.Authentication.Helpers;
using DEA.CRM.Authentication.JwtAuth;
using DEA.CRM.Authentication.Models;
using DEA.CRM.EfMigration.Npgsql;
using DEA.CRM.EfModel;
using DEA.CRM.Service.Contract.Helpers;
using DEA.CRM.Service.Helpers;
using DEA.CRM.Utils.Extensions;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using NJsonSchema;
using NSwag.AspNetCore;

namespace DEA.CRM.Api
{
    /// <summary>
    /// Configure application startup with Autofac as DI provider
    /// </summary>
    public class StartupWithAutofac
    {
        public StartupWithAutofac(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var appSettings = Configuration.GetSection("AppSettings");
            var hostingEnv = appSettings["HostingProviderEnvironment"];
            var dbProviderName = appSettings["DatabaseProvider"];

            if ("Postgre".EqualsIgnoreCase(dbProviderName))
            {
                // Postgre Add framework services.
                services.AddEntityFrameworkNpgsql().AddDbContext<AuthApplicationDbContext>(options =>
                    options.UseNpgsql(
                        Configuration.GetConnectionString("PostgreCrmAppIdentityDb"),
                        dbCtxOptsBuilder => dbCtxOptsBuilder.MigrationsAssembly("DEA.CRM.Authentication")
                    ));

                services.AddEntityFrameworkNpgsql().AddDbContext<NpgsqlCRMAppDbContext>(options => options.UseNpgsql(
                    Configuration.GetConnectionString("PostgreCrmAppDb"),
                    dbCtxOptsBuilder => dbCtxOptsBuilder.MigrationsAssembly("DEA.CRM.EfMigration.Npgsql")
                ));
            }
            else
            {
                // MsSQL Add framework services.
                services.AddDbContext<AuthApplicationDbContext>(options =>
                    options.UseSqlServer(
                        Configuration.GetConnectionString("CrmAppIdentityDb"),
                        dbCtxOptsBuilder => dbCtxOptsBuilder.MigrationsAssembly("DEA.CRM.Authentication")
                    ));

                services.AddDbContext<CRMAppDbContext>(options => options.UseSqlServer(
                    Configuration.GetConnectionString("CrmAppDb")
                ));
            }

            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IIdentityParser<ApplicationUser>, AppIdentityParser>();

            // jwt wire up

            var secretKey = appSettings["TokenSigningSecretKey"];
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;
            });

            // api user claim policy
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiUser", policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.ApiAccess));
            });

            // add identity
            var builder = services.AddIdentityCore<ApplicationUser>(o =>
            {
                // configure identity options
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
            });
            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder
                .AddEntityFrameworkStores<AuthApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddSignInManager<ApplicationSignInManager>();

            MapperConfigurator.ConfigureMappers();

            // CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsAllowAllPolicy", policyBuilder =>
                    policyBuilder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<StartupWithAutofac>());

            // S3 client
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonS3>();

            // Add Autofac
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule(new AppAutofacModule(hostingEnv));
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
                {
                    var authContext = serviceScope.ServiceProvider.GetRequiredService<AuthApplicationDbContext>();
                    authContext.Database.Migrate();

                    var appSettings = Configuration.GetSection("AppSettings");
                    var dbProviderName = appSettings["DatabaseProvider"];
                    if ("Postgre".EqualsIgnoreCase(dbProviderName))
                    {
                        var appContext = serviceScope.ServiceProvider.GetRequiredService<NpgsqlCRMAppDbContext>();
                        appContext.Database.Migrate();
                    }
                }
            }

            app.UseExceptionHandler(
                builder =>
                {
                    builder.Run(
                        async context =>
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                            var error = context.Features.Get<IExceptionHandlerFeature>();
                            if (error != null)
                            {
                                var errorMsg = ExceptionHelper.GetInnerMostException(error.Error).Message;
                                context.Response.AddApplicationError(errorMsg);
                                await context.Response.WriteAsync(errorMsg).ConfigureAwait(false);
                            }
                        });
                });

            app.UseAuthentication();

            app.UseCors("CorsAllowAllPolicy");

            // Enable the Swagger UI middleware and the Swagger generator
            // Navigate to /swagger to view the Swagger UI.
            // Navigate to /swagger/v1/swagger.json to view the Swagger specification.            
            app.UseSwaggerUi(typeof(StartupWithAutofac).GetTypeInfo().Assembly, settings =>
            {
                settings.GeneratorSettings.Title = "CRM App API Documentation";
                settings.GeneratorSettings.Version = "v1";
                settings.GeneratorSettings.DefaultPropertyNameHandling = PropertyNameHandling.CamelCase;
            });

            app.UseMvc();
        }
    }
}
