﻿using DEA.CRM.Authentication.Helpers;
using DEA.CRM.Service.Contract.Core.Dto;
using Newtonsoft.Json;

namespace DEA.CRM.Api.ViewModels
{
    public class AuthResultViewModel
    {
        [JsonProperty("authToken")]
        public AppJwtToken AuthToken { get; set; }

        [JsonProperty("authenticatedUserInfo")]
        public UserDetailDto AuthenticatedUserInfo { get; set; }
    }
}
