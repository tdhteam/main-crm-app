﻿namespace DEA.CRM.Api.ViewModels
{
    public class DocumentFileUploadViewModel : BasicFileUploadViewModel
    {
        public string Name { get; set; }
    }
}