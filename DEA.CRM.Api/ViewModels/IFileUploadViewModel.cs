﻿using Microsoft.AspNetCore.Http;

namespace DEA.CRM.Api.ViewModels
{
    public class BasicFileUploadViewModel
    {
        public IFormFile UploadedFile { get; set; }
    }

    public interface IFileUploadResult
    {
        string FileName { get; set; }
        string Description { get; set; }
        long Length { get; }
        string ContentType { get; set; }
        byte[] FileContentBytes { get; set; }
        string FileUrl { get; set; }
        string DownloadFileUrl { get; set; }
    }

    public class BasicFileUploadResult : IFileUploadResult
    {
        public string FileName { get; set; }
        public string Description { get; set; }
        public long Length { get; set; }
        public string ContentType { get; set; }
        public byte[] FileContentBytes { get; set; }
        public string FileUrl { get; set; }
        public string DownloadFileUrl { get; set; }
    }    
}
