﻿using FluentValidation;

namespace DEA.CRM.Api.ViewModels.Validations
{
    public class CredentialsViewModelValidator : AbstractValidator<CredentialsViewModel>
    {
        public CredentialsViewModelValidator()
        {
            RuleFor(vm => vm.UserName).NotEmpty().WithMessage("I18N.Login.UserNameCannotBeEmpty");
            RuleFor(vm => vm.Password).NotEmpty().WithMessage("I18N.Login.PasswordCannotBeEmpty");
        }
    }
}
