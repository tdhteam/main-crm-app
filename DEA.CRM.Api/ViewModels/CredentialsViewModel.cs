﻿using DEA.CRM.Api.ViewModels.Validations;
using FluentValidation.Attributes;

namespace DEA.CRM.Api.ViewModels
{
    [Validator(typeof(CredentialsViewModelValidator))]
    public class CredentialsViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
