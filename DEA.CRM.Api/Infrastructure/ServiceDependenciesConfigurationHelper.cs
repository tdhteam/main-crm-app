﻿using DEA.CRM.Service.Calendar;
using DEA.CRM.Service.Contact;
using DEA.CRM.Service.Contract;
using DEA.CRM.Service.Contract.Calendar;
using DEA.CRM.Service.Contract.Contact;
using DEA.CRM.Service.Contract.Core;
using DEA.CRM.Service.Contract.HandOverContract;
using DEA.CRM.Service.Contract.Product;
using DEA.CRM.Service.Core;
using DEA.CRM.Service.Customer;
using DEA.CRM.Service.HandOverContract;
using DEA.CRM.Service.Product;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace DEA.CRM.Api.Infrastructure
{
    public class ServiceDependenciesConfigurationHelper
    {
        public static void ConfigureDependenciesInjectionForBusinessServices(IServiceCollection services)
        {
            services.TryAddSingleton<IServiceFactory, DefaultAppServiceFactory>();

            services.TryAddScoped<IUserPermissionService, DefaultUserPermissionService>();
            services.TryAddScoped<IPermissionService, DefaultPermissionService>();
            services.TryAddScoped<IUserInfoService, DefaultUserInfoService>();
            services.TryAddScoped<IRoleService, DefaultRoleService>();
            services.TryAddScoped<IProfileService, DefaultProfileService>();
            
            services.TryAddScoped<ITabService, DefaultTabService>();
            services.TryAddScoped<ISettingService, DefaultSettingService>();
            services.TryAddScoped<ICompanyService, DefaultCompanyService>();
            services.TryAddScoped<IDealerService, DefaultDealerService>();
            services.TryAddScoped<IShowroomService, DefaultShowroomService>();
            
            services.TryAddScoped<ICalendarEventService, DefaultCalendarEventService>();

            services.TryAddScoped<IContactService, DefaultContactService>();
            services.TryAddScoped<IHandOverContractService, DefaultHandOverContractService>();
            services.TryAddScoped<IProductService, DefaultProductService>();
            services.TryAddScoped<ICustomerService, DefaultCustomerService>();
            
            services.TryAddScoped<IPickListService, DefaultPickListService>();
        }
    }
}