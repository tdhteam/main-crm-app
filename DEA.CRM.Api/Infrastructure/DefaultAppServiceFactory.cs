﻿using System;
using DEA.CRM.Service.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace DEA.CRM.Api.Infrastructure
{
    /// <summary>
    /// Default implementation for <see cref="IServiceFactory"/> using DI container provided by aspnet core
    /// </summary>
    public class DefaultAppServiceFactory : IServiceFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public DefaultAppServiceFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public TService GetService<TService>()
        {
            return _serviceProvider.GetService<TService>();
        }
    }
}
