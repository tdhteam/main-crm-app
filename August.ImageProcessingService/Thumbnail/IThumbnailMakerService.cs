﻿namespace August.ImageProcessingService.Thumbnail
{
    public interface IThumbnailMakerService
    {
        /// <summary>
        /// Resize image with specific width and height
        /// </summary>
        /// <param name="srcImageBytes"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        byte[] Resize(byte[] srcImageBytes, int width, int height);

        /// <summary>
        /// Resize image keep ratio with width and height calculated from size
        /// </summary>
        /// <param name="srcImageBytes"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        byte[] Resize(byte[] srcImageBytes, int size);
    }
}
