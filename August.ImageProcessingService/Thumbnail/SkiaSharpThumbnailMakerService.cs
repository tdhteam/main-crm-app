﻿using System.IO;
using SkiaSharp;

namespace August.ImageProcessingService.Thumbnail
{
    public class SkiaSharpThumbnailMakerService : IThumbnailMakerService
    {
        public byte[] Resize(byte[] srcImageBytes, int width, int height)
        {
            using (var inStream = new MemoryStream(srcImageBytes))
            {
                using (var inputStream = new SKManagedStream(inStream))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        using (var resized = original.Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return null;

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var outStream = new MemoryStream())
                                {
                                    image.Encode(SKEncodedImageFormat.Png, 100)
                                        .SaveTo(outStream);
                                    return outStream.ToArray();
                                }
                            }
                        }
                    }
                }
            }
        }

        public byte[] Resize(byte[] srcImageBytes, int size)
        {
            using (var inStream = new MemoryStream(srcImageBytes))
            {
                using (var inputStream = new SKManagedStream(inStream))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width > original.Height)
                        {
                            width = size;
                            height = original.Height * size / original.Width;
                        }
                        else
                        {
                            width = original.Width * size / original.Height;
                            height = size;
                        }

                        using (var resized = original.Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return null;

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var outStream = new MemoryStream())
                                {
                                    image.Encode(SKEncodedImageFormat.Png, 100)
                                        .SaveTo(outStream);
                                    return outStream.ToArray();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
