﻿namespace August.ImageProcessingService.Thumbnail
{
    public interface IThumbnailMakerServiceFactory
    {
        IThumbnailMakerService CreateService(string providerName);
    }

    public class DefaultThumbnailMakerServiceFactory : IThumbnailMakerServiceFactory
    {
        public IThumbnailMakerService CreateService(string providerName)
        {
            throw new System.NotImplementedException();
        }
    }
}