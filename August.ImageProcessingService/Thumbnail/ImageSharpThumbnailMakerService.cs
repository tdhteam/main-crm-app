﻿using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Transforms;

namespace August.ImageProcessingService.Thumbnail
{
    public class ImageSharpThumbnailMakerService : IThumbnailMakerService
    {
        public byte[] Resize(byte[] srcImageBytes, int width, int height)
        {
            using (Image<Rgba32> image = Image.Load(srcImageBytes))
            {
                image.Mutate(x => x
                    .Resize(width, height));
                using (var outStream = new MemoryStream())
                {
                    image.SaveAsPng(outStream);
                    return outStream.ToArray();
                }
            }
        }

        public byte[] Resize(byte[] srcImageBytes, int size)
        {
            using (Image<Rgba32> image = Image.Load(srcImageBytes))
            {
                int width, height;
                if (image.Width > image.Height)
                {
                    width = size;
                    height = image.Height * size / image.Width;
                }
                else
                {
                    width = image.Width * size / image.Height;
                    height = size;
                }

                image.Mutate(x => x
                    .Resize(width, height));
                using (var outStream = new MemoryStream())
                {
                    image.SaveAsPng(outStream);
                    return outStream.ToArray();
                }
            }
        }
    }
}
